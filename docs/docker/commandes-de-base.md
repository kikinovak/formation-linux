
# Commandes de base de Docker

**Objectif** : prise en main des commandes de base pour manipuler un conteneur
Docker.

## Exécuter un conteneur

Lancez un conteneur Alpine Linux&nbsp;:

```
$ docker run -dit alpine
Unable to find image 'alpine:latest' locally
latest: Pulling from library/alpine
59bf1c3509f3: Pull complete
Digest: sha256:21a3deaa0d32a8057914f36584b5288d2e5ecc984380bc0118285c70fa8c9300
Status: Downloaded newer image for alpine:latest
57c00f853a75819084ca47a472100077c86d65b4249d1f5241f792e0dde504ea
```

> Si la commande se solde par un message d'erreur `Error response from daemon:
> connection refused` ne vous inquiétez pas. Docker Hub est parfois un peu dur
> de la feuille, et il suffit de réinvoquer la commande. 

* Les deux premières lignes de la réponse du *shell* indiquent que Docker ne
  dispose pas de l'image, et qu'il doit donc la récupérer dans le dépôt
  d'images par défaut, en l'occurrence Docker Hub.

* Une fois que Docker a téléchargé l'image, il démarre un conteneur qui utilise
  cette image.

* La dernière ligne de la sortie, c'est l'identifiant - ou l'ID - du conteneur,
  que nous pouvons utiliser pour gérer ce conteneur en particulier. Ne vous
  inquiétez pas, vous n'aurez pas tout ce code à rallonge à taper. Nous
  utiliserons plutôt une partie unique de l'ID ou même un nom convivial à la
  place. 

* L'option `-d` signifie *detach*. Elle permet à un conteneur de fonctionner en
  arrière-plan. Lorsque vous exécutez un conteneur en arrière-plan, Docker
  affiche un ID du conteneur comme il le fait ici.

* L'option `-i` signifie *interactive*. Avec cette option, Docker garde
  l'entrée standard (`STDIN`) ouverte même si vous n'êtes pas attaché au
  conteneur. Ce qui vous permet éventuellement de vous connecter directement au
  conteneur pour lui envoyer une entrée standard, ou plus simplement de taper
  des commandes dans le conteneur. 

* L'option `-i` est directement associée à l'option `-t` comme *terminal*.
  Si vous voulez interagir avec le *shell*, vous aurez besoin d'un terminal.

* Nous savons que notre conteneur a démarré parce que Docker nous a renvoyé un
  ID de conteneur une fois que nous avons exécuté la commande `docker run`. Si
  vous ne voyez pas l'ID de conteneur, c'est qu'il y a eu un problème.

## Afficher un conteneur en état d'exécution

La commande `docker ps` nous permet de vérifier que notre conteneur
fonctionne&nbsp;:

```
$ docker ps
CONTAINER ID  IMAGE   COMMAND    CREATED        ...  NAMES
57c00f853a75  alpine  "/bin/sh"  3 minutes ago  ...  hardcore_mccarthy
```

Le système nous affiche un certain nombre de détails sur le ou les conteneurs
en cours d'exécution&nbsp;:

* `CONTAINER ID` : `57c00f853a75` - *hash* de douze caractères

* `IMAGE` : `alpine` - l'image utilisée pour exécuter le conteneur

* `COMMAND` : `"/bin/sh"` - la commande avec laquelle le conteneur a démarré

* `CREATED` : `3 minutes ago` - la date de création du conteneur

* `STATUS` : `Up 2 minutes` - la durée de fonctionnement du conteneur

* `PORTS` : informations sur les ports du conteneur

* `NAMES` : `hardcore_mccarthy` - le nom humainement lisible du conteneur

## Arrêter et redémarrer un conteneur

Pour arrêter le conteneur en état d'exécution, nous pouvons nous servir de l'ID
qui s'affiche à gauche&nbsp;:

```
$ docker stop 57c00f853a75
57c00f853a75
```

Nous pouvons le relancer de la même manière&nbsp;:

```
$ docker start 57c00f853a75
57c00f853a75
```

Ce n'est pas la peine de spécifier l'ID dans son intégralité. Lorsqu'on dispose
de plusieurs conteneurs, il suffit théoriquement de taper juste assez de caractères pour
lever l'ambiguïté, mais rien n'empêche de trouver un juste compromis. Ici,
nous répétons l'opération de redémarrage en utilisant les quatre premiers caractères de
l'ID&nbsp;:

```
$ docker stop 57c0
57c0
$ docker start 57c0
57c0
```

Enfin, je peux également utiliser le nom "humainement lisible" du
conteneur&nbsp;:

```
$ docker stop hardcore_mccarthy
hardcore_mccarthy
$ docker start hardcore_mccarthy
hardcore_mccarthy
```

La commande `docker ps -a` permet d'afficher le ou les conteneurs
stoppés&nbsp;:

```
$ docker stop hardcore_mccarthy
hardcore_mccarthy
$ docker ps
CONTAINER ID   IMAGE     COMMAND   ...  NAMES
$ docker ps -a
CONTAINER ID   IMAGE     COMMAND   ...  NAMES
57c00f853a75   alpine    "/bin/sh" ...  hardcore_mccarthy
$ docker start hardcore_mccarthy
hardcore_mccarthy
$ docker ps
CONTAINER ID   IMAGE     COMMAND   ...  NAMES
57c00f853a75   alpine    "/bin/sh" ...  hardcore_mccarthy
```

Lancez deux autres conteneurs basés respectivement sur Debian et Ubuntu&nbsp;:

```
$ docker run -dit debian
$ docker run -dit ubuntu
```

Affichez les conteneurs en cours d'exécution&nbsp;:

```
$ docker ps
CONTAINER ID  IMAGE   COMMAND    CREATED         ...  NAMES
e7f353f1bffe  ubuntu  "bash"     30 seconds ago  ...  eager_colden
af0f9acf6507  debian  "bash"     2 minutes ago   ...  quizzical_haibt
57c00f853a75  alpine  "/bin/sh"  2 hours ago     ...  hardcore_mccarthy
```

Entraînez-vous un peu à les arrêter et à les redémarrer individuellement en
utilisant successivement l'ID à douze caractères, l'ID abrégé et le nom
humainement lisible. Utilisez systématiquement `docker ps` pour afficher les
conteneurs en cours d'exécution et `docker ps -a` pour voir également les
conteneurs stoppés. Pour finir, arrêtez tous les conteneurs.

## Détacher un conteneur&nbsp;?

Voici comment il ne faut **pas** lancer un conteneur&nbsp;:

```
$ docker run debian
$ docker ps
```

Que s'est-il passé ? Le conteneur a démarré pour s'arrêter immédiatement. C'est
pour cette raison que nous utilisons les options `-d`, `-i` et `t`. Comme ça le
conteneur ne s'arrête pas aussitôt et continue à s'exécuter en arrière-plan .

> Vous pouvez combiner les options comme bon vous semble. Peu importe si vous
> invoquez `docker run -dit debian`, `docker run -it -d debian` ou `docker run
> -i -t -d debian`, le résultat sera le même. 

Ceci étant dit, certains conteneurs peuvent être conçus pour s'acquitter d'une
seule tâche et s'arrêter tout de suite après. C'est le cas de la fameuse image
`hello-world` que l'on utilise généralement pour tester le bon fonctionnement
de Docker après son installation&nbsp;:

```
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:4c5f3db4f8a54eb1e017c385f683a2de6e06f75be442dc32698c9bbe6c861edd
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

## Afficher la liste des images locales

Une autre commande que vous utiliserez souvent permet d'afficher les images qui
ont été téléchargées localement par Docker&nbsp;:

```
$ docker images
REPOSITORY    TAG     IMAGE ID      CREATED       SIZE
ubuntu        latest  2b4cba85892a  6 days ago    72.8MB
debian        latest  d40157244907  9 days ago    124MB
alpine        latest  c059bfaa849c  3 months ago  5.59MB
hello-world   latest  feb5d9fea6a5  5 months ago  13.3kB
```

Vous pouvez également utiliser la syntaxe suivante&nbsp;:

```
$ docker image ls
REPOSITORY    TAG     IMAGE ID      CREATED       SIZE
ubuntu        latest  2b4cba85892a  6 days ago    72.8MB
debian        latest  d40157244907  9 days ago    124MB
alpine        latest  c059bfaa849c  3 months ago  5.59MB
hello-world   latest  feb5d9fea6a5  5 months ago  13.3kB
```

## Inspecter un conteneur

À présent, lancez un conteneur et jetez un œil sous le capot grâce à la
commande `docker inspect`&nbsp;:

```
$ docker run -dit alpine
6b238bee1856b02fec058d82dfc631d48e368993f36b4f33cb8fa7e3f2925222
$ docker ps
CONTAINER ID  IMAGE   COMMAND    CREATED        ...  NAMES
6b238bee1856  alpine  "/bin/sh"  5 seconds ago  ...  wizardly_chatelet
$ docker inspect 6b23 | less
```

Vous verrez tout un fatras d'informations qui tapissent votre terminal. Ne vous
préoccupez pas des détails pour l'instant, nous apprendrons à apprivoiser ces
infos en temps et en heure. 

## Utiliser l'aide en ligne

Affichez la branche principale de l'aide en ligne&nbsp;:

```
$ docker --help | less
```

L'aide en ligne vous affiche toute une série de sous-commandes comme par
exemple `image`, `network` ou `volume`. Pour en savoir plus sur chacune de ces
sous-commandes, vous pouvez afficher l'aide en ligne détaillée&nbsp;:

```
$ docker image --help
...
$ docker network --help
...
$ docker volume --help
...
```

Certaines sous-commandes acceptent d'autres sous-commandes, avec une aide en
ligne correspondante&nbsp;:

```
$ docker image prune --help
...
```

## Exercices

### Exercice 1

* Allez sur [Docker Hub](https://hub.docker.com) et notez les noms
  respectifs des images officielles de Rocky Linux, Alma Linux et CentOS.

* Lancez trois conteneurs correspondants avec ces images.

* Arrêtez et relancez chacun des trois conteneurs en utilisant successivement
  l'ID complet, l'ID abrégé à trois ou quatre caractères et le nom humainement
  lisible.

* Inspectez successivement les caractéristiques de chaque conteneur en état
  d'exécution.

* Arrêtez tous les conteneurs.

### Exercice 2

* Lancez un conteneur Alpine Linux en invoquant `docker run -it alpine`.

* Regardez votre invite de commandes et essayez de deviner ce qui se passe.

* Invoquez la commande `cat /etc/os-release` pour confirmer vos soupçons.

* Ouvrez un deuxième terminal et affichez les conteneurs en état d'exécution
  grâce à `docker ps`. 

* Revenez dans le premier terminal et quittez le *shell* en tapant `exit`.

* Affichez les conteneurs en état d'exécution. Que constatez-vous&nbsp;?

* Essayez d'expliquer avec vos mots ce qui s'est passé.

### Exercice 3

* Faites l'inventaire de toutes les images dont vous disposez localement en
  utilisant deux syntaxes différentes.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



Installer le paquet `docker` fourni par le dépôt SLE (*SUSE Linux
Enterprise*)&nbsp;:

```
# zypper install docker
```

L'installation du paquet a créé un groupe système `docker`&nbsp;:

```
# grep docker /etc/group
docker:x:463:
```

En temps normal, la commande `docker` doit être invoquée en tant que `root`.
Pour éviter cela, on pourra ajouter l'utilisateur au groupe système
`docker`&nbsp;:

```
# usermod -aG docker <votre_identifiant>
```

> Si l'utilisateur est connecté, il faudra qu'il quitte sa session et qu'il se
> reconnecte avant de pouvoir utiliser Docker.


Démarrer le service Docker et activer son lancement au démarrage&nbsp;:

```
# systemctl enable docker --now
```

Si tout s'est bien passé, la commande `docker version` invoquée en tant que
simple utilisateur affiche les versions respectives du client et du serveur&nbsp;:

```
$ docker version
Client:
 Version:           20.10.12-ce
 ...
Server:
 Engine:
  Version:          20.10.12-ce
 ...
```















# Exécuter un conteneur Docker

**Objectifs**&nbsp;:

* Connaître les principales options pour la commande `docker run`

* Donner un nom à vos conteneurs

* Gérer ces conteneurs en utilisant leurs noms respectifs

* Afficher l'état d'un conteneur en état d'exécution ou à l'arrêt

* Configurer un conteneur de manière à ce qu'il soit relancé automatiquement en
  cas de redémarrage de l'hôte

* Afficher la sortie d'un conteneur qui tourne en arrière-plan

* Supprimer rapidement des conteneurs qui ont été arrêtés

## Comprendre quelques concepts importants

Parfois, les utilisateurs qui débutent avec Docker essaient d'exécuter un
conteneur et se rendent compte que le conteneur ne tourne pas. Ce qui se passe
dans ces cas-là, c'est que le conteneur démarre puis s'arrête immédiatement.

Ce que les gens veulent faire en général, c'est démarrer un conteneur et le
faire fonctionner en arrière-plan. Dans ce cas, il vous faut indiquer
explicitement à Docker que c'est ce que vous voulez faire.

Docker traite le logiciel qui se trouve à l'intérieur du conteneur comme
n'importe quel processus système. Une fois que le processus est stoppé, Docker
arrête le conteneur. On doit donc le détacher pour qu'il continue à fonctionner
en arrière-plan.

L'exécution en avant-plan par défaut est une option valable si un conteneur
effectue une tâche unique bien précise, comme par exemple télécharger un
fichier, le traiter d'une manière ou d'une autre, puis enregistrer les données
traitées quelque part.

En revanche, un conteneur est généralement censé continuer à tourner et à
fournir un service ou un processus permanent.

```
$ docker run -dit debian
3d631b930885d7e1d135b348bf34c69e002aa9d455f7f438cbcbd4dcff9dbbe8
```

* L'option `-d` utilisée pour détacher le conteneur peut être considérée comme
  une façon de le *démoniser*. De manière générale, un *démon* (de l'anglais
  *daemon*) est un programme, un processus ou un ensemble de processus qui
  s'exécute en arrière-plan plutôt que sous le contrôle direct d'un
  utilisateur.

* Les options `-i` et `-t` associées permettent de disposer d'un shell
  interactif pour pouvoir accéder à ce conteneur.

* Les options `-dit` sont généralement utilisées ensemble.

## Donner un nom parlant à un conteneur

Manipuler les conteneurs en utilisant leur empreinte n'est pas très convivial.
C'est un peu le même principe que pour les adresses IP. Au lieu de mémoriser
une séquence de chiffres, on a recours au DNS pour que les humains puissent
utiliser un nom pour accéder à une adresse IP.

Docker nous permet de faire quelque chose de très similaire. Vous pouvez donner
un nom à un conteneur, puis utiliser ce nom plutôt que l'ID du conteneur pour
pour vous y référer . Cela vous permet de donner un nom approprié à vos
conteneurs.

Concrètement, au lieu de manipuler un conteneur `3a57f1...` nous pouvons
simplement le nommer `web`&nbsp;:

```
$ docker run -dit --name web debian
08c35d6da5dba579698eacfd0539ad002d31c1c265fe5a9ba8d01ca1916255ab
$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED          ...  NAMES
08c35d6da5db   debian    "bash"    9 seconds ago    ...  web
3d631b930885   debian    "bash"    17 minutes ago   ...  suspicious_mendel
```

Ici nous avons deux conteneurs en cours d'exécution. Le premier que nous avons
démarré a reçu un nom par Docker, en l'occurrence `suspicious_mendel`. Docker
génère ces noms de manière aléatoire. Quant au second container `web`, c'est
nous qui l'avons nommé explicitement.

> N'oubliez pas que la commande `docker ps` ne vous affiche que les conteneurs
> en état d'exécution. Si vous souhaitez jeter un œil sur les conteneurs à
> l'arrêt, il vous faudra utiliser l'option `-a` qui signifie *all*. 

À présent, lancez un troisième conteneur&nbsp;:

```
$ docker run -dit --name conteneur_trois debian
8d493bc46f0dd6023322e05061cf0d602ee04c1d68a7eca123d7ef0ce0cbb712
```

Voici la commande qui permet de savoir quel est le dernier conteneur qui a été
lancé&nbsp;:

```
$ docker ps -l
CONTAINER ID   IMAGE     COMMAND   CREATED          ...  NAMES
8d493bc46f0d   debian    "bash"    33 seconds ago   ...  conteneur_trois
```

* L'option `-l` signfie *latest* et nous affiche le dernier conteneur qui a été
  créé, indépendamment de son état, c'est-à-dire qu'il peut déjà être arrêté ou
  encore en état d'exécution.

Si un conteneur du même nom existe déjà, Docker refusera de vous laisser
exécuter un autre conteneur du même nom&nbsp;:

```
$ docker run -dit --name web debian
docker: Error response from daemon: Conflict.
```

## Arrêter et relancer un conteneur

Pour arrêter un conteneur, nous utilisons la commande `docker stop`&nbsp;:

```
$ docker stop web
web
```

* Maintenant que le conteneur `web` est arrêté, nous pouvons l'afficher avec la
  commande `docker ps -a`. Notez que son statut est `Exited`.

* Un simple `docker ps` ne nous permettra pas de voir le conteneur `web`, étant
  donné qu'il n'est pas en état d'exécution.

Supprimons ce conteneur&nbsp;:

```
$ docker rm web
web
$ docker ps -a
```

* Le conteneur a été supprimé pour de bon.

* Partant de là, le nom `web` est susceptible d'être réutilisé en cas de
  besoin.

La commande `docker start` permet de démarrer un conteneur arrêté&nbsp;:

```
$ docker run -dit --name web debian
ff45a7964c407e76b3024326df8ce1157714bcdb1372c9410728d819fd1a886d
$ docker ps
...
$ docker stop web
web
$ docker ps -a
CONTAINER ID  IMAGE   COMMAND  CREATED         STATUS  ...  NAMES
3db36cc4ab5e  debian  "bash"   44 seconds ago  Exited  ...  web
...
$ docker start web
$ docker ps
CONTAINER ID  IMAGE   COMMAND  CREATED         STATUS        ...  NAMES
3db36cc4ab5e  debian  "bash"   2 minutes ago   Up 5 seconds  ...  web
...
```

Arrêtez et supprimez ce conteneur&nbsp;:

```
$ docker stop web
$ docker rm web
$ docker ps -a
```

## Configurer le redémarrage automatique d'un conteneur

Imaginez ce qui se passe si la machine qui exécute le moteur Docker redémarre.
Il y aura très certainement une série de conteneurs que vous souhaitez
relancer. 

De même, au cas où un conteneur plante pour une raison ou pour une autre, vous
souhaitez probablement qu'il redémarre tout seul sans l'intervention d'un
administrateur humain. Docker fournit cette fonctionnalité grâce à l'option
`--restart`&nbsp;:

```
$ docker run -dit --restart always --name conteneur_quatre debian
497d59bd57d53ab987a511b7834db3dec43d75cf115d13a78212628319df7439
```

Si nous utilisons cette option, nous pouvons vérifier l'application de la
politique de redémarrage automatique en inspectant le conteneur comme
ceci&nbsp;:

```
$ docker inspect conteneur_quatre | grep -A 3 RestartPolicy
  "RestartPolicy": {
      "Name": "always",
      "MaximumRetryCount": 0
  },
``` 

* L'option `-A 3` indique à `grep` d'afficher l'occurence et les trois lignes
  subséquentes. Pour en avoir le cœur net, jetez un œil dans la page de manuel
  `grep(1)`. 

Voici l'ensemble des options que vous pouvez utiliser avec `docker run`. Vous
noterez qu'elles sont nombreuses&nbsp;:

```
$ docker run --help | less
```

## Forcer l'arrêt d'un conteneur

Il peut arriver que vous soyez confronté à un problème lorsque vous essayez
d'arrêter un conteneur&nbsp;:

```
$ docker stop tender_ellis
```

Lorsque Docker rencontre un problème lors de l'arrêt d'un conteneur, il vous
affiche un avertissement simple qui vous permet en règle générale de résoudre
le problème. 

Dans certains cas de figure, vous devrez utiliser la sous-commande `kill` pour
forcer l'arrêt d'un conteneur&nbsp;:

```
$ docker kill tender_ellis
tender_ellis
```

La commande `docker kill` peut vous tirer d'affaire, mais attention. Elle ne
permet pas de réaliser un arrêt en bonne et due forme comme le ferait la
commande `docker stop`. C'est rare, mais sachez qu'en utilisant `docker kill`,
vous pouvez mettre votre conteneur dans un état qui l'empêchera d'être
redémarré. En règle générale, cela n'a pas d'importance, puisque les conteneurs
sont conçus et exécutés de manière à être jetables.

## Supprimer les conteneurs arrêtés

La commande `docker system prune` peut être utilisé pour supprimer des
conteneurs arrêtés. C'est souvent plus simple que d'invoquer un `docker rm`
pour chaque conteneur à l'arrêt&nbsp;:

```
$ docker ps -a
...
$ docker system prune
WARNING! This will remove:
  - all stopped containers
  - all networks not used by at least one container
  - all dangling images
  - all dangling build cache

Are you sure you want to continue? [y/N] y
Deleted Containers:
4ee29e393a22d286e2a70de462c585cf40fdc27706f275da1fe94b8236ab6aa3
e53ad17d7fc138672d9e51acf18275ac4573b5165eb215f503e917aab8c1a0ce
...

```

## Supprimer automatiquement un conteneur qui s'arrête

Si vous souhaitez faire le ménage derrière vous automatiquement, utilisez
l'option `--rm` en combinaison avec la sous-commande `run`. Dès que le
processus principal d'un conteneur est terminé, Docker supprime automatiquement
ce conteneur si vous utilisez l'option `--rm`&nbsp;:

```
$ docker run --rm hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete
Digest: sha256:49a1c8800c94df04e9658809b006fd8a686cab8028d...
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
$ docker ps -a
```

* Nous ne voyons pas la moindre trace du conteneur. Ce qui s'est passé, c'est
  que le conteneur a été lancé, il a affiché quelque chose, il s'est arrêté, et
  puis Docker l'a supprimé justement parce qu'il s'est arrêté. 

* Ce n'est pas une mauvaise idée d'utiliser l'option `--rm` au quotidien pour
  limiter le bazar sur le système.

## Afficher les logs d'un conteneur

Tout à l'heure nous avons exécuté la commande `docker run hello-world`, et le
conteneur s'est exécuté en avant-plan en affichant ses données directement dans
la console. Comment pouvons-nous voir ces données si nous exécutons le
conteneur en arrière-plan&nbsp;?

La solution consiste ici à utiliser la commande `docker logs`&nbsp;:

```
$ docker run -dit hello-world
b9a9ba3116ec8f6341f313d5133885e28bd2535d341777ae2da...
$ docker logs b9a9

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

Affichez les options disponibles&nbsp;:

```
$ docker logs --help
```

L'option `-t` permet d'afficher l'horodatage (*timestamp*) de la sortie, ce qui
peut s'avérer utile pour un éventuel débogage&nbsp;:

```
$ docker logs -t b9a9 
2020-07-19T06:52:24.949260256Z
2020-07-19T06:52:24.949346103Z Hello from Docker!
2020-07-19T06:52:24.949370149Z This message shows that your installation
appears to be working correctly.
...
```

Pour afficher la sortie du container en temps réel, utilisez l'option `-f`. Si
avez l'habitude d'utiliser la commande `tail -f` sous Linux, le principe est
exactement le même. L'option `-f` signifie *follow* et affiche la sortie au fur
et à mesure qu'elle est générée&nbsp;:


```
$ docker logs -f 8b5c
```

Ces options peuvent très bien être combinées&nbsp;:

```
$ docker logs -tf 8b5c
```

## Exercice

* Lancez un conteneur basé sur l'image officielle du serveur Redis.

* Vérifiez si le conteneur est en état d'exécution.

* Notez l'identifiant et le nom humainement lisible du conteneur.

* Lancez un deuxième conteneur basé sur l'image officielle de Redis et
  nommez-le `container_redis`. 

* Vérifiez s'il tourne comme prévu.

* Arrêtez le conteneur `container_redis`.

* Affichez le ou les conteneurs en état d'exécution.

* Lancez un conteneur basé sur l'image `hello-world` de manière à ce qu'il
  s'autodétruise juste après son exécution.

* Vérifiez s'il ne reste aucune trace du conteneur que vous venez de lancer.

* Lancez un autre conteneur basé sur l'image `hello-world`, mais sans qu'il
  soit supprimé après exécution.

* Vérifiez la présence du conteneur à l'arrêt. 

* Lancez un troisième conteneur basé sur l'image `hello-world`, mais en
  arrière-plan. 

* Vérifiez s'il est en état d'exécution. 

* Demandez-vous ce qui se passe. 

* Le conteneur que vous venez de lancer a produit une sortie. Pouvez-vous la
  récupérer, et si oui, comment ?

* Pouvez-vous savoir à quelle heure exacte le conteneur a produit cette
  sortie&nbsp;?

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


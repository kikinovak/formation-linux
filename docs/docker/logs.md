
# Gérer les logs de Docker

**Objectifs**&nbsp;:

* Afficher les *logs* générés par les conteneurs pour savoir ce qui se passe
  sous le capot.

* Afficher les *logs* du moteur Docker.

## Les logs internes des conteneurs

Au fur et à mesure que vous avancerez dans l'apprentissage de Docker, vous
serez amené tôt ou tard à utiliser les journaux - ou les *logs* - pour résoudre
les problèmes, surtout lorsque vous utiliserez plusieurs conteneurs en même
temps.

Il vous faudra également accéder à la sortie standard et à la sortie d'erreur
standard de vos conteneurs tout comme sur n'importe quel système Linux.

Nous allons utiliser l'application Jenkins à des fins de démonstration. Jenkins
est un outil d'intégration continue, mais nous allons faire abstraction du rôle
précis de cette application. Elle nous servira uniquement pour montrer en
pratique le fonctionnement des *logs*. 

Lancez un conteneur Jenkins avec les options suivantes&nbsp;:

```
$ docker run -dit --name pleindelogs -p 8080:8080 -p 50000:50000 \
  jenkins/jenkins:lts
58a30d0285916966e22b3b02e6f39d80dd5119cfb0add02ba...
```

> Notez le nom de l'image `jenkins/jenkins:lts`. La première occurrence de
> `jenkins` avant la barre oblique, c'est l'identifiant Docker. Cherchez
> l'application Jenkins sur Docker Hub et lisez les infos affichées pour en
> savoir plus.

Vérifions si le conteneur s'exécute bien&nbsp;:

```
$ docker ps
```

Ici, vous avez besoin d'un mot de passe pour pouvoir accéder à l'application
depuis un navigateur Web. Lorsque vous utilisez l'image de Jenkins, le
conteneur génère automatiquement un mot de passe unique lors du premier
lancement. 

Utilisez la commande `docker logs` pour afficher la sortie générée par ce
conteneur&nbsp;:

```
# docker logs pleindelogs
...
*************************************************************************
Jenkins initial setup is required. An admin user has been created and a
password generated.  Please use the following password to proceed to
installation:

ea952fa0f8534898b52dd03c16b0794e

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
*************************************************************************
```

Rappelez-vous les deux options pratiques pour l'affichage des *logs*. L'option
`-f` qui signifie *follow* vous permet de suivre les *logs* "à chaud". 

```
$ docker logs -f pleindelogs
```

Quant à l'option `-t`, elle inclut l'horodatage des *logs* dans
l'affichage&nbsp;:

```
$ docker logs -t pleindelogs
```

À présent, repérez le mot de passe et copiez-le dans le presse-papier.

Ouvrez un navigateur web et connectez-vous au port 8080 du conteneur en
utilisant le mot de passe fourni dans les logs&nbsp;:

* `http://localhost:8080`

* Mot de passe&nbsp;: `ea952fa0f8534898b52dd03c16b0794e`

> Notez bien qu'ici nous faisons abstraction du rôle précis de l'application
 Jenkins. Tout ce qui nous intéresse ici, c'est la gestion des *logs*. 

Cliquez sur `Sélectionner les plugins à installer`, acceptez la configuration
par défaut et cliquez sur `Installer`. Affichez le suivi des *logs*&nbsp;:

```
$ docker logs -f pleindelogs
```

Parfois, les journaux sont également enregistrés à l'intérieur du conteneur.
Dans ce cas, il vous suffit d'examiner les fichiers dans `/var/log` comme vous
le feriez sur n'importe quel système Linux normal.

Ceci étant dit, les conteneurs empêchent le plus souvent l'écriture de fichiers
dans `/var/log` et préfèrent les rediriger vers la sortie standard et la sortie
d'erreur standard, qui peuvent toutes les deux être consultées à l'aide de la
commande `docker logs`.

## Les logs externes du moteur Docker

En dehors des *logs* qui enregistrent l'activité des conteneurs, le moteur
Docker crée également ses propres journaux. Comme la plupart des services,
Docker envoie ses journaux au système `syslog` de la machine hôte. 

* Sur les systèmes Red Hat Enterprise Linux et les clones compatibles comme
  Oracle Linux ou Rocky Linux ainsi que sous OpenSUSE Linux, ces journaux se
  trouvent dans le fichier `/var/log/messages`.

* Sous Debian et Ubuntu, ces *logs* seront inscrits dans `/var/log/syslog`.

```
# grep -i docker /var/log/messages
```

Nous pouvons également afficher les *logs* du moteur Docker à l'aide de
`journald`, l'outil de journalisation intégré dans `systemd`. 

Voici un exemple simple qui vous affiche les 25 dernières entrées des *logs*
relatifs au moteur Docker&nbsp;:

```
# journalctl -u docker.service -n 25
```

Dans certains cas, il est souhaitable de ne pas utiliser de visualiseur dans
l'affichage des résultats&nbsp;:

```
# journalctl --no-pager -u docker.service -n 25
```

## Exercice

* Lancez un conteneur Apache basé sur l'image `httpd:latest`, nommé
  `apache_logs` et qui écoute sur le port 8080.

* Affichez le suivi des journaux d'accès et essayez d'identifier le ou les
  postes clients qui se connectent au serveur.

* Faites la même chose avec le serveur Nginx. Vous utiliserez l'image
  `nginx:latest`, le conteneur sera nommé `nginx_logs` et il écoutera sur le
  port 8081.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

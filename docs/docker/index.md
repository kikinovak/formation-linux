
## Une formation Docker pour les admins en herbe

Vers le début des années 2000, la **virtualisation** a révolutionné le
quotidien des développeurs et des administrateurs. Désormais il était possible
de faire tourner plusieurs systèmes d'exploitation sur une seule machine, de
tester des applications web avec des navigateurs web exotiques ou obsolètes,
d'installer en parallèle différentes versions d'une pile logicielle ou d'un OS,
etc. 

La virtualisation continue certes à jouer un rôle important, pour les
développeurs aussi bien que pour les administrateurs. Le *cloud* dans ses
différentes moutures n'existerait pas sans la virtualisation. Malgré tout cela,
on observe depuis quelques années une tendance de plus en plus prononcée à
**remplacer les machines virtuelles par des conteneurs**. 

Les **conteneurs** permettent de faire tourner toute une série de composants
logiciels - serveurs web, bases de données, langages de programmation, etc. -
sans l'embonpoint d'une machine virtuelle. À quoi bon s'encombrer de tout un
système Linux dans une machine virtuelle s'il ne s'agit que d'une seule
fonctionalité spécifique&nbsp;:?

Les **avantages des conteneurs** sont nombreux. Les conteneurs sont plus
faciles à déployer et à répliquer que les systèmes virtualisés. Ils nécessitent
nettement moins de ressources et permettent de s'adapter beaucoup plus
facilement à une montée en charge. 

**Docker** n'est pas la seule technologie de conteneurisation - et ce n'est pas
la première non plus - mais c'est actuellement la plus utilisée. Actuellement,
Docker fait partie des technologies les plus populaires auprès des développeurs
et des administrateurs.

Cette **formation initiale** se propose de vous familiariser par la pratique
avec les aspects fondamentaux de Docker. Au lieu de vous plonger d'emblée dans
les surcouches plus abstraites et plus complexes de la conteneurisation, je
préfère vous guider pas à pas en commençant par le début.

Les ateliers pratiques proposés ici ne feront pas de vous un expert DevOps en
deux jours. En revanche, ils vous fourniront les **connaissances de base
indispensables** pour vous former par la suite aux aspects avancés de la
conteneurisation avec Docker.

## Le formateur

[Nicolas Kovacs](https://www.microlinux.fr/#contact) est le fondateur de
[Microlinux](https://www.microlinux.fr), une petite entreprise informatique
spécialisée dans Linux et l'Open Source.  Il est également
[traducteur](https://tinyurl.com/2p8tk495) du [projet de
documentation](https://learning.lpi.org) du *Linux Professional Institute*.
Lorsqu'il n'est pas en train d'administrer des systèmes Linux, de former de
futurs administrateurs Linux ou de réfléchir à voix haute sur son [blog
technique](https://blog.microlinux.fr), il se remet les neurones en place en
allant grimper sur les belles falaises du Gard et de l'Hérault.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

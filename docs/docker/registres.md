
# Utiliser les registres de conteneurs Docker

**Objectifs**&nbsp;:

* Savoir où sont stockées les images

* Connaître les conventions de nommage utilisées pour accéder à ces images

## Le registre par défaut Docker Hub

Dans la configuration par défaut, le client Docker se connecte au registre
Docker Hub pour récupérer les images.

Un registre sert principalement à stocker des images de conteneurs. Vous pouvez
récupérer les images d'un registre, mais vous pouvez également stocker vos
propres images, à condition de disposer des droits d'accès nécessaires.

Un dépôt dans un registre constitue une collection d'une ou plusieurs images.
Un seul dépôt peut contenir un certain nombre d'images Docker, chacune étant
stockée sous forme de *tag*.

De manière générale, les *tags* désignent la version d'une image, mais ils
peuvent également indiquer les différentes déclinaisons d'une image. À titre
d'exemple, vous pouvez avoir un dépôt dans lequel vous conservez des images
MySQL avec des images basées sur Alpine Linux, alors que d'autres sont basées
sur Debian Linux. Vous utiliserez un *tag* pour chacune de ces images.

Voyons en détail à quoi vous vous connectez concrètement lorsque vous récupérez
une image à partir d'un dépôt stocké dans un registre Docker. Nous allons
utiliser l'image Ubuntu comme exemple. Nous choisissons le *tag* `bionic` pour
cibler l'image correspondante dans le dépôt.

```
$ docker pull docker.io/ubuntu:bionic
bionic: Pulling from library/ubuntu
11323ed2c653: Pull complete
Digest: sha256:d8ac28b7bec51664c6b71a9dd1d8f788127ff310b8af30820560973bcfc605a0
Status: Downloaded newer image for ubuntu:bionic
docker.io/library/ubuntu:bionic
```

> Ne vous inquiétez pas si vous vous retrouvez confronté à un message d'erreur
> du genre `Error response from daemon`. Dans ce cas, il vous suffit de
> retenter le coup.

Cette dernière commande est exactement la même que celle-ci&nbsp;:

```
$ docker pull ubuntu:bionic
```

Cela tient au fait que le nom de domaine du registre Docker Hub est
`docker.io`. Le nom de domaine officiel a changé plusieurs fois au fil des
années, mais à l'heure actuelle il est recommandé d'utiliser `docker.io`. Les
anciens noms de domaine fonctionnent toujours, et il se peut que ce nom de
domaine change encore.

Récupérons une autre image&nbsp;:

```
$ docker pull registry.hub.docker.com/library/ubuntu:bionic
bionic: Pulling from library/ubuntu
Digest: sha256:d8ac28b7bec51664c6b71a9dd1d8f788127ff310b8af30820560973bcfc605a0
Status: Downloaded newer image for registry.hub.docker.com/library/ubuntu:bionic
registry.hub.docker.com/library/ubuntu:bionic
```

Nous avons récupéré une autre image d'Ubuntu avec le même *tag*, mais en
utilisant un nom de domaine différent, en l'occurrence
`registry.hub.docker.com`.

Jetons un œil sur nos images locales&nbsp;:

```
$ docker image ls
REPOSITORY                               TAG       IMAGE ID      ...  SIZE
ubuntu                                   bionic    b67d6ac264e4  ...  63.2MB
registry.hub.docker.com/library/ubuntu   bionic    b67d6ac264e4  ...  63.2MB
```

La colonne `IMAGE ID` affiche la même empreinte pour les deux images. Ce qui
veut dire qu'il s'agit de la même image ou qu'elles ont été construites avec la
même somme de contrôle résultante. La colonne `TAG` affiche également `bionic`
pour les deux images.

Pourtant, dans la colonne `REPOSITORY`, nous voyons que ces images ne
proviennent pas du même dépôt. Pour démarrer un conteneur avec l'une d'entre
elles, il nous faudra le préciser comme ceci par exemple&nbsp;:

```
$ docker run -dit registry.hub.docker.com/library/ubuntu:bionic
58d3adc6051a81d78ab04b33477287a976f9c133a1f0e30f267a028271664665
```

Autrement dit, notre image locale est nommée d'après le format suivant&nbsp;:

* l'adresse du registre&nbsp;: `registry.hub.docker.com`

* une barre oblique&nbsp;: `/`

* le dépôt&nbsp;: `library`

* une barre oblique&nbsp;: `/`

* l'image&nbsp;: `ubuntu`

* un deux-points&nbsp;: `:`

* un *tag*&nbsp;: `bionic`

À présent, supprimez l'image en provenance de `docker.io` qui s'affiche juste
comme `ubuntu` localement. N'oubliez pas de préciser le *tag* `bionic`, faute
de quoi vous ne pourrez pas supprimer l'image&nbsp;:

```
$ docker rmi ubuntu:bionic
Untagged: ubuntu:bionic
Untagged: ubuntu@sha256:d8ac28b7bec51664c6b71a9dd1d8f788127af...
```

Voici un autre exemple qui utilise une image non officielle&nbsp;:

```
$ docker pull mysql/mysql-server
Using default tag: latest
latest: Pulling from mysql/mysql-server
221c7ea50c9e: Pull complete
d32a20f3a6af: Pull complete
28749a63c815: Pull complete
3cdab959ca41: Pull complete
30ceffa70af4: Pull complete
e4b028b699c1: Pull complete
3abed4e8adad: Pull complete
Digest: sha256:6fca505a0d41c7198b577628584e01d3841707c3292499baae87037f886c9fa2
Status: Downloaded newer image for mysql/mysql-server:latest
docker.io/mysql/mysql-server:latest
```

Le nom de cette image suit la syntaxe la plus courante. Elle n'est pas listée
comme image officielle, et par conséquent l'espace de nommage est `mysql`,
suivi d'une barre oblique, suivi du dépôt appelé `mysql-server`, et puisque
nous n'avons pas spécifié de balise, nous utilisons simplement la balise par
défaut `latest`.

Prenons encore un autre exemple&nbsp;:

```
$ docker pull bitnami/wordpress-nginx:5.9.2
5.9.2: Pulling from bitnami/wordpress-nginx
...
```

Ici, l'espace de nommage (ou le *namespace*) est `bitnami`, le dépôt est
`wordpress-nginx` et le *tag* de l'image est `5.9.2`.

Lorsque vous commencerez à enregistrer vos propres images sur Docker Hub, vous
utiliserez ce format&nbsp;:

* votre identifiant Docker

* une barre oblique

* le nom du dépôt

* un deux-points

* un *tag*

Soyez conscients des risques de sécurité liés à l'utilisation d'images qui ne
sont pas marquées comme officielles. Même les images officielles présentent un
certain nombre de vulnérabilités logicielles que le fournisseur devra
vraisemblablement corriger à un moment donné dans le futur.

Dans le doute, construisez vos propres images depuis la case départ et avec un
maximum de circonspection.

## Utiliser d'autres registres de conteneurs

En dehors de Docker Hub, il existe un certain nombre de registres de
conteneurs. Par exemple&nbsp;:

  * [Amazon Elastic Container Registry](https://aws.amazon.com/fr/ecr/)

  * [Red Hat Quay](https://quay.io/)

  * [Google Container Registry](https://cloud.google.com/container-registry)

Les applications professionnelles utilisent souvent leur propre registre. Ce
n'est pas très compliqué de configurer un registre privé pour Docker. En effet,
Docker permet de mettre en place des dépôts privés pour lesquels vous devez
vous connecter afin d'accéder au registre.

## Utiliser l'authentification avec Docker Hub

Pour cette formation, nous utilisons principalement Docker Hub comme registre
pour la plupart des exemples. C'est le registre public le plus populaire. Un
détail important que nous allons aborder en revanche, c'est la connexion à un
registre. 

Pour vous authentifier, vous devez utiliser la commande `docker login`. Étant
donné que le registre Docker Hub est déjà utilisé par défaut, ce n'est pas la
peine de l'expliciter avec la commande `login`&nbsp;:

```
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't
have a Docker ID, head over to https://hub.docker.com to create one.
Username: kikinovak
Password:
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

## Exercice

* Supprimez toutes les images locales sur votre système.

* Téléchargez la dernière image officielle du serveur de bases de données
  PostgreSQL.

* Téléchargez la version 12.10 de cette image.

* Affichez les images locales et repérez les deux images respectives.

* Cherchez l'application GLPI sur Docker Hub et repérez le dépôt maintenu
  par `diouxx`.

* Téléchargez la toute dernière version de l'application, ainsi que la
  dernière version stable de la branche 9.x.

* Affichez l'ensemble des images locales et repérez bien les différentes
  versions.

* Créez un compte sur Docker Hub.

* Authentifiez-vous en ligne de commande.

* Supprimez toutes les images locales.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



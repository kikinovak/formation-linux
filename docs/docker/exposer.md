
# Exposer un conteneur Docker

**Objectifs**&nbsp;:

* Rendre une application qui tourne dans un conteneur publiquement accessible
  depuis la machine hôte.

* Partager des données entre un conteneur et le système hôte.

## Comment utiliser une image Docker&nbsp;?

Avant de lancer une image Docker, vous souhaitez probablement en savoir plus
sur les options appropriées à utiliser avec la commande `docker run` pour cette
image donnée. 

Prenons l'exemple du serveur web Nginx&nbsp;:

* Rendez-vous sur Docker Hub.

* Cherchez l'image officielle (*Official build*) de `nginx`.

* Repérez la section *How to use this image*.

> Idéalement, il vaut mieux s'en tenir aux images officielles pour être à
> l'abri des *malwares*.

## Rendre une application publiquement accessible

Nous allons utiliser l'exemple de la section *Exposing external port* en
l'adaptant à nos besoins&nbsp;:

```
$ docker run --name mon_nginx -d -p 8080:80 nginx
```

* L'option `-p 8080:80` signifie simplement qu'il faut ouvrir le port TCP 8080
  sur la machine hôte de Docker et rediriger le trafic vers et depuis ce port
  vers le port TCP 80 à l'intérieur du conteneur Nginx. Autrement dit, le port
  8080 représente le monde extérieur, le port 80 le monde intérieur du
  conteneur.

Voyons si le conteneur fonctionne&nbsp;:

```
$ docker ps
CONTAINER ID   IMAGE  ...  PORTS                  NAMES
a92710efd101   nginx  ...  0.0.0.0:8080->80/tcp   mon_nginx
```

* `0.0.0.0:8080` signifie que toutes les adresses IP locales de notre machine 
  hôte servent sur le port TCP 8080, et que celui-ci est dirigé vers le port 80
  à l'intérieur du conteneur.

Voyons ce que fait Nginx lorsque nous essayons de nous connecter au
conteneur&nbsp;:

```
$ curl http://localhost:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
```

Nous venons de lancer notre premier conteneur Docker publiquement accessible. 

Ouvrez un navigateur web sur la machine hôte pour accéder au serveur web Nginx
qui tourne dans le conteneur&nbsp;:

* `http://localhost:8080`

Si le conteneur tourne sur une autre machine du réseau, vous pouvez accéder au
serveur Nginx comme ceci par exemple&nbsp;:

* `http://alphamule.microlinux.lan:8080`

Alternativement, utilisez l'adresse IP de la machine&nbsp;:

* `http://192.168.2.2:8080`

Affichez les requêtes d'accès au serveur web&nbsp;:

```
$ docker logs mon_nginx
...
192.168.2.4 - - [21/Mar/2022:07:55:07 +0000] "GET /favicon.ico HTTP/1.1" 404
153 "http://192.168.2.2:8080/" "Mozilla/5.0 (X11; Linux x86_64; rv:98.0)
Gecko/20100101 Firefox/98.0" "-"
```

Arrêtez le conteneur&nbsp;:

```
$ docker stop mon_nginx
mon_nginx
```

## Partager des données entre l'hôte et le conteneur

À présent, essayons de remplacer la page par défaut de Nginx par du contenu
personnalisé&nbsp;:

```
$ mkdir -v ~/pagesweb
mkdir: création du répertoire '/home/kikinovak/pagesweb'
$ cd ~/pagesweb/
$ echo '<h1>Le site web de Nico</h1>' > index.html
$ cd ..
```

Lancez un conteneur Nginx avec une option supplémentaire comme ceci&nbsp;:

```
$ docker run --name autre_nginx -d -p 8080:80 \
  -v ${PWD}/pagesweb:/usr/share/nginx/html:ro nginx
6b456addfb69fb9fe5acdec3dd4e5e7fbad0808edaac0ac28293979b036e4c0d
```

* `${PWD}` est une simple variable d'environnement Bash qui fournit le
  répertoire de travail (*present working directory*). 

* L'option `-v` crée un volume pour le partage de fichiers. La syntaxe est `-v`
  suivi du chemin d'accès vers le répertoire sur la machine hôte auquel le
  conteneur est censé avoir accès, suivi de deux-points, suivi du chemin
  d'accès vers le répertoire du conteneur qui accèdera à ces données. Si vous
  souhaitez fournir des options, vous aurez besoin d'un autre deux-points,
  suivi des options. En l'occurrence, nous avons utilisé `ro` qui signifie
  *read only*, et qui monte le volume en lecture seule à l'intérieur du
  conteneur. Cela veut dire que les fichiers peuvent être modifiés depuis la
  machine hôte de Docker, mais le conteneur ne pourra pas modifier les
  fichiers. Le conteneur pourra uniquement lire les données contenues dans le
  volume. Dans mon exemple, le répertoire `/home/kikinovak/pagesweb` est
  disponible dans `/usr/share/nginx/html` à l'intérieur du conteneur Docker.

Voyons à présent si nous pouvons accéder à cette page personnalisée en envoyant
une requête vers le port 8080 du système hôte&nbsp;:

```
$ curl http://localhost:8080
<h1>Le site web de Nico</h1>
```

Et depuis une autre machine&nbsp;:

```
$ curl http://192.168.2.2:8080
<h1>Le site web de Nico</h1>
```

Pour finir, faites un brin de ménage&nbsp;:

```
$ docker stop autre_nginx
$ docker system prune
```

## Exercice

* Recherchez l'image officielle du serveur web Apache sur Docker Hub.

* Télécharchez la dernière version de l'image.

* Renseignez-vous sur les options courantes à utiliser avec cette image.

* Lancez un conteneur `apache_bienvenue` publiquement accessible sur le port
  9900 du système hôte.

* Vérifiez si le conteneur tourne correctement.

* Jetez un œil sur la configuration de la redirection des ports.

* Utilisez la commande `curl` en ligne de commande pour afficher la page par
  défaut du serveur web.

* Affichez cette page avec un navigateur web sur la machine hôte.

* Créez un répertoire `~/mapage` sur le système hôte, contenant un fichier
  `index.html` avec le contenu `<h1>Coucou !</h1>`. 

* Lancez un conteneur `apache_coucou` publiquement accessible sur le port 9901
  du système hôte, et qui utilise votre page `index.html` personnalisée comme
  page par défaut dans un volume partagé en lecture seule. Reportez-vous à la
  section *How to use this image* sur Docker Hub pour en savoir plus sur la
  configuration du serveur Apache, notamment l'emplacement de la page web par
  défaut.

* Vérifiez si le conteneur tourne correctement et jetez un œil sur la
  configuration de la redirection des ports.

* Là encore, affichez votre page personnalisée sur le système hôte en utilisant
  successivement `curl` et un navigateur web.

* Ouvrez un navigateur web sur une machine distante et connectez-vous
  successivement aux deux serveurs web tournant sur les conteneurs
  `apache_bienvenue` et `apache_coucou`.

* Affichez le suivi des journaux d'accès et essayez d'identifier le ou les
  postes clients qui se connectent aux deux serveurs.

* Supprimez les deux conteneurs `apache_bienvenue` et `apache_coucou` et faites
  le ménage sur votre système.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



**Objectif**&nbsp;: Installer et gérer des logiciels sur un système Linux de la
famille Red Hat avec le gestionnaire de paquets Yum.

**Système**&nbsp;: Oracle Linux 7.x.

Après l'installation des composants logiciels depuis le code source et la
gestion des paquets avec l'outil RPM, le moment est venu de vous présenter Yum
(*Yellowdog Updater Modified*), un gestionnaire de paquets en ligne de
commande, tout comme RPM. À la différence de ce dernier, il gère
automatiquement le téléchargement des paquets et la résolution des dépendances,
ce qui augmente considérablement le confort d'utilisation au quotidien.

> Yum est une réécriture complète de Yup (*Yellowdog Updater*), le gestionnaire
> de mises à jour utilisé par Yellow Dog Linux, une distribution basée sur
> CentOS et Fedora et développée pour les ordinateurs équipés d'un processeur
> PowerPC comme les Macintosh d'Apple d'avant 2006. La distribution Yellow Dog
> Linux n'est plus maintenue depuis 2009.

## Installer un paquet avec Yum

Pour illustrer le fonctionnement de Yum, partons d'une installation minimale
d'Oracle Linux dans sa configuration par défaut et utilisons-le pour installer
`tcpdump`&nbsp;:

```
$ sudo yum install tcpdump
...
Dependencies Resolved

============================================================================
 Package         Arch           Version                 Repository      Size
============================================================================
Installing:
 tcpdump         x86_64         14:4.9.2-4.0.1.el7_7.1  ol7_latest     421 k
Installing for dependencies
 libpcap         x86_64         14:1.5.3-12.el7         ol7_latest     138 k

Transaction Summary
============================================================================
Install  1 Package (+1 Dependent package)

Total download size: 559 k
Installed size: 1.3 M
Is this ok [y/d/N]:
```

L'écran affiche alors pléthore d'informations. Essayons de comprendre un peu ce
qui se passe. Dans un premier temps, Yum se connecte au dépôt de
téléchargement (`ol7_latest`) pour récupérer la liste des paquets
disponibles en ligne. Il identifie le paquet recherché (`tcpdump`) et se charge
de résoudre les dépendances requises. Il en trouve une (`libpcap`), comme il faut
s'y attendre. Au bout de cette opération, Yum affiche un tableau récapitulatif
qui résume ce qu'il a l'intention de faire&nbsp;: télécharger et installer le paquet
`tcpdump` comme prévu, de même que la dépendance requise `libpcap`. Il suffit de
confirmer par ++y++ (*yes*) et Yum s'exécute&nbsp;:

```
Is this ok [y/d/N]: y
Downloading packages:
(1/2): libpcap-1.5.3-12.el7.x86_64.rpm           | 138 kB  00:00:00
(2/2): tcpdump-4.9.2-4.0.1.el7_7.1.x86_64.rpm    | 421 kB  00:00:00
-------------------------------------------------------------------
Total                                   964 kB/s | 559 kB  00:00
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : 14:libpcap-1.5.3-12.el7.x86_64          1/2
  Installing : 14:tcpdump-4.9.2-4.0.1.el7_7.1.x86_64   2/2
  Verifying  : 14:tcpdump-4.9.2-4.0.1.el7_7.1.x86_64   1/2
  Verifying  : 14:libpcap-1.5.3-12.el7.x86_64          2/2

Installed:
  tcpdump.x86_64 14:4.9.2-4.0.1.el7_7.1

Dependency Installed:
  libpcap.x86_64 14:1.5.3-12.el7

Complete!
```

> Si vous effectuez cette opération sur un système sur lequel vous avez
> préalablement installé, supprimé ou mis à jour des paquets avec RPM, Yum vous
> affichera probablement l'avertissement suivant&nbsp;: `Warning: RPMDB altered
> outside of yum`. RPMDB est la base de données des paquets installés sur le
> système. Yum vous informe ici qu'elle a été modifiée par un autre
> gestionnaire de paquets, en l'occurrence RPM. Il s'agit là d'un avertissement
> bénin. Pour régler le problème, il suffit d'invoquer la commande `sudo yum
> history sync`.

Lors de la première utilisation d'une archive de téléchargement, Yum vous
demande s'il doit importer la clé GPG. Il s'agit d'une clé numérique qui sert à
garantir d'authenticité des paquets téléchargés.

> GNU Privacy Guard, de son petit nom GPG, est un clone libre de PGP (*Pretty
> Good Privacy*). Tous deux sont des systèmes de signature et chiffrement de
> l'information reposant sur un système de clés que l'on s'échange afin de
> pouvoir décrypter ou vérifier l'information. Pour en savoir plus sur GPG, je
> vous conseille la lecture de l'excellent ouvrage *PGP & GPG – Email for the
> Practical Paranoid* de Michael W. Lucas, dont la traduction française est
> disponible chez Eyrolles.

Réponsez simplement par ++y++ (*yes*)&nbsp;:

```
Public key for libpcap-1.5.3-12.el7.x86_64.rpm is not installed
(1/2): libpcap-1.5.3-12.el7.x86_64.rpm          | 138 kB  00:00:00     
(2/2): tcpdump-4.9.2-4.0.1.el7_7.1.x86_64.rpm   | 421 kB  00:00:00     
------------------------------------------------------------------
Total                                  910 kB/s | 559 kB  00:00:00     
Retrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-oracle
Importing GPG key 0xEC551F03:
 Userid     : "Oracle OSS group (Open Source Software group) ..."
 Fingerprint: 4214 4123 fecf c55b 9086 313d 72f9 7b74 ec55 1f03
 Package    : 7:oraclelinux-release-7.9-1.0.9.el7.x86_64 (@anaconda/7.9)
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle
Is this ok [y/N]: y
```

Vous avez envie de retenter l'expérience&nbsp;? Cette fois-ci, utilisez
l'option `-y` qui évite la demande de confirmation&nbsp;:

```
$ sudo yum install -y nmap
```

## Supprimer des paquets avec Yum

La suppression d'un paquet se fait grâce à l'option `remove`. Là aussi, Yum se
charge automatiquement de rétablir l'intégrité du système&nbsp;:

```
$ sudo yum remove nmap-ncat
```

Rappelez-vous que RPM nous interdisait la suppression de ce paquet tant que
`nmap` (qui en dépend) était installé. Voyons ce qu'en pense Yum&nbsp;:

```
=========================================================================
 Package        Arch        Version               Repository        Size
=========================================================================
Removing:
 nmap-ncat      x86_64      2:6.40-19.el7         @ol7_latest      423 k
Removing for dependencies:
 nmap           x86_64      2:6.40-19.el7         @ol7_latest       16 M

Transaction Summary
========================================================================
```

Ici, `nmap-ncat` sera enlevé sans problème, mais `nmap` sera également supprimé
dans la foulée (`Removing for dependencies`) pour que le système garde sa
cohérence.

Pour bien comprendre la gestion des dépendances lors de la suppression de
paquets, vérifiez si `tcpdump` est installé, supprimez le paquet `libpcap` et
observez ce qui se passe&nbsp;:

```
$ sudo yum remove libpcap
```

## Gérer les dépendances orphelines

À force d'installer et de supprimer des paquets, nous risquons de nous
retrouver avec un système encombré de dépendances orphelines, c'est-à-dire des
paquets qui ne servent plus à rien à partir du moment où les paquets qui en
dépendent sont supprimés.

Voici une petite expérience pratique pour illustrer ce concept&nbsp;:

```
$ sudo yum install nmap
...
$ sudo yum remove nmap
...
```

L'installation de `nmap` récupère automatiquement la dépendance `nmap-ncat` si
elle n'est pas déjà présente sur le système. En revanche, ce paquet reste en
place après l'opération de suppression et devient ainsi une dépendance
orpheline.

De même, lorsque vous supprimez le paquet `tcpdump` avec `yum remove`, le
paquet `libpcap` reste en place et ne sert plus à grand-chose.

L'option `autoremove` gère la suppression des dépendances orphelines&nbsp;:

```
$ sudo yum install nmap tcpdump
...
$ sudo yum autoremove nmap
...
$ sudo yum autoremove tcpdump
...
```

Cette fois-ci, les dépendances inutiles `nmap-ncat` et `libpcap` sont
automatiquement détectées et supprimées par Yum.

## Effectuer une mise à jour

La mise à jour des applications et même du système entier se fait avec une
facilité déconcertante. L'option `check-update` vous affiche la liste complète
de tous les paquets installés pour lesquels des mises à jour sont disponibles.
Elle s'invoque sans privilèges particuliers&nbsp;:

```
$ yum check-update
Loaded plugins: ulninfo

btrfs-progs.x86_64            5.4.0-1.el7              ol7_UEKR6
e2fsprogs.x86_64              1.45.4-3.0.5.el7         ol7_UEKR6
e2fsprogs-libs.x86_64         1.45.4-3.0.5.el7         ol7_UEKR6
iproute.x86_64                5.4.0-1.0.1.el7          ol7_UEKR6
libcom_err.x86_64             1.45.4-3.0.5.el7         ol7_UEKR6
libss.x86_64                  1.45.4-3.0.5.el7         ol7_UEKR6
xfsprogs.x86_64               5.4.0-1.0.1.el7          ol7_UEKR6
```

À partir de là, l'option `update` met à jour un paquet installé&nbsp;:

```
$ sudo yum update iproute
```

Si la mise à jour d'un paquet dépend de celle de dépendances, celle-ci
s'effectue automatiquement&nbsp;:

```
$ sudo yum update e2fsprogs
...
================================================================================
 Package             Arch        Version                  Repository       Size
================================================================================
Updating:
 e2fsprogs           x86_64      1.45.4-3.0.5.el7         ol7_UEKR6       1.0 M
Installing for dependencies:
 fuse-libs           x86_64      2.9.4-1.0.9.el7          ol7_latest       97 k
Updating for dependencies:
 e2fsprogs-libs      x86_64      1.45.4-3.0.5.el7         ol7_UEKR6       222 k
 libcom_err          x86_64      1.45.4-3.0.5.el7         ol7_UEKR6        44 k
 libss               x86_64      1.45.4-3.0.5.el7         ol7_UEKR6        48 k

Transaction Summary
================================================================================
...
```

La solution la plus simple consiste certainement à garder le système entier à
jour. Invoquée sans autre argument, `update` met à jour l'intégralité du
système&nbsp;:

```
$ sudo yum update
```

### Ce qu'il faut savoir sur les mises à jour

Dans le sillage de Red Hat Enterprise Linux, Oracle publie régulièrement des
versions *majeures* et *mineures* de sa distribution Linux clonée. À titre
d'exemple, Oracle Linux&nbsp;7.0 a été publié début juillet 2014. Les paquets
constituant cette version ont été régulièrement mis à jour pour corriger les
bogues et les failles de sécurité. Puis, fin mars 2015, toutes ces mises à jour
ont été incluses dans la publication de la version mineure 7.1. Au moment où
j'écris ces lignes (juin 2021), Oracle Linux&nbsp;7 en est arrivé à sa neuvième
version mineure.

Ce qu'il faut retenir, c'est que quelqu'un qui a installé le système en 2014
avec un support d'installation Oracle Linux&nbsp;7.0 peut effectuer une mise à
jour de son système grâce à `yum update` et se retrouver exactement avec le
même système sur sa machine que quelqu'un qui l'installe aujourd'hui à partir
d'un support de la version 7.9.

Chaque nouvelle version d'Oracle Linux, même mineure, prend en charge davantage
de matériels, sous forme de modules ajoutés au kernel. Si le support
d'installation d'une ancienne version d'Oracle Linux ne reconnaît pas un
certain matériel (comme la carte réseau), c'est le moment de tenter le coup
avec une version plus récente.


## Afficher les listes de paquets par états

L'option `list` combinée à d'autres arguments permet de lister les paquets par
états&nbsp;; par exemple, ceux qui sont installés&nbsp;:

```
$ yum list installed
```

Le résultat de cette commande est un peu plus lisible qu'un simple `rpm -qa`,
mais c'est une question de préférence personnelle. Pour afficher la liste
complète des paquets disponibles&nbsp;:

```
$ yum list available
```

Cette soupe alphabétique composée de quelques milliers de paquets ne vous
paraît probablement pas très parlante. Nous verrons un peu plus loin les
fonctions de recherche sur les paquets. Pour afficher la liste des mises à jour
disponibles&nbsp;:

```
$ yum list updates
```

Cette commande a le même effet que l'option `check-update`, au détail près que
la présentation du résultat est différente. Là aussi, c'est avant tout une
question de goût. Pour afficher la liste des nouveautés disponibles dans les
archives&nbsp;:

```
$ yum list recent
```

L'argument `extras` affiche la liste des paquets installés sur le système, qui
ne sont présents dans aucune archive. Cette commande ne retournera rien pour
l'instant&nbsp;:

```
$ yum list extras
```

Et pour avoir une idée de tout (!) ce qu'il y a&nbsp;:

```
$ yum list all
```

## Gérer les groupes de paquets

Certaines applications requièrent l'installation de toute une série de paquets,
que même l'administrateur le plus chevronné ne peut pas connaître par coeur.
Dans ce cas, les groupes de paquets nous facilitent la tâche&nbsp;:

```
$ yum group list
...
Available Environment Groups:
   Minimal Install
   Compute Node
   Infrastructure Server
   File and Print Server
   Basic Web Server
   Virtualization Host
   ...
Available Groups:
   Compatibility Libraries
   Console Internet Tools
   Development Tools
   ...
Done
```

Pour une raison mystérieuse, les groupes de paquets sont pour la plupart
occultés dans la configuration par défaut depuis Oracle Linux 7.0. L'option
`hidden` sert à les afficher&nbsp;:

```
$ yum group list hidden
...
Available Groups:
   Additional Development
   Anaconda Tools
   Backup Client
   Backup Server
   Base
   ...
   Console Internet Tools
   Core
   DNS Name Server
   ...
```

Les résultats de `group list` sont filtrables à l'aide de `grep`, comme ceci
par exemple&nbsp;:

```
$ yum group list hidden | grep -i database
   MariaDB Database Client
   MariaDB Database Server
   PostgreSQL Database Client
   PostgreSQL Database Server
```

À partir de là, je peux installer un groupe de paquets en utilisant `group
install`. Admettons que je veuille disposer de tous les paquets du système de
base&nbsp;:

```
$ sudo yum group install "Core"
```

Le groupe de paquets `Base` fournit un système de base étendu sous forme d'une
panoplie complète d'outils courants comme l'éditeur Vim, les pages de manuel en
ligne et bien d'autres choses encore. Sur un système Oracle Linux, c'est
l'équivalent des boîtes à outils complètes que vous trouvez dans les magasins
de bricolage. Je vous conseille donc d'installer ce groupe de paquets&nbsp;:

```
$ sudo yum group install "Base"
```

Dorénavant, le résultat de la commande `yum group list` s'organise en deux
sections&nbsp;:

* les groupes de paquets déjà présents sur votre système (`Installed
  Groups`)&nbsp;;

* ceux susceptibles d'être installés (`Available Groups`).

```
$ yum group list hidden
...
Installed Groups:
   Base
   Core
Available Groups:
   Additional Development
   Anaconda Tools
   Backup Client
   Backup Server
   ...
```

Admettons que je veuille installer un serveur de bases de données sur ma
machine, je pourrais le faire tout simplement comme ceci&nbsp;:

```
$ sudo yum group install "MariaDB Database Server"
```

Pour supprimer tout un groupe de paquets, c'est aussi simple que
l'installation&nbsp;:

```
$ sudo yum group remove "MariaDB Database Server"
```

> Cette dernière commande ne supprime pas les dépendances orphelines du
> système.

## Obtenir des informations sur les paquets

L'option `info` affiche les informations concernant un paquet&nbsp;:

```
$ yum info httpd
...
Nom          : httpd
Architecture : x86_64
Version      : 2.4.6
Révision     : 88.el7.centos
Taille       : 2.7 M
Dépôt        : base/7/x86_64
Résumé       : Apache HTTP Server
URL          : http://httpd.apache.org/
Licence      : ASL 2.0
Description  : The Apache HTTP Server is a powerful, efficient,
             : and extensible web server.
```

Le résultat est similaire à celui de `rpm -qi`, au détail près que la
présentation est différente. En revanche, les informations s'affichent même si
le paquet n'est pas installé.

## Rechercher un paquet

L'option `search` vous permet de rechercher un paquet&nbsp;:

```
$ yum search vim
...
==================== N/S matched: vim ====================
vim-X11.x86_64 : The VIM version of the vi editor for the X Window System
vim-common.x86_64 : The common files needed by any version of the VIM editor
vim-enhanced.x86_64 : A version of the VIM editor which includes recent 
                      enhancements
vim-filesystem.x86_64 : VIM filesystem layout
vim-minimal.x86_64 : A minimal version of the VIM editor
```

Si vous voulez effectuer une recherche sur un terme de la description du
paquet, la seule restriction sera d'ordre linguistique, étant donné que les
paquets sont décrits en anglais. Donc, si vous cherchez un éditeur, invoquez la
commande suivante&nbsp;:

```
$ yum search editor
```

Yum vous affichera la liste de tous les paquets qui contiennent le terme
`editor` dans leur description. Autrement, si vous voulez piocher dans les
navigateurs en mode texte, par exemple, vous pouvez le faire comme ceci&nbsp;:

```
$ yum search browser | grep text
elinks.x86_64 : A text-mode Web browser
less.x86_64 : A text file browser similar to more, but better
```

L'option `list` sert également à effectuer des recherches sur les paquets. Je
peux étendre la recherche avec un *joker*&nbsp;:

```
$ yum list vim*
...
Installed Packages
vim-common.x86_64          2:7.4.629-8.0.1.el7_9      @ol7_latest  
vim-enhanced.x86_64        2:7.4.629-8.0.1.el7_9      @ol7_latest  
vim-filesystem.x86_64      2:7.4.629-8.0.1.el7_9      @ol7_latest  
vim-minimal.x86_64         2:7.4.629-8.0.1.el7_9      @anaconda/7.9
Available Package
vim-X11.x86_64             2:7.4.629-8.0.1.el7_9      ol7_latest
```

L'option `provides` me permet de chercher un paquet qui contient un certain
fichier&nbsp;:

```
$ yum provides /usr/bin/mysql
...
1:mariadb-5.5.56-2.el7.x86_64 : A community developed branch of MySQL
Repo        : ol7_latest
Matched from:
Filename    : /usr/bin/mysql
...
```

## Configuration des dépôts pour Yum

Les *dépôts* ou *archives de téléchargement* sont les endroits (sites Internet
ou locaux) à partir desquels Yum télécharge les logiciels et leurs mises à
jour.  On pourrait également utiliser le terme plus explicite de *sources de
téléchargement*.

### Où est-ce que ça se configure ?

Dans les premières versions de Yum, toute la configuration s'effectuait dans un
seul fichier&nbsp;: `/etc/yum.conf`. Dans les versions plus récentes, la
configuration des dépôts s'écrit dans des fichiers à part, qui se trouvent dans
le répertoire `/etc/yum.repos.d`&nbsp;:

```
$ ls /etc/yum.repos.d/
oracle-linux-ol7.repo  uek-ol7.repo  virt-ol7.repo
```

Dans notre installation par défaut, `/etc/yum.repos.d` contient trois fichiers
`.repo`. L'extension `.repo` est une convention qui indique qu'il s'agit d'un
*repository*, c'est-à-dire d'un dépôt.

### Les dépôts officiels de la distribution

Jetons un oeil sur le fichier `oracle-linux-ol7.repo`. Il est constitué d'une
petite vingtaine de stances qui suivent toutes le même schéma.

Chacune des stances commence par le nom de l'archive correspondante entre
crochets : `[ol7_latest]`, `[ol7_u0_base]`, `[ol7_u1_base]`,
`[ol7_optional_latest]`, `[ol7_addons]`, etc. Dans la configuration par défaut,
seule l'archive `[ol7_latest]` est activée (`enabled=1`), les autres sont
toutes désactivées (`enabled=0`). 

```
[ol7_latest]
name=Oracle Linux $releasever Latest ($basearch)
baseurl=https://yum$ociregion.$ocidomain/repo/OracleLinux/OL7/latest/$basearch/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-oracle
gpgcheck=1
enabled=1

[ol7_u0_base]
name=Oracle Linux $releasever GA installation media copy ($basearch)
baseurl=https://yum$ociregion.$ocidomain/repo/OracleLinux/OL7/0/base/$basearch/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-oracle
gpgcheck=1
enabled=0
...
```

Les stances du fichier `oracle-linux-ol7.repo` s'organisent suivant la même
structure. La ligne commençant par `name` contient le nom de l'archive de
téléchargement. La directive `baseurl` définit l'URL de l'archive
correspondante. L'option `gpgcheck=1` signfie à Yum de procéder à la
vérification de la signature des paquets avant l'installation, et `gpgkey`
fournit l'emplacement de la clé GPG publique correspondante. Quant aux
différents paramètres comme `$releasever` ou `$basearch`, il s'agit de
variables qui renseignent sur la version d'Oracle Linux et sur l'architecture
du processeur pour lequel le système a été compilé.

Partant de là :

* Affichez les fichiers `uek-ol7.repo` et `virt-ol7.repo`.

* Essayez de comprendre la configuration par défaut.

* Affichez la page [Oracle Linux 7 Package
  Repositories](http://yum.oracle.com/oracle-linux-7.html) dans un navigateur
  web.

* Tentez d'identifier les dépôts de téléchargement configurés par défaut sur
  votre système.

### Désactiver le dépôt UEK

Le dépôt UEK fournit une série de paquets développés et/ou maintenus par Oracle
comme le noyau UEK (*Unbreakable Enterprise Kernel*) ou les outils nécessaires
pour gérer le système de fichiers Btrfs. Ces composants (comme les paquets
`kernel-uek` ou `btrfs-progs`) ne figurent pas dans Red Hat Enterprise Linux et
constituent en quelque sorte une spécialité du distributeur Oracle. 

Nous n'en avons pas vraiment besoin, étant donné que nous avons opté pour le
noyau "classique" cloné depuis Red Hat Enterprise Linux (paquet `kernel`) et un
système de fichiers tout aussi classique. Nous pouvons donc sereinement
désactiver le dépôt UEK en éditant le fichier `uek-ol7.repo` comme ceci&nbsp;:

```
[ol7_UEKR6]
...
enabled=0
```

À présent, nous utilisons le seul dépôt `[ol7_latest]`&nbsp;:

```
$ yum repolist
Loaded plugins: ulninfo
repo id                   repo name                             status
ol7_latest/x86_64         Oracle Linux 7Server Latest (x86_64)  22,766
repolist: 22,766
```

Pour avoir une vague idée du nombre de paquets disponibles dans la
configuration actuelle, vous pouvez utiliser l'astuce suivante&nbsp;:

```
$ yum list all | wc -l
5569
```

### Activer le dépôt Optional Latest

Le dépôt *Optional Latest* correspond au dépôt *Optional* de la distribution Red Hat
Enterprise Linux en amont. Il n'est pas activé dans la configuration par
défaut, mais nous pouvons remédier à cela en éditant
`oracle-linux-ol7.repo`&nbsp;:

```
[ol7_optional_latest]
...
enabled=1
```

Vérifiez si le dépôt apparaît bien dans la liste des dépôts configurés&nbsp;:

```
$ yum repolist
Loaded plugins: ulninfo
repo id                     repo name                                      status
ol7_latest/x86_64           Oracle Linux 7Server Latest (x86_64)           22,766
ol7_optional_latest/x86_64  Oracle Linux 7Server Optional Latest (x86_64)  16,313
repolist: 39,079
```

Notez que cette petite manipulation nous a permis de doubler le nombre de
paquets disponibles pour notre système&nbsp;:

```
$ yum list all | wc -l
10871
```

### Activer le dépôt Addons

Tout comme le dépôt UEK, le dépôt Addons fournit une série de composants
officiellement maintenus par Oracle, mais que l'on ne trouvera pas dans Red Hat
Enterprise Linux. 

Là aussi, on pourra activer ce dépôt en éditant `oracle-linux-ol7.repo`&nbsp;:

```
[ol7_addons]
...
enabled=1
```

Au total, nos trois dépôts officiels nous fournissent près de 11000
paquets&nbsp;:

```
$ yum list all | wc -l
10958
```

### Oracle Linux, le parent pauvre des distributions ?

Un choix de près de 11000 paquets, ça paraît beaucoup, mais ça ne l'est pas
tant que ça, surtout si l'on regarde du côté des distributions comme OpenSUSE,
Ubuntu, Debian ou Fedora, qui en proposent jusqu'à cinq ou six fois plus.
Est-ce que cela signifie qu'Oracle Linux est un parent pauvre et que nous
aurions mieux fait de porter notre choix sur une autre distribution&nbsp;? Non,
pas vraiment.

Oracle Linux est un *Enterprise Linux* et une des particularités d'une telle
distribution à usage professionnel, c'est qu'elle offre un nombre restreint de
paquets tout en apportant un soin particulier à la cohérence et à la stabilité
de l'ensemble.

### Paquets à gogo !

Pour obtenir des applications autres que celles proposées directement par les
principaux dépôts officiels d'Oracle Linux, la solution consiste tout
simplement à configurer l'un ou plusieurs des nombreux dépôts supplémentaires.

N'oublions pas que Red Hat Enterprise Linux - la distribution en amont d'Oracle
Linux - provient directement de la distribution Fedora. On peut très bien
considérer que cette dernière constitue une version de développement de Red Hat
Enterprise Linux qui, inversement, représente une "Fedora stabilisée", mais
n'en contenant qu'un nombre limité de paquets.

Certaines sources de téléchargement - notamment le dépôt EPEL - contiennent des
paquets de Fedora recompilés pour Oracle Linux. Ce genre de dépôt présente un
avantage en même temps qu'un inconvénient. Il met à disposition un nombre
important de paquets mais ne préserve pas l'intégrité "entreprise" de votre
système. Concrètement, l'utilisation de ce genre d'archive peux facilement
multiplier par deux ou par trois le nombre de paquets disponibles pour votre
système, mais en contrepartie, l'installation de certains d'entre eux forcera
la mise à jour de ceux du système de base. Cela ne veut pas dire que votre
système plantera, mais le système de base ultra-stable Oracle Linux en tant que
clone de Red Hat Enterprise Linux contiendra désormais des composants qui n'ont
pas été aussi dûment testés et qui ne bénéficient pas du même suivi. 

Une solution élégante à ce dilemme consiste tout simplement à définir des
priorités pour Yum. Concrètement, il suffit de configurer une source de
téléchargement tierce tout en indiquant au système&nbsp;: attention,
interdiction d'installer des paquets qui risquent de remplacer des composants
du système de base. Cela a l'air compliqué en théorie mais, dans la pratique,
c'est relativement simple, comme vous allez le voir tout de suite.

### Protéger le système de base avec Yum-Priorities

Avant d'aller plus loin dans la configuration des archives, nous allons
installer une extension pour Yum, le plug-in (ou greffon) Yum-Priorities. 

```
$ sudo yum install yum-plugin-priorities
```

Cette extension permet de définir des priorités pour les différentes archives
de téléchargement. Il suffit de définir une variable `priority=N`, avec
`N` compris entre `1` (la plus haute) et `99` (la plus basse). Concrètement, si
vous avez défini une priorité de `1` pour le dépôt `[ol7_latest]` et une
priorité de `10` (ou `30`, ou `99`, peu importe) pour les dépôts tiers et/ou
semi-officiels que nous verrons un peu plus loin, les paquets de ces derniers
ne pourront jamais remplacer les paquets de base. Yum les en empêchera tout
simplement, en les excluant de la liste.

### Configurer les dépôts de paquets officiels

Éditez `oracle-linux-ol7.repo` et activez les dépôts `[ol7_latest]`,
`[ol7_optional_latest]` et `[ol7_addons]` avec une priorité maximale&nbsp;:

```
[ol7_latest]
name=Oracle Linux $releasever Latest ($basearch)
...
enabled=1
priority=1

...

[ol7_optional_latest]
name=Oracle Linux $releasever Optional Latest ($basearch)
...
enabled=1
priority=1

[ol7_addons]
name=Oracle Linux $releasever Add ons ($basearch)
...
enabled=1
priority=1

...

```

### Configurer le dépôt tiers EPEL

Le dépôt tiers EPEL (*Extra Packages for Enterprise Linux*) fournit des paquets
qui ne sont pas inclus dans la distribution Oracle Linux. Il se configure très
simplement à l'aide du paquet correspondant&nbsp;:

```
$ sudo yum install -y oracle-epel-release-el7
```

Le paquet a installé un fichier `oracle-epel-ol7.repo` dans le répertoire
`/etc/yum.repos.d`. Ouvrons-le et éditons-le pour définir les priorités du
dépôt&nbsp;:

```
[ol7_developer_EPEL]
...
enabled=1
priority=10
```

À partir de là, nous pouvons vérifier si la gestion des priorités fonctionne
comme prévu&nbsp;:

```
$ yum check-update
Loaded plugins: priorities, ulninfo
259 packages excluded due to repository priority protections
...
```

Nous disposons désormais d'un choix de paquets bien plus important&nbsp;:

```
$ yum list all | wc -l
26419
$ yum search browser | grep text
elinks.x86_64 : A text-mode Web browser
less.x86_64 : A text file browser similar to more, but better
links.x86_64 : Web browser running in both graphics and text mode
lynx.x86_64 : A text-based Web browser
```

### Installer un paquet RPM téléchargé avec Yum

Pour terminer cet atelier pratique, je vous montre un cas de figure que l'on
rencontre de temps en temps. Dans l'exemple qui suit, je cherche à configurer
le dépôt tiers Icinga qui me permet d'installer le système de *monitoring* du
même nom. 

```
$ mkdir -v RPMS
mkdir: created directory ‘RPMS’
$ cd RPMS/
$ links https://packages.icinga.com/epel/
```

Je télécharge le paquet correspondant à ma distribution et je quitte
Links&nbsp;:

```
$ ls
icinga-rpm-release-7-latest.noarch.rpm
```

Je pourrais très bien l'installer en utilisant `rpm -ivh` ou même `rpm -Uvh`.
Rappelez-vous que j'utilisation conjointe de Yum et de RPM entraîne
l'avertissement suivant&nbsp;: `Warning: RPMDB altered outside of yum`. 

Pour éviter des incohérences dans la base de données des paquets RPM, la
meilleure solution consiste à utiliser Yum avec l'option `localinstall`&nbsp;:

```
$ sudo yum localinstall icinga-rpm-release-7-latest.noarch.rpm
...
Running transaction
  Installing : icinga-rpm-release-7-4.el7.icinga.noarch   1/1
  Verifying  : icinga-rpm-release-7-4.el7.icinga.noarch   1/1

Installed:
  icinga-rpm-release.noarch 0:7-4.el7.icinga

Complete!
```

Cette façon de faire présente un autre avantage. Au cas où un paquet RPM
téléchargé présente des dépendances locales, `yum localinstall` tentera de les
résoudre automatiquement.

## Petit tour d'horizon sur les gestionnaires de paquets

Les outils de gestion des paquets constituent un point de distinction assez
important entre les différentes distributions Linux. Le simple format des
paquets permet d'organiser la majorité des distributions en grandes
familles&nbsp;:

* paquets au format `.rpm`&nbsp;: Oracle Linux, Red Hat Enterprise Linux,
  CentOS, Fedora, SUSE et OpenSUSE&nbsp;;

* paquets au format `.deb`&nbsp;: Debian et Ubuntu&nbsp;;

* paquets au format `.tgz` et `.txz`&nbsp;: Slackware&nbsp;;

* etc.

Cette liste est loin d'être exhaustive, étant donné que la communauté Linux
respecte la *Grande Charte Anarchiste du Troupeau de Chats* et développe
régulièrement de nouvelles distributions avec des gestionnaires de paquets
encore plus révolutionnaires et surtout incompatibles entre eux.

Chaque format de paquets dispose de son propre gestionnaire à la base, ce qui
signifie que vous pourrez utiliser RPM pour gérer les paquets de n'importe
laquelle des distributions basée sur ce format, du moins manuellement. Les
différences se situeront au niveau des frontaux plus confortables&nbsp;:

* Oracle Linux, Red Hat Enterprise Linux et CentOS utilisent Yum.

* Fedora est gérée par DNF.

* SUSE et OpenSUSE utilisent Zypper en plus de RPM.

Les distributions de la famille Debian et dérivées gèrent leurs paquets au
format `.deb` avec l'utilitaire `dpkg` et des frontaux en ligne de commande
puissants et flexibles comme APT et Aptitude.

Ce foisonnement d'outils a de quoi décourager les utilisateurs nouveaux venus.
Mon conseil&nbsp;: n'essayez pas d'apprendre trop de choses à la fois. Explorez
Yum et RPM et les possibilités qu'ils offrent. Une fois que vous aurez saisi
leurs principes de fonctionnement et que vous les manierez avec le sourire,
l'apprentissage d'un autre gestionnaire de paquets comme APT ne vous fera plus
peur. Le fonctionnement de base est exactement le même&nbsp;; ce n'est que la
syntaxe des commandes qui change.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


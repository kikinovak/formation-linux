
## Une formation Linux pour les admins en herbe

Cette formation s'adresse à tous les administrateurs en herbe qui veulent
**installer, configurer et gérer des serveurs sous Linux**. Aucun savoir,
aucune compétence spécifique n'est présupposée de votre côté. Si vous êtes doté
d'une dose saine de curiosité et d'un certain plaisir à expérimenter,
considérez-vous comme le public idéal. 

Ce support de cours a été élaboré et peaufiné dans le cadre des cours
d'introduction à l'administration des systèmes Linux que je dispense entre
Nîmes, Alès et Montpellier. Il est **le fruit de quelques années de terrain**,
qui a mûri lentement sous le soleil de la garrigue gardoise, à la lumière des
nombreuses questions de mes stagiaires. 

L'approche de la formation est résolument pragmatique&nbsp;: **vous faire
découvrir les bases de Linux en plongeant les mains dans le cambouis, sans
prérequis externes, en suivant une progression pédagogique cohérente**. Elle se
concentre sur l'administration de base&nbsp;: l'installation, les bases du
shell et de la ligne de commande, la gestion des utilisateurs, les droits
d'accès, les processus, la gestion des disques et des applications, etc. 

## La version papier

Depuis 2009, j'ai publié une [série d'ouvrages spécialisés sur
Linux](https://www.editions-eyrolles.com/Auteur/94948/kiki-novak) chez
l'éditeur Eyrolles, sous le nom de plume *Kiki Novak*. Cette formation en ligne
reprend le contenu de quelques-uns de ces titres, avec quelques différences
cependant.

* Certains chapitres sont un peu plus détaillés dans les livres, alors que le
  cours en ligne va *droit au but*. 

* Le livre constitue en quelque sorte une version stable et figée dans le temps
  de la formation.

* En contrepartie, les cours en ligne sont en perpétuelle évolution et peuvent
  être considérés comme la version de développement du prochain ouvrage
  imprimé. 

## Le formateur

[Nicolas Kovacs](https://www.microlinux.fr/#contact) est le fondateur de
[Microlinux](https://www.microlinux.fr), une petite entreprise informatique
spécialisée dans Linux et l'Open Source.  Il est également
[traducteur](https://tinyurl.com/2p8tk495) du [projet de
documentation](https://learning.lpi.org) du *Linux Professional Institute*.
Lorsqu'il n'est pas en train d'administrer des systèmes Linux, de former de
futurs administrateurs Linux ou de réfléchir à voix haute sur son [blog
technique](https://blog.microlinux.fr), il se remet les neurones en place en
allant grimper sur les belles falaises du Gard et de l'Hérault.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

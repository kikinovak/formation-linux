
**Objectif** : définir qui a accès à quoi dans un système Linux.

## Caractéristiques d'un système multi-utilisateurs

Linux est un vrai système multi-utilisateurs, tout comme son ancêtre Unix. Pour
comprendre la portée de cette assertion, imaginez un poste de travail comme on
peut en trouver dans la salle informatique d'une grande université, fréquentée
par une bonne dizaine de milliers d'étudiants. Chaque étudiant inscrit a le
droit d'utiliser les machines de la salle informatique. Il possède donc son
identifiant personnel et son mot de passe, qui lui permettent de se connecter à
une machine de la salle informatique pour y travailler, c'est-à-dire effectuer
ses recherches, écrire ses devoirs, rédiger son mémoire ou sa thèse, etc. Une
telle installation doit répondre à quelques exigences.

* Chaque utilisateur du système doit disposer de son répertoire personnel,
  c'est-à-dire d'un endroit pour lui seul, utilisable par lui seul, où il peut
  stocker toutes ses données.

* La confidentialité doit être assurée, c'est-à-dire qu'un étudiant connecté ne
  pourra pas aller fouiner librement dans les données de ses collègues.

* Il ne faut pas non plus qu'un utilisateur puisse effacer par mégarde (ou même
  intentionnellement) les données qui ne lui appartiennent pas.

* Enfin, l'intégrité du système ne doit en aucun cas être mise en péril par les
  utilisateurs. 

Notre configuration de test sera beaucoup plus modeste qu'une série de postes
de travail dans la salle informatique d'une université. Il n'empêche que
l'approche multi-utilisateurs est tout aussi pertinente, même pour un usage sur
une machine locale. Après tout, peu importe si le système gère deux ou trois
utilisateurs ou vingt-cinq mille. 

> Techniquement parlant, une telle installation dans une université se
> différencie d'une installation domestique d'un poste de travail par la
> configuration itinérante des profils d'utilisateurs. Dans une telle
> configuration, l'ensemble des données, les identifiants de connexion et les
> mots de passe sont stockés de façon centralisée sur le serveur. À partir de
> là, chaque étudiant peut se connecter sur n'importe quelle machine de la
> salle informatique et retrouver son environnement, alors que sur une
> installation comme un poste de travail domestique, chaque compte
> d'utilisateur reste lié à la machine locale. Il existe plusieurs manières de
> mettre en place les profils itinérants dans un réseau local, et nous les
> aborderons en temps et en heure.  Pour l'instant, nous nous concentrons sur
> la gestion locale des utilisateurs sur une seule machine tournant sous Linux.

## Ajouter de nouveaux utilisateurs : useradd

Lors de la configuration post-installation, j'ai défini un premier utilisateur
du "commun des mortels" pour mon système. Cela signifie que ma machine connaît
déjà deux comptes&nbsp;:

* l'administrateur `root`

* l'utilisateur en question (`microlinux`)

En dehors de mon utilisateur initial, je vais créer quelques comptes
supplémentaires&nbsp;:

* Agnès Debuf (`adebuf`)

* Jean Mortreux (`jmortreux`)

* Fanny Banester (`fbanester`)

* Franck Teyssier (`fteyssier`)

Chacun des utilisateurs sera créé à l'aide de la commande `useradd`.
L'invocation de cette commande requiert des droits d'administrateur. Dans un
premier temps, nous allons acquérir ces droits de façon peu élégante, en nous
déconnectant et en nous reconnectant en tant que `root`.

Je lance la création de ma première utilisatrice&nbsp;:

```
# useradd -c "Agnès Debuf" adebuf
# passwd adebuf
Changing password for user adebuf.
New password: ********
Retype new password: ********
passwd: all authentication tokens updated successfully.
```

Un coup d'oeil rapide dans la page `man` de `useradd` nous renseigne sur la
signification exacte de l'option `-c`, que vous avez probablement devinée dans
le contexte.

```
-c, --comment COMMENT
    Any text string. It is generally a short description of the login, and is
    currently used as the field for the user's full name.
```

Notez qu'il y a une vérification sur la robustesse du mot de passe défini. Pour
un utilisateur quelconque, le système refuserait tout simplement de créer le
mot de passe s'il est faible (puisque la commande `passwd` peut être invoquée
par un utilisateur pour changer son propre mot de passe). En revanche, `root` a
tous les droits, y compris celui ce forcer l'utilisation d'un mot de passe trop
simple (ce que je vous déconseille néanmoins).

```
# useradd -c "Jean Mortreux" jmortreux
# passwd jmortreux
Changing password for user jmortreux.
New password: ****
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: ****
passwd: all authentication tokens updated successfully.
```

Procédez de même pour créer les autres utilisateurs de la machine.

## Utiliser n'est pas administrer

Tout au long de notre initiation à la ligne de commande, nous avons travaillé
en tant que simples utilisateurs – à quelques rares exceptions près – pour
créer, éditer, visualiser, déplacer, copier et effacer des fichiers. Ces tâches
ne mettaient pas en péril le fonctionnement du système ou les données des
autres utilisateurs et ne nécessitaient par conséquent aucun privilège
spécifique. Il n'en est plus de même pour la gestion des utilisateurs, qui
comprend entre autres choses&nbsp;:

* la création d'un nouvel utilisateur&nbsp;;

* la définition de son mot de passe&nbsp;;

* la configuration de ses droits&nbsp;: à quoi aura-t-il accès dans le
  système&nbsp;?

* la suppression éventuelle de l'utilisateur ainsi que de toutes ses données.

## Changer d'identité et devenir root

Lors de l'installation du système, nous avons défini un mot de passe pour
l'utilisateur `root`. Un peu plus haut, nous avons eu besoin des privilèges de
`root` pour créer quelques utilisateurs supplémentaires, que nous avons acquis
en nous déconnectant et en nous reconnectant. Or, il existe un moyen bien plus
simple grâce à la commande `su` (*switch user*, c'est-à-dire "changer
d'utilisateur"). Tapez ce qui suit, en saisissant le mot de passe `root`
lorsque le système vous le demande.

```
[microlinux@linuxbox ~]$ su -
Password: ********
Last login: Mon May 17 08:05:24 CEST 2021 on pts/0
[root@linuxbox ~]#
```

Notez le tiret `-` qui suit la commande `su`. Il précise qu'il faut devenir
`root` en récupérant toutes les variables d'environnement de ce compte. Nous y
reviendrons. Contentez-vous pour l'instant de connaître la démarche.

> Une mise en garde solennelle s'impose. En acquérant les droits de `root`,
> vous voilà en quelque sorte détenteur du fameux bouton rouge. Cela ne veut
> pas dire que vous allez forcément déclencher une guerre nucléaire, mais une
> simple commande bien sentie suffirait à enclencher une apocalypse numérique
> sur votre système. En un mot&nbsp;: prudence. Et gare aux fautes de frappe.  

S'il est utile de savoir comment acquérir les pleins pouvoirs sur la machine,
il est tout aussi indispensable de savoir comment revenir en sens inverse pour
se débarrasser de tous ces super-pouvoirs lorsqu'on n'en a plus besoin. Dans ce
cas, c'est exactement la même commande que pour quitter une session dans la
console. Vous avez donc le choix entre les deux commandes `logout` et `exit`, à
moins que vous ne préfériez le raccourci clavier ++ctrl+d++.

```
[root@linuxbox ~]# exit
logout
[microlinux@linuxbox ~]$ 
```

## Savoir qui l'on est

La commande `su` ne nous permet pas seulement de devenir `root`. Si le système
dispose d'un utilisateur `fteyssier`, je pourrais très bien devenir `fteyssier`
en invoquant la commande suivante (et en saisissant son mot de passe)&nbsp;:

```
[microlinux@linuxbox ~]$ su - fteyssier
Password: ******** 
[fteyssier@linuxbox ~]$ 
```

Là encore, notez l'utilisation du tiret `-` pour indiquer que vous souhaitez
devenir un autre utilisateur en utilisant ses variables d'environnement.
L'invite de commandes (`[fteyssier@linuxbox ~]$`) nous indique qu'un changement
d'identité a eu lieu. Pour le vérifier, nous avons tout loisir de demander à
notre système qui nous sommes, grâce à la commande `whoami` ("*Who am
I&nbsp;?*", "Qui suis-je&nbsp;?"). Voici une petite démonstration
pratique&nbsp;:

```
[microlinux@linuxbox ~]$ su - fteyssier
Password: ********
[fteyssier@linuxbox ~]$ whoami
fteyssier
[fteyssier@linuxbox ~]$ exit
logout
[microlinux@linuxbox ~]$ whoami
microlinux
[microlinux@linuxbox ~]$ su -
Password: ********
[root@linuxbox ~]# whoami
root
[root@linuxbox ~]# exit
logout
[microlinux@linuxbox ~]$ whoami
microlinux
```

Vous remarquerez que si j'invoque `su` sans autre argument que le tiret `-`,
cela revient exactement à la même chose que `su - root`.

```
[microlinux@linuxbox ~]$ su -
Password: ******** 
[root@linuxbox ~]# 
```

## En savoir un peu plus sur les utilisateurs : id, groups, finger

Chacun des utilisateurs que nous avons créés jusqu'ici possède un certain
nombre de caractéristiques, comme son UID unique, son GID, les groupes
secondaires auxquels il appartient, son répertoire d'utilisateur, son *shell*
de connexion, etc. Voyons maintenant comment afficher ces différentes
informations. Commençons par nous-mêmes, en utilisant la commande `id`&nbsp;:

```
$ id
uid=1000(microlinux) gid=1000(microlinux) groups=1000(microlinux),10(wheel) 
context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

Invoquée sans autre argument, la commande `id` nous affiche l'UID, le GID,
ainsi que la liste complète des groupes secondaires auxquels l'utilisateur est
affecté. Elle nous affiche également le contexte SELinux (*Security Enhanced
Linux*), que nous allons laisser de côté pour l'instant. SELinux est une
technologie quelque peu complexe, et nous l'aborderons (beaucoup) plus loin.

Afficher l'UID (*User Identification*) de l'utilisateur&nbsp;:

```
$ id -u
1000
```

Afficher le GID (*Group Identification*)&nbsp;:

```
$ id -g
1000
```

Afficher le nom du groupe&nbsp;:

```
$ id -gn
microlinux
```

Afficher les groupes dont l'utilisateur est membre&nbsp;:

```
$ id -G
1000 10
```

Afficher les noms des groupes dont l'utilisateur est membre&nbsp;:

```
$ id -Gn
microlinux wheel
```

Pour cette dernière commande, nous disposons d'une alternative plus
courante&nbsp;:

```
$ groups
microlinux wheel
```

Évidemment, personne ne vous demande de retenir toutes ces options par coeur.
N'oubliez pas que vous avez la page du manuel pour cela&nbsp;:

```
$ man id
```

Pour en savoir plus sur les autres utilisateurs du système, il suffit de
fournir leur nom en argument. Ces informations sont accessibles à tous les
utilisateurs non privilégiés du système.

```
$ id adebuf
uid=1001(adebuf) gid=1001(adebuf) groups=1001(adebuf)
$ id jmortreux
uid=1002(jmortreux) gid=1002(jmortreux) groups=1002(jmortreux)
```

Les arguments et les options peuvent évidemment être combinés à souhait, par
exemple pour afficher l'UID d'un autre utilisateur.

```
$ id -u fteyssier
1004
```

Enfin, la commande `finger` permet d'afficher quelques renseignements sur les
utilisateurs du système comme le nom, le répertoire utilisateur et le shell de
connexion utilisé. Elle ne fait pas partie du système minimal, mais nous
pouvons l'installer facilement.

```
# yum install -y finger
```

Une fois installée, la commande `finger` affiche ces informations&nbsp;:

```
$ finger fteyssier
Login: fteyssier                        Name: Franck Teyssier
Directory: /home/fteyssier              Shell: /bin/bash
Last login Mon May 17 09:00 (CEST) on pts/0
...
```

## La gestion des utilisateurs sous le capot

### Comprendre le fichier /etc/passwd

Essayons de voir un peu plus en détail comment se passe la gestion des
utilisateurs au niveau du système. Affichez le contenu du fichier
`/etc/passwd`&nbsp;:

```
$ less /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
...
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
chrony:x:997:995::/var/lib/chrony:/sbin/nologin
microlinux:x:1000:1000:microlinux:/home/microlinux:/bin/bash
adebuf:x:1001:1001:Agnès Debuf:/home/adebuf:/bin/bash
jmortreux:x:1002:1002:Jean Mortreux:/home/jmortreux:/bin/bash
fbanester:x:1003:1003:Fanny Banester:/home/fbanester:/bin/bash
fteyssier:x:1004:1004:Franck Teyssier:/home/fteyssier:/bin/bash
```

> Certains d'entre vous seront peut-être vaguement surpris voire inquiets de
> pouvoir lire ce fichier sans autres privilèges. C'est tout à fait normal et
> nous y viendrons.

Le fichier `/etc/passwd` contient l'ensemble des informations qui régissent la
connexion des utilisateurs. Chaque ligne de ce fichier correspond à un
utilisateur. Essayez de repérer l'entrée qui correspond à votre humble
personne&nbsp;:

```
microlinux:x:1000:1000:Microlinux:/home/microlinux:/bin/bash
```

Comment décrypter ce charabia&nbsp;? Il s'agit en fait d'une série de champs
séparés par deux-points, où l'on trouve dans l'ordre...&nbsp;:

* l'identifiant de connexion (`microlinux`)&nbsp;;

* la lettre `x`, signifiant que le mot de passe chiffré de l'utilisateur se
  situe dans le fichier `/etc/shadow`&nbsp;;

* l'UID (*User Identification*, ici `1000`), que le système utilise plutôt que
  votre identifiant pour gérer les droits d'accès de vos fichiers&nbsp;;

* le GID (*Group Identification*, également `1000` ici), groupe primaire auquel
  appartient l'utilisateur&nbsp;;

* le nom complet de l'utilisateur (`Microlinux`)&nbsp;;

* le répertoire de connexion (`/home/microlinux`)&nbsp;;

* le *shell* de connexion de l'utilisateur (`/bin/bash`).

> Pour être précis, le shell de connexion est la commande que le système doit
> exécuter lorsque l'utilisateur se connecte. En pratique, il s'agit de
> l'interpréteur de commandes de l'utilisateur.  

### Les utilisateurs système

Qui sont donc tous ces utilisateurs mystérieux sur votre système&nbsp;? Vous
n'avez pas défini ces gens aux identifiants pour le moins curieux&nbsp;:
`daemon`, `operator`, `nobody`... Rassurez-vous, votre machine n'est peuplée ni
par des démons, ni par des hommes invisibles. Il s'agit là des utilisateurs
système.

À titre d'exemple, si vous installez le serveur web Apache (`yum install
httpd`), l'installation crée un utilisateur système `apache`. Lorsque le
serveur Apache est lancé, le processus "n'appartient" pas à l'utilisateur
`root`, mais à l'utilisateur système `apache`. Il peut arriver (et dans le
monde réel, cela arrive effectivement) qu'une personne malintentionnée décide
d'attaquer le serveur, en exploitant une faille de sécurité. Or, si le serveur
fonctionnait avec des droits `root`, cela rendrait l'attaquant tout-puissant
sur la machine.  Le recours à un utilisateur système permet donc de limiter les
dégâts dans un tel cas de figure. Je vous épargne les détails complexes d'une
telle situation.  Retenez juste que l'existence des utilisateurs système est
avant tout motivée par des considérations de sécurité.

Dorénavant, nous pouvons établir une classification sommaire des utilisateurs
sur notre machine&nbsp;:

* L'administrateur `root`, l'utilisateur tout-puissant. Son UID est toujours
  égal à `0`.

* Les utilisateurs système, gérant les droits d'accès d'un certain nombre de
  services sur la machine. Leur UID est compris entre `1` et `999`.

* Les utilisateurs "normaux", c'est-à-dire les personnes physiques comme vous
  et moi (`microlinux`, `adebuf`, `jmortreux`). Notre UID sera supérieur ou
  égal à `1000`.

### Comprendre le fichier /etc/shadow

Dans le bon vieux temps, le fichier `/etc/passwd` contenait également les mots de
passe des utilisateurs. Ils étaient certes chiffrés, mais l'algorithme de
chiffrement utilisé à l'époque n'était pas très performant. Avec l'avènement
des processeurs de plus en plus puissants, un attaquant avait la perspective de
décrypter les mots de passe en un temps raisonnable. 

Pour cette raison, les mots de passe ont été délocalisés vers le fichier
`/etc/shadow`. Contrairement à `/etc/passwd`, il n'est pas accessible à tout le
monde. 

```
$ cat /etc/shadow
cat: /etc/shadow: Permission denied
```

Les permissions de ce fichier sont beaucoup plus restrictives. Seul `root` a le
droit d'en afficher le contenu&nbsp;:

```
# cat /etc/shadow
root:$6$nYUyqivY6aOSet2AgoLVSmGsxP5.oSFwkpb9z/1t8ZuKSn1pwbrF/::0:99999:7:::
bin:*:18222:0:99999:7:::
daemon:*:18222:0:99999:7:::
adm:*:18222:0:99999:7:::
lp:*:18222:0:99999:7:::
sync:*:18222:0:99999:7:::
shutdown:*:18222:0:99999:7:::
halt:*:18222:0:99999:7:::
mail:*:18222:0:99999:7:::
operator:*:18222:0:99999:7:::
games:*:18222:0:99999:7:::
ftp:*:18222:0:99999:7:::
nobody:*:18222:0:99999:7:::
systemd-network:!!:18762::::::
dbus:!!:18762::::::
polkitd:!!:18762::::::
sshd:!!:18762::::::
postfix:!!:18762::::::
chrony:!!:18762::::::
microlinux:$6$yF/uUl.WgLE8Nz61Q3OzEgUqTJg73hRxQZ3cG1rvJSrQc0HDo//W0::0:99999:7:::
adebuf:$6$ZmTxgZkH$FuYo6Y6CvRZvEPX03iZkk9FfLdFA/uTdrgu/:18764:0:99999:7:::
jmortreux:$6$gVvKCj/qAW6Urc4.HNLS6qART51fOFVy6g/ESzCU2Dk0.:18764:0:99999:7:::
fbanester:$6$jyI.D3ECe2zLeNl5OK6BPPOEML9ozb5s23BVYg5HePLY/:18764:0:99999:7:::
fteyssier:$6$j5GhStPf/DXRS5hSgS1SsSeGyQ6tE3MKg6PfWHsqnlG5.:18764:0:99999:7:::
```

À première vue, la syntaxe de ce fichier ressemble de très loin à celle que
nous avons pu trouver dans `/etc/passwd`&nbsp;: 

```
jmortreux:$6$gVvKCj/qAW6Urc4.HNLS6qART51fOFVy6g/ESzCU2Dk0.:18764:0:99999:7:::
```

Là aussi, nous disposons d'une série d'informations séparées par
deux-points&nbsp;:

* l'identifiant de connexion (`microlinux`)&nbsp;;

* le mot de passe chiffré (`$6$gVvKCj/qAW6U...zCU2Dk0.`)&nbsp;;

* la date du dernier changement de mot de passe (`18764`)&nbsp;;

* l'âge minimum du mot de passe (`0`)&nbsp;;

* l'âge maximum du mot de passe (`99999`)&nbsp;;

* la période d'expiration du mot de passe (`7`)&nbsp;;

* etc.

La syntaxe du fichier mérite quelques remarques.

* Chacun des identifiants de connexion (`microlinux`, `adebuf`, `jmortreux`,
  etc.) est également présent dans `/etc/passwd`. Ce champ peut donc être
  considéré comme un connecteur entre les deux fichiers.

* Les mots de passe chiffrés commencent tous par `$6$`, ce qui signifie que
  l'algorithme de hachage SHA-512 a été utilisé. 

* Au cas où deux utilisateurs ont le même mot de passe, les empreintes
  correspondantes dans `/etc/shadow` seront différentes grâce au salage.

* La date de dernière modification du mot de passe (`18764`) est indiquée en
  nombre de jours depuis le 1er janvier 1970.

> Le salage est une méthode utilisée en cryptographie. Elle permet de renforcer
> la sécurité des informations destinées à être hachées en ajoutant une donnée
> supplémentaire. Cette technique permet d'éviter que deux informations
> identiques conduisent à la même empreinte.  

Les pages de manuel en ligne du système ne nous renseignent pas seulement sur
le rôle et la syntaxe des commandes. Les fichiers de configuration du système y
sont également décrits en détail.

```
$ apropos shadow
gpasswd (1)          - administer /etc/group and /etc/gshadow
gshadow (5)          - shadowed group file
...
shadow (5)           - shadowed password file
...
$ man 5 shadow
```

### Trouver les utilisateurs physiques du système

Admettons que nous voulions afficher tous les vrais utilisateurs, c'est-à-dire
tous ceux qui ne sont pas des utilisateurs système. Comment nous y
prendrions-nous&nbsp;?

Une première approche consisterait à considérer que les vrais utilisateurs
disposent tous d'un *shell* de connexion, en l'occurrence `/bin/bash`. Il
suffirait donc d'afficher toutes les lignes du fichier `/etc/passwd` qui
contiennent la chaîne de caractères `/bin/bash` ou, plus simplement, `bash`.
C'est tout à fait possible. J'en profite pour vous présenter la commande
`grep`.

```
$ grep bash /etc/passwd
root:x:0:0:root:/root:/bin/bash
microlinux:x:1000:1000:Microlinux:/home/microlinux:/bin/bash
adebuf:x:1001:1001:Agnès Debuf:/home/adebuf:/bin/bash
jmortreux:x:1002:1002:Jean Mortreux:/home/jmortreux:/bin/bash
fbanester:x:1003:1003:Fanny Banester:/home/fbanester:/bin/bash
fteyssier:x:1004:1004:Franck Teyssier:/home/fteyssier:/bin/bash
```

L'opération ressemble à un succès. Même si `root` semble être un cas à part,
les utilisateurs en chair et en os sont tous là. Or, notre approche souffre
d'un certain nombre de points faibles. Si l'un de nos utilisateurs décide de
choisir un autre shell de connexion que `/bin/bash` (ce qui est tout à fait
possible), il ne s'affichera plus. Essayons donc une approche différente.

Nous avons vu plus haut que ce qui distingue les utilisateurs "en chair et en
os", c'est leur UID supérieur ou égal à `1000`. Nous avons vu également que le
fichier `/etc/passwd` était organisé en colonnes séparées par des deux-points.
Je vais me servir de l'outil de filtrage `awk` pour arriver à mes fins.  GNU
AWK est un véritable langage de traitement de lignes qui sert à manipuler des
fichiers textes. Voyons quelques exemples simples.

La première colonne du fichier `/etc/passwd` contient les noms d'utilisateurs.

```
$ awk -F: '{print $1}' /etc/passwd
root
bin
daemon
...
microlinux
adebuf
jmortreux
fbanester
fteyssier
```

L'option `-F` indique à `awk` que le fichier `/etc/passwd` utilise les
deux-points comme séparateur, et `'{print $1}'` signifie "affiche la première
colonne".

Les UID des utilisateurs figurent dans la troisième colonne. Je peux donc les
"extraire" de la sorte&nbsp;:

```
$ awk -F: '{print $3}' /etc/passwd
0
1
2
3
...
1000
1001
1002
1003
1004
```

À partir de là, j'ai la réponse à ma question. Il suffit que j'affiche la
première colonne (`$1`) de chaque ligne où le contenu de la troisième colonne
(`$3`) est strictement supérieur à `999`&nbsp;:

```
$ awk -F: '$3 > 999 {print $1}' /etc/passwd
microlinux
adebuf
jmortreux
fbanester
fteyssier
```

Enfin, je peux combiner la commande précédente avec `sort` pour afficher le
résultat par ordre alphabétique&nbsp;:

```
$ awk -F: '$3 > 999 {print $1}' /etc/passwd | sort
adebuf
fbanester
fteyssier
jmortreux
microlinux
```

### Vue détaillée sur l'ajout d'un utilisateur 

La commande `useradd` telle qu'on la trouve dans un système Oracle Linux
présente un comportement par défaut qui la rend immédiatement utilisable, sans
que l'on ait trop à se casser la tête sur les différentes options à spécifier.
En résumé, que se passe-t-il lors de la création simple d'un utilisateur sans
autre option, c'est-à-dire en invoquant la commande `useradd
<identifiant>`&nbsp;?

* L'utilisateur reçoit un UID supérieur ou égal à `1000`. Tout laisse à penser
  (selon le contenu de `/etc/passwd`) que le système choisit le premier UID
  disponible à partir de `1000`. 

* Tous les utilisateurs sont affectés d'emblée à un groupe nommé d'après leur
  identifiant, dont le GID est égal à l'UID. Dans notre exemple, `jmortreux`
  (UID `1002`) est également membre du groupe `jmortreux` (GID `1002`).

* Un répertoire d'utilisateur est créé, et c'est l'identifiant de l'utilisateur
  qui servira de base pour le nommer. L'utilisatrice `adebuf` aura donc un
  répertoire `/home/adebuf`.

* Par défaut, c'est l'interpréteur de commandes Bash (`/bin/bash`) qui sera
  utilisé.

> Vous vous doutez probablement que les options par défaut de la commande
> `useradd` ne sont pas gravées dans le marbre. Elles sont fournies par les
> fichiers de configuration `/etc/login.defs` et `/etc/default/useradd`.

### Les données GECOS des utilisateurs

Nous avons utilisé l'option `-c` (ou `--comment`) pour créer les utilisateurs
sur notre système, en fournissant le prénom et le nom correspondant en
argument. Ces données se retrouvent dans le cinquième champ du fichier
`/etc/passwd`, entre le GID et le répertoire utilisateur. La page de manuel en
ligne de ce fichier (`man 5 passwd`) nous dit qu'il s'agit du champ GECOS. 

> GECOS signifie *General Electric Comprehensive Operating System*, du nom d'un
> système d'exploitation *mainframe* créé originellement par la *General
> Electric*.

Le champ GECOS est une relique du bon vieux temps où les utilisateurs d'Unix
dans les entreprises et dans les universités se connectaient à un ordinateur
central par le biais d'un terminal. Il permet d'enregistrer des informations
sommaires sur un compte utilisateur. La commande `finger` permet d'afficher ces
informations.

Les informations GECOS sont accessibles à tous les utilisateurs&nbsp;:

```
$ finger fteyssier
Login: fteyssier                        Name: Franck Teyssier
Directory: /home/fteyssier              Shell: /bin/bash
Last login Mon May 17 09:00 (CEST) on pts/0
...
```

La commande `chfn` (*change your finger information* d'après la page `man`)
permet de modifier les informations GECOS, notamment&nbsp;:

* le nom en toutes lettres&nbsp;;

* le bureau dans le bâtiment&nbsp;;

* le numéro de téléphone professionnel&nbsp;;

* le numéro de téléphone personnel.

Concrètement, voici à quoi cela ressemble :

```
[microlinux@linuxbox ~]$ chfn
Changing finger information for microlinux.
Name [Microlinux]:
Office []: 101
Office Phone []: 04 66 63 10 32
Home Phone []: 06 51 80 12 12

Password: ********
Finger information changed.
```

Ces informations sont désormais accessibles à tout le monde :

```
$ finger microlinux
Login: microlinux             Name: Microlinux
Directory: /home/microlinux   Shell: /bin/bash
Office: 101, 04 66 63 10 32   Home Phone: 06 51 80 12 12
...
```

Sous le capot, elles sont stockées dans le cinquième champ du fichier
`/etc/passwd` et utilisent la virgule comme séparateur&nbsp;:

```
microlinux:x:1000:1000:Microlinux,101,04 66 63 10 32,
  06 51 80 12 12:/home/microlinux:/bin/bash
```

> De nos jours, on ne renseigne guère plus que le nom des utilisateurs dans ce
> champ. Ceci étant dit, c'est bien de connaître son rôle historique. 

### Le profil par défaut des nouveaux utilisateurs

Parmi les utilisateurs nouvellement créés, prenons-en un qui ne s'est pas
encore connecté au système&nbsp;:

```
# finger fteyssier
Login: fteyssier            Name: Franck Teyssier
Directory: /home/fteyssier  Shell: /bin/bash
Never logged in.
...
```

Au moment de la création de son compte, son répertoire d'utilisateur se trouve
apparemment vide&nbsp;:

```
# tree /home/fteyssier/
/home/fteyssier/

0 directories, 0 files
```

En fait, il n'est pas si vide que cela, si nous regardons d'un peu plus
près&nbsp;:

```
# tree -a /home/fteyssier/
/home/fteyssier/
├── .bash_logout
├── .bash_profile
└── .bashrc

0 directories, 3 files
```

D'où sortent ces mystérieux fichiers `.bash_logout`, `.bash_profile` et
`.bashrc`&nbsp;?  Jetons un oeil dans la page `man` de `useradd`, dans la
section `OPTIONS`&nbsp;:

```
-k, --skel SKEL_DIR
    The skeleton directory, which contains files and directories to be copied
    in the user's home directory, when the home directory is created by
    useradd.

    This option is only valid if the -m (or --create-home) option is specified.

    If this option is not set, the skeleton directory is defined by the SKEL
    variable in /etc/default/useradd or, by default, /etc/skel.
    ...
```

Vérifions la définition de la variable `SKEL` dans le fichier
`/etc/default/useradd`&nbsp;:

```
# grep SKEL /etc/default/useradd 
SKEL=/etc/skel
```

Le répertoire squelette de notre système, c'est bien `/etc/skel`. Affichons le
contenu de ce répertoire en prenant soin d'afficher les fichiers cachés&nbsp;:

```
# tree -a /etc/skel/
/etc/skel/
├── .bash_logout
├── .bash_profile
└── .bashrc

0 directories, 3 files
```

Cela ressemble effectivement au contenu de mon répertoire d'utilisateur
nouvellement créé. Pour en avoir le coeur net, tentons une petite expérience.

### Personnaliser le profil par défaut des nouveaux utilisateurs

Nous pourrions très bien imaginer la confection d'un fichier de bienvenue pour
les nouveaux utilisateurs de notre système. Dans un premier temps, nous créons
le message dans `/etc/skel`&nbsp;:

```
# cat > /etc/skel/LISEZ_MOI.txt << EOF
> Bienvenue sur votre nouveau système Linux !
> EOF
```

> Notez que vous pourriez très bien vous servir de Vi pour éditer votre message.

À présent, créons un nouvel utilisateur `fantasio`&nbsp;:

```
# useradd -c "Fantasio" fantasio
# passwd fantasio
```

Avant même qu'il ne se connecte pour la première fois, penchons-nous sur le
contenu de son répertoire d'utilisateur&nbsp;:

```
# ls -la /home/fantasio/
total 20
drwx------. 2 fantasio fantasio   83 May 17 10:34 .
drwxr-xr-x. 8 root     root     4096 May 17 10:34 ..
-rw-r--r--. 1 fantasio fantasio   18 Nov 22  2019 .bash_logout
-rw-r--r--. 1 fantasio fantasio  193 Nov 22  2019 .bash_profile
-rw-r--r--. 1 fantasio fantasio  231 Nov 22  2019 .bashrc
-rw-r--r--. 1 fantasio fantasio   45 May 17 10:34 LISEZ_MOI.txt
```

L'utilisation de `/etc/skel` ne se limite pas à l'ajout de documents par
défaut&nbsp;; de manière plus générale, tout fichier peut être distribué aux
nouveaux utilisateurs. On peut donc également répliquer des configurations par
défaut du bureau ou des logiciels et définir par exemple le même fond d'écran
et le même thème d'icônes par défaut pour tous les utilisateurs. Il suffit pour
cela de repérer le fichier (ou répertoire) de configuration en question et de
le recopier dans `/etc/skel` avant la création des utilisateurs.  

## Modifier le mot de passe d'un utilisateur

Nous avons vu que la création d'un nouveau compte comprend l'affectation d'un
mot de passe avec la commande `passwd`. Celle-ci n'est d'ailleurs pas réservée à
l'administrateur. Appelée par un simple utilisateur, elle lui servira à changer
son mot de passe pour quelque chose de plus personnalisé&nbsp;:

```
[fantasio@linuxbox ~]$ passwd
Changing password for user fantasio.
Changing password for fantasio.
(current) UNIX password:
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

Il arrive assez souvent que les utilisateurs choisissent des mots de passe trop
simples. Dans ce cas, le système les en empêchera et se chargera de les
sermonner&nbsp;:

```
[fantasio@linuxbox ~]$ passwd
Changing password for user fantasio.
Changing password for fantasio.
(current) UNIX password:
New password:
BAD PASSWORD: The password is shorter than 8 characters
New password:
BAD PASSWORD: The password is shorter than 8 characters
New password:
BAD PASSWORD: The password is shorter than 8 characters
passwd: Have exhausted maximum number of retries for service
```

Notez que cette restriction ne vaut pas pour l'administrateur `root`. Autrement
dit, si vous avez la mauvaise idée de définir un mot de passe simpliste comme
`1234` ou `azerty` pour l'utilisateur `fantasio`, vous devrez le faire comme
ceci, en insistant un peu&nbsp;:

```
# passwd fantasio
```

## Associer les utilisateurs aux groupes

La commande `groups` présentée un peu plus haut nous a affiché le ou les
groupes auxquels appartient l'utilisateur. Vous pouvez vous représenter cette
appartenance à un groupe comme une carte de membre, qui vous ouvre les portes
vers des lieux dont l'accès est normalement restreint. 

Sur un système Oracle Linux, chaque utilisateur nouvellement créé fait partie
au minimum d'un groupe nommé d'après son identifiant&nbsp;:

```
$ groups jmortreux
jmortreux : jmortreux
```

Les utilisateurs membres de certains groupes bénéficient de toute une série de
"privilèges", qui leur donnent accès à certaines parties du système&nbsp;:

```
$ groups microlinux
microlinux : microlinux wheel
```

Comment se fait-il que l'utilisateur `microlinux` soit membre du groupe
`wheel`&nbsp;?  Rappelez-vous qu'il a été créé lors de l'installation initiale.
Sur un système Oracle Linux, le groupe `wheel` sert avant tout à différencier
les utilisateurs qui ont le droit de se servir de la commande `sudo` (nous y
viendrons). Si nous avons coché la case `Faire de cet utilisateur un
administrateur`, nous avons ajouté notre utilisateur initial à ce groupe
privilégié.

Admettons que je souhaite élever l'utilisateur `jmortreux` au rang
d'administrateur. Je peux l'ajouter au groupe `wheel` comme ceci&nbsp;:

```
# usermod -aG wheel jmortreux
```

> Si `jmortreux` est connecté pendant cette opération, il faudra qu'il se
> déconnecte et qu'il se reconnecte pour que l'ajout au groupe `wheel` prenne
> effet.  

N'hésitez pas à ouvrir la page `man 8 usermod` pour avoir une idée un peu plus
précise de ce que nous venons de faire.

* `usermod` modifie un compte utilisateur.

* L'option `-a` (comme `--append`) ajoute l'utilisateur à un groupe
  supplémentaire.

* Elle s'utilise conjointement avec l'option `-G` (`--groups`).

### Comprendre le fichier /etc/group

Maintenant que vous êtes familiarisé avec le fonctionnement des fichiers
`/etc/passwd` et `/etc/shadow`, affichez le fichier `/etc/group`&nbsp;:

```
# less /etc/group
root:x:0:
bin:x:1:
daemon:x:2:
sys:x:3:
...
wheel:x:10:microlinux,jmortreux
cdrom:x:11:
mail:x:12:postfix
...
postdrop:x:90:
postfix:x:89:
chrony:x:996:
microlinux:x:1000:
adebuf:x:1001:
jmortreux:x:1002:
fbanester:x:1003:
fteyssier:x:1004:
fantasio:x:1005:
```

Pour comprendre en détail la syntaxe de ce fichier, nous avons développé le bon
réflexe&nbsp;:

```
# apropos group
# man 5 group
```

Tout comme pour les utilisateurs, nous avons ici&nbsp;:

* un groupe `root` avec un GID de `0`&nbsp;;

* une série de groupes système avec des GID compris entre `1` et `999`&nbsp;;

* les groupes correspondant aux utilisateurs, avec des GID supérieurs ou égaux
  à `1000`.

Dans l'exemple ci-dessus, regardez la ligne qui définit le groupe `wheel`. Notez
que les membres de ce groupe sont énumérés avec une virgule comme séparateur,
et sans espace.

### Supprimer un utilisateur d'un groupe

La manière la plus simple pour révoquer l'appartenance d'un utilisateur à un
groupe, c'est d'utiliser la commande `gpasswd`. Admettons que `jmortreux` ait
mis en péril notre installation. Nous allons le supprimer du groupe
`wheel`&nbsp;:

```
# groups jmortreux
jmortreux : jmortreux wheel
# gpasswd -d jmortreux wheel
Removing user jmortreux from group wheel
# groups jmortreux
jmortreux : jmortreux
```

Là encore, jetez un oeil dans `man gpasswd` et repérez l'option `-d` comme
`--delete`.

## Créer et supprimer des groupes

Bien évidemment, nous ne sommes pas limités à l'utilisation des groupes
prédéfinis sur la machine. En guise d'exemple, créons deux groupes `formateurs`
et `direction`, puis ajoutons les utilisateurs `microlinux`, `adebuf`, `jmortreux`,
`fbanester` et `fteyssier` à leurs groupes respectifs, sachant que certains
appartiennent aux deux groupes&nbsp;:

```
# groupadd formateurs
# groupadd direction
# usermod -aG formateurs microlinux
# usermod -aG formateurs jmortreux
# usermod -aG formateurs fteyssier
# usermod -aG direction adebuf
# usermod -aG direction jmortreux
# usermod -aG direction fbanester
# usermod -aG direction fteyssier
```

Pour apprécier le résultat de l'opération, ouvrez `/etc/group` et examinez les
deux dernières lignes du fichier. Vous pourriez très bien utiliser `cat`, `more` ou
`less` pour visualiser `/etc/group`, mais j'en profite en passant pour vous
présenter la commande `tail`. Dans sa configuration par défaut, `tail` affiche les
dix dernières lignes du fichier fourni en argument. L'option `-n 2` nous limitera
aux deux dernières lignes&nbsp;:

```
# tail -n 2 /etc/group
formateurs:x:1006:microlinux,jmortreux,fteyssier
direction:x:1007:adebuf,jmortreux,fbanester,fteyssier
```

Pour supprimer un groupe, utilisez la commande `groupdel`&nbsp;:

```
# groupdel formateurs
# groupdel direction
```

## Supprimer un utilisateur 

Vous voilà en mesure de créer des utilisateurs et de gérer leurs comptes. Il ne
vous reste plus qu'à savoir comment vous en débarrasser, le cas échéant. Comme
un employé doit vider son bureau et rendre son badge, il arrive qu'un
utilisateur n'ait plus la place sur votre système. Dans ce cas, vous serez
amené à supprimer son compte. C'est l'objet de la commande `userdel`. 

Appliquons-la sur l'utilisateur `fantasio` :

```
# userdel fantasio
```

Malheureusement, il y a un détail auquel nous n'avons pas pensé&nbsp;:

```
# ls -l /home/
total 8
drwx------. 2 adebuf     adebuf       62 May 17 08:05 adebuf
drwx------. 2       1005       1005 4096 May 17 12:10 fantasio
drwx------. 2 fbanester  fbanester    62 May 17 08:12 fbanester
drwx------. 2 fteyssier  fteyssier    83 May 17 09:00 fteyssier
drwx------. 2 jmortreux  jmortreux    62 May 17 08:09 jmortreux
drwx------. 2 microlinux microlinux 4096 May 16 15:47 microlinux
```

La commande `userdel` s'est chargée de supprimer l'utilisateur `fantasio`, mais
pas son répertoire.

### Vider l'eau du bain avec le bébé

Le répertoire `/home/fantasio` existe toujours et semble intact, à un détail
près&nbsp;:

```
# ls -la /home/fantasio/
total 28
drwx------. 2 1005 1005 4096 May 17 12:10 .
drwxr-xr-x. 8 root root 4096 May 17 10:34 ..
-rw-------. 1 1005 1005   76 May 17 12:10 .bash_history
-rw-r--r--. 1 1005 1005   18 Nov 22  2019 .bash_logout
-rw-r--r--. 1 1005 1005  193 Nov 22  2019 .bash_profile
-rw-r--r--. 1 1005 1005  231 Nov 22  2019 .bashrc
-rw-r--r--. 1 1005 1005   45 May 17 10:34 LISEZ_MOI.txt
```

Les fichiers et répertoires de `/home/fantasio` sont toujours là, mais ils
"appartiennent" à présent à l'UID `1005` et au GID `1005`. Admettons que vous
ayez maintenant l'idée de créer un nouveau compte et de lui affecter ces
identifiants d'utilisateur et de groupe désormais vacants. Il se retrouverait
propriétaire de tous les fichiers de `fantasio`. Autrement dit&nbsp;:
`fantasio` a rendu son badge, mais il n'a vidé ni son bureau ni son casier, et
c'est le nouvel employé qui a désormais la clé. 

Lors de la suppression, il peut donc s'avérer nécessaire de supprimer le
répertoire d'utilisateur correspondant au compte. Cependant, si nous affichons
la page `man` de la commande `userdel`, nous lisons ceci&nbsp;:

```
OPTIONS
...
-r, --remove
    Files in the user's home directory will be removed along with the home
    directory itself and the user's mail spool. Files located in other file
    systems will have to be searched for and deleted manually.
...
```

Essayons cette option sur l'utilisatrice `fbanester`, que nous avons dans le
collimateur depuis un petit moment&nbsp;:

```
# userdel -r fbanester
[root@linuxbox ~]# ls -l /home/
total 8
drwx------. 2 adebuf     adebuf       62 May 17 08:05 adebuf
drwx------. 2       1005       1005 4096 May 17 12:10 fantasio
drwx------. 2 fteyssier  fteyssier    83 May 17 09:00 fteyssier
drwx------. 2 jmortreux  jmortreux    62 May 17 08:09 jmortreux
drwx------. 2 microlinux microlinux 4096 May 16 15:47 microlinux
```

La commande `userdel` suivie de l'option `-r` a donc bien supprimé le compte
utilisateur `fbanester` ainsi que le répertoire utilisateur `/home/fbanester`
avec toutes les données qu'il contenait.

## Administrer le système avec sudo

Au début de cette leçon, nous avons vu qu'il était possible de changer
d'identité grâce à la commande `su` (*switch user*), notamment pour devenir
`root`&nbsp;: 

```
[microlinux@linuxbox ~]$ ls /boot/grub2/
ls: cannot open directory /boot/grub2/: Permission denied
[microlinux@linuxbox ~]$ su -
Password:
[root@linuxbox ~]# ls /boot/grub2/
device.map  fonts  grub.cfg  grubenv  i386-pc  locale
```

Une autre manière de faire, c'est d'utiliser la commande `sudo`. Elle permet
littéralement de "faire (*do*) en se substituant à l'utilisateur (*su*)", en
l'occurrence lancer une commande en tant qu'administrateur. Dans l'exemple qui
suit, notez bien que je saisis le mot de passe de `microlinux` et non pas celui
de `root`&nbsp;:

```
[microlinux@linuxbox ~]$ ls /boot/grub2/
ls: cannot open directory /boot/grub2/: Permission denied
[microlinux@linuxbox ~]$ sudo ls /boot/grub2/
[sudo] password for microlinux: ********
device.map  fonts  grub.cfg  grubenv  i386-pc  locale
```

Lorsqu'un utilisateur invoque `sudo` pour la première fois, le système affiche un
avertissement solennel&nbsp;:

```
We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.
```

Rappelez-vous que lors de l'installation du système et de la création de
l'utilisateur initial, nous avons coché la case `Faire de cet utilisateur un
administrateur`, ce qui a ajouté l'utilisateur au groupe privilégié `wheel`. Le
principal privilège de ce groupe est défini dans le fichier
`/etc/sudoers`&nbsp;:

```
## Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
```

Voilà comment se présentent les choses lorsque `sudo` est invoqué par un
utilisateur qui ne dispose pas de ce privilège&nbsp;:

```
[fteyssier@linuxbox ~]$ sudo ls /boot/grub2/
[sudo] password for fteyssier:
fteyssier is not in the sudoers file.  This incident will be reported.
```

Le signalement en question a lieu dans le fichier `/var/log/secure` et
ressemble à ceci&nbsp;:

```
Sep  1 08:14:17 linuxbox sudo: fteyssier : user NOT in sudoers ; TTY=pts/1 ; 
PWD=/home/fteyssier ; USER=root ; COMMAND=/bin/ls /boot/grub2/
```

### Utiliser su ou sudo ?

Dans le monde d'Unix et de Linux, il existe en gros deux écoles pour
s'acquitter des tâches administratives&nbsp;:

* travailler directement en tant que `root` lorsque cela est nécessaire&nbsp;;

* se connecter en tant qu'utilisateur simple et utiliser `sudo` lorsqu'une
  tâche le requiert.

Certaines distributions – comme Ubuntu par exemple – favorisent explicitement
l'approche avec `sudo` en désactivant le compte `root` dans la configuration
par défaut.

Quelle est donc la "meilleure" approche ici&nbsp;? Je me garderai de trancher
la question, et je me contenterai d'attirer votre attention sur les principales
différences entre les deux manières de faire&nbsp;: 

* Travailler directement en tant que `root` est certainement plus confortable.

* Utiliser `sudo` n'a jamais empêché personne de se tirer dans le pied. 

* Le principal argument en faveur de `sudo`, c'est sans doute la journalisation
  des opérations dans `/var/log/secure`. Si vous travaillez en tant
  qu'utilisateur simple et que vous utilisez `sudo`, tout ce que vous faites
  est enregistré dans ce fichier. Ce qui n'est pas le cas pour `root`. Essayez
  pour voir. 

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


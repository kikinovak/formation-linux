
**Objectif** : mise en place d'un environnement de test pour Linux sur un poste
de travail ou un portable sous Linux, Windows ou macOS.

## Présentation de VirtualBox

[VirtualBox](https://www.virtualbox.org) est un logiciel libre de
virtualisation de systèmes d'exploitation publié par Oracle. En utilisant les
ressources matérielles de l'ordinateur (*système hôte*), VirtualBox crée un
ordinateur virtuel dans lequel s'installent d'autres systèmes d'exploitation
(*systèmes invités*). Les systèmes invités fonctionnent en même temps que le
système hôte, mais seul ce dernier a accès directement au véritable matériel de
l'ordinateur.

## Quelques vérifications

### Activer la virtualisation matérielle

Vérifiez si la virtualisation matérielle est activée dans le BIOS de votre
machine physique&nbsp;:

* `VT-x` pour processeurs Intel

* `AMD-v` pour processeurs AMD

La plupart des machines activent ces options par défaut, mais cela peut varier
selon les fabricants. Si la virtualisation matérielle n'est pas activée, vous
risquez de vous retrouver confronté à un message d'erreur du genre `This kernel
requires an x86-64 CPU`.

### Pour les utilisateurs de Windows

#### Désactiver Hyper-V

VirtualBox est incompatible avec Hyper-V (*Windows Server Virtualization*). Il
faut donc songer à désactiver Hyper-V sur une installation de Windows Server.

#### Conflits avec l'antivirus

Certains antivirus comme Avira peuvent empêcher VirtualBox de tourner
correctement. Si le démarrage d'une machine virtuelle vous affiche une erreur,
essayez tout d'abord de désactiver votre antivirus. 


## Installation

### Sous OpenSUSE Leap 15.3

* Ouvrez YaST&nbsp;: `Applications` > `Système` > `YaST`.

* Dans l'onglet `Logiciel` cliquez sur `Installer et supprimer des logiciels`.

* Cherchez `virtualbox` dans le champ de recherche.

* Cochez le paquet `virtualbox-qt` dans la liste des résultats.

* Cliquez sur `Accepter`.

* Confirmez l'installation des dépendances en cliquant sur `Continuer`.

* Patientez pendant l'installation de VirtualBox et cliquez sur `Terminer`.

L'installation de VirtualBox a créé un nouveau groupe système `vboxusers`.
L'utilisateur qui souhaite utiliser VirtualBox devra faire partie de ce groupe. 

* Dans YaST, ouvrez l'onglet `Sécurité et Utilisateurs`.

* Cliquez sur `Gestion des groupes et des utilisateurs`.

* Sélectionnez votre utilisateur et cliquez sur `Modifier`.

* Repérez l'onglet `Détails` et ouvrez-le.

* Dans la liste des `Groupes supplémentaires`, cochez `vboxusers`.

* Confirmez successivement par `OK` et quittez YaST.

* Redémarrez la machine.

Si vous avez déjà un peu de bouteille et que la ligne de commande ne vous fait
pas peur, rien ne vous empêche d'installer VirtualBox comme ceci&nbsp;:

```
# zypper refresh
# zypper install virtualbox-qt
# usermod -aG vboxusers <votre_utilisateur>
```


### Sous Windows et macOS

* Téléchargez VirtualBox dans la rubrique
   [*Downloads*](https://www.virtualbox.org/wiki/Downloads) du site.

* Démarrez l'installateur.

* Confirmez toutes les options présélectionnées par défaut.

* Fournissez le mot de passe de l'administrateur.

* Redémarrez votre système.

### Premier test

Lancez VirtualBox et faites un test sommaire pour vérifier s'il fonctionne
correctement.

* Au premier lancement, confirmez `Enable USB passthrough`.

* Dans la fenêtre principale de VirtualBox, cliquez sur `Nouvelle`.

* Vérifiez si VirtualBox vous propose la création de machines virtuelles
  `64-bit` dans le menu déroulant.

* Dans le cas contraire, il faudra activer la virtualisation matérielle dans le
  BIOS ou l'UEFI, comme nous l'avons vu un peu plus haut.

## Pour en savoir plus

VirtualBox est une solution de virtualisation complète, ce qui signifie qu'il
vous permet d'exécuter un système d'exploitation non modifié avec tous ses
logiciels installés dans un environnement isolé sur votre système
d'exploitation existant.

Cet environnement isolé est appelé une *machine virtuelle* ou aussi une VM de
l'anglais *Virtual Machine*. Le logiciel de virtualisation crée cet
environnement un peu particulier en interceptant l'accès à certains composants
matériels et certaines fonctionnalités du système. 

La machine hôte représente la machine physique qui va héberger une ou plusieurs
machines virtuelles. VirtualBox est installé sur la machine hôte. En anglais,
on la nomme *host*. Le système hôte (ou *host system*) représente le système
d'exploitation qui est installé sur la machine hôte.

La machine invitée représente la machine virtuelle qui sera allouée et gérée
par VirtualBox, et le système invité (ou *guest system*) c'est le système
d'exploitation qui est installé sur la machine virtuelle.

Le code du système invité tourne tel quel, c'est-à-dire sans aucune
modification. Le système invité se comporte comme s'il tournait sur une vraie
machine physique&nbsp;; il ne sait pas qu'il tourne à l'intérieur d'une machine
virtuelle. 

Si vous travaillez dans une machine virtuelle et que l'OS de cette machine
virtuelle vous annonce quelque chose comme `Cette action effacera toutes les
données de votre disque`, il n'y a pas de quoi s'inquiéter. En réalité, il fait
référence au disque virtuel auquel il a accès, et non pas au disque physique de
votre ordinateur hôte physique. 

Plus généralement, vous pouvez effectuer des actions destructives à l'intérieur
de la machine virtuelle, et ces actions seront toujours contenues dans la
machine virtuelle et n'interféreront jamais avec votre ordinateur physique. 

La création d'une machine virtuelle avec VirtualBox est relativement simple. En
règle générale, vous téléchargez un fichier ISO Linux sur le site web de la
distribution, vous le rangez sur votre machine locale et vous dites à
VirtualBox d'utiliser cet ISO pour démarrer le système. Alternativement, vous
pouvez très bien utiliser un CD-Rom ou un DVD physique dans le lecteur optique
de votre ordinateur et dire à VirtualBox d'utiliser ce support physique. À
partir de là, vous installez le système d'exploitation sur une machine
virtuelle comme vous le feriez sur du vrai hardware physique.

VirtualBox fournit un logiciel supplémentaire qui peut être installé sur le
système d'exploitation invité. Ce petit logiciel s'appelle *VirtualBox Guest
Additions*. Les Additions Invité permettent au système d'exploitation invité
d'accéder aux dossiers partagés sur le système hôte, de partager le
presse-papiers et d'autres actions similaires. Cette installation
supplémentaire n'est pas obligatoire mais elle peut être assez utile. 

> Si nous recherchions une solution de virtualisation pour faire
tourner des machines virtuelles en production, nous nous orienterions plutôt
vers des solutions comme KVM ou VMWare. Mais pour notre objectif – c'est-à-dire
créer des machines de test sur notre machine locale – VirtualBox est
certainement la solution la plus adaptée. 

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



**Objectif** : prise en main de l'organisation d'un disque dur sur un système
Linux.

## Le partitionnement automatique

Lors de [l'installation initiale d'Oracle Linux](install-ol7-virtualbox.md),
nous avons opté pour le partitionnement automatique. Le but de l'opération
était de disposer d'un système Linux fonctionnel pour nous entraîner, sans nous
soucier des menus détails de l'organisation du système sous le capot.

Dans la configuration par défaut, l'installateur organise le disque en
utilisant le gestionnaire de volumes logiques LVM (*Logical Volume Manager*).
Il s'agit là d'une couche d'abstraction entre le système de fichiers et les
partitions, qui permet une approche plus souple dans l'organisation du disque.

Nous aurons l'occasion d'aborder cette manière de faire les choses un peu plus
loin. Pour l'instant, LVM constitue tout au plus une couche de complexité
inutile dont nous allons nous débarrasser, afin de mieux comprendre les bases
de l'organisation d'un disque dur sous Linux.

Tout au long de cet atelier pratique, nous allons procéder à une série de
réinstallations successives d'un système minimal Oracle Linux&nbsp;7, en optant
pour le partitionnement manuel, avec des schémas de partitionnement allant du
cas de figure le plus rudimentaire à des scénarios un peu plus élaborés.

## Les tables de partitionnement MBR et GPT

Les informations de partitionnement sont actuellement gérées par deux
procédures distinctes&nbsp;:

* MBR (*Master Boot Record*)&nbsp;;

* GPT (*GUID Partition Tables*).

Les concepts de partitionnement basés sur les tables de partitions MBR
remontent à l'époque MS-DOS, ce qui explique tout un ensemble de limitations et
de règles plus ou moins farfelues. Ce concept reste pourtant valable pour à peu
près tous les disques durs utilisés dans des PC Windows et/ou Linux jusqu'en
2012. Dans ce cas de figure, la table de partitions est stockée dans le MBR,
c'est-à-dire le premier secteur du disque dur.

GPT est un standard créé il y a quelque temps déjà pour éviter les nombreuses
limitations liées au MBR. Les ordinateurs de la marque Apple utilisent GPT
depuis 2005. Les machines Windows y ont migré depuis la présentation de
Windows&nbsp;8 à l'automne&nbsp;2012. L'UEFI, le remplaçant du BIOS, exige
l'utilisation d'une table de partitionnement GPT.

En théorie, le partitionnement GPT est censé avoir remplacé le modèle MBR
depuis belle lurette. En pratique, ce dernier reste largement répandu et nous
retrouvons ce paradigme classique où une technologie obsolète a la vie dure et
refuse de céder la place au progrès.

### Identifier le type de partitionnement sur mon système

Lors de l'installation initiale d'Oracle Linux, c'est l'installateur qui a
sélectionné automatiquement le type de partitionnement qu'il fallait utiliser
sous le capot.

* Dans la majorité des cas, nous retrouverons un partitionnement MBR, notamment
  avec les BIOS traditionnels, les BIOS modernes en mode *legacy* et les
  installations dans VirtualBox avec la configuration par défaut.

* Le partitionnement GPT sera utilisé sur des systèmes où l'UEFI a remplacé
  le BIOS.

Pour savoir quel est le type de partitionnement utilisé sur votre machine,
invoquez la commande suivante&nbsp;:

```
$ sudo fdisk -l /dev/sda 2> /dev/null | grep type
Disk label type: dos
```

Ici, j'ai utilisé `fdisk` avec l'option `-l` pour afficher la table de
partitions de mon disque `sda`, en faisant fi des éventuels messages d'erreur
et en filtrant l'affichage pour obtenir la ligne qui me renseigne sur le type
d'étiquette du disque. L'étiquette `dos` correspond à une table de
partitionnement MBR.

Sur une machine UEFI, la même commande m'affiche un type
d'étiquette `gpt`&nbsp;: 

```
$ sudo fdisk -l /dev/sda 2> /dev/null | grep type
Disk label type: gpt
```

Pour éviter la confusion, nous allons commencer par traiter le partitionnement
MBR, étant donné que c'est le cas de figure que nous allons rencontrer le plus
souvent sous Oracle Linux&nbsp;7. Une fois que nous serons à l'aise avec cette
manière traditionnelle de faire les choses, nous attaquerons les tables de
partitions GPT et les outils pour les gérer.

## Le schéma de partitionnement le plus simple

Pour commencer, nous allons choisir le schéma de partitionnement le plus simple
pour notre installation. Un système Linux a besoin d'au moins deux
partitions&nbsp;:

* une partition d'échange ou `swap`&nbsp;;

* une partition principale.

### La partition d'échange

La partition `swap` est une sorte de mémoire virtuelle de votre machine, une
extension dans le cas où vous arriveriez aux limites de la mémoire vive.
Lorsque ce cas de figure se présente, c'est-à-dire si la RAM arrive à
saturation, son contenu est délocalisé sur cette zone du disque pour éviter de
bloquer le système. La taille de la partition `swap` sera égale à celle de la
mémoire vive disponible.

### Les systèmes de fichiers pour la partition principale

Une fois que nous aurons créé notre `swap`, la partition principale occupera
simplement tout l'espace restant du disque. L'installateur d'Oracle Linux
propose une multitude de systèmes de fichiers au choix pour la formater&nbsp;:

* `ext2` (*second extended file system*) est le système de fichiers historique
  de GNU/Linux.

* `ext3` est un autre système de fichiers GNU/Linux, une évolution d'`ext2`. Ce
  qui le caractérise principalement, c'est l'utilisation d'un fichier journal.
  On arrive ainsi à éviter la longue phase de récupération lors d'un arrêt
  brutal de la machine.

* `ext4` est le successeur d'`ext3`. C'est le système de fichiers que nous
  utiliserons pour formater notre partition principale.

* En dehors de cela, il existe une multitude de systèmes de fichiers comme
  `xfs`, `jfs`, `reiserfs` ou `btrfs`, qui ont chacun des avantages et des
  inconvénients.  L'installateur d'Oracle Linux&nbsp;7 utilise `xfs` par défaut
  pour formater le disque dur si l'on opte pour le partitionnement automatique.

### Retour à la case départ

Démarrez une installation fraîche d'Oracle Linux&nbsp;7. Si vous disposez d'une
machine virtuelle sous VirtualBox, vous avez le choix&nbsp;:

* créez une nouvelle machine virtuelle&nbsp;;

* écrasez le système invité existant.

Dans tous les cas, renseignez la langue du système, le fuseau horaire et la
disposition du clavier comme nous avons pu le faire auparavant. Désactivez le
service Kdump, activez l'interface réseau et renseignez le nom d'hôte. Ensuite,
cliquez sur `Destination de l'installation`.

* Cliquez sur `Je vais configurer le partitionnement`, puis sur `Terminé`.

* Si vous écrasez un système Oracle Linux installé, cliquez sur la petite
  flèche `>` à gauche du nom du système pour afficher les partitions
  existantes.

* Sélectionnez n'importe quelle partition, puis cliquez sur le petit bouton `-`
  en bas à gauche de l'écran pour la supprimer.

* Confirmez la suppression de toutes les données sur le disque.

### Créer la partition d'échange

* Nous n'utilisons pas LVM. Sélectionnez le schéma de partitionnement
  `Partition standard` dans le menu déroulant.

* Cliquez sur le bouton `+` en bas à gauche de l'écran pour ajouter un point de
  montage pour la partition d'échange.

* Sélectionnez le point de montage `swap` et définissez sa taille. Elle sera
  égale à la quantité de RAM disponible, par exemple `1 Gio`.

* Définissez l'étiquette `swap` pour la partition (facultatif) et cliquez sur
  `Mise à jour des paramètres`.

> La combinaison de touches ++ctrl+alt+f2++ vous permet d'ouvrir une console
> virtuelle depuis l'installateur. Invoquez alors la commande `free -m` pour
> afficher la quantité de RAM disponible dans votre machine. Revenez à la
> fenêtre de l'installateur avec ++ctrl+alt+f6++. Si vous installez une machine
> virtuelle dans VirtualBox, utilisez les raccourcis clavier respectifs
> ++ctrl+f2++ et ++ctrl+f6++. 

### Créer la partition principale

* Cliquez sur le bouton `+` en bas à gauche de l'écran pour ajouter un point de
  montage pour la partition principale.

* Sélectionnez le point de montage `/` sans renseigner la capacité souhaitée,
  afin d'utiliser la totalité de l'espace disponible.

* Sélectionnez le système de fichiers `ext4` dans le menu déroulant, définissez
  l'étiquette `root` pour la partition (facultatif) et cliquez sur `Mise à jour
  des paramètres`.

* Cliquez sur le bouton `Terminé` en haut à gauche pour passer à l'écran
  suivant.

* L'installateur vous affiche un récapitulatif des opérations. Confirmez en
  acceptant les modifications, ce qui vous ramène à l'écran principal. 

Démarrez l'installation du système, définissez un mot de passe pour `root`,
ajoutez un utilisateur normal et allez boire un café en attendant
l'installation du système minimal.

### État des lieux

Au terme du redémarrage initial, connectez-vous en tant qu'utilisateur normal
et invoquez la commande `mount`&nbsp;:

```
$ mount | grep sda
/dev/sda1 on / type ext4 (rw,relatime,seclabel,data=ordered)
```

Dans le [précédent atelier pratique](peripheriques.md), nous avons vu que la
commande `mount` invoquée sans argument affiche l'ensemble des systèmes de
fichiers montés. Les principes qui s'appliquent pour les disques externes
restent valables pour les disques internes. Concrètement, je vois que la
première partition de mon disque dur (`/dev/sda1`) correspond à ma partition
principale (`/`), que le système de fichiers utilisé est l'`ext4` et que le
tout est accessible en lecture et en écriture (`rw`).

Dans les systèmes Linux, c'est le fichier `/etc/fstab` (*file systems table*)
qui me renseigne sur l'organisation des systèmes de fichiers. Jetons un oeil
dans ce fichier.

```
$ cat /etc/fstab
#
# /etc/fstab
...
UUID=89956d0e-c53e-4e3f-9b7e-16c8aab657c4 /    ext4 defaults 1 1
UUID=88142203-6702-480c-b2fb-00dd074876fc swap swap defaults 0 0
```

La page de manuel en ligne `fstab(5)` nous renseigne sur la syntaxe de ce
fichier. Chaque ligne contient une série de six champs, séparés par des espaces
ou des tabulations, qui décrivent respectivement&nbsp;:

* le périphérique bloc à monter&nbsp;;

* le point de montage&nbsp;;

* le type de système de fichiers&nbsp;;

* les options de montage&nbsp;;

* deux indications pour la sauvegarde et la vérification de l'intégrité du
  système.

Les éléments potentiellement déroutants dans notre fichier `/etc/fstab`, ce
sont les périphériques désignés par les UUID, c'est-à-dire les premiers champs
qui commencent par `UUID=quelquechose`.

### Comprendre les UUID

L'UUID (*Universally Unique IDentifier*) est un identifiant universel unique.
Il s'agit là d'une chaîne de caractères théoriquement unique au monde qui
identifie sans ambiguïté chaque élément de notre système, en l'occurrence
chaque partition de notre disque dur. Ce n'est pas de la magie noire et, pour
savoir quelle partition correspond à quel UUID, il suffit de jeter un oeil dans
le répertoire `/dev/disk/by-uuid`&nbsp;:

```
$ ls -l /dev/disk/by-uuid/
total 0
lrwxrwxrwx. ... 88142203-6702-480c-b2fb-00dd074876fc -> ../../sda2
lrwxrwxrwx. ... 89956d0e-c53e-4e3f-9b7e-16c8aab657c4 -> ../../sda1
```

Le répertoire contient deux liens symboliques commençant par `881422` et
`89956d` sur mon système et qui pointent respectivement vers `/dev/sda2` et
`/dev/sda1`.

Pour mieux comprendre les UUID, tentez une petite expérience pratique. Éditez
le fichier `/etc/fstab` et remplacez chaque UUID par le périphérique qu'il est
censé représenter, en vous méfiant des fautes de frappe potentiellement
fatales. Vous devrez obtenir quelque chose comme ceci&nbsp;:

```
/dev/sda1 /    ext4 defaults 1 1
/dev/sda2 swap swap defaults 0 0
```

Redémarrez le système. Vous constatez qu'il est toujours fonctionnel.

> Actuellement, nous n'utilisons qu'un seul disque dur. Admettons que nous
> souhaitions en ajouter un après l'installation du système (et c'est
> d'ailleurs ce que nous allons faire tout à l'heure). Dans certains cas, il
> est théoriquement possible que le noyau décide de renommer les disques durs.
> Le nouveau serait nommé `sda` et l'ancien disque `sda` deviendrait `sdb`. Il
> en résulterait que notre système ne démarrerait plus. Le recours aux UUID
> permet donc d'identifier les disques et les partitions de façon persistante.

### Pourquoi les partitions ont-elles été inversées ?

Vous aurez peut-être remarqué que l'ordre des partitions `sda1` et `sda2` n'est
pas celui que nous avions défini lors du partitionnement manuel. Cela est dû au
fait que le programme d'installation a rectifié le tir en reléguant la
partition d'échange vers la fin du disque.

Nous n'allons pas trop rentrer dans les détails mécaniques de géométrie de
disque. Retenez juste le fait qu'il est plus logique de ranger la partition
d'échange à la fin d'un disque dur et de réserver le début du disque au système
à proprement parler, étant donné que ces secteurs – physiquement situés au
centre – sont un peu plus rapides d'accès pour les têtes de lecture.

## Un schéma de partitionnement un peu plus élaboré

Après cette première prise en main du partitionnement manuel de l'installateur,
je vous propose un deuxième exercice pour vous mettre à l'aise avec la
procédure. Voici un schéma de partitionnement courant, que j'utilise sur mon
ordinateur portable :

* une partition `/boot` de 500 Mo, formatée en `ext2`&nbsp;;

* une partition `swap` dont la taille équivaut à la RAM disponible&nbsp;;

* une partition principale occupant l'espace restant, formatée en `ext4`.

### Préparation du disque

* Dans le menu principal de l'installateur, cliquez sur `Destination de
  l'installation`.

* Vérifiez si le disque est bien sélectionné.

* Cochez `Je vais configurer le partitionnement` et cliquez sur `Terminé`.

* Supprimez toutes les partitions existantes.

* Dans le menu déroulant, sélectionnez `Partition standard` au lieu de `LVM`.

### Créer la partition /boot

* Cliquez sur le bouton `+` pour créer un nouveau point de montage.

* Créez le point de montage `/boot` avec une capacité de `500 Mio`.

* Sélectionnez le système de fichiers `ext2`.

* Définissez l'étiquette `boot` (facultatif).

* Confirmez en cliquant sur `Mise à jour des paramètres`.

### Créer la partition d'échange

* Cliquez sur le bouton `+` pour créer un autre point de montage.

* Créez le point de montage `swap` en spécifiant sa capacité en Gio, par
  exemple `1 Gio`.

* Définissez l'étiquette `swap` (facultatif).

* Confirmez en cliquant sur `Mise à jour des paramètres`.

### Créer la partition principale

* Cliquez sur le bouton `+` pour créer un dernier point de montage.

* Créez le point de montage `/` sans spécifier la capacité souhaitée. La
  partition occupera alors tout l'espace restant du disque.

* Sélectionnez le système de fichiers `ext4`.

* Définissez l'étiquette `root` (facultatif).

* Confirmez en cliquant sur `Mise à jour des paramètres`, puis sur `Terminé`.

* Notez en passant que, là aussi, la partition d'échange est reléguée à la fin
  du disque par l'installateur.

### État des lieux

Une fois l'installation terminée, voyons comment se présente le système sous le
capot. Invoquez la commande `mount` en filtrant les résultats pour les
partitions du disque dur&nbsp;:

```
$ mount | grep sda
/dev/sda2 on / type ext4 (rw,relatime,seclabel,data=ordered)
/dev/sda1 on /boot type ext2 (rw,relatime,seclabel)
```

Notre fichier `/etc/fstab` comporte une ligne de plus pour la partition
`/boot`&nbsp;:

```
$ cat /etc/fstab
#
# /etc/fstab
...
UUID=c485fdd0-b10b-46f6-b64e-343a4c355c84 /     ext4 defaults 1 1
UUID=ff0e7554-aebc-4df7-b571-16646e99af04 /boot ext2 defaults 1 2
UUID=103cca88-f0fb-41b0-bdcb-d20c0c10c778 swap  swap defaults 0 0
```

Jetons un oeil dans `/dev/disk/by-uuid` pour retrouver les correspondances
entre les partitions et les UUID&nbsp;:

```
$ ls -l /dev/disk/by-uuid/
total 0
lrwxrwxrwx. ... 103cca88-f0fb-41b0-bdcb-d20c0c10c778 -> ../../sda3
lrwxrwxrwx. ... c485fdd0-b10b-46f6-b64e-343a4c355c84 -> ../../sda2
lrwxrwxrwx. ... ff0e7554-aebc-4df7-b571-16646e99af04 -> ../../sda1
```

### Comprendre les étiquettes

Lors de l'installation du système, nous avons défini des étiquettes `boot`,
`root` et `swap` pour les partitions respectives. Cette opération n'était pas
strictement nécessaire, mais nous pouvons voir ici à quoi elle sert. Le rôle
des étiquettes est similaire à celui des UUID. En effet, regardons dans
`/dev/disk/by-label`&nbsp;:

```
$ ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx. 1 root root 10 Jun  1 12:45 boot -> ../../sda1
lrwxrwxrwx. 1 root root 10 Jun  1 12:45 root -> ../../sda2
lrwxrwxrwx. 1 root root 10 Jun  1 12:45 swap -> ../../sda3
```

Les étiquettes sont une autre manière d'obtenir un nommage persistant pour les
partitions du système. Là aussi, je peux tenter l'expérience en éditant
`/etc/fstab` et en remplaçant les UUID par les étiquettes correspondantes.
Voici à quoi cela ressemble sur mon système&nbsp;:

```
LABEL=root   /      ext4    defaults        1 1
LABEL=boot   /boot  ext2    defaults        1 2
LABEL=swap   swap   swap    defaults        0 0
```

### Afficher l'espace occupé et l'espace disponible

La commande `df` (*disk free*) nous renseigne sur l'espace occupé et l'espace
disponible au sein des systèmes de fichiers. L'option `-h` (`--human-readable`)
affiche les résultats dans un format plus lisible&nbsp;:

```
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
...
/dev/sda2        58G  1.4G   54G   3% /
/dev/sda1       485M  101M  359M  22% /boot
...
```

### Une partition pour /home

Faisons maintenant un petit exercice pratique pour nous familiariser davantage
avec les concepts de partitionnement et de systèmes de fichiers. Cette fois-ci,
effectuez une installation minimale d'Oracle Linux en respectant le schéma de
partitionnement suivant&nbsp;:

* une partition `/boot` de 500 Mo étiquetée `boot` et formatée en `ext2`&nbsp;;

* une partition `swap` d'une taille égale à la RAM disponible,
  étiquetée `swap`&nbsp;;

* une partition principale de 5 Go étiquetée `root` et formatée
  en `ext4`&nbsp;;

* une partition `/home` étiquetée `home`, occupant l'espace restant et formatée
  en `ext4`&nbsp;;

Vous devrez obtenir quelque chose comme ceci&nbsp;:

```
$ df -h | grep sda
/dev/sda3       4.8G  1.4G  3.3G  30% /
/dev/sda1       485M  101M  359M  22% /boot
/dev/sda2        53G   53M   50G   1% /home
```

Partant de là, éditez `/etc/fstab` et remplacez les UUID par les étiquettes
correspondantes.

## Ajouter un disque à une installation existante

Les différents scénarios de partitionnement et de formatage que nous avons vus
jusqu'ici ont tous pu être mis en pratique avec l'interface graphique de
l'installateur d'Oracle Linux. Nous abordons maintenant un cas de figure où
nous ne disposons plus de cet outil plus ou moins convivial, que les
administrateurs durs à cuire appellent un "clicodrome".

Notre point de départ, ce sera le schéma de partitionnement que nous avons vu
précédemment, à savoir&nbsp;:

* une partition `/boot` de 500 Mo, formatée en `ext2`&nbsp;;

* une partition `swap` dont la taille équivaut à la RAM disponible&nbsp;;

* une partition principale occupant l'espace restant, formatée en `ext4`.

Concrètement, notre disque dur `sda` est organisé en trois partitions `sda1`,
`sda2` et `sda3`&nbsp;:

```
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   60G  0 disk
├─sda1   8:1    0  500M  0 part /boot
├─sda2   8:2    0 58.5G  0 part /
└─sda3   8:3    0    1G  0 part [SWAP]
sr0     11:0    1 1024M  0 rom
```

### Insérer et connecter le deuxième disque dur

Si vous disposez d'une installation physique d'Oracle Linux sur un serveur ou
un PC faisant office de serveur, insérez le deuxième disque et connectez-le.
Allumez la machine et vérifiez si les deux disques apparaissent bien dans
le BIOS.

### Ajouter un disque dans une machine virtuelle

Vous pouvez très bien ajouter un deuxième disque à un système invité installé
dans VirtualBox.

* Éteignez le système invité et cliquez sur `Configuration`.

* Ouvrez l'onglet `Stockage` et repérez le `Contrôleur SATA`.

* Cliquez-droit sur le `Contrôleur SATA` et sélectionnez `Ajouter un
  disque dur`.

* Dans la fenêtre subséquente, cliquez sur `Créer un nouveau disque`.

* Confirmez le type de fichier de disque dur `VDI` par défaut.

* Confirmez la taille dynamiquement allouée du fichier.

* Nommez le disque `Disque2.vdi` et définissez sa taille, par exemple
  `20,00 Gio`.

* Sélectionnez le disque `Disque2.vdi` et cliquez sur `Choisir`.

### Vérifier la prise en charge du deuxième disque

Une fois que le nouveau disque dur est installé dans la machine – physique ou
virtuelle – il faut démarrer le système. Voyons tout d'abord s'il s'affiche en
tant que nouveau périphérique bloc&nbsp;:

```
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   60G  0 disk
├─sda1   8:1    0  500M  0 part /boot
├─sda2   8:2    0 58.5G  0 part /
└─sda3   8:3    0    1G  0 part [SWAP]
sdb      8:16   0   20G  0 disk
sr0     11:0    1 1024M  0 rom
```

L'affichage de `lsblk` me montre bien la présence d'un deuxième disque `sdb`
non partitionné en dessous du disque `sda` et de ses trois partitions `sda1`,
`sda2` et `sda3`.

### Partitionner le deuxième disque

Nous utiliserons `fdisk` pour créer une partition de type `Linux` sur le
disque `sdb`&nbsp;:

```
$ sudo fdisk /dev/sdb
```

Appuyez sur ++n++ pour créer une nouvelle partition et confirmez successivement
par ++enter++ toutes les valeurs proposées par défaut&nbsp;:

```
Command (m for help): n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): [Enter]
Using default response p
Partition number (1-4, default 1): [Enter]
First sector (2048-41943039, default 2048): [Enter]
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-41943039, default 41943039): [Enter]
Using default value 41943039
Partition 1 of type Linux and of size 20 GiB is set
```

J'affiche la table des partitions en appuyant sur la touche ++p++
(*print*)&nbsp;:

```
Command (m for help): p

Disk /dev/sdb: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x991dd27b

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    41943039    20970496   83  Linux
```

Ici, je viens de créer une partition de type `Linux` (`83`). J'appuie sur ++w++
(*write*) pour écrire la nouvelle table de partitions sur le disque&nbsp;:

```
Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
```

Je dispose bien d'une nouvelle partition `/dev/sdb1`&nbsp;:

```
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   60G  0 disk
├─sda1   8:1    0  500M  0 part /boot
├─sda2   8:2    0 58.5G  0 part /
└─sda3   8:3    0    1G  0 part [SWAP]
sdb      8:16   0   20G  0 disk
└─sdb1   8:17   0   20G  0 part
sr0     11:0    1 1024M  0 rom
```

### Formater le deuxième disque

Maintenant que mon nouveau disque est partitionné, je peux le formater. Nous
avons vu un peu plus haut que, sous Linux, je dispose de toute une série de
systèmes de fichiers au choix comme `ext2`, `ext3`, `ext4` ou `xfs`. Dans cet
exemple, j'opte pour `ext4`&nbsp;:

```
$ sudo mkfs -t ext4 /dev/sdb1
mke2fs 1.42.9 (28-Dec-2013)
...
```

Une fois que j'ai formaté la partition, je peux éventuellement
l'étiqueter&nbsp;:

```
$ sudo e2label /dev/sdb1 data
```

L'étiquetage n'est pas obligatoire, mais il permet d'organiser les choses un
peu plus proprement&nbsp;:

```
$ ls -l /dev/disk/by-label/
total 0
lrwxrwxrwx. 1 root root 10 Jun  1 09:12 boot -> ../../sda1
lrwxrwxrwx. 1 root root 10 Jun  1 09:27 data -> ../../sdb1
lrwxrwxrwx. 1 root root 10 Jun  1 09:12 root -> ../../sda2
lrwxrwxrwx. 1 root root 10 Jun  1 09:12 swap -> ../../sda3
```

Ensuite, je crée le point de montage `/data` qui me permettra d'accéder aux
données de mon nouveau disque&nbsp;:

```
$ sudo mkdir -v /data
mkdir: created directory ‘/data’
```

Dans un premier temps, je monte le disque manuellement&nbsp;:

```
$ sudo mount -v -t ext4 /dev/sdb1 /data
mount: /dev/sdb1 mounted on /data.
```

> Lors de la première tentative de montage du disque, vous serez probablement
> confronté au message d'erreur suivant&nbsp;: `mount : /data does not contain
> SELinux labels.` SELinux est un mécanisme de sécurité que nous aborderons
> plus loin.  Pour l'instant, vous pouvez résoudre le problème en invoquant la
> commande suivante&nbsp;: `sudo restorecon -R /data`.

J'aurais pu invoquer la commande de montage plus simplement&nbsp;:

```
$ sudo mount /dev/sdb1 /data
```

Et puisque nous avons vu que les UUID et les étiquettes identifient les
partitions sans ambiguïté, nous aurions très bien pu nous y prendre
comme ceci&nbsp;:

```
$ sudo mount LABEL=data /data
```

Dans un cas comme dans l'autre, vérifions si le disque est bien monté&nbsp;:

```
$ mount | grep sdb1
/dev/sdb1 on /data type ext4 (rw,relatime,seclabel,data=ordered)
```

Nous pouvons d'ores et déjà écrire des données sur le deuxième disque.
N'oubliez pas que nous n'avons pas encore défini de permissions particulières
pour cette arborescence. Pour l'instant, `/data` appartient à l'utilisateur
`root` et au groupe `root`&nbsp;:

```
$ cd /data/
$ sudo mkdir repertoiretest
$ sudo touch repertoiretest/fichiertest
```

Vous serez peut-être vaguement surpris de trouver un répertoire `lost+found` à la
racine du disque. C'est tout à fait normal. Linux utilise ce répertoire lors
des tests d'intégrité des systèmes de fichiers.

```
$ ls -l
total 20
drwx------. 2 root root 16384 Jun  1 09:25 lost+found
drwxr-xr-x. 2 root root  4096 Jun  1 09:34 repertoiretest
```

Tout fonctionne comme prévu et nous pouvons démonter le disque à la main&nbsp;:

```
$ cd
$ sudo umount /data
```

Il ne reste plus qu'à éditer `/etc/fstab` en fournissant les informations de ce
nouveau système de fichiers&nbsp;:

```
/dev/sdb1  /data  ext4  defaults  0 2
```

Si l'on veut faire les choses plus proprement, on peut identifier le disque par
son UUID&nbsp;:

```
UUID=c84be805-27d1-4ee3-8b20-f37fa1614a3e /data ext4 defaults 0 2
```

Alternativement, on pourra utiliser l'étiquette du disque&nbsp;:

```
LABEL=data /data ext4 defaults 0 2
```

Croisez les doigts et redémarrez. Vérifiez si le disque a bien été monté
automatiquement au démarrage du système&nbsp;:

```
$ mount | grep sdb1
/dev/sdb1 on /data type ext4 (rw,relatime,seclabel,data=ordered)
```

Le deuxième disque est désormais inclus dans mon système. Dorénavant, je
dispose de 20 Go d'espace supplémentaire dans l'arborescence `/data`&nbsp;:

```
$ df -h | grep sd
/dev/sda2        58G  1.8G   53G   4% /
/dev/sda1       485M  101M  359M  22% /boot
/dev/sdb1        20G   45M   19G   1% /data
```

## Splendeur et misère des tables de partitions MBR

Il y aurait encore beaucoup de choses à dire sur les tables de partitions MBR.
Au lieu de cela, je préfère vous fournir une synthèse rapide des possibilités
et surtout des limitations.

Les disques munis d'un MBR distinguent trois types de partitions&nbsp;:

* primaire

* étendue

* logique

Un disque peut être muni d'un maximum de quatre partitions primaires, ce qui
constitue une limitation considérable. C'est pourquoi on a la possibilité de
définir une partition étendue à la place d'une des primaires. À l'intérieur de
cette partition étendue, on pourra ensuite créer une série de partitions
logiques.

Certains outils de partitionnement – par exemple, le programme d'installation
d'Oracle Linux – ne distinguent pas les différents types de partitions en
surface et gèrent la répartition des types de partitionnement automatiquement
sous le capot.

Une partition étendue ne constitue qu'une sorte de conteneur pour les
partitions logiques. Le stockage des données à proprement parler s'effectue sur
les partitions primaires et/ou logiques. Attention, ne confondez pas le "type
de partitions" avec celui que l'on utilise dans un autre contexte pour indiquer
le système d'exploitation censé occuper la partition ou le rôle joué par la
partition&nbsp;: Windows, Linux, Linux swap, Linux RAID, BSD, etc.

Avec un MBR, Linux peut gérer un maximum de quinze partitions, dont un maximum
de onze logiques. En conséquence, la meilleure solution consiste à créer les
trois primaires dans un premier temps, puis la partition étendue qui occupera
tout le reste du disque et que l'on remplira de partitions logiques selon le
besoin.

La taille maximale d'une telle partition est de deux téraoctets. Il existe une
poignée d'astuces pour utiliser le partitionnement MBR avec des disques allant
jusqu'à quatre téraoctets, mais il vaut mieux éviter de sauter à travers des
cerceaux en feu et passer directement au partitionnement GPT.

## Partitionnement manuel d'un système UEFI

Précédemment, nous avons défini un schéma de partitionnement relativement
simple constitué de trois partitions&nbsp;: `/boot`, `swap` et la principale.
Est-ce que nous pouvons partitionner un système UEFI de la même manière&nbsp;?
Oui, il suffit d'ajouter une partition EFI au début du disque.

> VirtualBox peut émuler un système UEFI. Dans la configuration de la machine
> virtuelle, ouvrez l'onglet `Système`, repérez les `Fonctions avancées` et
> cochez `Activer EFI`.

* Commencez par créer une partition `/boot/efi` et définissez une capacité de
  `200 Mio`.

* Définissez éventuellement l'étiquette `EFI` pour cette partition.

* Créez les autres partitions comme de coutume. 

> Comment savoir à coup sûr si vous disposez d'un système UEFI&nbsp;?
> L'installateur d'Oracle Linux vous le dira. Une fois que vous avez
> sélectionné le partitionnement manuel et que vous définissez le premier point
> de montage, vérifiez la présence d'une entrée `/boot/efi` dans le menu
> déroulant `Point de montage`. Si elle ne s'affiche pas, c'est que vous êtes
> en présence d'un BIOS traditionnel.  

Une fois que le système est installé, voilà comment cela se présente&nbsp;:

```
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   60G  0 disk
├─sda1   8:1    0  200M  0 part /boot/efi
├─sda2   8:2    0  500M  0 part /boot
├─sda3   8:3    0 58,3G  0 part /
└─sda4   8:4    0    1G  0 part [SWAP]
sr0     11:0    1 1024M  0 rom
```

L'installateur a choisi automatiquement une table de partitions GPT pour le
partitionnement&nbsp;:

```
$ sudo fdisk -l /dev/sda 2> /dev/null | grep type
Disk label type: gpt
```

## Ajouter un disque avec une table de partitions GPT

Si je veux ajouter un deuxième disque à cette installation, il va falloir que
j'utilise une table de partitions GPT. Notez que rien ne m'empêche d'utiliser
le partitionnement GPT sur une installation traditionnelle. Dans l'exemple
ci-après, je vais transformer mon deuxième disque MBR en disque GPT.

Tout d'abord, je supprime l'entrée correspondante dans `/etc/fstab`&nbsp;:

```
UUID=c84be805-27d1-4ee3-8b20-f37fa1614a3e /data ext4 defaults 0 2
```

Je démonte le disque manuellement&nbsp;:

```
$ sudo umount /data
```

Je supprime la table de partitions du disque en utilisant la
commande `dd`&nbsp;:

```
$ sudo dd if=/dev/zero of=/dev/sdb bs=512 count=64
64+0 records in
64+0 records out
32768 bytes (33 kB) copied, 0.00715704 s, 4.6 MB/s
```

Pour supprimer la table de partitions, nous la remplissons de zéros. Plus
exactement, nous utilisons `dd` pour écrire 64 blocs de 512 zéros sur le début
du disque `sdb`. Consultez la page de manuel de `dd` pour les détails.

Effectivement, le disque n'affiche plus aucune partition&nbsp;:

```
$ sudo fdisk -l /dev/sdb

Disk /dev/sdb: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
```

Il existe plusieurs solutions pour le partitionnement GPT d'un disque. Pour ma
part, j'ai une préférence marquée pour l'outil `gdisk`, qui fonctionne à peu de
chose près comme `fdisk`, sauf qu'il écrit des tables de partitions GPT.

L'outil de partitionnement `gdisk` ne fait pas partie d'une installation minimale
d'Oracle Linux. Vous pouvez l'installer comme ceci&nbsp;:

```
$ sudo yum install -y gdisk
```

Lancez `gdisk` en fournissant le disque `sdb` en argument&nbsp;:

```
$ sudo gdisk /dev/sdb
GPT fdisk (gdisk) version 0.8.10

Partition table scan:
  MBR: not present
  BSD: not present
  APM: not present
  GPT: not present

Creating new GPT entries.

Command (? for help):
```

La création d'une nouvelle partition se fait comme avec `fdisk`. Appuyez
sur ++n++ et confirmez successivement toutes les valeurs proposées par
défaut&nbsp;:

```
Command (? for help): n
Partition number (1-128, default 1): [Enter]
First sector (34-41943006, default = 2048) or {+-}size{KMGTP}: [Enter]
Last sector (2048-41943006, default = 41943006) or {+-}size{KMGTP}: [Enter]
Current type is 'Linux filesystem'
Hex code or GUID (L to show codes, Enter = 8300): [Enter]
Changed type of partition to 'Linux filesystem'
```

Affichez la table des partitions en appuyant sur la touche ++p++
(*print*)&nbsp;:

```
Command (? for help): p
Disk /dev/sdb: 41943040 sectors, 20.0 GiB
Logical sector size: 512 bytes
Disk identifier (GUID): 84133633-424B-45F7-9F5B-4466BFAF32BA
Partition table holds up to 128 entries
First usable sector is 34, last usable sector is 41943006
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048        41943006   20.0 GiB    8300  Linux filesystem
```

Nous venons de créer une partition de type `Linux` (`8300`). Appuyons sur ++w++
(*write*) pour l'écrire sur le disque&nbsp;:

```
Command (? for help): w

Final checks complete. About to write GPT data. THIS WILL OVERWRITE EXISTING
PARTITIONS!!

Do you want to proceed? (Y/N): y
OK; writing new GUID partition table (GPT) to /dev/sdb.
The operation has completed successfully.
```

Notre deuxième disque dispose bel et bien d'une table de partitions GPT&nbsp;:

```
$ sudo fdisk -l /dev/sdb 2> /dev/null | grep type
Disk label type: gpt
```

> Vous aurez probablement noté que la partition disposait d'un identifiant
> `8300`, par opposition à l'identifiant `83` pour le partitionnement MBR.

À partir de là, nous pouvons formater, étiqueter et monter notre disque GPT
exactement comme nous l'avons fait pour le disque MBR&nbsp;:

```
$ sudo mkfs -t ext4 /dev/sdb1
$ sudo e2label /dev/sdb1 data
$ sudo mount /dev/sdb1 /data
...
```

## Les GUID Partition Tables (GPT)

Avec les tables GPT, chacune des partitions est identifiée grâce à un *Global
Unique Identifier* (GUID). Une table GPT offre théoriquement de la place pour
128 partitions, mais Linux n'en gère que les 15 premières. Toutes les
partitions sont égales, c'est-à-dire que l'on ne fait plus la distinction entre
les partitions primaires, étendues et logiques. Chaque partition peut avoir une
taille théorique de 273 octets, autrement dit près d'un milliard de téraoctets.
Cela devrait suffire pour quelque temps.

La table de partitions se situe dans les premiers 32× 512 = 17408 octets du
disque dur. Une copie de ces informations se situe dans les 17 derniers
kilo-octets du disque.

Pour des raisons de sécurité, la table de partitions GPT commence par des
informations de partitionnement MBR pour suggérer aux outils compatibles MBR
que l'ensemble du disque est déjà utilisé par une seule partition.

En principe, le partitionnement GPT peut être utilisé sur tous (!) les disques
durs. Tous les systèmes d'exploitation modernes savent gérer ce type de
partitionnement&nbsp;:

* les distributions Linux actuelles&nbsp;;

* macOS&nbsp;;

* toutes les versions 64-bits de Windows depuis XP.

## Supprimer une table de partitions GPT

Nous avons vu précédemment que, pour supprimer une table de partitions MBR, il
suffisait d'écrire directement une série de zéros – ou n'importe quelle série
d'octets aléatoire – sur le premier secteur du disque. Cette procédure ne
fonctionne pas avec les tables de partitions GPT. Si nous voulons réinitialiser
un disque GPT – pour appliquer un partitionnement MBR par exemple – nous devons
nous y prendre autrement.

La manière la plus simple consiste à utiliser l'outil `gdisk`. Assurez-vous que
le disque n'est plus monté, puis ouvrez-le&nbsp;:

```
$ sudo gdisk /dev/sdb
GPT fdisk (gdisk) version 0.8.6

Partition table scan:
  MBR: protective
  BSD: not present
  APM: not present
  GPT: present

Found valid GPT with protective MBR; using GPT.

Command (? for help):
```

La touche ++x++ nous donne accès aux fonctionnalités avancées de `gdisk`&nbsp;:

```
Command (? for help): x

Expert command (? for help):
```

Une fois que le mode expert est activé, la touche ++z++ (`zap GPT data
structures and exit`) supprime la table de partitions&nbsp;:

```
Expert command (? for help): z
About to wipe out GPT on /dev/sdb. Proceed? (Y/N): y
GPT data structures destroyed! You may now partition the disk using fdisk or other utilities.
Blank out MBR? (Y/N): y
```

À partir de là, le disque est proprement réinitialisé&nbsp;:

```
$ sudo gdisk -l /dev/sdb
GPT fdisk (gdisk) version 0.8.6

Partition table scan:
  MBR: not present
  BSD: not present
  APM: not present
  GPT: not present
```

## Formater un disque dur externe ou une clé USB

Dans le [précédent atelier pratique](peripheriques.md), nous avons vu que les
clés USB sont normalement formatées en FAT, un système de fichiers géré aussi
bien par Microsoft Windows que par macOS ou Linux.

Dans mon quotidien professionnel, il m'arrive de temps en temps qu'un client
vienne me voir parce qu'il n'arrive pas à accéder à sa clé USB ou à son disque
dur externe. Voici les deux cas de figure les plus fréquents auxquels je suis
confronté :

* La clé USB est formatée en NTFS et n'est pas accessible sous macOS.

* Le disque dur externe a été formaté en HFS+ sous macOS et n'est pas
  accessible sous Microsoft Windows.

Le meilleur moyen pour rétablir l'interopérabilité entre les systèmes, c'est de
vider les données du disque externe sur une machine capable de le gérer avant
de le reformater de manière appropriée.

Dans l'exemple qui suit, je dispose d'une clé USB d'une capacité nominale de
8&nbsp;Go, que je souhaite formater de manière appropriée. Voyons comment les
choses se présentent&nbsp;:

```
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0 55.9G  0 disk
├─sda1   8:1    0  500M  0 part /boot
├─sda2   8:2    0 51.4G  0 part /
└─sda3   8:3    0    4G  0 part [SWAP]
sdb      8:16   1  7,2G  0 disk
├─sdb1   8:17   1  1,7G  0 part
└─sdb2   8:18   1  2,3M  0 part
sr0     11:0    1 1024M  0 rom
```

La clé USB `sdb` comporte deux partitions `sdb1` et `sdb2`. Je pourrais
théoriquement m'intéresser au détail des partitions en invoquant `sudo
fdisk -l /dev/sdb`. Au lieu de cela, je vais simplement supprimer la table de
partitions de la clé pour commencer sur des bases saines&nbsp;:

```
$ sudo dd if=/dev/zero of=/dev/sdb bs=512 count=64
64+0 records in
64+0 records out
32768 bytes (33 kB) copied, 1.33173 s, 24.6 kB/s
```

À partir de là, j'invoque `fdisk` pour créer une nouvelle table de partitions
sur la clé&nbsp;:

```
$ sudo fdisk /dev/sdb
```

Je crée une nouvelle partition en confirmant les valeurs proposées par défaut :

```
Command (m for help): n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): [Enter]
Using default response p
Partition number (1-4, default 1): [Enter]
First sector (2048-15132671, default 2048): [Enter]
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-15132671, default 15132671): [Enter]
Using default value 15132671
Partition 1 of type Linux and of size 7.2 GiB is set
```

J'affiche la table de partitions&nbsp;:

```
Command (m for help): p

Disk /dev/sdb: 7747 MB, 7747928064 bytes, 15132672 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x6b3b2f65

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    15132671     7565312   83  Linux
```

Dans la configuration par défaut, `fdisk` crée des partitions de type
`Linux` (`83`).  Je vais changer le type de ma partition grâce à la
touche ++t++ (*type*)&nbsp;:

```
Command (m for help): t
Selected partition 1
Hex code (type L to list all codes):
```

Si j'appuie sur ++l++ (*list*), `fdisk` m'affiche l'ensemble des types de
partitions qu'il est capable de gérer. Essayez, vous verrez qu'il en existe une
quantité impressionnante. Le type qui nous intéresse ici, c'est `W95 FAT32`, ou
encore `0b` pour les intimes&nbsp;:

```
Hex code (type L to list all codes): 0b
Changed type of partition 'Linux' to 'W95 FAT32'

Command (m for help): p

Disk /dev/sdb: 7747 MB, 7747928064 bytes, 15132672 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x6b3b2f65

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    15132671     7565312    b  W95 FAT32
```
À présent, je peux écrire les modifications sur le disque&nbsp;:

```
Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
```

Nous avons fait la moitié du chemin. La clé USB dispose désormais d'une
partition de type `W95 FAT32`. Il ne reste plus qu'à la formater. Nous
utiliserons la commande `mkdosfs` fournie par le paquet `dosfstools`. Installez
ce paquet (`sudo yum install -y dosfstools`), puis formatez la clé&nbsp;:

```
$ sudo mkdosfs -v -n MYDATA /dev/sdb1
mkfs.fat 3.0.20 (12 Jun 2013)
Auto-selecting FAT32 for large filesystem
/dev/sdb1 has 239 heads and 62 sectors per track,
logical sector size is 512,
using 0xf8 media descriptor, with 15130624 sectors;
filesystem has 2 32-bit FATs and 8 sectors per cluster.
FAT size is 14748 sectors, and provides 1887637 clusters.
There are 32 reserved sectors.
Volume ID is 2c90b033, volume label MYDATA.
```

À partir de là, vous pouvez utiliser la clé comme nous l'avons vu dans le
chapitre précédent&nbsp;:

```
$ sudo mount -v /dev/sdb1 /mnt
mount: /dev/sdb1 mounted on /mnt.
$ mount | grep sdb
/dev/sdb1 on /mnt type vfat (rw,relatime,...)
```

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


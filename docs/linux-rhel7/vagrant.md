# Vagrant et VirtualBox

[VirtualBox](virtualbox.md) facilite considérablement la tâche lorsqu'on a
besoin d'une ou plusieurs machines de test pour les travaux pratiques lors
d'une formation. Il n'en demeure pas moins que l'installation et la
configuration post-installation d'une machine virtuelle reste une activité
relativement chronophage. Pour installer une machine virtuelle basée sur une
distribution Linux minimale, il faut à chaque fois récupérer le fichier ISO de
l'installateur, créer et configurer une machine virtuelle et suivre toutes les
étapes du programme d'installation.

C'est là où [Vagrant](https://www.vagrantup.com/) entre en jeu. Vagrant est une
surcouche en ligne de commande à toute une panoplie d'hyperviseurs comme
HyperV, KVM, Parallels, VMWare et VirtualBox. Le grand avantage de Vagrant,
c'est qu'il permet la mise en place d'une machine virtuelle "jetable" en deux
minutes chrono. 

> Dans l'atelier pratique ci-dessous, nous utiliserons Vagrant conjointement
> avec VirtualBox.

## Installation

### Sous OpenSUSE Leap 15.3

* Ouvrez YaST&nbsp;: `Applications` > `Système` > `YaST`.

* Dans l'onglet `Logiciel` cliquez sur `Installer et supprimer des logiciels`.

* Cherchez `vagrant` dans le champ de recherche.

* Cochez le paquet `vagrant` dans la liste des résultats.

* Vérifiez si les paquets `vagrant-vim` et `vagrant-bash-completion` sont
  cochés automatiquement.

* Décochez le paquet `vagrant-libvirt`.

* Cliquez sur `Accepter`.

* Confirmez l'installation des dépendances en cliquant sur `Continuer`.

* Patientez pendant l'installation de Vagrant et cliquez sur `Terminer`.

Alternativement, vous pouvez très bien installer Vagrant en ligne de commande
comme ceci&nbsp;:

```
# zypper refresh
# zypper install --no-recommends vagrant vagrant-vim vagrant-bash-completion
```

## Premier test

Je ne dispose pas encore de systèmes installables sur ma machine&nbsp;:

```
$ vagrant box list
There are no installed boxes! Use `vagrant box add` to add some.
```

> Il se peut qu'à la toute première utilisation, Vagrant vous informe qu'une
> nouvelle version est disponible au téléchargement. Vous pouvez sereinement
> ignorer cet avertissement.

Pour commencer, je vais installer une image d'Alpine Linux, un système léger
dont la taille réduite permet un téléchargement rapide&nbsp;:

```
$ vagrant box add roboxes/alpine38
==> box: Loading metadata for box 'roboxes/alpine38'
    box: URL: https://vagrantcloud.com/roboxes/alpine38
```

Les images diffèrent selon le système de virtualisation utilisé. Je récupère
l'image pour VirtualBox&nbsp;:

```
This box can work with multiple providers! The providers that it
can work with are listed below. Please review the list and choose
the provider you will be working with.

1) hyperv
2) libvirt
3) parallels
4) virtualbox
5) vmware_desktop

Enter your choice: 4
```

Je crée une arborescence de test dans mon répertoire utilisateur&nbsp;:

```
$ mkdir -pv ~/Vagrant/Alpine
mkdir: création du répertoire '/home/microlinux/Vagrant'
mkdir: création du répertoire '/home/microlinux/Vagrant/Alpine'
$ cd ~/Vagrant/Alpine/
```

J'initialise mon système Alpine Linux&nbsp;:

```
$ vagrant init roboxes/alpine38
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

La dernière commande a généré un fichier `Vagrantfile` dans mon répertoire
`~/Vagrant/Alpine`. Pour l’instant, ce fichier est constitué majoritairement
d'options commentées. Je peux l'éditer et le simplifier comme ceci&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "roboxes/alpine38"
end
```

Je lance ma machine virtuelle&nbsp;:

```
$ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'roboxes/alpine38'...
==> default: Matching MAC address for NAT networking...
...
```

Une fois qu'elle a démarré, je peux ouvrir une session SSH comme ceci&nbsp;:

```
$ vagrant ssh
alpine38:~$
```

Je suis bien dans un système Alpine Linux&nbsp;:

```
alpine38:~$ cat /etc/os-release
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.8.5
PRETTY_NAME="Alpine Linux v3.8"
HOME_URL="http://alpinelinux.org"
BUG_REPORT_URL="http://bugs.alpinelinux.org"
```

Dans cette machine virtuelle, je suis l'utilisateur `vagrant`&nbsp;:

```
alpine38:~$ whoami
vagrant
```

À partir de là, je peux invoquer `sudo` pour les tâches administratives
courantes&nbsp;:

```
alpine38:~$ sudo apk update
fetch http://sjc.edge.kernel.org/alpine/v3.8/main/x86_64/APKINDEX.tar.gz
fetch http://sjc.edge.kernel.org/alpine/v3.8/community/x86_64/APKINDEX.tar.gz
v3.8.5-67-gf94de196ca [http://sjc.edge.kernel.org/alpine/v3.8/main]
v3.8.5-66-gccbd6a8ae7 [http://sjc.edge.kernel.org/alpine/v3.8/community]
OK: 9563 distinct packages available
```

Je quitte ma machine virtuelle comme n’importe quelle session SSH&nbsp;:

```
alpine38:~$ exit
logout
Connection to 127.0.0.1 closed.
```

Il ne me reste plus qu’à arrêter la machine virtuelle&nbsp;:

```
$ vagrant halt
==> default: Attempting graceful shutdown of VM...
```

Si je n'ai plus besoin de cette machine, je peux la supprimer&nbsp;:

```
$ vagrant destroy
    default: Are you sure you want to destroy the 'default' VM? [y/N] y
==> default: Destroying VM and associated drives...
```

Et si je n'ai plus du tout l'intention de travailler avec Alpine Linux, je peux
même supprimer l'image correspondante de mon système&nbsp;:

```
$ vagrant box remove roboxes/alpine38
Removing box 'roboxes/alpine38' (v3.6.12) with provider 'virtualbox'...
```

## Trois OS en deux minutes

Imaginons que vous ayez besoin de vérifier le fonctionnement d'une commande
sous Red Hat Enterprise Linux, Oracle Linux et CentOS. Pour commencer,
récupérez les images des trois systèmes&nbsp;:

```
$ vagrant box add roboxes/rhel7
$ vagrant box add roboxes/oracle7
$ vagrant box add roboxes/centos7
```

> Ces images fonctionnent avec plusieurs hyperviseurs. Assurez-vous à chaque
> fois de sélectionner `virtualbox`.

Créez un répertoire pour chacun des systèmes virtualisés&nbsp;:

```
$ mkdir -pv ~/Vagrant/{RHEL,Oracle,CentOS}
mkdir: création du répertoire '/home/microlinux/Vagrant/RHEL'
mkdir: création du répertoire '/home/microlinux/Vagrant/Oracle'
mkdir: création du répertoire '/home/microlinux/Vagrant/CentOS'
```

Initialisez Red Hat Enterprise Linux&nbsp;:

```
$ cd ~/Vagrant/RHEL/
$ vagrant init roboxes/rhel7
```

Éditez éventuellement le fichier `Vagrantfile` pour le rendre plus
lisible&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "roboxes/rhel7"
end
```

Procédez de même pour les machines virtuelles sous Oracle Linux et CentOS.

Partant de là, lancez chaque machine individuellement, connectez-vous, affichez
le contenu du fichier `/etc/os-release`, déconnectez-vous, arrêtez la machine
virtuelle et supprimez-la lorsque vous n'en avez plus besoin. 

## Trouver des machines

Le [site de Vagrant](https://www.vagrantup.com/) permet de rechercher et
d’essayer un nombre assez conséquent de machines virtuelles. Cliquez sur le
bouton [Find Boxes](https://app.vagrantup.com/boxes/search) sur la page
d’accueil du projet et cherchez le ou les systèmes dont vous avez besoin pour
vos tests.

Alternativement, allez jeter un œil sur la page du projet
[Roboxes](https://roboxes.org/). C'est là où je récupère toutes les images de
base pour mes tests et mes ateliers pratiques.

## Monter un cluster virtuel

Certains ateliers pratiques - comme par exemple l'orchestration avec Docker
Swarm - nécessitent d'avoir un *cluster* de serveurs sous la main. Dans
l'atelier pratique ci-dessous, nous allons mettre en place trois machines
virtuelles dotées d'un système Oracle Linux minimal&nbsp;:

* Machine n° 1&nbsp;: `server-01` / `10.23.45.10`

* Machine n° 2&nbsp;: `server-02` / `10.23.45.20`

* Machine n° 3&nbsp;: `server-03` / `10.23.45.30`

Récupérez l'image d'Oracle Linux pour VirtualBox si ce n'est pas déjà
fait&nbsp;:

```
$ vagrant box add roboxes/oracle7
```

Créez l'arborescence de répertoires pour les machines virtuelles&nbsp;:

```
$ mkdir -pv ~/Vagrant/Cluster/server-{01,02,03}
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster'
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster/server-01'
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster/server-02'
mkdir: création du répertoire '/home/microlinux/Vagrant/Cluster/server-03'
```

Éditer un fichier `/etc/vbox/networks.conf` pour autoriser la création de
réseaux privés virtuels&nbsp;:

```
# /etc/vbox/networks.conf
* 0.0.0.0/0 ::/0
```

Rendez-vous dans le répertoire correspondant à la première machine
virtuelle&nbsp;:

```
$ cd ~/Vagrant/Cluster/server-01/
```

Initialisez la machine virtuelle&nbsp;:

```
$ vagrant init roboxes/oracle7
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

Cette opération a créé un fichier `Vagrantfile` dans le répertoire courant.
Éditez ce fichier comme suit&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "roboxes/oracle7"
  config.vm.hostname = "server-01"
  config.vm.network "private_network",
    ip: "10.23.45.10"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "server-01"
    vb.memory = 512
  end
end
```

> Notez qu'ici j'ai limité la RAM disponible pour la VM à 512 Mo, ce qui est
> largement suffisant pour un serveur Linux sans système graphique. Si vous
> disposez de plus de ressources sur le système hôte, vous pourrez remplacer
> 512 par 1024 ou 2048.

Lancez la machine `server-01`, connectez-vous et éditez le fichier `/etc/hosts`
en tant que `root`&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
10.23.45.10 server-01
10.23.45.20 server-02
10.23.45.30 server-03
```

Déconnectez-vous de la VM, rendez-vous dans le répertoire `Cluster/server-02`
et éditez le fichier `Vagrantfile` correspondant&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "roboxes/oracle7"
  config.vm.hostname = "server-02"
  config.vm.network "private_network",
    ip: "10.23.45.20"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "server-02"
    vb.memory = 512
  end
end
```

Lancez la machine `server-02`, connectez-vous et éditez le fichier `/etc/hosts`
en tant que `root`&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
10.23.45.10 server-01
10.23.45.20 server-02
10.23.45.30 server-03
```

Déconnectez-vous de la VM, rendez-vous dans le répertoire `Cluster/server-03`
et éditez le fichier `Vagrantfile` correspondant&nbsp;:

```
Vagrant.configure("2") do |config|
  config.vm.box = "roboxes/oracle7"
  config.vm.hostname = "server-03"
  config.vm.network "private_network",
    ip: "10.23.45.30"
  config.vm.provider "virtualbox" do |vb|
    vb.name = "server-03"
    vb.memory = 512
  end
end
```

Lancez la machine `server-03`, connectez-vous et éditez le fichier `/etc/hosts`
en tant que `root`&nbsp;:

```
# /etc/hosts
127.0.0.1   localhost.localdomain localhost
10.23.45.10 server-01
10.23.45.20 server-02
10.23.45.30 server-03
```

Déconnectez-vous de la VM et affichez la vue d'ensemble&nbsp;:

```
$ vagrant global-status
id      name    provider   state   directory
-----------------------------------------------------------------------------
57d530f default virtualbox running /home/microlinux/Vagrant/Cluster/server-01
cb11736 default virtualbox running /home/microlinux/Vagrant/Cluster/server-02
34d7a55 default virtualbox running /home/microlinux/Vagrant/Cluster/server-03
```

Partant de là, connectez-vous successivement à chacune des machines virtuelles
et testez la connectivité avec les deux autres&nbsp;:

```
[vagrant@server-01 ~]$ hostname
server-01
[vagrant@server-01 ~]$ ping -c 1 server-02
PING server-02 (10.23.45.20) 56(84) bytes of data.
64 bytes from server-02 (10.23.45.20): icmp_seq=1 ttl=64 time=0.714 ms

--- server-02 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.714/0.714/0.714/0.000 ms
[vagrant@server-01 ~]$ ping -c 1 server-03
PING server-03 (10.23.45.30) 56(84) bytes of data.
64 bytes from server-03 (10.23.45.30): icmp_seq=1 ttl=64 time=1.29 ms

--- server-03 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.291/1.291/1.291/0.000 ms
```

Votre cluster local virtualisé est prêt à être utilisé.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


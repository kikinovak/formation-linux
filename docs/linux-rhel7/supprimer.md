
**Objectif** : prise en main des opérations de suppression de fichiers et de
répertoires.

## Gare aux armes de destruction massive

La commande `rm` (comme *remove*) sert à supprimer des fichiers et des
arborescences de répertoires. Accessoirement, elle vous autorise à vous tirer
dans le pied, car elle est capable d'anéantir des dizaines de sites web, des
années de courriels archivés, voire un serveur entier en un tournemain. Dans la
panoplie des outils Unix, `rm` fait partie des instruments affûtés et
tranchants qu'il convient de manier avec précaution.

Pour supprimer un fichier, il suffit de spécifier son nom en argument&nbsp;:

```
$ rm bonjour.txt
```

Vous n'obtiendrez pas de demande de confirmation du genre `Êtes-vous sûr de` ou
autres `Voulez-vous vraiment`. Votre système Linux n'a rien d'une nounou qui
vous prend par la main. Vous lui avez ordonné de supprimer le fichier
`bonjour.txt` et c'est sans broncher qu'il s'est exécuté pour l'envoyer au
paradis des octets.  Ici, vous ne trouvez pas de Corbeille non plus, où vous
auriez pu repêcher vos données malencontreusement supprimées.

> Sur un poste de travail Linux, les environnements de bureau comme KDE, GNOME,
> Xfce ou MATE disposent bien d'une Corbeille qui permet de repêcher des
> fichiers supprimés en mode graphique par le biais du navigateur de fichiers.
> En revanche, un fichier supprimé en ligne de commande sera perdu à jamais.

## Travailler avec ou sans filet ?

Si ces manières expéditives vous mettent mal à l'aise, vous pouvez toujours
invoquer `rm` avec l'option `-i` (comme *interactive*), ce qui produit une
demande de confirmation avant chaque destruction de fichier. Tapez ++y++ pour
répondre *yes*&nbsp;:

```
$ rm -i bonjour2.txt 
rm: remove regular file 'bonjour2.txt'? y
$ rm -i bonjour3.txt 
rm: remove regular file 'bonjour3.txt'? y
```

Ce fonctionnement peut être implémenté par défaut grâce à ce qu'on appelle un
alias de commande. Ouvrons une petite parenthèse sur les alias.

### Les alias de commande 

Invoquez la commande suivante&nbsp;:

```
$ alias
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias which='alias | /usr/bin/which --tty-only –read-alias 
  --show-dot --show-tilde'
```

Ici, chaque entrée commençant par `alias` correspond à la définition d'un
raccourci de commandes, le but du jeu étant visiblement d'ajouter un peu de
couleur ou de vous simplifier l'utilisation de ces quelques commandes. Vous
vous apercevez que le simple `ls` que vous avez pu invoquer jusqu'ici est
enrichi par défaut avec `--color=auto`.

La plupart des distributions Linux grand public prédéfinissent plusieurs alias
de commandes, afin de rendre l'utilisation du *shell* un peu plus agréable.
Essayons d'en définir un nous-même :

```
$ alias rm='rm -i'
$ touch fichiertest
$ rm fichiertest 
rm: remove regular empty file 'fichiertest'? y
```

Il se peut que vous soyez confronté au cas de figure inverse, c'est-à-dire un
alias de commande `rm -i` qui serait défini pour `rm` dans votre environnement de
travail, alors que vous souhaitiez supprimer des fichiers directement, sans
avoir à passer par la confirmation de suppression. Dans ce cas, utilisez `rm`
avec l'option `-f` comme *force*&nbsp;:

```
$ alias rm
alias rm='rm -i'
$ touch fichierbidon
$ rm -f fichierbidon
```

> Notre définition d'alias n'est pas persistante. Autrement dit, lorsque vous
> démarrerez une nouvelle session, en vous déconnectant et en vous reconnectant
> par exemple, votre alias individualisé aura disparu. La personnalisation
> persistante du *shell* sera abordée plus loin.

## Supprimer des répertoires avec rmdir

De façon analogue à la commande `rm`, `rmdir` (*remove directory*) sert à
supprimer des répertoires du système de fichiers&nbsp;:

```
$ mkdir repertoiretest
$ ls -ld repertoiretest
drwxrwxr-x. 2 vagrant vagrant 6 Oct 17 05:42 repertoiretest/
$ rmdir repertoiretest
```

Le répertoire que vous souhaitez supprimer doit impérativement être vide. Dans
le cas contraire, `rmdir` refuse de s'exécuter et vous obtenez un message
d'erreur&nbsp;:

```
$ mkdir repertoiretest
$ touch repertoiretest/fichiertest
$ rmdir repertoiretest/
rmdir: failed to remove 'repertoiretest/': Directory not empty
```

Dans ce cas, c'est-à-dire si l'on souhaite supprimer un répertoire ainsi que
tout son contenu, on peut avoir recours à la commande `rm` suivie de
l'option `-r` (comme *recursive*). Dans l'exemple suivant, j'ajoute
l'option `-i` pour bien expliciter chaque opération de suppression&nbsp;:

```
$ tree repertoiretest 
repertoiretest
`-- fichiertest
$ rm -ri repertoiretest 
rm: descend into directory 'repertoiretest'? y
rm: remove regular empty file 'repertoiretest/fichiertest'? y
rm: remove directory 'repertoiretest'? y
```

Dans la pratique quotidienne, c'est plutôt l'inverse que l'on souhaite faire.
Il est souvent fastidieux de confirmer chaque suppression dans une arborescence
de répertoires. Dans certains cas de figure, c'est même impossible. Lorsque
vous décidez par exemple de nettoyer vos anciens répertoires de sources du
noyau, il vous faudrait confirmer quelques dizaines de milliers d'opérations de
suppression. C'est là que l'option `-f` (*force*) intervient&nbsp;:

```
$ alias rm
alias rm='rm -i'
$ mkdir repertoiretest
$ touch repertoiretest/fichiertest
$ rm -rf repertoiretest
```

Comme l'exemple précédent vous le montre, `rm -rf` ne vous demande pas votre
avis et anéantit joyeusement tout ce que vous spécifiez, à condition que vous
en ayez le droit, bien sûr. Invoquée en tant que `root`, la commande `rm -rf`
peut même vous faire commettre l'équivalent numérique d'un Seppuku. 

## Un coup d'essuie-glace avec clear

J'en profite pour vous montrer l'équivalent d'un coup d'essuie-glace dans votre
terminal. Remplissez ce dernier avec n'importe quelle commande susceptible de
bien l'encombrer (par exemple `ls /etc`), puis essayez ceci&nbsp;:

```
$ clear
```

Pour aller plus vite, utilisez simplement le raccourci clavier ++ctrl+l++, ce
qui revient au même.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


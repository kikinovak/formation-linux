
**Objectif** : prise en main de la création de fichiers et de répertoires.

## Modifier l'horodatage d'un fichier avec touch

L'affichage détaillé de `ls` avec l'option `-l` nous montre que chaque fichier
est horodaté. Prenons par exemple le fichier `~/.bashrc`&nbsp;:

```
$ ls -l .bashrc
-rw-r--r--. 1 microlinux microlinux 231 Apr  1  2020 .bashrc
```

En l'occurrence, ce fichier a été créé – ou modifié pour la dernière fois – le
1er avril 2020. Maintenant, essayons ce qui suit&nbsp;:

```
$ touch .bashrc
$ ls -l .bashrc
-rw-r--r--. 1 microlinux microlinux 231 May  7 06:21 .bashrc
```

Nous constatons que l'horodatage du fichier indique maintenant le 7 mai à
06h21. En effet, cela correspond à la date et à l'heure auxquelles j'écris ces
lignes.

## Créer un fichier vide avec touch

Si le fichier spécifié n'existe pas, `touch` prend soin de le créer. Essayons
avec un nom de fichier qui n'existe pas dans le répertoire courant&nbsp;:

```
$ touch exemple.txt
$ ls -l exemple.txt 
-rw-rw-r--. 1 microlinux microlinux 0 May  7 06:33 exemple.txt
```

Ici, la commande `touch` a créé un fichier vide `exemple.txt` d'une taille de
0 octet.

## Créer un fichier texte sans éditeur de texte

### Avec cat

Voici une méthode pour créer un fichier texte simple, à l'aide de la seule
commande `cat`&nbsp;:

```
$ cat > ~/livres.txt << EOF
> Alice au pays des merveilles
> La montagne magique
> Faust
> EOF
$ ls -l livres.txt 
-rw-rw-r--. 1 microlinux microlinux 55 May  7 06:37 livres.txt
$ cat livres.txt
Alice au pays des merveilles
La montagne magique
Faust
```

Nous avons écrit trois lignes de texte dans un fichier `~/livres.txt`.
N'oubliez pas que le symbole tilde `~` représente ici le répertoire
utilisateur, en l'occurrence `/home/microlinux`. La suite de caractères `EOF`
(comme *End Of File*) définit la fin du fichier.

### Avec echo

Aurions-nous pu obtenir quelque chose de comparable avec la commande
`echo`&nbsp;? Essayons&nbsp;:

```
$ echo Beethoven > compositeurs.txt
$ cat compositeurs.txt
Beethoven
```

La commande `echo` a créé ici un nouveau fichier `compositeurs.txt` en y
écrivant une ligne `Beethoven`. Jusqu'ici, cela ressemble beaucoup à ce que
nous avons fait auparavant avec `bonjour.txt`. Maintenant, essayons ceci&nbsp;:

```
$ echo Bach > compositeurs.txt
$ cat compositeurs.txt
Bach
```

Ce n'était donc pas la bonne méthode pour ajouter une ligne à notre fichier. Le
dernier contenu en date a tout simplement écrasé l'ancien. Nous allons donc
nous y prendre autrement&nbsp;:

```
$ echo Bartok >> compositeurs.txt
$ cat compositeurs.txt
Bach
Bartok
```

Voilà qui est mieux. L'utilisation du chevron `>>` au lieu de la simple
flèche `>` a provoqué l'ajout de la chaîne de caractères à la fin du fichier,
en évitant la substitution du contenu précédent. Si nous souhaitons ajouter un
troisième nom à la liste, il devrait donc suffire de répéter la dernière
commande en insérant un autre nom. Essayons&nbsp;:

```
$ echo Schubert >> compositeurs.txt
$ cat compositeurs.txt
Bach
Bartok
Schubert
```

Effectivement, c'est bien cela. Soit dit en passant, nous en avons profité pour
avoir un autre petit aperçu de la redirection sous Linux. Passons maintenant à
la création de répertoires.

## Créer des répertoires avec mkdir

La commande `mkdir` (comme *make directory*, vous aurez remarqué que les
informaticiens ont un problème avec les voyelles) sert à créer un nouveau
répertoire, dont on spécifie le nom. Créons un répertoire `Documents` dans
notre répertoire utilisateur&nbsp;:

```
$ mkdir Documents
$ ls -ld Documents 
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:46 Documents
```

Il est également possible de spécifier le chemin complet du répertoire à créer.
Pour créer un répertoire `Images` à l'intérieur de `/home/microlinux`, je
pourrais le faire comme suit&nbsp;:

```
$ mkdir /home/microlinux/Images
$ ls -ld Images
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:48 Images
```

Bien évidemment, dans cet exemple, il vous faudra éventuellement remplacer
`microlinux` dans le chemin par votre nom d'utilisateur. D'ailleurs, pour être
sûr que c'est bien dans le répertoire utilisateur que l'on crée le dossier,
nous aurions pu écrire la commande suivante&nbsp;:

```
$ mkdir ~/Images
```

## Créer une série de répertoires

Admettons qu'à l'intérieur du répertoire `~/Images`, nous souhaitions créer
trois sous-répertoires `Photos`, `Graphismes` et `Captures`. Nous procéderions
de la façon suivante&nbsp;:

```
$ cd ~/Images
$ mkdir Photos Graphismes Captures
$ ls -l
total 0
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:51 Captures
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:51 Graphismes
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:51 Photos
```

Ce dernier exemple appelle deux remarques&nbsp;: 

* D'une part, il est tout à fait possible de créer une série de répertoires à
  la louche. Il suffit de spécifier leurs noms respectifs en argument, séparés
  par des espaces. 

* D'autre part, notez bien le `d` comme *directory* en tête des attributs
  complets (`drwxrwxr-x`), qui signifie que nous avons affaire à des
  répertoires.

## Gare aux espaces&nbsp;!

N'oublions pas de dire deux mots sur un détail important qui constitue une
source d'erreur fréquente&nbsp;: les espaces dans les noms de fichiers et de
répertoires. 

* Dans certains cas de figure (sur les serveurs, par exemple, ou dans les
  réseaux hétérogènes, c'est-à-dire composés de machines dotées de systèmes
  d'exploitation différents), il vaut mieux tout faire pour les éviter. 

* Dans d'autres cas, il est tout à fait possible de les utiliser, à condition
  d'être sûr de ce que l'on fait. 

Je vous donne un exemple pour vous sensibiliser à la problématique. Retournez
dans votre répertoire d'utilisateur (`cd` sans argument), créez un répertoire
`Test` et, à l'intérieur de ce dernier, créez un répertoire `Mes Documents`,
dont le nom vous semblera vaguement familier si vous venez de Windows&nbsp;:

```
$ mkdir Test
$ cd Test
$ mkdir Mes Documents
$ ls -l
total 0
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:56 Documents
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:56 Mes
```

Vous voyez le problème. La commande `mkdir` nous a créé deux répertoires
distincts, `Mes` et `Documents`. Ce n'est pas ce que nous voulions faire.

Prenons un autre exemple pour voir comment nous aurions pu nous y prendre.
Revenons dans notre répertoire d'utilisateur, créons un répertoire `Test2` et à
l'intérieur, essayons de créer trois répertoires distincts `Mes Documents`,
`Mes Images` et `Mes Films`&nbsp;:

```
$ cd
$ mkdir Test2
$ cd Test2
$ mkdir "Mes Documents"
$ mkdir 'Mes Images'
$ mkdir Mes\ Films
$ ls -l
total 0
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:59 Mes Documents
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:59 Mes Films
drwxrwxr-x. 2 microlinux microlinux 6 Oct 15 06:59 Mes Images
```

Cette fois-ci, nous avons bien obtenu le résultat escompté. Vous aurez
certainement remarqué que pour chacun des trois répertoires, je me suis servi
d'une syntaxe différente, en utilisant respectivement des guillemets doubles,
des guillemets simples et un caractère d'échappement devant l'espace.

## Un peu de pratique

Je vous propose de souffler un peu en faisant un petit exercice de
révision&nbsp;:

* Dans votre répertoire d'utilisateur, créez un dossier `Fichiers`&nbsp;;

* À l'intérieur de ce dernier, créez trois sous-répertoires `Documents`,
  `Images` et `Films`&nbsp;;.

* Créez-y trois fichiers vides nommés respectivement `texte.txt`, `photo.jpg`
  et `film.avi`.

## Les arborescences en un coup d'oeil avec tree

Puisque nous sommes en plein dans les arborescences de répertoires, le moment
est venu de vous présenter un cousin lointain de `ls`, la commande `tree`.
Curieusement, on ne la rencontre pas souvent dans les manuels d'initiation à la
ligne de commande sous Linux.

### Installer la commande tree

La commande `tree` ne fait pas partie de notre système minimal, mais il est
facile de l'installer, en anticipant quelque peu sur l'atelier qui traite de la
gestion des logiciels.

Vérifiez si vous êtes bien connectés à Internet et si la source de
téléchargement est joignable&nbsp;:

```
$ ping -c 3 yum.oracle.com
...
... icmp_seq=1 ttl=63 time=30.7 ms
... icmp_seq=2 ttl=63 time=30.9 ms
... icmp_seq=3 ttl=63 time=31.1 ms
...
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
```

Ensuite, installez l'application `tree` comme ceci&nbsp;:

```
$ sudo yum install tree
```

Répondez par l'affirmative pour l'installation du paquet (++y++ comme *yes*) et
le tour est joué.

La commande `tree` offre des fonctionnalités fort pratiques. Dans le cas de
notre petit exercice de révision, elle nous permettra d'apprécier le résultat
en un simple coup d'oeil. Essayez&nbsp;:

```
$ cd
$ tree Fichiers
Fichiers
├── Documents
│   └── texte.txt
├── Films
│   └── film.avi
└── Images
    └── photo.jpg

3 directories, 3 files
```

Les anglophones parmi vous auront deviné que la commande `tree` – qui signifie
"arbre" en anglais – sert à représenter des arborescences.

Notre système contient une série de fichiers et de répertoires cachés, que nous
pouvons afficher avec l'option `-a` comme pour la commande `ls`&nbsp;:

```
$ tree /etc/skel/
/etc/skel/
...
$ tree -a /etc/skel/
/etc/skel/
├── .bash_logout
├── .bash_profile
└── .bashrc
...
```

Puisque nous avons parlé d'arbre, vous pouvez très bien imaginer les suites de
répertoires et de sous-répertoires comme autant de branches qui se ramifient.

```
$ tree /usr/share/icons/hicolor
/usr/share/icons/hicolor
├── 16x16
│   └── apps
│       ├── fedora-logo-icon.png
│       └── system-logo-icon.png
├── 22x22
│   └── apps
│       ├── fedora-logo-icon.png
│       └── system-logo-icon.png
...
```

Les fichiers (comme `fedora-logo-icon.png` ou `system-logo-icon.png`)
correspondent alors aux feuilles de cet arbre. Pour filer la métaphore, tout se
rejoint à la racine. 

> Les pinailleurs noteront que notre arbre est à l'envers. La racine est en
> haut et il faut descendre vers les feuilles.

L'option `-d` de `tree` montre les différents embranchements, mais sans les
feuilles. En d'autres termes, `tree -d` (comme *directory*) affichera seulement
les répertoires d'une arborescence&nbsp;:

```
$ tree -d /usr/share/icons/hicolor
/usr/share/icons/hicolor
├── 16x16
│   └── apps
├── 22x22
│   └── apps
├── 24x24
│   └── apps
├── 256x256
│   └── apps
├── 32x32
│   └── apps
├── 36x36
│   └── apps
├── 48x48
│   └── apps
├── 96x96
│   └── apps
└── scalable
    └── apps
...
```

## Créer une arborescence de répertoires

Admettons maintenant que nous voulions créer une série de sous-répertoires
imbriqués les uns dans les autres, à la manière des poupées gigognes. Le
résultat ressemble à peu près à l'arborescence suivante&nbsp;:

```
$ tree branche1
branche1
└── branche2
    └── branche3
        └── branche4
...
```

La première idée sera sans doute d'invoquer `mkdir` avec le chemin complet des
sous-répertoires. Malheureusement, voici ce qui se passe si nous procédons
ainsi&nbsp;:

```
$ mkdir branche1/branche2/branche3/branche4
mkdir: cannot create directory 'branche1/branche2/branche3/branche4': 
No such file or directory
```

Réprimons un instant une éventuelle pulsion de traverser du poing l'écran de
l'ordinateur. Au lieu de cela, regardons de plus près le message d'erreur et
prenons-le au pied de la lettre. Ce que notre shell essaie de nous faire
comprendre (de façon un peu laconique, certes), c'est qu'il n'arrive pas à
créer le répertoire `branche4`  parce que les répertoires parents `branche1`,
`branche2` et `branche3` n'existent pas. 

Nous devons donc invoquer `mkdir` avec l'option `-p` (comme *parent*)&nbsp;:

```
$ mkdir -p branche1/branche2/branche3/branche4
$ tree branche1
branche1
└── branche2
    └── branche3
        └── branche4
...
```

Je disais que notre shell se montrait un peu laconique à notre égard. Sachez
que, dans bien des cas, il ne tient qu'à nous de le rendre plus bavard. Créons
une autre série de répertoires imbriqués, mais cette fois-ci, utilisons
l'option supplémentaire `-v` comme `--verbose`, c'est-à-dire "bavard"&nbsp;:

```
$ mkdir -pv poupee1/poupee2/poupee3
mkdir: created directory 'poupee1'
mkdir: created directory 'poupee1/poupee2'
mkdir: created directory 'poupee1/poupee2/poupee3'
```

> Cette option `-v` est applicable pour un grand nombre de commandes.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



**Objectif** : installer et gérer une application ou une bibliothèque depuis
une archive de code source sur un système Linux.

**Systèmes** : Oracle Linux 7.x

L'installation, la mise à jour et la suppression de logiciels constituent une
partie significative des tâches qui incombent à l'administrateur d'un serveur
Linux. C'est aussi un domaine qui différencie de manière fondamentale l'univers
de Windows et celui de Linux. Non content de cela, c'est également là que l'on
verra diverses façons de gérer les paquets logiciels selon les familles de
distributions Linux.

## Chasser l'ours à mains nues

Pour commencer, nous allons effectuer un petit retour à l'âge de pierre. Nos
ancêtres sous Unix et Linux chassaient l'ours à mains nues et compilaient
toutes leurs applications depuis le code source. Nous allons faire pareil, sous
forme d'un atelier pratique qui vous permettra de bien comprendre les bases de
la gestion logicielle.

Si nous partons d'un système Oracle Linux 7 minimal, nous avons besoin d'une
poignée d'outils pour construire des applications&nbsp;:

* le jeu de compilateurs GCC (*GNU Compiler Collection*)&nbsp;;

* l'analyseur syntaxique GNU Bison&nbsp;;

* l'analyseur lexical Flex&nbsp;;

* le navigateur Links pour récupérer les archives de code source.

Non, nous n'allons pas nous amuser à construire nos outils de construction
depuis le code source. Cela nous mènerait trop loin. Nous allons donc tricher
un peu en empruntant un raccourci&nbsp;:

```
$ sudo yum install -y oracle-epel-release-el7
$ sudo yum install -y gcc bison flex links
```

Les archives de code source seront rangées dans un répertoire `~/src`&nbsp;:

```
$ mkdir -v ~/src
mkdir: created directory ‘/home/microlinux/src’
$ cd ~/src/
```

## Naviguer sur le Web en mode texte avec Links

Links est un navigateur en mode texte, à l'ergonomie très rudimentaire. Il
utilise le clavier comme seule interface et ne gère pas la souris. Il ne prend
pas en compte non plus certaines fonctionnalités avancées du Web comme le
JavaScript, les images, les vidéos, etc. On évitera donc de l'utiliser pour
naviguer sur Internet au quotidien. En contrepartie, Links nous sera utile
lorsque nous travaillerons sur une machine dépourvue d'interface graphique et
que nous souhaiterons récupérer une archive de code source hébergée sur un
site web.

Pour l'utiliser, il suffit de lancer `links` et de fournir le nom d'un site en
argument&nbsp;:

```
$ links http://yum.oracle.com
```

Ou encore&nbsp;:

```
$ links https://www.google.fr
```

La navigation sur une page web se fait de manière très rudimentaire&nbsp;:

* La touche ++down++ permet de sauter d'un lien à un autre, dans l'ordre où ils
  se présentent sur la page.

* ++up++ fait revenir vers le lien précédent.

* Pour suivre un lien, appuyez sur ++enter++ lorsque vous êtes dessus.

* ++left++ revient à la page précédente.

* Sur une page longue, on peut chercher un lien et/ou du contenu en utilisant
  la touche ++slash++. Une interface de recherche de texte s'affiche et il suffit de
  saisir la chaîne de caractères désirée.

* Les fichiers au format texte simple (`README`, `ChangeLog.txt`, `HOWTO.txt`,
  etc.) sont directement affichés dans Links. Là aussi, la touche ++left++ sert
  à quitter le document et revenir à la page précédente.

* Pour télécharger un fichier, on utilisera la touche ++d++ (comme *Download*).
  Il suffit de vérifier le nom du fichier téléchargé et de confirmer par `OK`.
  Une barre de progression s'affiche alors. Le fichier téléchargé est
  enregistré dans le répertoire courant, c'est-à-dire celui à partir duquel on
  a invoqué `links`.

* La touche ++q++ permet de quitter Links.

## Atelier pratique n° 1 : Joe's Own Editor

Notre premier exercice consistera à compiler l'éditeur de texte JOE (*Joe's Own
Editor*) depuis son code source. JOE est un éditeur léger de la famille Emacs,
conçu pour Unix.

Depuis le répertoire `~/src`, j'ouvre le navigateur Links à l'adresse du site
de JOE&nbsp;:

```
$ links https://joe-editor.sourceforge.io
```

Une fois qu'on est sur la page d'accueil du projet, voici ce qu'il faut faire
pour récupérer le code source de JOE&nbsp;:

* Ouvrir le lien `JOE 4.6 source`.

* Confirmer (`Save`) le téléchargement qui s'affiche.

* Enregistrer (`Save to file`) le fichier `joe-4.6.tar.gz`.

* Quitter Links.

Je décompresse l'archive de code source et je me place dans le répertoire
nouvellement créé&nbsp;:

```
$ tar -xvzf joe-4.6.tar.gz
joe-4.6/
joe-4.6/charmaps/
joe-4.6/charmaps/klingon.in
joe-4.6/setup.hint
joe-4.6/missing
joe-4.6/config.sub
joe-4.6/acinclude.m4
joe-4.6/README.md
joe-4.6/Makefile.am
joe-4.6/po/
...
$ cd joe-4.6/
```

En règle générale, les archives de code source fournissent des informations
succinctes dans une série de fichiers `README`, `INSTALL`, `NEWS`, etc. Jetez
un oeil dans les fichiers `README.md`, `INSTALL.md` et `NEWS.md` pour avoir une
vague idée de ce qu'ils contiennent.

À quelques détails près, la séquence `configure` / `make` / `make install` est
commune à la plupart des applications écrites en C pour les systèmes Unix et
Linux. C'est ce que nous allons utiliser ici&nbsp;:

```
$ ./configure
...
$ make
...
$ sudo make install
```

La dernière commande a installé l'ensemble des fichiers qui constituent
l'application dans l'arborescence `/usr/local`&nbsp;:

* Le fichier binaire exécutable `joe` se retrouve dans `/usr/local/bin`, ainsi
  que `rjoe`, `jmacs`, `jpico` et `jstar`.

* Les fichiers de configuration sont rangés dans `/usr/local/etc/joe`.

* Les pages de manuel en ligne figurent dans `/usr/local/share/man`.

* Les traductions, les modèles de couleurs et autres fichiers auxiliaires sont
  installés dans `/usr/local/share/joe`.

L'éditeur JOE est désormais installé sur votre système. Vous pouvez afficher la
page de manuel en ligne&nbsp;:

```
$ man joe
```

Testez-le pour éditer un fichier&nbsp;:

```
$ joe test.txt
```

> Une fois que vous avez tapé un peu de texte, utilisez les touches ++ctrl+k++
> et ++x++ pour enregistrer les modifications et quitter l'éditeur.

Félicitations, vous venez de compiler votre première application depuis son
code source&nbsp;!

## Atelier pratique n° 2 : TCPdump

Forts de cette première expérience, essayons maintenant de compiler une autre
application depuis son code source. TCPdump est un analyseur de paquets en
ligne de commande, qui surveille ce qui se passe sur les interfaces réseau
d'une machine. Pour la construction, nous allons procéder comme pour l'éditeur
JOE. Dans un premier temps, nous devons récupérer le code source sur le site du
projet&nbsp;:

```
$ cd ~/src/
$ links https://www.tcpdump.org
```

Sur la page d'accueil, repérez le lien vers le fichier `tcpdump-4.99.0.tar.gz`,
téléchargez-le et quittez Links.

Là encore, je décompresse l'archive de code source et je me place dans le
répertoire nouvellement créé&nbsp;:

```
$ tar -xvzf tcpdump-4.99.0.tar.gz
$ cd tcpdump-4.99.0
```

Je lance la première séquence pour la construction&nbsp;:

```
$ ./configure
```

La série de tests mouline pendant quelques secondes et s'interrompt en
affichant le message d'erreur suivant&nbsp;:

```
...
checking whether to look for a local libpcap... yes
checking for local pcap library... not found
checking for pkg-config... /usr/bin/pkg-config
checking whether there are .pc files for libpcap... no
checking for pcap-config... no
checking for main in -lpcap... no
configure: error: see the INSTALL doc for more info
```

Je prends le message au pied de la lettre et je regarde dans le fichier
`INSTALL.txt`&nbsp;:

```
$ less INSTALL.txt
If you have not built libpcap, and your system does not have libpcap
installed, install libpcap first.  Your system might provide a version
of libpcap that can be installed; if so, to compile tcpdump you might
need to install a "developer" version of libpcap as well as the
"run-time" version.  You can also install tcpdump.org's version of
libpcap; see the README file in this directory for the ftp location.
```

Le fichier `INSTALL.txt` m'explique que TCPdump a besoin de la bibliothèque
`libpcap` pour fonctionner. Cette bibliothèque est également fournie par la
page web du projet&nbsp;:

```
$ cd ..
$ links https://www.tcpdump.org
```

Sur la page d'accueil, je télécharge le fichier `libpcap-1.10.0.tar.gz` et je
quitte Links.

Je décompresse l'archive de code source et je me place dans le répertoire
nouvellement créé&nbsp;:

```
$ tar -xvzf libpcap-1.10.0.tar.gz
$ cd libpcap-1.10.0/
```

Je jette un oeil dans les fichiers `README.md` et `INSTALL.md`, puis je lance
la construction de la bibliothèque&nbsp;:

```
$ less README.md
...
$ less INSTALL.md
...
$ ./configure
...
$ make
...
$ sudo make install
```

> Un programme n'est pas forcément un bloc monolithique&nbsp;; il se sert
> souvent d'un ensemble de fonctions qui sont définies dans une bibliothèque
> partagée. Cette dernière ne peut pas s'exécuter directement, mais contient du
> code que l'on veut éviter de réécrire chaque fois qu'un programme doit
> exécuter une fonction similaire, par exemple calculer un cosinus ou capturer
> du trafic réseau.  

Maintenant que `libpcap` est installée, je retente ma chance avec
TCPdump&nbsp;:

```
$ cd ../tcpdump-4.99.0/
```

Apparemment, la bibliothèque `libpcap` est bien détectée&nbsp;:

```
$ ./configure
...
checking for pcap_list_datalinks... yes
checking for pcap_free_datalinks... yes
checking for pcap_datalink_name_to_val... yes
checking for pcap_datalink_val_to_description... yes
checking for pcap_set_datalink... yes
checking for pcap_breakloop... yes
checking for pcap_create... yes
...
```

Il ne me reste plus qu'à lancer la compilation et l'installation&nbsp;:

```
$ make
...
$ sudo make install
```

Je consulte la page de manuel en ligne :

```
$ man tcpdump
```

Pour tester TCPdump, je lance la commande suivante depuis une session à
distance et j'observe ce qui se passe :

```
$ sudo /usr/local/bin/tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
10:12:34.562497 IP linuxbox.microlinux.lan.ssh > alphamule.microlinux.lan.36304: 
Flags [P.], seq 3825316388:3825316584, ack 2004710682, win 328, options 
[nop,nop,TS val 4841670 ecr 2557534762], length 196
...
```

## Inconvénients d'une installation depuis le code source

Nous venons d'installer deux petites applications depuis le code source. À
première vue, l'opération s'est plutôt bien déroulée. Est-ce que cette façon de
procéder présente des inconvénients&nbsp;? Oui&nbsp;; plusieurs même&nbsp;:

* La compilation d'une petite application comme JOE ou TCPdump prend quelques
  minutes tout au plus. Or, dès que nous souhaitons construire une application
  un peu plus importante, les temps de compilation deviennent parfois assez
  longs.

* Certaines applications présentent une véritable cascade de dépendances
  enchevêtrées, qu'il faudra compiler et installer dans un ordre cohérent tout
  en respectant les éventuels problèmes de compatibilité entre les différentes
  versions.

* Les opérations de maintenance s'avèrent aussi fastidieuses que l'installation
  initiale. Si je veux mettre à jour une application construite depuis le code
  source, je dois d'abord savoir quels sont les fichiers qui ont été installés
  par l'opération `make install` avant de les supprimer, ce qui n'est pas
  toujours évident. Dans certains cas, je peux invoquer un `make uninstall`
  depuis la racine du code source, mais cette possibilité n'est pas toujours
  donnée.

Essayons de faire un brin de ménage sur notre système en supprimant `joe`,
`libpcap` et `tcpdump`&nbsp;:

```
$ cd ~/src/joe-4.6/
$ sudo make uninstall
...
$ cd ../libpcap-1.10.0/
$ sudo make uninstall
...
$ cd ../tcpdump-4.99.0/
$ sudo make uninstall
...
```

Regardons dans l'arborescence `/usr/local` ; nous constatons non sans un certain
dépit que le ménage n'a pas été fait très proprement. Les répertoires
d'installation `etc/joe`, `share/joe` et `share/doc/joe` sont restés en place et il
nous reste un binaire `bin/tcpdump.4.99.0` qui a manifestement été oublié par le
script de suppression `make uninstall` de `tcpdump`.

```
$ sudo rmdir /usr/local/etc/joe
$ sudo rmdir /usr/local/share/doc/joe/
$ sudo rm -rf /usr/local/share/joe/
$ sudo rm /usr/local/bin/tcpdump.4.99.0
```

## Garder la trace des fichiers installés

À partir du moment où je sais que les fichiers résultant de la compilation vont
être installés dans `/usr/local`, je peux utiliser l'astuce suivante pour en
garder la trace&nbsp;:

```
$ sudo make install
$ find /usr/local -mmin 1 > liste.txt
```

Ici, `find` m'affiche tout ce qui a été créé ou modifié durant la dernière
minute dans l'arborescence `/usr/local` et le résultat de la recherche est
redirigé dans un fichier `liste.txt`. D'un certain point de vue, c'est le
gestionnaire de paquets le plus rudimentaire qui existe.

## Pour aller beaucoup trop loin : Linux From Scratch

*Linux From Scratch* (LFS pour les intimes) est un projet pédagogique qui vise à
construire sa propre distribution Linux de A à Z à partir des sources des
logiciels&nbsp;:

* [https://www.linuxfromscratch.org/](https://www.linuxfromscratch.org/)

Une traduction en français est maintenue ici&nbsp;:

* [http://fr.linuxfromscratch.org/](http://fr.linuxfromscratch.org)

> LFS s'adresse aux utilisateurs confirmés de Linux. Comprenez par là que le
> public idéal du projet, c'est l'informaticien chevronné qui adore construire
> des cathédrales avec des allumettes ou mettre en bouteille des maquettes de
> grands voiliers pour se changer les idées.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


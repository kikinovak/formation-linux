**Objectif** : accéder aux données d'un CD-Rom, d'un DVD ou d'une clé USB sur
un système Linux.

"Où sont les données de mon CD-Rom&nbsp;? Comment accéder à ma clé USB&nbsp;?"
Voilà ce que l'on est sûr de retrouver dans le palmarès des questions les plus
fréquemment posées par des utilisateurs venant de Microsoft Windows. Linux
n'utilise pas les lettres de l'alphabet pour désigner ses périphériques et
c'est en vain que vous chercherez des lecteurs `D:`, `E:` ou `F:`.  Au lieu de
cela, les périphériques sont inclus directement dans la hiérarchie du système
de fichiers. Si cela vous paraît obscur, le présent atelier vous expliquera
tout en détail, pas à pas. 

## Les fichiers de périphérique

Pour commencer, jetez un oeil au répertoire `/dev`. Son nom provient du mot
*device* qui signifie "périphérique". Faites un petit `ls` pour en afficher le
contenu.  La première chose qui saute aux yeux, c'est la couleur de tous ces
fichiers&nbsp;: jaune&nbsp;! Ce ne sont vraisemblablement pas des fichiers
comme tous les autres.  Cette première suspicion est d'ailleurs étayée par
l'affichage détaillé&nbsp;:

```
$ ls -l /dev/sda
brw-rw----. 1 root disk 8, 0 May 30 08:39 /dev/sda
$ ls -l /dev/input/mouse0 
crw-rw----. 1 root input 13, 32 May 30 08:39 /dev/input/mouse0
```

Le `b` et le `c` initiaux nous indiquent qu'il ne s'agit ni de fichiers
classiques, ni de répertoires. Nous trouvons certes une série de liens
symboliques, mais ceux-ci semblent pointer vers les mêmes fichiers mystérieux à
l'intérieur de ce répertoire&nbsp;:

```
$ ls -l /dev/cdrom
lrwxrwxrwx. 1 root root 3 May 30 08:39 /dev/cdrom -> sr0
```

Les fichiers contenus dans `/dev` sont des fichiers de périphérique (*device
files*)&nbsp;: chacun représente (ou symbolise) un périphérique bien spécifique
sur votre système. Ils ne contiennent pas de données à proprement parler, mais
servent à communiquer avec le noyau.

Dans l'affichage détaillé, les lettres `b` et `c` différencient respectivement
les périphériques de type bloc (*block devices*) et ceux de type caractère
(*character devices*)&nbsp;:

* Le disque dur, par exemple, est un périphérique bloc&nbsp;: il lit ou écrit
  des données sous forme de blocs de taille fixe.

* La souris, le clavier, la console ou encore la carte audio sont des
  périphériques caractère&nbsp;: ils lisent ou écrivent un flux d'octets
  en série.

Quant aux deux chiffres qui apparaissent avant la date, ce sont les numéros
majeur et mineur&nbsp;:

```
$ ls -l /dev/sda?
brw-rw----. 1 root disk 8, 1 May 30 08:39 /dev/sda1
brw-rw----. 1 root disk 8, 2 May 30 08:39 /dev/sda2
```

> Tout comme l'astérisque `*`, le point d'interrogation `?` est un caractère de
> substitution. Alors que le premier représente une suite de caractères
> quelconque, le second est un joker pour un seul caractère quelconque.

Le numéro majeur donne une indication sur le pilote du noyau qui gère le
périphérique en question. Le numéro mineur sert à différencier des
périphériques similaires, par exemple les différentes partitions d'un
disque dur. Pour la plupart, les pilotes et leur numéro majeur figurent sur
[cette page de la documentation du noyau
Linux](https://www.kernel.org/doc/Documentation/admin-guide/devices.txt).

* Les noms des disques durs (SCSI, SATA, IDE, USB, Firewire) commencent
  par `sd` (*SCSI Disk*). La lettre qui suit dépend d'une part du branchement,
  d'autre part de l'ordre des branchements&nbsp;: `sda`, `sdb`, `sdc`,
  `sdd`, etc.

* Les lecteurs CD/DVD SCSI seront répertoriés en tant que `sr` ou `scd`
  (*SCSI CD*).

* Les périphériques d'entrée comme le clavier et la souris sont symbolisés par
  les fichiers dans le répertoire `/dev/input`.

* Etc.

Certains fichiers ne correspondent pas vraiment à un périphérique physique,
concret et tangible. Ils remplissent une fonction bien spécifique dans le
système&nbsp;:

* `/dev/null` est une sorte de "nirvana numérique", un véritable trou noir vers
  lequel on peut envoyer des données qui sont censées disparaître, par exemple
  les messages de la console que l'on ne souhaite pas afficher.

* `/dev/zero` est une source inépuisable de zéros, que l'on utilise parfois
  pour en remplir certains fichiers jusqu'à une taille prédéfinie.

* `/dev/random` et `/dev/urandom` génèrent des nombres aléatoires.

## La notion de montage/démontage

Pour donner l'accès aux divers périphériques ainsi qu'aux systèmes de fichiers
qu'ils contiennent, il faut établir une connexion entre le périphérique d'un
côté et l'arborescence du système de fichiers Linux de l'autre. Lorsque le
périphérique est inclus dans cette arborescence, on dit qu'il est "monté". Un
peu de pratique vous permettra de comprendre cette façon de procéder.

## Monter manuellement un CD ou un DVD

Commençons par le lecteur CD/DVD. Dans un premier temps, il nous faut connaître
son fichier de périphérique. La cible du lien symbolique `/dev/cdrom` nous
fournit déjà une première piste&nbsp;:

```
$ ls -l /dev/cdrom
lrwxrwxrwx. 1 root root 3 May 30 08:39 /dev/cdrom -> sr0
```

Pour en avoir le coeur net, nous utiliserons la commande `lsblk` (*list block
devices*), qui affiche les périphériques de type bloc du système. Notez le
périphérique `/dev/sr0` (type `rom`) tout en bas de la liste&nbsp;:

```
$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   60G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0   59G  0 part
  ├─ol-root 253:0    0 38.3G  0 lvm  /
  ├─ol-swap 253:1    0    2G  0 lvm  [SWAP]
  └─ol-home 253:2    0 18.7G  0 lvm  /home
sr0          11:0    1 1024M  0 rom
```

> Vous pouvez très bien accéder à votre lecteur CD/DVD depuis un système invité
> installé dans VirtualBox. Ouvrez la configuration de la machine virtuelle et,
> dans l'onglet `Stockage`, vérifiez si vous avez sélectionné le `Lecteur de
> l'hôte` dans le contrôleur IDE, avec le `Mode direct` activé.  

Pour la machine virtuelle qui tourne dans ma station de travail, c'est `sr0`.
Il se peut que chez vous ce soit `scd0`, `sdb` ou autre chose, mais peu
importe. La cible du lien symbolique `/dev/cdrom` et la liste fournie par
`lsblk` vous le diront.

Insérez par exemple le CD d'installation dans le lecteur et invoquez la
commande suivante, en prenant soin de remplacer `/dev/sr0` par le fichier
correspondant à votre lecteur. Comme vous vous en doutez, vous pouvez bien
évidemment remplacer `/dev/sr0` par le lien symbolique correspondant
`/dev/cdrom`&nbsp;:

```
$ sudo mount -v -t iso9660 /dev/cdrom /mnt
mount: /dev/sr0 is write-protected, mounting read-only
mount: /dev/sr0 mounted on /mnt.
```

Maintenant, allez voir dans le répertoire `/mnt`&nbsp;:

```
$ cd /mnt
$ ls
EFI  images  isolinux  LiveOS
```

Effectivement, `/mnt` contient les données du CD d'installation. Naviguez un
peu dans les répertoires avec `cd`, vous verrez que tout y est. Le CD est
effectivement "monté" sur `/mnt`. Voici quelques explications sur la
commande&nbsp;:

* `mount` sert à monter un périphérique et requiert les privilèges de
  l'administrateur (`sudo`)&nbsp;;

* `-v` indique à `mount` d'opérer en mode "bavard"&nbsp;;

* `-t` est l'option qui précise le système de fichiers (ici, `iso9660`, utilisé
  sur un CD/DVD)&nbsp;;

* `/dev/cdrom` est le fichier de périphérique qui représente le lecteur de
  CD/DVD ou, plus exactement, un lien symbolique qui pointe vers
  celui-ci&nbsp;;

* `/mnt` est le point de montage, autrement dit le répertoire où les données du
  périphérique seront accessibles.

## Démonter le CD ou le DVD

Pour procéder en sens inverse, c'est-à-dire pour retirer le contenu du DVD de
notre système de fichiers, il suffit de le "démonter". Pour ce faire,
placez-vous en dehors de l'arborescence de `/mnt` (par exemple, en
invoquant `cd` sans arguments) et lancez la commande suivante&nbsp;:

```
$ sudo umount -v /mnt
umount: /mnt (/dev/sr0) unmounted
```

L'option `-v` demande simplement à la commande `umount` de nous tenir informés
de ce qu'elle fait. Jetez un oeil dans `/mnt`&nbsp;:

```
$ ls /mnt
```

Il ne reste plus rien&nbsp;; le répertoire est désormais vide. Il semblerait
que nous ayons réussi le montage et le démontage d'un périphérique. Avant
d'aborder les questions de détail, essayons de faire la même chose avec notre
clé USB.

## Monter et démonter manuellement une clé USB

Avant de pouvoir faire quoi que ce soit, il faut d'abord connaître le fichier
de périphérique qui la représente.

Puisque le répertoire `/dev` contient tous les fichiers symbolisant les
périphériques de la machine, on pourrait&nbsp;:

* afficher le contenu de `/dev` avec `ls`&nbsp;;

* brancher la clé USB et attendre quelques secondes&nbsp;;

* afficher une nouvelle fois le contenu de `/dev` en repérant une éventuelle
  nouvelle entrée nommée `sdquelque_chose`.

Oui, ce serait possible. C'est assez fréquent que l'on fasse les choses de
manière passablement compliquée, pour se rendre compte ensuite (parfois des
années plus tard) qu'il existe un autre moyen, beaucoup plus élégant et surtout
plus simple. 

> Tout comme pour le lecteur CD/DVD, vous pouvez accéder à votre clé USB depuis
> un système invité installé dans VirtualBox, mais vous devez l'enregistrer au
> préalable. Insérez la clé USB, ouvrez la configuration de la machine
> virtuelle et, dans l'onglet `USB`, cliquez sur l'icône ornée d'un petit
> signe `+` pour ajouter la clé à la liste des disques externes gérés par le
> système invité.  Dans la configuration par défaut de VirtualBox, vous ne
> pouvez utiliser que l'USB 1.1, mais ce n'est pas bien grave, vu que nous ne
> nous en servons qu'à des fins de démonstration.

Lorsque vous branchez un périphérique USB, le noyau en prend note dans son
journal, le fichier `/var/log/messages`. Regardez ce que ce fichier
contient&nbsp;:

```
$ sudo cat /var/log/messages
```

Du charabia&nbsp;? Je suis d'accord avec vous. Mais peut-être pas si charabia
que ça, comme vous allez le voir tout de suite. Rappelez-vous la commande
`tail` que nous avons utilisée dans un précédent atelier pour afficher la fin
d'un fichier&nbsp;:

```
$ sudo tail /var/log/messages
```

Le comportement par défaut de `tail` ressemble à celui de `head`. Il affiche
dix lignes, pas plus. Cependant, une option bien particulière sert à suivre en
direct l'évolution d'un fichier lorsque des données s'y ajoutent&nbsp;:

```
$ sudo tail -f /var/log/messages
```

Maintenant, branchez votre clé USB et attendez un peu. Au bout de quelques
secondes, vous verrez apparaître une série de messages ressemblant à ce qui
suit&nbsp;:

```
...
May 30 09:49:30 linuxbox kernel: sdb: sdb1
May 30 09:49:30 linuxbox kernel: sd 5:0:0:0: [sdb] Attached SCSI removable disk
...
```

Le noyau nous envoie une bonne quinzaine de lignes, dans lesquelles nous
retrouvons facilement l'information qui nous intéresse. Le fichier de
périphérique correspondant à notre clé USB est donc `sdb` et la partition
contenant le système de fichiers est `sdb1`. Attention, sur votre machine ce
sera peut-être autre chose : `sdc1`, `sdd1`, `sde1`, etc. Appuyez sur
++ctrl+c++ pour interrompre `tail -f` et récupérer la main sur la console. Si
vous êtes scrupuleux, vérifiez l'information délivrée par le noyau&nbsp;:

```
$ ls -l /dev/sdb1
brw-rw----. 1 root disk 8, 17 May 30  09:49 /dev/sdb1
```

Enfin, la commande `lsblk` nous aurait également permis d'identifier notre
clé USB en tant que périphérique bloc de type `disk`&nbsp;:

```
$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   60G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0   59G  0 part
  ├─ol-root 253:0    0 38.3G  0 lvm  /
  ├─ol-swap 253:1    0    2G  0 lvm  [SWAP]
  └─ol-home 253:2    0 18.7G  0 lvm  /home
sdb           8:16   1  7.5G  0 disk
└─sdb1        8:17   1  7.5G  0 part
sr0          11:0    1  649M  0 rom
```

> Rappelons ici que les clés USB sont normalement formatées en FAT, un système
> de fichiers originaire de Microsoft. Il ne brille pas spécialement par ses
> performances, bien au contraire. Néanmoins, c'est le seul qui soit lisible
> aussi bien par Windows et macOS que Linux, d'où l'intérêt de préférer ce
> dénominateur commun pour le formatage.

Là encore, remplacez `/dev/sdb1` par le fichier de périphérique symbolisant
votre clé USB&nbsp;:

```
$ sudo mount -v -t vfat /dev/sdb1 /mnt
mount: /dev/sdb1 mounted on /mnt.
```

Essayons de traduire cette dernière commande en français&nbsp;: "monte
(`mount`) la clé USB (`/dev/sdb1`) qui contient un système de fichiers FAT
(`-t vfat`) sur le point de montage `/mnt` (`/mnt`) et dis-nous ce que tu
fais (`-v`)."

Voyons si le contenu de la clé apparaît bien en dessous de `/mnt`&nbsp;:

```
$ ls /mnt
Eyrolles
INFRES-Groupe-1.pdf
INFRES-Groupe-2.pdf
System Volume Information
```

Bravo&nbsp;! Il ne nous reste plus qu'à démonter la clé. Sortons de
l'arborescence `/mnt` (`cd` sans arguments, par exemple), puis invoquons la
commande suivante&nbsp;:

```
$ sudo umount -v /mnt
umount: /mnt (/dev/sdb1) unmounted
```

Là aussi, nous pouvons vérifier que `/mnt` ne contient plus rien après le
démontage du périphérique.

## Quelques observations en vrac

Les premiers essais de montage et de démontage en ligne de commande ont été
concluants. Quelques remarques s'imposent.

* Les options `-v` et `-t` ont été utilisées à des fins pédagogiques. Elles
  sont facultatives. Ainsi, vous auriez pu vous contenter d'évoquer
  `mount /dev/cdrom /mnt` pour monter le CD-Rom et `mount /dev/sdb1 /mnt` pour
  la clé USB.  Essayez.

* Les anglophones parmi vous objecteront que le contraire de *mount* serait
  plutôt *unmount*. Pourquoi alors dit-on `umount` ? Cela tient au fait que
  beaucoup d'informaticiens ont un rapport problématique à l'orthographe.
  Le `n` a dû se perdre en route quelque part. C'est aussi bête que cela.
    
* La commande `umount` accepte en argument aussi bien le point de montage que
  le nom du fichier de périphérique. Pour démonter le CD-Rom, nous aurions pu
  écrire aussi bien `umount /dev/cdrom` que `umount /mnt`. Pareillement, la
  clé USB aurait pu être démontée par le biais de la commande
  `umount /dev/sdb1`.
    
* Notons enfin que, sur un poste de travail Linux, la gestion des périphériques
  se fait de manière tout aussi transparente que sous Microsoft Windows ou
  macOS. Par "transparent", entendez que, si vous utilisez un environnement de
  bureau comme KDE, GNOME ou Xfce, il suffit généralement d'insérer le
  périphérique en question et de cliquer sur une icône pour accéder à son
  contenu.
    
## Scier la branche sur laquelle on est assis

Pourquoi faut-il quitter l'arborescence du point de montage avant de démonter
un périphérique&nbsp;? Pour la simple raison que le système refuse de vous
laisser scier la branche sur laquelle vous êtes assis. Ce point mérite une
illustration détaillée, car il s'agit d'une erreur qui revient fréquemment.
Insérez le CD-Rom et montez-le&nbsp;:

```
$ sudo mount /dev/cdrom /mnt
```

Placez-vous à la racine du CD-Rom&nbsp;:

```
$ cd /mnt
```

Maintenant, à partir de `/mnt`, essayez de démonter le lecteur et voyez ce que le
système vous dit&nbsp;:

```
$ sudo umount /mnt
umount: /mnt: target is busy.
```

Tant que vous vous trouvez dans l'arborescence `/mnt`, vous ne pourrez donc
tout simplement pas procéder au démontage.

## Éjecter un DVD ou un CD-Rom : eject

Voici une commande qui amuse beaucoup les enfants (et les administrateurs
restés jeunes dans l'âme)&nbsp;: `eject`. Assurez-vous de vous trouver en
dehors de toute arborescence de point de montage et essayez-la&nbsp;:

```
$ sudo eject
```

Si tout se passe bien, le tiroir du lecteur CD/DVD s'ouvre. Pour le refermer,
invoquez simplement ceci&nbsp;:

```
$ sudo eject -t
```

> La commande `eject -t` ne fonctionnera pas sur les lecteurs *slim*, qui sont
> conçus différemment. Ils s'ouvrent automatiquement, mais leur fermeture est
> manuelle.

La commande `eject` combine le démontage et l'éjection d'un périphérique.
Autrement dit, si votre CD-Rom est monté, ce n'est pas la peine d'invoquer
`umount`&nbsp;; `eject` s'en charge. Comme `umount`, `eject` accepte en argument le nom d'un
périphérique, à condition que celui-ci soit éjectable bien sûr. Invoquée sans
arguments, la commande agit sur le périphérique défini par défaut. Pour le
connaître, utilisez l'option `-d`&nbsp;:

```
$ eject -d
eject: default device: `/dev/cdrom'
```

Normalement, `eject` fonctionne avec les lecteurs CD/DVD et les graveurs, ainsi
qu'avec les lecteurs ZIP, même si ces derniers sont tombés en désuétude.

> Les administrateurs de salles de serveurs utilisent parfois `eject` de
> manière abusive, à condition que les machines soient équipées de lecteurs
> optiques&nbsp;: ils invoquent cette commande pour savoir où se trouve
> physiquement la machine sur laquelle ils sont connectés.

## Monter plusieurs périphériques en même temps

Jusque-là, le point de montage `/mnt` nous a servi pour tous nos tests
successifs avec le lecteur DVD et la clé USB. Certains d'entre vous se sont
peut-être demandé s'il était possible de monter les deux conjointement.

Au départ, `/mnt` est juste un répertoire vide qui fait office de point de
montage par convention. En théorie, rien ne nous empêche d'en créer d'autres où
bon nous semble. En pratique, nous pouvons très bien combiner la création de
nouveaux points de montage avec le respect des conventions.

```
$ sudo mkdir -v /mnt/{cdrom,disk}
mkdir: created directory ‘/mnt/cdrom’
mkdir: created directory ‘/mnt/disk’
```

Je me sers du point de montage `/mnt/cdrom` pour monter le
lecteur CD/DVD&nbsp;:

```
$ sudo mount /dev/cdrom /mnt/cdrom
```

Quant à la clé USB, elle est montée sur `/mnt/disk`&nbsp;:

```
$ sudo mount /dev/sdb1 /mnt/disk
```

À partir de là, les contenus respectifs du CD-Rom et de la clé USB sont
accessibles dans les deux arborescences distinctes.

## Obtenir des informations sur les périphériques montés

Pour savoir si un périphérique est monté ou non, il suffit d'invoquer la
commande `mount` sans le moindre argument. Cette opération ne requiert pas de
privilèges d'administrateur&nbsp;:

```
$ mount
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime,...)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
devtmpfs on /dev type devtmpfs (rw,nosuid,seclabel,...)
...
```

Pour l'instant, intéressez-vous juste à la dernière ligne de cet exemple&nbsp;:

```
/dev/sr0 on /mnt/cdrom type iso9660 (ro,relatime)
```

Si vous avez tout bien suivi jusqu'ici, la lecture de cette information ne
devrait pas vous poser trop de problèmes. La ligne nous dit simplement que le
périphérique `/dev/sr0` est monté sur `/mnt/cdrom`, que le système de fichiers
est l'ISO&nbsp;9660 et qu'il est monté en lecture seule (`ro` pour
*read only*).

Si je monte ma clé USB sur `/mnt/disk` et que j'invoque `mount`,
j'obtiens ceci&nbsp;:

```
/dev/sdb1 on /mnt/disk type vfat (rw,relatime,...)
```

Dans ce cas, l'information se lit comme suit&nbsp;: `/dev/sdb1` est monté sur
`/mnt/disk`, le périphérique est formaté en FAT et il est accessible en lecture
et écriture (`rw` pour *read/write*).

Par ailleurs, le fichier `/etc/mtab` donne les mêmes informations sur les
périphériques montés que `mount` sans argument, à quelques détails près&nbsp;:

```
$ cat /etc/mtab
...
/dev/sr0 /mnt/cdrom iso9660 ro,relatime  0 0
/dev/sdb1 /mnt/disk vfat rw,relatime,... 0 0
```

Là encore, notez la présence du CD-Rom et de la clé USB sur les deux dernières
lignes, montés respectivement sur `/mnt/cdrom` et `/mnt/disk`.

Il reste encore beaucoup de choses à dire sur le montage des systèmes de
fichiers, notamment sur l'organisation de ces derniers sur le ou les disque(s)
dur(s). Je vous propose de voir cela dans le prochain atelier, qui nous
fournira l'occasion de mettre en application directe les compétences
nouvellement acquises. Pour l'instant, laissez reposer un peu la pâte et
digérez ce que vous avez appris jusqu'ici.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



**Objectif** : prise en main des bases de la configuration réseau sur un
serveur Linux.

**Systèmes** : Red Hat Enterprise Linux 7.x, Oracle Linux 7.x, CentOS 7.x

À partir du moment où vous connectez deux ou plusieurs ordinateurs et où vous
envoyez des données d'une machine à l'autre, vous fonctionnez en réseau. Cet
atelier pratique vous prend par la main et vous initie pas à pas aux bases du
réseau sous Linux.

## Une confusion babylonienne

La communication est un besoin essentiel pour l'être humain, au même titre que
respirer, boire et manger. De nos jours, on peut filer la métaphore et
légitimement considérer que la communication avec d'autres machines fait partie
du minimum syndical que l'on peut exiger d'un ordinateur, à plus forte raison
lorsqu'il s'agit d'un serveur. Le hic, c'est que la communication entre les
ordinateurs est une chose très complexe, probablement autant que celle entre
les humains. Ça parle plusieurs langues, ça utilise des patois différents, ça
crée des malentendus, ça entend de travers, ça monopolise le discours, c'est
sourd comme un pot et parfois con comme un balai. Bref, c'est la pagaille.

Il existe certes une série d'ouvrages sur le sujet, toute la gamme allant de
*pour les nuls* à *pour les pros*. En règle générale, ils comprennent une
histoire exhaustive des réseaux depuis la guerre froide et ARPANET, suivie
d'une introduction détaillée à l'algèbre binaire, octale et hexadécimale. Vous
feuilletez ces livres de plus en plus vite, puis vous les reposez en vous
sentant progressivement envahi par une vague nausée existentielle. MAC&nbsp;?
ARP&nbsp;?  IP&nbsp;? TCP&nbsp;? UDP&nbsp;? DHCP&nbsp;? DNS&nbsp;? NTP&nbsp;?
HTTP&nbsp;? FTP&nbsp;? Comment vous en sortir&nbsp;?

Je prends donc le parti de vous initier aux réseaux et à leur fonctionnement
par une approche résolument pratique, en mettant les mains à la pâte, avec un
minimum de théorie. Nous avancerons par étapes successives, en partant du cas
de figure le plus simple. Puis, peu à peu, lorsque vous aurez digéré les
notions de base, je vous présenterai des configurations un peu plus
sophistiquées, quitte à rectifier le tir et à reprendre les simplifications
abusives dont je me serai rendu coupable. Le tout part d'un exemple très
concret, le cas de figure le plus simple&nbsp;: une machine locale reliée à
Internet par le biais d'une Freebox. 

> Pour cet atelier pratique, n'importe quel modem routeur du genre Freebox,
> Livebox, SFR Box, etc. fera l'affaire. 

## Prérequis matériel et logiciel

### Le modem routeur 

Lorsque vous souscrivez un abonnement Internet auprès d'un des grands
fournisseurs d'accès, celui-ci vous fournit un modem routeur. Que vous soyez
abonné chez Free, Orange, SFR, Nerim ou autre, le principe technique de la
connexion à Internet sera le même. Ce qui changera, c'est la forme du boîtier
de votre modem et quelques menus détails dans la configuration.

Dans le cas d'un modem routeur comme la Freebox dans mon bureau, la prise ADSL
à l'arrière du boîtier est branchée à la prise du téléphone. Quant aux prises
Ethernet, elles sont reliées à une ou plusieurs machines locales ou à un
switch.

### Questions de câblage

Le modem routeur est équipé d'un point d'accès Wi-Fi, mais nous
n'allons pas l'utiliser. Dans un premier temps, notre configuration de base
nécessitera en tout et pour tout deux câbles (sans compter l'alimentation du
modem, bien sûr)&nbsp;:

* Le câble reliant la prise téléphonique au modem. Il vous faudra probablement
  songer à installer un filtre. Normalement, une feuille explicative livrée
  avec le modem détaille ce genre de chose de manière assez claire.

* Le câble Ethernet reliant le modem routeur à la carte Ethernet du serveur ou
  au switch local.

### Choisir le bon type de câble

Il existe deux types de câbles Ethernet&nbsp;: plat et croisé. Les branchements
des fils aux connecteurs ne sont pas les mêmes selon le type de câble.
Attention à ne pas les confondre, vous risqueriez de passer des heures à vous
demander pourquoi votre réseau ne fonctionne pas. Les câbles croisés sont
uniquement utilisés pour les connexions directes de PC à PC. Ce n'est pas ce
qu'il nous faut ici. Pour les connexions entre l'ordinateur et le modem, nous
devons utiliser un câble plat.

Les câbles réseau sont classés en différentes catégories en fonction de leurs
niveaux de performances. Repérez des indications comme `Cat 5`, `Cat 5E` ou
`Cat 6` imprimées sur le câble.

* Les `Cat 5E` et `Cat 6` répondent à des normes plus élevées de transmission
  de données et peuvent effectuer des transferts à 1000&nbsp;Mbps. C'est ce
  qu'il nous faut.

* Les `Cat 5` ou inférieurs plafonnent à 100&nbsp;Mbps, voire moins, et nous
  éviterons de les utiliser.

> Les connecteurs d'un câble Ethernet sont plus généralement connus
sous le nom de "connecteurs RJ45".

## La carte Ethernet

Si nous procédons méthodiquement, nous devons d'abord nous poser la
question&nbsp;: le serveur dispose-t-il d'une carte Ethernet&nbsp;? Si oui,
est-elle branchée correctement&nbsp;? Bien sûr, nous pouvons jeter un coup
d'oeil sur les branchements de la machine, ce qui nécessite parfois de grimper
derrière le meuble où il est rangé. Alternativement, nous pouvons rester assis
devant et nous contenter d'invoquer la commande suivante&nbsp;:

``` 
$ lspci | grep -i eth
02:00.0 Ethernet controller: Intel Corporation 82574L Gigabit Network Connection
03:00.0 Ethernet controller: Intel Corporation 82574L Gigabit Network Connection
``` 

La machine en question est un HP Proliant Microserver qui fait office de
serveur de sauvegardes dans mon bureau. Le résultat de la commande `lspci` nous
montre que le serveur est équipé de deux cartes réseau Intel identiques.

Attention&nbsp;: cette commande nous dit uniquement qu'une carte Ethernet est
bien présente **physiquement** sur le serveur. Cela ne veut pas forcément dire
que la carte est effectivement gérée par le système d'exploitation. Si nous
voulons savoir ce que ce dernier – en l'occurrence le noyau – "pense" du
périphérique en question, nous pouvons utiliser la commande suivante&nbsp;:

```
$ dmesg | grep -i network
... e1000e: Intel(R) PRO/1000 Network Driver - 3.2.6-k
... e1000e ... eth0: Intel(R) PRO/1000 Network Connection
... e1000e ... eth1: Intel(R) PRO/1000 Network Connection
```

Le résultat de cette dernière commande nous montre que, apparemment, le noyau
reconnaît les deux cartes Ethernet. Dans le cas contraire, nous aurions eu
droit à un message d'erreur de ce genre&nbsp;:

```
eth0: unknown interface: No such device
```

Ce n'est pas le cas (heureusement) et nous savons que le noyau est capable de
gérer le matériel avec le module `e1000e`. Il est peut-être utile de s'attarder
un moment sur cette notion de "module".

## Les pilotes sous Linux

Un module, ce n'est rien d'autre que ce que vous connaissez peut-être par
ailleurs sous le nom de pilote (*driver*, en anglais), c'est-à-dire un bout de
code qui permet au système d'exploitation de communiquer avec le matériel et
donc de le gérer. Sur un système Linux, la commande `lsmod` sert à afficher le
statut des modules.

```
$ lsmod
...
ata_generic            12923  0 
pata_acpi              13053  0 
i2c_algo_bit           13413  0 
drm_kms_helper        179394  0 
e1000e                248519  0
...
```

Filtrons ce résultat s'il est trop long&nbsp;:

```
$ lsmod | grep e1000
e1000e                248519  0 
ptp                    19231  1 e1000e
```

Les modules se trouvent dans `/lib/modules`, dans le répertoire correspondant à
la version du noyau en cours. Concrètement, le pilote de notre carte se trouve
donc dans l'arborescence `/lib/modules/3.10.0-1160.25.el7.x86_64`, dans le
sous-répertoire `kernel/drivers/net/ethernet/intel/e1000`, et c'est le fichier
`e1000.ko.xz`. L'extension `.ko` signifie *kernel object* et `.xz` nous indique
qu'il s'agit d'un fichier compressé. C'est précisément ce fichier qui est
chargé par le noyau pour gérer une carte Ethernet équipée d'une puce Intel
82574L.

Prenons un autre exemple pour illustrer ceci. Un vieux Lenovo ThinkCentre qui
me sert de bac à sable pour mes tests en local est équipé d'une carte réseau
bas de gamme&nbsp;:

```
$ lspci | grep -i eth
03:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. 
RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 0c)
```

Cette carte semble être prise en charge par le noyau&nbsp;:

```
$ dmesg | grep -i eth
... r8169 ... eth0: RTL8168g/8111g, 44:37:e6:da:44:72, XID 4c0, IRQ 28
... r8169 ... eth0: jumbo features [frames: 9200 bytes, tx checksumming: ko]
```
> Vous aurez remarqué que les messages du noyau varient en fonction du type de
> la carte réseau. Sur la première machine, j'ai filtré `dmesg` avec le terme
> `network`&nbsp;; sur la deuxième, j'ai utilisé `eth`. 

La carte en question est donc gérée par le module `r8169` situé dans
l'arborescence `/lib/modules/3.10.0-1160.25.1.el7.x86_64`&nbsp;:

```
$ cd /lib/modules/3.10.0-1160.25.1.el7.x86_64/
$ find . -name 'r8169*'
./kernel/drivers/net/ethernet/realtek/r8169.ko.xz
```

## Principe de fonctionnement des réseaux

### Afficher la configuration des interfaces réseau

Après avoir vérifié que mes cartes Ethernet sont gérées, j'invoque la commande
suivante&nbsp;:

```
$ ip address
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN 
       group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP group default qlen 1000
    link/ether 44:37:e6:da:44:72 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.18/24 brd 192.168.0.255 scope global noprefixroute 
       dynamic enp3s0 valid_lft 41078sec preferred_lft 41078sec
    inet6 2a01:e0a:85f:f780:fe43:f4ff:eaac:19ac/64 scope global noprefixroute 
       dynamic valid_lft 86363sec preferred_lft 86363sec
    inet6 fe80::28a4:290:3e81:6baf/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Le résultat de la commande `ip address` se décompose en deux sections dans
notre exemple&nbsp;: `lo` et `enp3s0`. La partie `lo` (comme `localhost`)
désigne la boucle locale, une interface qui représente approximativement le
journal intime de votre machine et qui lui permet de soliloquer. Laissons-la de
côté pour l'instant. L'interface qui nous intéresse plus particulièrement est
celle qui n'est **pas** `lo`, en l'occurrence `enp3s0`. Essayons de lire les
informations qui nous concernent&nbsp;:

```
$ ip address show enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP group default qlen 1000
    link/ether 44:37:e6:da:44:72 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.18/24 brd 192.168.0.255 scope global noprefixroute 
       dynamic enp3s0 valid_lft 40977sec preferred_lft 40977sec
    inet6 2a01:e0a:85f:f780:fe43:f4ff:eaac:19ac/64 scope global noprefixroute 
       dynamic valid_lft 86262sec preferred_lft 86262sec
    inet6 fe80::28a4:290:3e81:6baf/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

### L'adresse MAC de votre carte

La ligne `link/ether 44:37:e6:da:44:72` identifie la carte réseau d'un point de
vue purement matériel. Il s'agit d'une série de six chiffres hexadécimaux
séparés par des symboles deux-points qui constitue l'empreinte digitale de
votre carte Ethernet, en quelque sorte. Cette empreinte ou adresse MAC (*Media
Access Control*, rien à voir avec les ordinateurs de la marque Apple) est
normalement unique au monde. Nous pouvons tranquillement l'ignorer pour nos
besoins de configuration, mais sachez au moins de quoi il s'agit.

Si nous avions voulu limiter l'affichage à l'adresse MAC, nous aurions utilisé
la commande suivante&nbsp;:

```
$ ip link show enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP mode DEFAULT group default qlen 1000
    link/ether 44:37:e6:da:44:72 brd ff:ff:ff:ff:ff:ff
```

> Dans le système décimal, on compte de un à dix. Dans le système hexadécimal,
> on compte de un à seize. On dispose donc des "chiffres" supplémentaires A, B,
> C, D, E et F.

### L'adresse IP et le réseau

L'adresse IP `inet 192.168.0.18` caractérise ma machine dans le réseau.
Autrement dit, `192.168.0.18` est l'adresse IP de mon ordinateur dans le réseau
local `192.168.0.0/24`. Je laisse de côté la valeur `brd 192.168.0.255` (`brd`
pour *broadcast*) et je me concentre sur la notation `inet 192.168.0.18/24`.

Le `/24` signifie ici que mon réseau peut disposer d'un maximum de 254 hôtes
distincts. Vous vous demanderez probablement comment j'en viens à cette
conclusion. Dans le cas présent, je retire 24 de 32, j'obtiens 8, puis
j'effectue l'opération
2<small><sup>8</sup></small>&nbsp;-&nbsp;2&nbsp;=&nbsp;254. Gardez cette
formule magique dans un coin de la tête, nous y reviendrons un peu plus loin.

Normalement, chaque ordinateur dans un réseau possède une adresse IP, une série
de quatre nombres `a`.`b`.`c`.`d`&nbsp;: `a` et `d` sont compris entre 1 et 254,
`b` et `c` peuvent prendre toutes les valeurs comprises entre 0 et 255. Tentez
l'expérience&nbsp;: lancez `ip address` sur votre machine pour noter votre
adresse IP et votre réseau. Vous aurez quelque chose comme `192.168.1.10` ou
`10.23.2.4` (ou autre chose) pour l'adresse IP et `/24` ou `/23` ou `/16` (ou
autre chose) pour le réseau.

Théoriquement, tous les ordinateurs de la terre se répartissent donc des
adresses IP allant de `1.0.0.1` à `254.255.255.254`. Je dis bien
*théoriquement* et je prie les puristes de bien vouloir fermer les yeux. Mais à
quelques (gros) détails près, c'est ce qui se passe en réalité.

### IPv4 et IPv6

Vous vous êtes peut-être demandé ce que peut bien signifier `inet6
2a01:e0a:85f:f780:fe43:f4ff:eaac:19ac/64`. Il s'agit là tout simplement d'un
nouveau protocole d'adressage IP, dans la mesure où un protocole réseau mis au
point vers la fin des années 1990 peut être qualifié de "nouveau". 

Les adresses IP que nous traitons dans cet atelier pratique font partie du
protocole IPv4 à 32 bits&nbsp;: *grosso modo*, quatre nombres de 0 à 255 (en
notation décimale), séparés par des points. Le hic avec ce protocole, c'est que
le nombre d'adresses distinctes possibles est non seulement limité, mais
véritablement épuisé dans la plupart des pays de la planète. Les adresses IP
existantes y ont effectivement toutes été attribuées.

IPv6 constitue le remède à cette pénurie d'adresses. C'est un protocole à
128 bits&nbsp;; le nombre d'adresses possibles passe donc de 232 à 2128. En
contrepartie, les adresses IP du futur ressembleront à quelque chose du genre
`fe80::ebc5:8c3:26c4:1a13`.

> Le protocole IPv6 est donc l'avenir du réseau et, dans l'état actuel des
> choses, il le restera pour un bon moment.

Vous pouvez très bien afficher l'adresse IPv4 en faisant fi de l'IPv6&nbsp;:

```
$ ip -family inet address show enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP group default qlen 1000
    inet 192.168.0.18/24 brd 192.168.0.255 scope global noprefixroute 
       dynamic enp3s0 valid_lft 40351sec preferred_lft 40351sec
```

### Les adresses IP

#### Un peu de pratique

Voici une petite expérience amusante pour illustrer la notion d'adresse IP.
Prenons au hasard un nom de site web suffisamment connu, par exemple
`www.google.fr`. Ouvrons un terminal et invoquons la commande suivante&nbsp;:

```
$ host www.google.fr
www.google.fr has address 172.217.22.131
www.google.fr has IPv6 address 2a00:1450:4007:816::2003
```

> La commande `host` fait partie du paquet `bind-utils`. Installez-la en
> invoquant la commande `sudo yum install bind-utils`.

La commande me retourne deux réponses&nbsp;: une adresse IPv4 `172.217.22.131`
et une adresse IPv6 dont je ne me préoccupe pas.

Maintenant, ouvrez un navigateur web - par exemple Mozilla Firefox - sur votre
poste de travail, effacez le contenu de la barre d'adresses et mettez-y
`http://172.217.22.131`. Que constatez-vous&nbsp;?

> Notez que Mozilla Firefox – comme tous les navigateurs modernes – remplace
> aussitôt l'adresse IP saisie dans la barre d'adresses par le nom de domaine
> correspondant, dès l'ouverture de la page. Non content de cela, vous serez
> redirigé vers le protocole sécurisé HTTPS. Enfin, vous vous doutez
> probablement que derrière la machine `www.google.fr` se cache en réalité tout
> un parc de machines.  

L'expérience ne fonctionne pas avec n'importe quel nom de domaine, puisqu'elle
dépend de la configuration du serveur web correspondant. Vous pouvez quand même
la retenter avec un autre site, par exemple `www.centos.org`.

Sans aller chercher plus loin, vous commencez à avoir une idée un peu plus
concrète de l'utilité d'une adresse IP. Maintenant, revenons à notre réseau
local et à notre machine. J'ai les informations suivantes&nbsp;:

* Mon adresse IP est `192.168.0.18`.

* Mon réseau est `192.168.0.0/24`.

#### Un peu de théorie

Chaque adresse IP se subdivise en une partie réseau et une partie hôte. Prenons
deux exemples.

* J'ai un réseau `192.168.0.0/24`. Rappelez-vous la formule magique que nous
  avons vue un peu plus haut. Je retire 24 de 32, j'obtiens 8 et j'effectue
  l'opération 2<small><sup>8</sup></small>&nbsp;-&nbsp;2&nbsp;=&nbsp;254. Je
  peux donc configurer un réseau de 254 hôtes différents, avec des adresses IP
  allant de `192.168.0.1` à `192.168.0.254`. Ces machines pourront toutes
  communiquer entre elles.

* J'ai un réseau `192.168.0.0/16`. Je retire 16 de 32, j'obtiens 16 et
  j'effectue l'opération
  2<small><sup>16</sup></small>&nbsp;-&nbsp;2&nbsp;=&nbsp;65534. Je peux donc
  configurer un réseau de 65534 hôtes différents, avec des adresses IP allant
  de `192.168.0.1` à `192.168.255.254`. Ces machines pourront également toutes
  communiquer entre elles.

Pour différencier la partie réseau de la partie hôte dans une adresse IP, on
utilise souvent le masque de sous-réseau. L'outil `ip` préfère la notation CIDR
(*Classless Inter-Domain Routing*), qui donne le numéro du réseau suivi par une
barre oblique et le nombre de bits à 1 dans la notation binaire du masque de
sous-réseau. Pour trouver le masque de sous-réseau de `192.168.2.3/24`, je
peux utiliser l'outil `ipcalc` qui fait partie de mon système de base&nbsp;:

```
$ ipcalc --netmask 192.168.0.18/24
NETMASK=255.255.255.0
$ ipcalc --netmask 192.168.0.18/16
NETMASK=255.255.0.0
```

Un sous-réseau est un groupe de machines dont les adresses IP respectent un
certain ordre. Il peut être défini à l'aide de deux éléments&nbsp;:

* le réseau (*network prefix*)&nbsp;: quelque chose comme `192.168.0.0` ou
  `10.23.2.0`.

* le masque de sous-réseau (*subnet mask*)&nbsp;: quelque chose comme
  `255.255.255.0` ou `255.255.0.0`.

À première vue, on ne voit pas trop comment ces deux éléments sont censés
définir l'ensemble des adresses IP d'un sous-réseau. Or, il suffit de les
convertir en binaire pour mieux comprendre. Le masque définit l'emplacement des
*bits* dans une adresse IP qui sont communs au sous-réseau. Voici par exemple
les formes binaires de `192.168.0.0` et `255.255.255.0`&nbsp;:

```
192.168.0.0   : 11000000 10101000 00000000 00000000
255.255.255.0 : 11111111 11111111 11111111 00000000
```

Et voici la même conversion pour `10.23.2.0` et `255.255.0.0`&nbsp;:

```
10.23.2.0     : 00001010 00010111 00000010 00000000
255.255.0.0   : 11111111 11111111 00000000 00000000
```

Dans chacun des exemples, je peux faire varier les *bits* dans la partie à
droite au-dessus des zéros du masque pour obtenir une adresse IP valide dans le
sous-réseau.

#### Peaufiner l'affichage des adresses IP

L'outil `ip` dispose de toute une série d'options d'affichage, que nous allons
explorer ici. Revenons à la commande que nous avons invoquée un peu plus
haut&nbsp;:

```
$ ip address show enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP group default qlen 1000
    link/ether 44:37:e6:da:44:72 brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.18/24 brd 192.168.0.255 scope global noprefixroute 
       dynamic enp3s0 valid_lft 40078sec preferred_lft 40078sec
    inet6 2a01:e0a:85f:f780:fe43:f4ff:eaac:19ac/64 scope global noprefixroute 
       dynamic valid_lft 86394sec preferred_lft 86394sec
    inet6 fe80::28a4:290:3e81:6baf/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Nous avons vu que l'option `-family inet` nous limite à l'affichage de
l'adresse IPv4&nbsp;:

```
$ ip -family inet address show enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP group default qlen 1000
    inet 192.168.0.18/24 brd 192.168.0.255 scope global noprefixroute 
       dynamic enp3s0 valid_lft 40018sec preferred_lft 40018sec
```

L'option `-color` met de la couleur dans l'affichage du résultat et le rend
beaucoup plus lisible :

```
$ ip -color -family inet address show enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP group default qlen 1000
    inet 192.168.0.18/24 brd 192.168.0.255 scope global noprefixroute 
       dynamic enp3s0 valid_lft 39974sec preferred_lft 39974sec
```

Enfin, l'option `-brief` va droit au but et limite l'affichage à l'état de
l'interface réseau et son adresse IP&nbsp;:

```
$ ip -brief -family inet address show enp3s0
enp3s0           UP             192.168.0.18/24
```

Jusqu'ici, j'ai fait exprès de détailler les options longues parce qu'elles
sont plus parlantes. Jetez un oeil dans la page de manuel de la
commande `ip(8)`&nbsp;; vous verrez que chaque option comprend également une
version courte&nbsp;

```
$ ip -c -4 a s enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast 
       state UP group default qlen 1000
    inet 192.168.2.3/24 brd 192.168.2.255 scope global noprefixroute 
       dynamic enp3s0 valid_lft 80844sec preferred_lft 80844sec
```

### Établir un contact avec une machine distante : ping

Retenons le fait que les machines d'un même réseau sont capables de communiquer
entre elles. Pour l'instant, notre réseau ne comporte que deux machines, si
l'on peut dire : le serveur Linux et le routeur. 

> Le routeur est aussi une machine (ou un hôte), même s'il n'est doté ni d'un
> clavier ni d'un écran.  Il contient un petit système d'exploitation embarqué
> (Linux, eh oui&nbsp;!) et il s'acquitte des quelques tâches simples pour
> lesquelles il est construit.

Normalement, un routeur est livré avec une adresse IP fixe préconfigurée par
défaut, indiquée sur la petite note explicative qui l'accompagne. À titre
d'exemple, les Livebox de chez Orange sont souvent préconfigurées avec une
adresse `192.168.1.1`. Dans notre cas, la Freebox a une adresse IP par défaut
`192.168.0.254`. Voyons voir si j'arrive à établir une liaison avec la box.
Pour cela, j'utilise la commande `ping`, qui dit en quelque sorte "Allô, il y a
quelqu'un&nbsp;?"&nbsp;:

```
$ ping -c 4 192.168.0.254
PING 192.168.0.254 (192.168.0.254) 56(84) bytes of data.
64 bytes from 192.168.0.254: icmp_seq=1 ttl=64 time=0.298 ms
64 bytes from 192.168.0.254: icmp_seq=2 ttl=64 time=0.274 ms
64 bytes from 192.168.0.254: icmp_seq=3 ttl=64 time=0.283 ms
64 bytes from 192.168.0.254: icmp_seq=4 ttl=64 time=0.307 ms

--- 192.168.0.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3000ms
rtt min/avg/max/mdev = 0.274/0.290/0.307/0.021 ms
```

Le routeur a répondu&nbsp;!

### Le routeur : un centre de tri pour paquets numériques

Je viens d'insister sur le fait que les machines d'un même réseau peuvent
communiquer entre elles. Dans ce cas, comment se fait-il qu'on puisse
communiquer, par exemple, avec la machine `172.217.22.131`, alors que celle-ci
ne fait manifestement pas partie de notre réseau `192.168.0.0/24`&nbsp;?

```
$ ping -c 4 172.217.22.131
PING 172.217.22.131 (172.217.22.131) 56(84) bytes of data.
64 bytes from 172.217.22.131: icmp_seq=1 ttl=119 time=19.7 ms
64 bytes from 172.217.22.131: icmp_seq=2 ttl=119 time=19.6 ms
64 bytes from 172.217.22.131: icmp_seq=3 ttl=119 time=19.6 ms
64 bytes from 172.217.22.131: icmp_seq=4 ttl=119 time=19.6 ms

--- 172.217.22.131 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 19.657/19.680/19.710/0.141 ms 
```

Réponse : parce que nous passons par le routeur. Ce dernier est défini en tant
que passerelle (*gateway*), qui sert à faire communiquer votre machine avec le
monde extérieur. L'information que vous envoyez ou que vous recevez transite
sur le réseau sous forme de paquets. Si une passerelle est définie, tous les
paquets qui ne concernent pas le réseau local sont envoyés par cette porte vers
le monde extérieur. La commande `ip route show` indique si la passerelle est
définie correctement&nbsp;:

```
$ ip route show
default via 192.168.0.254 dev enp3s0 proto dhcp metric 100
192.168.0.0/24 dev enp3s0 proto kernel scope link src 192.168.0.18 metric 100
```

#### Sous les pavés numériques, la plage d'adresses IP privées

La commande `ip address` vous a retourné votre adresse IP sous la forme `inet
192.168.0.18/24`. Même si le nom le suggère, cette adresse n'est pas
normalement "joignable" à partir d'Internet. Elle fait partie de la plage
standardisée d'adresses IP privées réservées aux réseaux locaux privés&nbsp;:

* `10.0.0.0` à `10.255.255.255`

* `172.16.0.0` à `172.31.255.255`

* `192.168.0.0` à `192.168.255.255`

Bien sûr, la solution la plus simple consisterait à utiliser une adresse
publique. Le hic, c'est que vous devez acheter celle-ci chez un fournisseur
d'accès. Un abonnement à Internet vous fournit en général une seule adresse
publique pour vous rattacher au monde extérieur. C'est largement suffisant si
vous n'utilisez qu'une seule machine. Pour configurer votre réseau domestique,
il vous faudrait acheter un bloc d'adresses entier, ce qui revient très cher,
mais rassurez-vous, il existe une solution au problème.

#### Relier le public et le privé

Un routeur est un hôte un peu spécial, qui assure la communication entre deux
réseaux. Il fait transiter les paquets d'un réseau à un autre. Dans les réseaux
domestiques, c'est le plus souvent le modem routeur qui assume ce rôle.  Dans
le réseau local d'une TPE ou d'une PME – comme dans le bureau où j'écris ces
lignes – c'est souvent une machine un peu spéciale, un serveur équipé d'au
moins deux cartes réseau, qui fait office de passerelle vers le monde
extérieur. Dans un cas comme dans l'autre, à partir du moment où nous avons un
réseau local avec plusieurs machines, tous les hôtes du réseau utilisent cette
passerelle pour communiquer avec les machines en dehors du réseau.

Sans trop entrer dans les détails, l'astuce du routeur consiste à manipuler les
paquets qu'il fait transiter. Côté Internet, le routeur dispose d'une adresse
publique. Il se fait donc passer lui-même pour l'expéditeur de chaque paquet
qu'il envoie sur Internet. La procédure s'inverse pour les paquets
entrants&nbsp;: la passerelle remplace l'adresse de destination par
l'adresse IP privée de la machine qui attend une réponse d'Internet.

> S'il ne fallait retenir que cela&nbsp;: dans un réseau local privé, la
> passerelle vous permet de communiquer avec Internet.

#### Le système de noms de domaine : l'annuaire d'Internet

Maintenant, comment s'y retrouver dans toute cette jungle d'adresses IP&nbsp;?
Est-ce qu'il faut se constituer un agenda, comme dans le bon vieux temps où les
téléphones avaient juste un cadran et rien d'autre&nbsp;? Eh non, ce n'est pas
nécessaire grâce à DNS, le système de noms de domaine (*Domain Name System*).
Les humains savent passablement mémoriser et gérer des noms comme
`www.google.fr` ou `www.centos.org`. Le système de noms de domaine nous épargne
donc la tâche pénible de devoir mémoriser des adresses comme `172.217.22.131`
ou `85.12.30.226`.  Vous pouvez d'ailleurs filer la métaphore avec les numéros
de téléphone en considérant DNS comme un service d'annuaire global.

Les serveurs DNS (qu'on appelle aussi "serveurs de noms") sont organisés de
façon hiérarchique. Lorsqu'un serveur n'arrive pas à résoudre un certain nom,
c'est-à-dire à fournir une adresse IP correspondante, il envoie à son tour une
requête au prochain serveur dans la hiérarchie et ainsi de suite. Chaque client
doit donc connaître au moins un serveur DNS pour ensuite se faufiler dans cette
hiérarchie.

Lorsque nous avons invoqué la commande `host` un peu plus haut, nous avons fait
exactement cela&nbsp;: nous avons envoyé une requête à un serveur de noms en
lui fournissant un nom de domaine et il nous a gracieusement retourné une
adresse IP.

## Configurer une connexion à Internet

Tentons un petit récapitulatif. Quel est le minimum syndical dont nous avons
besoin pour configurer une connexion à Internet&nbsp;? Ici, je vais partir du
principe que le matériel est correctement géré par le système&nbsp;:

* une adresse IP pour la machine&nbsp;;

* un masque de sous-réseau pour la machine&nbsp;;

* l'adresse IP de la passerelle&nbsp;;

* au moins une adresse IP de serveur DNS.

C'est tout. Ces quatre conditions suffisent pour vous connecter à Internet.

Dans notre exemple, nous n'avons pas vraiment configuré quoi que ce soit. Nous
nous sommes contentés d'afficher les détails de la configuration. D'où nous
viennent donc ces données&nbsp;?

### Configuration dynamique : DHCP

La réponse à notre question est simple et elle s'appelle DHCP (*Dynamic Host
Configuration Protocol*). Ce sigle désigne le protocole d'allocation dynamique
d'adresses IP.

Dans la configuration par défaut d'Oracle Linux, les messages de démarrage du
système sont remplacés par une simple barre de progression horizontale en bas
de l'écran. Lorsqu'on appuie sur la touche ++esc++ pour les faire apparaître,
ils défilent à une telle allure qu'on n'a pas vraiment le temps de les lire.

Si nous voulons en savoir un peu plus sur ce qui s'est passé lors du dernier
démarrage, nous pouvons jeter un oeil dans le fichier `/var/log/messages`. En
cherchant un peu, nous tombons sur la partie qui nous intéresse&nbsp;:

```
Jun  6 08:47:56 ... dhcp4 (enp3s0):   address 192.168.0.18
Jun  6 08:47:56 ... dhcp4 (enp3s0):   plen 24 (255.255.255.0)
Jun  6 08:47:56 ... dhcp4 (enp3s0):   gateway 192.168.0.254
Jun  6 08:47:56 ... dhcp4 (enp3s0):   lease time 43200
Jun  6 08:47:56 ... dhcp4 (enp3s0):   nameserver '192.168.0.254'
```

En clair, notre machine a requis un bail DHCP (*DHCP lease*). Le serveur DHCP
attribue alors l'adresse `192.168.0.18` et le masque de sous-réseau
`255.255.255.0` à notre machine, pour une durée de douze heures (`43200`
secondes). Enfin, l'adresse IP de la passerelle `192.168.0.254` (`gateway`) est
également définie, ainsi que le serveur DNS (`nameserver`) avec la même
adresse IP.

Votre modem routeur intègre un serveur DHCP. C'est lui qui se charge de
configurer votre machine *par magie* au moment du démarrage. 

#### Paramétrer le serveur DHCP intégré dans le modem routeur

Le paramétrage du modem routeur s'effectue généralement par le biais d'une
interface web, en ouvrant un navigateur web à l'adresse IP locale de la box.
Dans d'autres cas de figure - comme les Freebox - la configuration se fait
sur le site web du fournisseur d'accès.

Ouvrez l'interface de paramétrage de votre box et essayez de retrouver les
informations suivantes&nbsp;:

* l'adresse IP de la box&nbsp;;

* le masque de sous-réseau&nbsp;;

* l'activation et la désactivation du serveur DHCP intégré&nbsp;;

* la plage d'adresses dynamiques attribuées par le serveur DHCP.

#### La configuration DHCP en détail

Il est temps maintenant de voir comment notre ordinateur est configuré
"en DHCP". Lorsque j'ai installé Oracle Linux sur mon serveur bac à sable,
l'installateur a reconnu que l'interface `enp3s0` correspondait à la carte
réseau active et l'a configurée en DHCP. Voyons maintenant ce que cela a donné
sous le capot et rendons-nous dans le répertoire
`/etc/sysconfig/network-scripts`. Comme son nom le suggère, ce répertoire
contient toute une série de fichiers qui gèrent la configuration réseau de la
machine. Intéressons-nous plus particulièrement aux fichiers dont le nom
commence par `ifcfg`&nbsp;:

```
$ cd /etc/sysconfig/network-scripts/
$ ls -l ifcfg-*
-rw-r--r--. 1 root root 282 Jun  7 07:59 ifcfg-enp3s0
-rw-r--r--. 1 root root 254 May 22  2020 ifcfg-lo
```

Ne vous inquiétez pas si vous ne voyez pas la même chose que dans l'exemple.
Chacun des fichiers `ifcfg-*` correspond à une interface réseau de la machine. La
configuration de mon interface `enp3s0` est donc écrite dans le fichier
`ifcfg-enp3s0`.

> Lorsque vous vous demandez ce que peut bien contenir un fichier, de quel type
> il est – si c'est un fichier texte, une image, un document PDF, un binaire
> exécutable ou autre chose – la commande `file` vous le dit. Certes, dans de
> nombreux cas, un suffixe de fichier (comme `.txt` ou `.jpg`) vous en informe,
> mais sachez que ce suffixe ne vous garantit rien. En d'autres termes, il est
> possible de tricher en renommant le fichier. Pour en avoir le coeur net,
> utilisez la commande `file`. 

Voyons d'abord de quel type de fichier il s'agit&nbsp;:

```
$ file ifcfg-enp3s0
ifcfg-enp3s0: ASCII text
```

Il s'agit d'un fichier texte ASCII. Ce qui m'importe ici, c'est qu'il soit
humainement lisible et que je puisse afficher son contenu à l'aide d'outils
comme `cat`, `less`, `head` ou de mon éditeur de texte préféré&nbsp;:

```
$ cat ifcfg-enp3s0
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp3s0
UUID=90aa8415-5641-4332-96da-c1642d9570c2
DEVICE=enp3s0
ONBOOT=yes
```

> ASCII ou *American Standard Code for Information Interchange* signifie qu'il
> s'agit de texte simple, c'est-à-dire **pas** écrit par des mangeurs de
> grenouilles et de fromages qui cavalent sur la table. Ce texte ne contient
> donc pas de caractères comme `ç`, `é`, `ô` ou autres horreurs du genre.

Ce fichier `ifcfg-enp3s0` a été généré automatiquement lors de l'installation
de la machine. Même si vous ne comprenez pas tout, vous devinez probablement le
sens des principales directives&nbsp;:

* `DEVICE="enp3s0"` – Un *device*, c'est un périphérique. En l'occurrence,
  c'est l'interface réseau en question, c'est-à-dire notre carte Ethernet.

* `BOOTPROTO="dhcp"` – Cette ligne indique que la machine doit demander sa
  configuration IP à un serveur DHCP sur le réseau.

* `ONBOOT="yes"` – L'interface est activée au démarrage.

Le serveur DHCP nous a donc fourni l'adresse IP, le masque de sous-réseau, la
passerelle et l'adresse du serveur DNS. Comment faisons-nous pour connaître ce
dernier d'ailleurs&nbsp;? C'est le fichier `/etc/resolv.conf` qui nous le
dira&nbsp;:

```
$ cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 192.168.0.254
...
```

Le serveur de noms utilisé ici est donc celui qui est intégré dans ma Freebox.

## Gérer la configuration réseau avec NetworkManager

NetworkManager est un outil de gestion de réseau développé par Red Hat depuis
2004, et qui a changé la donne notamment pour les utilisateurs d'ordinateurs
portables sous Linux.

Red Hat met en avant l'utilisation de NetworkManager depuis RHEL&nbsp;7.0.
Cette préférence a tendance à s'accentuer depuis la publication de
RHEL&nbsp;8.0.

* Sous RHEL et Oracle Linux 7.x il est tout à fait possible de supprimer tous les
  paquets `NetworkManager-*` et de passer par la configuration manuelle des
  scripts `ifcfg-*` dans `/etc/sysconfig/network-scripts`.

* Sous RHEL et Oracle Linux 8.x cette manière de procéder est tolérée, quoique
  considérée officiellement comme obsolète. Le guide officiel *Considerations
  in adopting Red Hat Enterprise Linux 8* le dit bien&nbsp;: *Network scripts
  are deprecated in Red Hat Enterprise Linux 8 and are no longer provided by
  default.*

Bref, fini l'âge d'or de la philosophie Unix et du principe KISS où les
administrateurs réseau chassaient l'ours à mains nues et éditaient une poignée
de scripts pour configurer leurs serveurs.

Parmi les avantages de NetworkManager, on notera l'abolition des idiosyncrasies
de configuration spécifiques aux distributions. De ce point de vue,
NetworkManager est au réseau ce que `systemd` est à l'initialisation et la
gestion des services.

### Configuration statique

Jusque-là, c'est le serveur DHCP qui a "décidé" de la configuration IP de notre
machine. Il est possible de faire autrement, en configurant notre machine de
manière statique. Sur un serveur, c'est même la manière orthodoxe de procéder.

Nous avons vu que le minimum syndical pour configurer une connexion à Internet,
c'était une adresse IP et un masque de sous-réseau pour la machine, une
adresse IP de passerelle et – au moins – une autre pour un serveur DNS.

Voici quelques règles à respecter dans le choix d'une adresse IP&nbsp;

* D'une part, l'adresse IP doit appartenir au même réseau que les autres
  machines, le routeur en l'occurrence. Si votre routeur est `192.168.0.254`
  avec un masque de sous-réseau `255.255.255.0` et si vous définissez une
  adresse `192.168.3.2` avec un masque `255.255.255.0`, vous n'irez pas bien
  loin, étant donné que les deux adresses IP appartiennent à des réseaux
  différents. Les deux machines ne pourront pas s'envoyer de `ping`.

* D'autre part, il ne doit pas y avoir de conflit d'adresses, c'est-à-dire que
  toutes les machines du réseau doivent avoir des adresses IP bien distinctes.
  Dans une salle de classe, ce n'est pas bien grave si deux élèves répondent au
  nom de "Tom"&nbsp;; l'institutrice fera la distinction et tranchera en cas de
  confusion. Dans un réseau informatique en revanche, c'est très ennuyeux si
  deux machines ont la même adresse IP&nbsp;; vous ne pourrez pas en montrer
  une du doigt et lui dire&nbsp;: "Non, pas toi, l'autre&nbsp;!"

* Enfin, pour faire les choses proprement, prenez l'habitude de définir les
  adresses IP statiques en dehors de la plage d'adresses attribuées par le
  serveur DHCP. Comme cela, si quelqu'un vient se connecter à l'improviste à
  votre réseau, avec son ordinateur portable par exemple, il ne se verra pas
  attribuer une adresse déjà utilisée par ailleurs.

Dans notre exemple, je choisis l'adresse IP `192.168.0.250` pour ma machine.
D'une part, elle n'est utilisée par aucune autre machine dans le réseau.
D'autre part, elle se situe en dehors de la plage d'adresses attribuées par le
serveur DHCP.

Sur un serveur sans interface graphique, je dispose de deux options pour
contrôler la configuration de NetworkManager&nbsp;:

* l'outil en ligne de commande `nmcli`&nbsp;;

* l'interface graphique en mode texte NetworkManager TUI (*Text User
  Interface*) invoquée par la commande `nmtui`.

Dans le cas de figure présent, l'outil graphique – basé sur la bibliothèque
`ncurses` – constitue la solution la plus confortable.

Affichez les interfaces réseau gérées par NetworkManager&nbsp;

```
$ nmcli dev
DEVICE  TYPE      STATE      CONNECTION
enp3s0  ethernet  connected  enp3s0
lo      loopback  unmanaged  --
```

Lancez NetworkManager TUI pour configurer la connexion&nbsp;:

```
$ sudo nmtui
```

* Sélectionnez `Éditer la connexion`.

* Éditez la connexion.

* Remplacez le nom de profil par quelque chose de parlant comme `LAN`.

* Passez `IPv4 CONFIGURATION` de `Automatic` à `Manual`.

* Cliquez sur `Show` pour afficher les détails.

* Fournissez l'adresse IP du serveur en notation CIDR, par exemple
  `192.168.0.250/24`.

* Renseignez l'adresse IP de la passerelle dans le champ `Gateway`.

* N'indiquez rien dans les champs `DNS server` et `Search domains`.

* Passez `IPv6 CONFIGURATION` de `Automatic` à `Ignore`.

* Confirmez par `OK`.

* Revenez à la fenêtre principale.

* Activez la connexion `LAN` : `Activate a connection` > `LAN`

* Quittez NetworkManager TUI : `Quit`

Redémarrez pour prendre en compte la nouvelle configuration&nbsp;:

```
$ sudo reboot
```

> Les experts objecteront que j'aurais pu me passer d'un redémarrage. Certes,
> mais au vu du fonctionnement idiosyncratique de NetworkManager, je n'ai pas
> voulu compliquer les choses inutilement.

#### Renseigner les serveurs DNS

Il ne nous reste plus qu'un détail à régler pour rendre notre configuration
statique fonctionnelle. Éditez `/etc/resolv.conf` et indiquez l'adresse du
serveur DNS, précédée de la directive `nameserver`&nbsp;:

```
# /etc/resolv.conf
nameserver 192.168.0.254
```

Si vous souhaitez utiliser les serveurs DNS de Cloudflare, vous éditerez
`/etc/resolv.conf` comme suit&nbsp;:

```
# /etc/resolv.conf
nameserver 1.1.1.1
nameserver 1.0.0.1
```

> Les serveurs DNS fournis par Cloudflare sont rapides et respectent votre vie
> privée. Malheureusement, vous ne pourrez pas les utiliser avec certaines
> Livebox, étant donné que les ingénieurs de chez Orange ont eu la mauvaise
> idée de considérer l'adresse IP publique `1.1.1.1` comme une adresse privée
> utilisée par la puce du modem.

Alternativement, vous pouvez utiliser les serveurs DNS fournis par
Google&nbsp;:

```
# /etc/resolv.conf
nameserver 8.8.8.8
nameserver 8.8.4.4
```

Testez la connexion en envoyant un `ping` vers une machine extérieure&nbsp;:

```
$ ping -c 4 www.google.fr
PING www.google.fr (142.250.179.99) 56(84) bytes of data.
64 bytes from ... (142.250.179.99): icmp_seq=1 ttl=119 time=19.5 ms
64 bytes from ... (142.250.179.99): icmp_seq=2 ttl=119 time=19.7 ms
64 bytes from ... (142.250.179.99): icmp_seq=3 ttl=119 time=19.6 ms
64 bytes from ... (142.250.179.99): icmp_seq=4 ttl=119 time=19.6 ms

--- www.google.fr ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 19.509/19.653/19.770/0.195 ms
```

## Diagnostiquer le réseau en cas de panne

Tout cela vous paraît bien compliqué&nbsp;? Sachez que dans l'écrasante
majorité des cas, les paramètres réseau d'une machine se définissent lors de
son installation. Une fois que c'est fait, c'est assez rare que l'on soit
obligé de remettre les mains à la pâte.

En contrepartie, il peut arriver que quelque chose coince dans le réseau et,
dans ce cas, la connaissance des principes de base et d'une poignée d'outils de
diagnostic fera toute la différence. Le jour où votre connexion à Internet sera
interrompue pour une raison inconnue, vous ne resterez plus "comme le bœuf
devant la nouvelle porte de l'étable", comme on dit dans mon pays natal.

Les mécaniciens dans les garages BMW disposent de véritables *checklists* qui
les aident à faire face de façon un peu plus systématique à toute une série de
dysfonctionnements éventuels. Nous pourrions nous amuser à établir notre propre
liste de détails à vérifier pour le cas d'une connexion à Internet défaillante.
La liste ne sera pas forcément exhaustive, mais cela vous aidera.

* La carte réseau est-elle physiquement installée dans la machine&nbsp;? La
  question peut paraître stupide, mais il arrive qu'un utilisateur qui garde le
  serveur sous son bureau lui donne un coup de pied par inadvertance et, de ce
  fait, déloge la carte Ethernet de son emplacement. Si la commande `lspci
  | grep -i eth` ne vous retourne rien, il faudra ouvrir le serveur et remettre
  la carte réseau en place.

* La carte est-elle correctement gérée par le système ? Que disent `ip address`
  et `dmesg | grep -i eth`&nbsp;?

* La machine est-elle censée être configurée en IP dynamique (DHCP) ou en
  IP statique&nbsp;?

* L'interface est-elle activée&nbsp;? Si vous invoquez `ip address`, est-ce que
  vous voyez bien `state UP`&nbsp;?

* Sommes-nous physiquement reliés au modem routeur&nbsp;? Arrivons-nous à
  envoyer un `ping` à ce dernier ? Là aussi, les petites languettes sur les
  connecteurs RJ45 s'arrachent parfois et il en résulte de simples faux
  contacts. Une petite lumière verte sur la prise Ethernet vous indique si le
  contact est bon.

* Parvenons-nous à envoyer un `ping` à d'autres machines du réseau&nbsp;? Dans
  le cas contraire, avons-nous des doublons dans le réseau&nbsp;? Y a-t-il un
  conflit d'adresses IP quelque part&nbsp;?

* Arrivons-nous à envoyer un `ping` vers une adresse IP sur Internet&nbsp;? Essayez
  par exemple `8.8.8.8` ou `8.8.4.4` (les deux serveurs DNS de Google), ou les DNS
  de votre FAI. Si cela ne marche pas, c'est probablement un problème de
  passerelle mal définie. Vérifiez le résultat de `ip route show`.

* Essayez d'envoyer un `ping` vers un nom de domaine comme `google.fr` ou
  `yahoo.fr`. Si vous arrivez à joindre des adresses IP sur Internet, alors que
  la résolution de noms de domaine échoue, c'est à coup sûr un problème de DNS
  mal renseigné. Dans ce cas, vérifiez le contenu de `/etc/resolv.conf` et
  voyez si les adresses IP des serveurs DNS y figurent bien.

## Tradition : ifconfig et route

Les commandes `ifconfig` et `route` fournies par le paquet `net-tools` sont
officiellement considérées comme obsolètes et c'est la commande `ip` – plus
fonctionnelle et plus flexible – qui est censée les remplacer. Dans la pratique
quotidienne, les administrateurs du monde entier continuent allègrement à
utiliser les anciennes commandes et vous risquez de tomber dessus tôt ou tard,
par exemple en lisant des pages de documentation un peu anciennes. Je vais donc
vous les présenter très brièvement.

Installez les commandes réseau obsolètes&nbsp;:

```
$ sudo yum install -y net-tools
```

Affichez la configuration des interfaces réseau actives&nbsp;:

```
$ ifconfig
enp3s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.250  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::4637:e6ff:feda:4472  prefixlen 64  scopeid 0x20<link>
        inet6 2a01:e0a:85f:f780:4637:e6ff:feda:4472  prefixlen 64  ...
        ether 44:37:e6:da:44:72  txqueuelen 1000  (Ethernet)
        RX packets 2576  bytes 521167 (508.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1738  bytes 169536 (165.5 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 104  bytes 7640 (7.4 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 104  bytes 7640 (7.4 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Affichez toutes les interfaces réseau disponibles&nbsp;:

```
$ ifconfig -a
```

Affichez la table de routage&nbsp;:

```
$ route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.0.254   0.0.0.0         UG    100    0        0 enp3s0
192.168.0.0     0.0.0.0         255.255.255.0   U     100    0        0 enp3s0
```

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



**Objectif** : afficher les messages système en anglais. 

## La lingua franca du monde du libre

Actuellement, de nombreuses distributions Linux courantes et moins courantes
sont à peu près intégralement traduites en français. Autrement dit, vous
choisissez votre langue au moment de lancer l'installation et, à partir de là,
la procédure s'effectue entièrement en français, comme nous avons pu le voir
lors de l'installation d'Oracle Linux.

Si le simple **utilisateur** d'un poste de travail sous Linux peut légitimement
s'attendre à ce que son environnement graphique s'affiche entièrement dans sa
propre langue, cela n'est pas tout à fait vrai pour **l'administrateur** du
système, pour plusieurs raisons.

* Les manuels en ligne ne sont que partiellement disponibles dans d'autres
  langues que l'anglais. Leur traduction dans toutes les langues est une
  entreprise d'une envergure pharaonique.

* La *lingua franca* d'un grand nombre de projets du monde du libre (comme
  Oracle Linux) reste l'anglais. Il existe certes des projets localisés pour
  chaque pays, mais très souvent, le gros de la communication passe par les
  forums, les listes de diffusion et les canaux IRC anglophones. Dans le cas
  d'Oracle Linux, le forum [*Applications and Infrastructure
  Community*](https://community.oracle.com/tech/apps-infra/categories/oracle_linux)
  constitue le principal canal de communication du projet. Même s'il est
  théoriquement possible de survivre en tant qu'administrateur Linux sans
  parler un seul mot d'anglais, attendez-vous à quelques complications. Autant
  vouloir entamer une carrière au Vatican sans comprendre un mot de latin.

* Sur un système configuré en français, certains messages système comme les
  audits SELinux s'affichent dans un mélange de français et d'anglais assez
  folklorique, et il vaut mieux privilégier l'anglais.

## Basculer vers l'anglais

Dans la configuration par défaut, notre système Oracle Linux est en français,
étant donné que nous avons sélectionné `Français/France` lors de la définition
des paramètres régionaux.

Connectez-vous en tant qu'utilisateur simple&nbsp;:

```
linuxbox login : microlinux
Password : ********
```

Vous vous retrouvez face à l'invite de commande&nbsp;:

```
[microlinux@linuxbox ~]$ _
```

La commande `localectl status` permet d'afficher les paramètres régionaux du
système&nbsp;:

```
[microlinux@linuxbox ~]$ localectl status
   System Locale: LANG=fr_FR.UTF-8
       VC Keymap: ch-fr
      X11 Layout: ch
     X11 Variant: fr
```

La directive `list-locales` affiche la liste des localisations disponibles sur
notre système&nbsp;:

```
[microlinux@linuxbox ~]$ localectl list-locales
C.utf8
aa_DJ
aa_DJ.iso88591
aa_DJ.utf8
aa_ER
aa_ER.utf8
aa_ER.utf8@saaho
aa_ER@saaho
aa_ET
aa_ET.utf8
...
en_US
en_US.iso88591
en_US.iso885915
en_US.utf8
...
fr_FR
fr_FR.iso88591
fr_FR.iso885915@euro
fr_FR.utf8
...
```

* Utilisez la touche ++space++ pour avancer d'une page.

* La touche ++q++ permet de quitter la liste des localisations.

Pour basculer le système vers l'anglais, nous allons utiliser la directive
`set-locale` comme ceci&nbsp;:

```
$ sudo localectl set-locale LANG=en_US.utf8
```

À partir de là, les messages du système s'affichent en anglais.

```
[microlinux@linuxbox ~]$ make love
make: *** No rule to make target `love'.  Stop.
```

## SSH et les variables d'environnement

Si vous vous connectez à distance, vous pouvez vous retrouver confronté à un
comportement pour le moins surprenant&nbsp;:

```
[kikinovak@alphamule:~] $ ssh microlinux@linuxbox
microlinux@linuxbox's password:
Last login: Sat May 15 14:08:29 2021 from alphamule.microlinux.lan
[microlinux@linuxbox ~]$ localectl status
   System Locale: LANG=en_US.utf8
       VC Keymap: ch-fr
      X11 Layout: ch
     X11 Variant: fr
[microlinux@linuxbox ~]$ echo $LANG
fr_FR.UTF-8
```

L'inconsistance manifeste des paramètres régionaux s'explique ici par le fait
que la variable `LANG` est transmise depuis le client SSH, en l'occurrence ma
station de travail OpenSUSE Leap qui utilise le français (`fr_FR.UTF-8`) par
défaut. 

Pour éviter ce genre de situation, j'ai le choix entre trois solutions&nbsp;:

* Redéfinir la variable `LANG` avant la connexion.

* Empêcher le client SSH d'envoyer les variables d'environnement.

* Empêcher le serveur de recevoir les variables en question.

### Redéfinir explicitement la variable LANG

Pour redéfinir ma variable `LANG` avant de me connecter à distance, il suffit
que j'invoque la commande `ssh` comme ceci :

```
$ LANG=en_US.utf8 && ssh microlinux@linuxbox
```

> C'est la solution la plus simple, qui n'exige aucune compétence particulière.
> Les deux solutions ci-après s'adressent aux administrateurs Linux qui ont
> déjà un peu d'expérience.

### Empêcher le client SSH d'envoyer les variables

Sur ma station de travail OpenSUSE Leap, le client SSH se configure par le
biais du fichier `/etc/ssh/ssh_config`. J'ouvre ce fichier et je repère les
directives `SendEnv`&nbsp;:

```
SendEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
SendEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
SendEnv LC_IDENTIFICATION LC_ALL
```

Dans un premier temps, je commente toutes ces directives, c'est-à-dire je les
fais précéder d'un signe dièse `#` pour les rendre inopérantes&nbsp;:

```
# SendEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
# SendEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
# SendEnv LC_IDENTIFICATION LC_ALL
```

Je prends en compte les modifications :

```
$ sudo systemctl reload sshd
```

À partir de là les variables d'environnement ne sont plus transmises.

### Empêcher le serveur de recevoir les variables 

Sur le serveur Oracle Linux, le service SSH se configure par le biais du
fichier `/etc/ssh/sshd_config`. Ouvrez ce fichier et repérez les directives
`AcceptEnv`. 

```
# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS
```

Là encore, je commente toutes les directives, c'est-à-dire je les fais précéder
d'un signe dièse `#` pour les rendre inopérantes&nbsp;:

```
# Accept locale-related environment variables
# AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
# AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
# AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
# AcceptEnv XMODIFIERS
```

Je prends en compte les modifications&nbsp;:

```
$ sudo systemctl reload sshd
```

Et le tour est joué.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


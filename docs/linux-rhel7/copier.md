
**Objectif** : prise en main des opérations de copie, de déplacement et de
renommage de fichiers.

## Copier des fichiers et des répertoires avec cp

La commande `cp` (*copy*) sert à copier des fichiers. Dans son utilisation la plus
basique, elle duplique un fichier d’un endroit à un autre. Prenons par exemple
un fichier de notre répertoire d’utilisateur et copions-le dans le
répertoire `/tmp`&nbsp;:

```
$ ls -l livres.txt 
-rw-rw-r--. 1 microlinux microlinux 55 Oct 15 06:37 livres.txt
$ cp livres.txt /tmp/
$ ls -l /tmp/livres.txt 
-rw-rw-r--. 1 microlinux microlinux 55 Oct 16 04:58 /tmp/livres.txt
```

Pour copier des répertoires entiers avec leur contenu, il faudra invoquer `cp`
avec l’option `-R` (comme *recursive*, "récursif"). Dans l’exemple, j’utilise
en plus l’option `-v` qui explicite bien chaque détail de l’opération&nbsp;:

```
$ tree Fichiers 
Fichiers
|-- Documents
|   `-- texte.txt
|-- Films
|   `-- film.avi
`-- Images
    `-- photo.jpg
$ cp -Rv Fichiers /tmp/
'Fichiers' -> '/tmp/Fichiers'
'Fichiers/Documents' -> '/tmp/Fichiers/Documents'
'Fichiers/Documents/texte.txt' -> '/tmp/Fichiers/Documents/texte.txt'
'Fichiers/Films' -> '/tmp/Fichiers/Films'
'Fichiers/Films/film.avi' -> '/tmp/Fichiers/Films/film.avi'
'Fichiers/Images' -> '/tmp/Fichiers/Images'
'Fichiers/Images/photo.jpg' -> '/tmp/Fichiers/Images/photo.jpg'
$ tree /tmp/Fichiers 
/tmp/Fichiers
|-- Documents
|   `-- texte.txt
|-- Films
|   `-- film.avi
`-- Images
    `-- photo.jpg
```

Voici maintenant une utilisation de `cp` qui peut ressembler (de loin) au
quotidien réel d’un administrateur système. Créez un fichier de configuration
`config` dans votre répertoire utilisateur. Effectuez-en ensuite une copie de
sauvegarde `config.orig`, qui représentera en quelque sorte l’état initial de
votre fichier de configuration. Maintenant, modifiez `config`, en ajoutant une
ligne par exemple. Vos fichiers ne sont désormais plus les mêmes.

```
$ cat > config << EOF
> Option 1
> Option 2
> Option 3
> EOF
$ cp -v config config.orig
'config' -> 'config.orig'
$ ls -l config*
-rw-rw-r--. 1 microlinux microlinux 27 Oct 16 05:04 config
-rw-rw-r--. 1 microlinux microlinux 27 Oct 16 05:04 config.orig
$ echo Option 4 >> config
$ ls -l config*
-rw-rw-r--. 1 microlinux microlinux 36 Oct 16 05:05 config
-rw-rw-r--. 1 microlinux microlinux 27 Oct 16 05:04 config.orig
```

Dans ce dernier exemple, j’ai introduit une petite nouveauté qui semblera
familière aux utilisateurs de MS-DOS, même si son fonctionnement diffère
quelque peu : le joker `*`. L’astérisque `*` signifie "n’importe quelle chaîne
de caractères". Là encore, prenons un exemple.

Depuis que nous avons entrepris notre initiation à la ligne de commande, les
fichiers et les répertoires s’entassent dans notre répertoire utilisateur. Le
prochain atelier pratique sera d’ailleurs consacré aux commandes de
suppression, ce qui nous permettra d’envisager un brin de ménage. Pour
l’instant, vous devez faire avec tout ce fatras. Admettons que vous ne vouliez
afficher des renseignements précis que sur vos seuls fichiers dont le nom
commence par `bonjour`. Vous pourriez très bien expliciter à la suite les
quatre fichiers en argument&nbsp;:

```
$ ls -l bonjour.txt bonjour2.txt bonjour3.txt bonjourtous.txt 
-rw-rw-r--. 1 microlinux microlinux 19 Oct 16 05:10 bonjour.txt
-rw-rw-r--. 1 microlinux microlinux 17 Oct 16 05:11 bonjour2.txt
-rw-rw-r--. 1 microlinux microlinux 22 Oct 16 05:11 bonjour3.txt
-rw-rw-r--. 1 microlinux microlinux 58 Oct 16 05:11 bonjourtous.txt
```

Toutefois, il y a moyen de faire plus court :

```
$ ls -l bonjour*
-rw-rw-r--. 1 microlinux microlinux 19 Oct 16 05:10 bonjour.txt
-rw-rw-r--. 1 microlinux microlinux 17 Oct 16 05:11 bonjour2.txt
-rw-rw-r--. 1 microlinux microlinux 22 Oct 16 05:11 bonjour3.txt
-rw-rw-r--. 1 microlinux microlinux 58 Oct 16 05:11 bonjourtous.txt
```

> Les habitués de MS-DOS verront tout de suite la différence. Sous Windows, le
> joker aurait dû être invoqué sous la forme `BONJOUR*.*`, voire
> `BONJOUR*.TXT`.

## Sauvegarder un répertoire

Pour en revenir à `cp`, je peux également copier l’intégralité d’un répertoire
vers un répertoire d’un autre nom&nbsp;:

```
$ cp -Rv Fichiers CopieFichiers
'Fichiers' -> 'CopieFichiers'
'Fichiers/Documents' -> 'CopieFichiers/Documents'
'Fichiers/Documents/texte.txt' -> 'CopieFichiers/Documents/texte.txt'
'Fichiers/Films' -> 'CopieFichiers/Films'
'Fichiers/Films/film.avi' -> 'CopieFichiers/Films/film.avi'
'Fichiers/Images' -> 'CopieFichiers/Images'
'Fichiers/Images/photo.jpg' -> 'CopieFichiers/Images/photo.jpg'
```

Admettons maintenant que je souhaite effectuer une copie complète du répertoire
Fichiers et de tout son contenu vers un autre endroit du système, tout en
donnant un autre nom au répertoire copié, par exemple `Sauvegarde20210507`.
Dans ce cas, voici ce qu’il faut faire&nbsp;:

```
$ cp -Rv Fichiers /tmp/Sauvegarde20210507
'Fichiers' -> '/tmp/Sauvegarde20210507'
'Fichiers/Documents' -> '/tmp/Sauvegarde20210507/Documents'
'Fichiers/Documents/texte.txt' -> '/tmp/Sauvegarde20210507/Documents/texte.txt'
'Fichiers/Films' -> '/tmp/Sauvegarde20210507/Films'
'Fichiers/Films/film.avi' -> '/tmp/Sauvegarde20210507/Films/film.avi'
'Fichiers/Images' -> '/tmp/Sauvegarde20210507/Images'
'Fichiers/Images/photo.jpg' -> '/tmp/Sauvegarde20210507/Images/photo.jpg'
```

> Le seul aspect peu réaliste de ce dernier exemple, c’est que `/tmp` n’est pas
> un endroit approprié pour ranger des sauvegardes. De meilleurs endroits
> seront à découvrir un peu plus loin.

Certains parmi vous auront probablement remarqué une certaine incohérence dans
l’utilisation de `/` à la fin des noms de répertoires. Concrètement, lorsque je
copie un répertoire `Fichiers`, je peux écrire `cp Fichiers/ CopieFichiers` ou
bien `cp Fichiers CopieFichiers`. La barre oblique s’ajoute d’emblée à la fin
d’un nom de répertoire lorsque j’utilise la complétion automatique, comme nous
le verrons un peu plus loin.

## Déplacer des fichiers et des répertoires avec mv

La commande `mv` (*move* comme "bouger") sert à déplacer des fichiers&nbsp;:

```
$ mv bonjour.txt /tmp/
```

Le fichier `~/bonjour.txt` a été déplacé vers le répertoire `/tmp`.

`mv` ne s’applique pas seulement sur des fichiers, mais également sur des
répertoires entiers. Pour essayer, créez une autre copie du répertoire
`Fichiers` et déplacez-la vers `/tmp`&nbsp;:

```
$ cp -R Fichiers/ AutreCopieFichiers
$ mv AutreCopieFichiers/ /tmp/
```

Question épineuse : comment déplacer à nouveau le fichier `/tmp/bonjour.txt`
vers mon répertoire d’utilisateur lorsque je me trouve dans ce dernier&nbsp;?
Voici la réponse&nbsp;:

```
$ mv /tmp/bonjour.txt .
```

Et je pourrais faire de même avec `/tmp/AutreCopieFichiers`&nbsp;:

```
$ mv /tmp/AutreCopieFichiers/ .
```

Rappelez-vous que le point `.` signifie "ici". La première des deux commandes
précédentes peut se lire littéralement comme ceci&nbsp;: "déplace (`mv`) le
fichier `bonjour.txt` qui se situe dans le répertoire `/tmp`
(`/tmp/bonjour.txt`) vers ici (`.`)".

## Renommer des fichiers et des répertoires avec mv

La commande `mv` ne sert pas seulement à déplacer, mais aussi à renommer des
fichiers et des répertoires. Cette double utilisation tourmente habituellement
les novices de la ligne de commande sous Linux. 

Rassurez-vous : ce n’est qu’une simple habitude à prendre. Un expert venant
d’un autre système d’exploitation mettra un peu moins de trois mois à s’y
habituer. Et un enfant de dix ans aura assimilé ce double fonctionnement en
moins de trois minutes.&nbsp;:o)

```
$ mv bonjour.txt hello.txt
```

Là, nous venons tout simplement de renommer le fichier `bonjour.txt` en
`hello.txt`. 

Pour ajouter à la confusion, demandons-nous s’il existe un moyen de déplacer ce
fichier `hello.txt` vers `/tmp`, tout en le renommant par la même opération en
`bonjour.txt`&nbsp;? Oui, et c’est même très simple&nbsp;:

```
$ mv hello.txt /tmp/bonjour.txt
```

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

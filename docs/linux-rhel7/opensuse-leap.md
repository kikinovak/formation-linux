# Installer un hyperviseur OpenSUSE Leap

**Objectif**&nbsp;: installation d'un poste de travail Linux qui servira
d'hyperviseur pour l'ensemble des ateliers pratiques.

Un hyperviseur est une machine qui permet d'exécuter des machines virtuelles (VM). 

> Si le mot "hyperviseur" vous impressionne et vous fait un peu peur, pensez
> "bac à sable". 

## Le choix de la distribution

La distribution OpenSUSE est proposée en deux moutures&nbsp;:

* la version stable Leap&nbsp;;

* la version de développement Tumbleweed.

OpenSUSE Leap constitue une synthèse assez réussie entre la stabilité et
l'innovation, si l'on peut dire. Le système de base est constitué des paquets
directement issus de SUSE Linux Enterprise. Sur cette base éprouvée, les
mainteneurs de la distribution proposent une panoplie d'environnements de
bureau modernes avec des applications graphiques récentes.

> J'avoue platement ma préférence marquée pour l'environnement de bureau KDE.
> Plasma&nbsp;5 me semble être le bureau le plus mature et le plus fonctionnel
> du monde du logiciel libre. C'est l'environnement graphique proposé par
> défaut par OpenSUSE. Son implémentation dans cette distribution a toujours
> été particulièrement soignée, avec une petite touche personnalisée sobre et
> sympathique.  

## Télécharger OpenSUSE Leap 15.3

Les fichiers ISO de la distribution sont disponibles sur la plateforme
[download.opensuse.org](https://download.opensuse.org), dans le répertoire
[distribution/leap/15.3/iso](https://download.opensuse.org/distribution/leap/15.3/iso/).

Téléchargez le fichier
[`openSUSE-Leap-15.3-DVD-x86_64-Current.iso`](https://download.opensuse.org/distribution/leap/15.3/iso/openSUSE-Leap-15.3-DVD-x86_64-Current.iso)

## Confectionner la clé USB d'installation

Le fichier ISO d'OpenSUSE Leap 15.3 pèse près de 4.3&nbsp;Go. Nous pouvons
l'écrire sur une clé USB de 8&nbsp;Go ou plus.

### Sous Windows

Sous Microsoft Windows, il existe une multitude de "solutions" pour écrire une
image ISO sur une clé USB, qui se distinguent toutes par le fait qu'elles ne
fonctionnent pas. Une seule application s'acquitte correctement de cette
tâche&nbsp;: le logiciel libre [Rufus](https://rufus.ie/fr/).

* Insérez une clé USB formatée.

* Lancez Rufus.

* Vérifiez si votre clé USB s'affiche bien dans le champ `Périphérique`.

* Sélectionnez le fichier ISO.

* Cliquez sur `Démarrer`.

### Sous macOS

* Insérez une clé USB formatée.

* Lancez l'`Utilitaire de Disque`.

* Sélectionnez la clé USB.

* Choisissez l'option `Restaurer`.

* Faites un simple glisser/déposer du fichier ISO vers le champ de texte au
milieu de la fenêtre.

### Sous Linux

* Insérez une clé USB (formatée ou non, peu importe).

* Identifiez le fichier de périphérique `/dev/sdX` à l'aide de la commande
`lsblk`.

* Utilisez la commande `dd` en tant que `root` pour écrire le fichier ISO sur
la clé.

```
# dd if=openSUSE-Leap-15.3-DVD-x86_64-Current.iso of=/dev/sdX
```

> Comptez entre 10 et 15 minutes pour l'écriture du fichier ISO sur la clé.

## Installer OpenSUSE Leap 15.3

### Démarrer l'installation

L'écran d'accueil de l'installateur ne s'affiche pas de la même manière si la
machine utilise un BIOS traditionnel ou l'UEFI. La version traditionnelle vous
permet de choisir la langue au moment du démarrage. Si vous utilisez l'UEFI,
vous ferez ce choix un peu plus tard.

* Insérer la clé USB dans le PC.

* Allumer le PC en affichant le menu de démarrage.

* Sélectionner la clé USB en mode `UEFI` ou `Legacy`, peu importe.

* Sur une machine équipée d'un BIOS traditionnel, appuyer sur ++f2++ pour
  afficher les paramètres régionaux et sélectionner `Français` dans le menu
  déroulant.

* Sélectionner `Installation`.

> Sur un système UEFI, vous pouvez très bien laisser l'option `Secure Boot`
> activée, étant donné que la distribution OpenSUSE le gère parfaitement.

### Langue et clavier

* Choisir la langue si ce n'est déjà fait&nbsp;: `French - Français`.

* Éventuellement, choisir la disposition clavier par défaut.

* Cliquer sur `Suivant`.

### Dépôts en ligne

* Activer les dépôts en ligne maintenant&nbsp;? `Oui`.

* Confirmer les dépôts par défaut en cliquant sur `Suivant`.

* Attendre la synchronisation initiale des dépôts en ligne.

### Rôle système

* Sélectionner `Bureau avec KDE Plasma`.

* Cliquer sur `Suivant`.

### Partitionnement

OpenSUSE Leap utilise un partitionnement basé sur `Btrfs` par défaut. On optera
pour un schéma plus simple basé sur le système de fichiers `ext4`.

* Cliquer sur `Installation guidée`.

* Systèmes Windows existants&nbsp;: `Supprimer même si ce n'est pas
  nécessaire`.

* Partitions Linux existantes&nbsp;: `Supprimer même si ce n'est pas
  nécessaire`.

* Autres partitions&nbsp;: `Supprimer même si ce n'est pas nécessaire`.

* Cliquer sur `Suivant`.

* Ne pas cocher `Activer la gestion des volumes logiques (LVM)`.

* Ne pas cocher `Enable Disk Encryption`.

* Cliquer sur `Suivant`.

* Type de système de fichiers&nbsp;: `ext4`.

* Ne pas cocher `Proposer une partition personnelle séparée`.

* Cocher `Proposer une partition swap séparée`.

* Cocher `Agrandir jusqu'à la taille de la RAM pour la mise en veille`.

* Cliquer sur `Suivant` pour afficher le récapitulatif du partitionnement.

* Confirmer en cliquant sur `Suivant`.

### Fuseau horaire

* Vérifier la région&nbsp;: `Europe`.

* Vérifier le fuseau horaire&nbsp;: `France`.

* Adapter cette configuration selon les besoins.

* Cocher `Horloge matérielle réglée sur UTC`.

* Cliquer sur `Suivant`.

### Utilisateurs locaux

Créer un utilisateur initial&nbsp;:

* Nom complet&nbsp;: par exemple `Stagiaire France TV`.

* Nom d'utilisateur&nbsp;: par exemple `stagiaire`.

* Choisir un mot de passe raisonnablement compliqué.

* Confirmer le mot de passe.

* Cocher `Utiliser ce mot de passe pour l'administrateur du système`.

* Décocher `Login automatique`.

* Cliquer sur `Suivant`.

### Paramètres d'installation

* Mitigations CPU&nbsp;: cliquer sur `Auto`.

* Dans l'écran subséquent, passer les mitigations CPU de `Auto` à `Désactivé`
  dans le menu déroulant.

* Confirmer par `OK` pour revenir à la vue d'ensemble des paramètres
  d'installation.

* Pare-feu&nbsp;: cliquer sur `désactiver`.

* Service SSH&nbsp;: cliquer sur `activer`.

* Cliquer sur `Installer` pour lancer l'installation.

* Confirmer en cliquant encore une fois sur `Installer`.

### Redémarrage initial

* Au terme de l'installation, OpenSUSE redémarre automatiquement. 

* Dès que le gestionnaire de connexion SDDM s'affiche, vous pouvez sereinement
  enlever la clé USB d'installation.

* Saisissez votre mot de passe et ouvrez une session. 

* Si tout se passe bien, le bureau KDE s'affiche dans sa configuration par
  défaut.

## Configuration post-installation

OpenSUSE est la seule distribution Linux qui peut être entièrement configurée
en mode graphique grâce à l'outil YaST (*Yet another Setup Tool*)&nbsp;:

* `Menu Démarrer` > `Applications` > `Système` > `YaST`.

### Dépôts de logiciels

Dans YaST, ouvrir le menu `Logiciel` > `Dépôts de logiciels` et supprimer tous
les dépôts en ligne qui ne sont **pas** activés par défaut&nbsp;:

* dépôts *Source*

* dépôts *Debug*

On doit se retrouver au total avec les six dépôts en ligne suivants&nbsp;:

* Dépôt principal OSS

* Dépôt principal de mise à jour OSS

* Dépôt Non-OSS

* Dépôt de mise à jour Non-OSS

* Dépôt OpenSUSE Backports

* Dépôt de mise à jour SUSE Linux Enterprise 15

> Les deux dépôts OSS fournissent des logiciels libres (*Open Source
> Software*). Tous les paquets dont la licence contient une quelconque
> restriction propriétaire sont rangés dans les dépôts Non-OSS. Quant au dépôt
> *Backports*, il fournit des paquets plus récents qui ont été rétroportés dans
> la distribution.

### Terminal graphique

Pour la plupart des ateliers pratiques, nous utiliserons le terminal graphique
Konsole&nbsp;:

* `Menu Démarrer` > `Applications` > `Système` > `Konsole`.

On peut éventuellement modifier l'aspect du terminal pour le rendre plus
lisible&nbsp;:

* `Configuration` > `Modifier le profil actuel` > `Apparence`.

* Choisir un schéma de couleurs adapté. 

* Agrandir la police à chasse fixe définie par défaut.

## Pour aller plus loin

Le poste de travail OpenSUSE Leap présenté ici nous servira principalement
comme hyperviseur pour les ateliers pratiques en ligne de commande. 

Si vous souhaitez en faire un poste de travail configuré "aux petits oignons",
vous pouvez utiliser mon [script de configuration
automatique](https://gitlab.com/kikinovak/opensuse-lp153). 

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


**Objectif** : Prise en main de la console. Découverte des commandes de base
pour naviguer dans un système Linux. 

## Afficher le contenu d'un répertoire avec ls

La commande `ls` (comme *list*) affiche la liste des fichiers dans un
répertoire.  Invoquée sans arguments, elle montre le contenu du répertoire
courant. 

> Le répertoire courant, c'est celui dans lequel vous vous trouvez au moment où
vous saisissez la commande. 

Pour l'instant, le répertoire utilisateur de `microlinux` est encore
vide&nbsp;:

```
[microlinux@linuxbox ~]$ ls
```

Pour afficher le contenu de la racine du système de fichiers, saisissez
ceci&nbsp;:

```
[microlinux@linuxbox ~]$ ls /
bin   dev  home  lib64  mnt  proc  run   srv  tmp  var
boot  etc  lib   media  opt  root  sbin  sys  usr
```

Et pour voir ce qu'il y a dans `/usr`, il suffit d'invoquer la commande
suivante&nbsp;:

```
[microlinux@linuxbox ~]$ ls /usr
bin  games    lib    libexec  sbin   src
etc  include  lib64  local    share  tmp
```

> Les habitués de Windows et de DOS l'auront deviné&nbsp;: `ls` sous Linux
> équivaut à la commande `DIR`.

### Qu'est-ce qui est quoi là-dedans ?

Les différentes couleurs de l'affichage nous suggèrent qu'il ne s'agit
peut-être pas d'éléments du même type. Essayez&nbsp;:

```
[microlinux@linuxbox ~]$ ls /etc
```

Le résultat de cette commande dépassera éventuellement la taille d'un écran.
Pour revenir en arrière, maintenez la touche ++shift++ enfoncée et utilisez les
touches ++up++ et ++down++ pour faire défiler l'affichage en arrière et en
avant.

Certains éléments apparaissent en bleu, d'autres en turquoise, d'autres en noir
et blanc et il y a même un peu de rouge. Pour en avoir le coeur net, il va
falloir utiliser `ls` avec l'option `-F`, qui donne des détails&nbsp;:

```
[microlinux@linuxbox ~]$ ls -F /etc
adjtime                  hosts                     rc0.d@
aliases                  hosts.allow               rc1.d@
aliases.db               hosts.deny                rc2.d@
alternatives/            init.d@                   rc3.d@
anacrontab               inittab                   rc4.d@
asound.conf              inputrc                   rc5.d@
audisp/                  iproute2/                 rc6.d@
...
```

Réinvoquez `ls -F` pour afficher le contenu de `/etc/ppp`&nbsp;:

```
[microlinux@linuxbox ~]$ ls -F /etc/ppp
ip-down*          ip-up*          ipv6-down*  peers/
ip-down.ipv6to4*  ip-up.ipv6to4*  ipv6-up*
```

Vous constatez que certains éléments sont suivis d'une barre oblique `/`,
d'autres d'une arobase `@` ou d'un astérisque `*`&nbsp;; le reste des éléments
ne contient aucun suffixe.

* La barre oblique `/` (couleur par défaut&nbsp;: bleu) désigne un répertoire.

* L'absence de suffixe (couleur par défaut&nbsp;: noir, blanc ou gris, selon
  votre terminal) indique qu'il s'agit d'un fichier régulier non exécutable.

* L'arobase `@` (couleur par défaut&nbsp;: turquoise) montre qu'il s'agit d'un
  lien symbolique, ce qui constitue l'équivalent d'un raccourci sous Windows.
  Nous verrons les liens symboliques un peu plus loin.

* L'astérisque `*` (couleur par défaut&nbsp;: vert) indique qu'il s'agit d'un
  fichier exécutable.

Il nous en manque encore quelques-uns, mais nous nous contenterons des éléments que nous avons pour l'instant.

### Afficher les informations détaillées

Ces informations paraissent un peu maigres. Nous pouvons en afficher davantage
en utilisant l'option `-l` (comme *long*)&nbsp;:

```
[microlinux@linuxbox ~]$ ls -l /etc/sysconfig
total 104
-rw-r--r--. 1 root root  145 Sep 22 00:52 anaconda
-rw-r--r--. 1 root root  483 Sep 22 00:51 authconfig
drwxr-xr-x. 2 root root   43 Sep 22 00:48 cbq
-rw-r--r--. 1 root root   46 Aug  8  2019 chronyd
-rw-r--r--. 1 root root   27 Sep 22 00:56 clock
drwxr-xr-x. 2 root root    6 Apr  1  2020 console
-rw-r--r--. 1 root root  150 Aug 25 17:33 cpupower
-rw-------. 1 root root  110 Aug  8  2019 crond
-rw-------. 1 root root 1390 Apr 11  2018 ebtables-config
-rw-r--r--. 1 root root   73 Apr  7  2020 firewalld
...
```

L'utilisateur non averti trouvera cet affichage quelque peu énigmatique. En
fait, il est facile à lire une fois que l'on sait à quoi correspond chaque
terme.

Tout à fait à gauche, vous avez une série de dix caractères. Le tout premier
vous indique s'il s'agit d'un fichier (tiret `-`) ou d'un répertoire (`d` comme
*directory*). Ensuite, la série de neuf caractères (en fait, trois fois trois)
indique les droits d'accès au fichier ou au répertoire. Nous traiterons la
question des droits d'accès un peu plus loin. Les caractères `r`, `w`, `x`
et `-` décrivent ce que l'on a le droit de faire avec le fichier ou le
répertoire&nbsp;:

* lire : `r` comme *read*&nbsp;;

* écrire (modifier) : `w` comme *write*&nbsp;;

* exécuter : `x` pour *e[x]ecute*&nbsp;;

* rien du tout : `-`.

Je disais : ce que l'*on* a le droit de faire. Ce *on* est en fait assez bien
spécifié&nbsp;: les trois premiers caractères de la série concernent le
propriétaire du fichier, les trois suivants le groupe et les trois derniers le
reste du monde. Nous y reviendrons en détail.

> Vous vous êtes peut-être demandé ce qui signifie le point juste après les
droits d'accès, c'est-à-dire la série de caractères qui ressemblent à quelque
chose comme `-rw-r--r--`. Ne vous en préoccupez pas pour l'instant. Sachez
quand-même que le point `.` indique que l'élément en question possède un
contexte SELinux. *Security Enhanced Linux* est un mécanisme de sécurité
développé par la NSA et utilisé principalement sur les serveurs Red Hat
Enterprise Linux et Oracle Linux. C'est une technologie assez complexe, que
vous aurez l'occasion de manipuler lorsque vous serez un peu plus aguerris. 

Le chiffre qui suit (ici&nbsp;: la série de `1` et de `2` dans la deuxième
colonne) n'est pas d'une grande importance pour nous. Précisons tout de même
qu'il indique le nombre de liens pointant vers le fichier ou le répertoire.

Les deux indications immédiatement après montrent le propriétaire du fichier,
ainsi que le groupe auquel il appartient. Dans l'exemple, les fichiers et les
répertoires appartiennent à l'utilisateur `root` et au groupe `root`.

La prochaine indication correspond à la taille du fichier. Ici, l'astuce est
d'invoquer `ls` avec l'option supplémentaire `-h` ou `--human-readable`.
Essayez&nbsp;:

```
[microlinux@linuxbox ~]$ ls -lh /etc/sysconfig 
total 104K
-rw-r--r--. 1 root root  145 Sep 22 00:52 anaconda
-rw-r--r--. 1 root root  483 Sep 22 00:51 authconfig
drwxr-xr-x. 2 root root   43 Sep 22 00:48 cbq
-rw-r--r--. 1 root root   46 Aug  8  2019 chronyd
-rw-r--r--. 1 root root   27 Sep 22 00:56 clock
drwxr-xr-x. 2 root root    6 Apr  1  2020 console
-rw-r--r--. 1 root root  150 Aug 25 17:33 cpupower
-rw-------. 1 root root  110 Aug  8  2019 crond
-rw-------. 1 root root 1.4K Apr 11  2018 ebtables-config
-rw-r--r--. 1 root root   73 Apr  7  2020 firewalld
lrwxrwxrwx. 1 root root   15 Sep 22 00:54 grub -> ../default/grub
-rw-r--r--. 1 root root  798 Apr  1  2020 init
-rw-------. 1 root root 2.1K Apr  2  2020 ip6tables-config
-rw-------. 1 root root 2.1K Apr  2  2020 iptables-config
...
```

La taille des fichiers est tout de suite beaucoup plus lisible, car le système
l'indique en kilooctets (`K`), mégaoctets (`M`) ou gigaoctets (`G`).

En passant, nous faisons également deux autres constats. D'une part, les
options des commandes peuvent se combiner. Nous aurions donc très bien pu
invoquer la commande comme ceci&nbsp;:

```
[microlinux@linuxbox ~]$ ls -l -h /etc/sysconfig
```

D'autre part, de nombreuses options de commande ont une version courte et une
longue. L'option `-h` (qui signifie "lisible par un humain", comme si les
informaticiens ne faisaient pas vraiment partie de l'espèce) s'écrit donc très
bien comme dans l'exemple qui suit. Je vous conseille cette option uniquement
si vous avez un penchant prononcé pour la dactylographie&nbsp;:

```
[microlinux@linuxbox ~]$ ls -l --human-readable /etc/sysconfig
```

Quant aux deux dernières colonnes, elles nous indiquent respectivement la date
de la création ou de la dernière modification et, pour finir, le nom du fichier
ou du répertoire.

### Les fichiers cachés

Une autre option fréquemment utilisée est `-a` (ou `--all`&nbsp;: tout).
Appliquez-la sur votre répertoire utilisateur et vous serez probablement
surpris&nbsp;:

```
[microlinux@linuxbox ~]$ ls -a
.  ..  .bash_logout  .bash_profile  .bashrc  .ssh
```

Cette option sert à afficher les fichiers et répertoires cachés. Dans un
système Linux, les fichiers et répertoires dont le nom commence par un point
`.` ne s'affichent pas lorsque `ls` est invoquée normalement. Vous ne les
verrez donc qu'en utilisant l'option `-a`.

À quoi servent ces fichiers et quel est l'intérêt de les dissimuler&nbsp;? Les
fichiers cachés ou *dotfiles* (de l'anglais *dot*&nbsp;: point) contiennent la
configuration personnalisée de vos applications. Concrètement, les fichiers
`~/.bash_profile`, `~/.bashrc` et `~/.bash_logout` contiennent la configuration de
votre shell Bash (qui est une application). Le fichier `~/.bash_history` renferme
l'historique des commandes précédemment invoquées. 

À la différence des fichiers situés dans `/etc`, qui définissent une
configuration globale, c'est-à-dire valable pour tous les utilisateurs, les
fichiers et répertoires cachés que nous rencontrons ici ne sont valables que
pour vous seul. Nous aborderons le rôle du répertoire `/etc` un peu plus loin.

Vous vous demandez certainement ce que signifie le tilde `~` que j'ai utilisé à
plusieurs reprises. Sur les systèmes Linux (tout comme dans Unix), ce symbole
désigne votre répertoire utilisateur. Le fichier `~/.bashrc` de l'utilisateur
`microlinux` sera donc `/home/microlinux/.bashrc` dans sa notation explicite,
tandis que le fichier `~/.bashrc` de `nkovacs` correspondra à
`/home/nkovacs/.bashrc`. Étant donné que chaque utilisateur est libre de
configurer le shell Bash à sa guise (ce que nous aurons l'occasion de voir),
tout le monde aura donc son propre fichier `.bashrc`.

Enfin, vous aurez probablement déjà noté le tilde `~` dans l'invite de
commande&nbsp;:

```
[microlinux@linuxbox ~]$
```

Jetons un oeil sur l'invite de commande telle qu'elle se présente en mode
console, dans sa configuration par défaut. Elle est très simple à
décrypter&nbsp;:

* La première indication, c'est le nom de l'utilisateur (ici&nbsp;:
`microlinux`).

* Il est séparé par une arobase `@` du nom de la machine (ici&nbsp;:
  `linuxbox`).

* La troisième indication, c'est le répertoire courant (ici&nbsp;: `~` à savoir
  `/home/microlinux`, puisque c'est l'utilisateur `microlinux`).

* Et enfin, le signe dollar `$` signifie par convention qu'il s'agit d'un
  utilisateur du "commun des mortels". S'il s'agissait de l'utilisateur `root`,
  nous verrions ici un dièse `#` à la place du `$`.

> L'aspect même de l'invite peut être paramétré à souhait, ce que nous verrons en
temps et en heure.

### Afficher les informations détaillées d'un répertoire

Il nous reste à voir une dernière option importante pour `ls`. Admettons que
vous souhaitiez afficher les informations détaillées pour le répertoire
`/etc`&nbsp;: les droits d'accès, le propriétaire, le groupe, etc. Vous
invoquez donc hardiment `ls` suivie de l'option `-l` et de l'argument `/etc` et
vous voyez... les informations détaillées de tout le contenu du répertoire,
mais pas du répertoire lui-même.

```
[microlinux@linuxbox ~]$ ls -l /etc 
total 1148
-rw-r--r--.  1 root root     5090 Aug  6  2019 DIR_COLORS
-rw-r--r--.  1 root root     5725 Aug  6  2019 DIR_COLORS.256color
...
```

Comment faire ? Tout simplement en invoquant l'option supplémentaire `-d`
(comme *directory*, c'est-à-dire "répertoire"). Cette option affiche les
répertoires avec la même présentation que les fichiers, sans lister leur
contenu&nbsp;:

```
[microlinux@linuxbox ~]$ ls -ld /etc
drwxr-xr-x. 74 root root 8192 Oct 11 05:19 /etc
```

## pwd : "Vous êtes ici !"

La commande `pwd` (*print working directory*) s'acquitte d'une seule tâche.
Elle vous affiche (*print*) quel est le répertoire courant (*working
directory*), c'est-à-dire le répertoire dans lequel vous vous situez
actuellement&nbsp;:

```
[microlinux@linuxbox ~]$ pwd
/home/microlinux
```

Lorsque vous vous promenez dans une grande ville, il vous arrive de vous
perdre. Avec un peu de chance, vous tombez sur un de ces grands plans de la
ville, avec une flèche et un petit rond bien visible, qui vous indique&nbsp;:
"VOUS ÊTES ICI". C'est exactement ce que fait `pwd`. Et maintenant que nous
savons nous repérer, apprenons à nous déplacer.

## On bouge avec cd !

La commande `cd` (*change directory*) est utilisée pour changer de répertoire
courant. Il suffit de taper `cd` puis le chemin du répertoire dans lequel on
veut se placer. Dans cet exemple, l'invocation de la commande `pwd` après `cd`
permet de vérifier que nous sommes bien dans le répertoire demandé.

```
[microlinux@linuxbox ~]$ cd /
[microlinux@linuxbox /]$ pwd
/
[microlinux@linuxbox /]$ cd bin
[microlinux@linuxbox bin]$ pwd
/bin
[microlinux@linuxbox bin]$ cd /etc
[microlinux@linuxbox etc]$ pwd
/etc
[microlinux@linuxbox etc]$ cd /usr/bin
[microlinux@linuxbox bin]$ pwd
/usr/bin
```

### Chemin relatif ou absolu ?

Lorsque je me trouve dans le répertoire racine `/` et que je souhaite me
déplacer vers le répertoire `/bin`, je peux écrire `cd bin`. Cela correspond au
chemin relatif, c'est-à-dire celui indiqué à partir du répertoire dans lequel
je me situe, en l'occurrence `/`. Quant à `cd /bin`, c'est le chemin absolu,
autrement dit l'emplacement à partir du répertoire racine.

En revanche, lorsque je me trouve dans le répertoire `/etc` et que je veux me
placer dans le répertoire `/bin`, je suis obligé – pour l'instant – d'utiliser un
chemin absolu. Pour saisir la distinction, je vous donne un exemple qui
illustre ce qu'il ne faut **pas** faire&nbsp;:

```
[microlinux@linuxbox bin]$ cd /etc
[microlinux@linuxbox etc]$ pwd
/etc
[microlinux@linuxbox etc]$ cd bin
-bash: cd: bin: No such file or directory
```

Ces deux exemples de la vie courante vous permettront peut-être de saisir la
nuance&nbsp;:

* "Remontez la rue devant vous, tournez à gauche, continuez deux cents mètres,
  puis tournez à droite et encore à droite" (chemin relatif)&nbsp;;

* "Partez du Vieux Port, remontez la Canebière, puis prenez le boulevard
  Longchamp et arrêtez-vous au Palais Longchamp" (chemin absolu).

Dans l'exemple précédent, nous nous situons dans le répertoire `/etc`. Si nous
écrivons `cd bin` sans la barre oblique `/` qui précède, l'interpréteur de
commandes cherche un répertoire inexistant `/etc/bin` et affiche une erreur.

### À court d'arguments

Pour revenir dans votre répertoire d'utilisateur, il suffit d'invoquer `cd`
sans arguments&nbsp;:

```
[microlinux@linuxbox etc]$ cd /etc
[microlinux@linuxbox etc]$ pwd
/etc
[microlinux@linuxbox etc]$ cd sysconfig
[microlinux@linuxbox sysconfig]$ pwd
/etc/sysconfig
[microlinux@linuxbox sysconfig]$ cd
[microlinux@linuxbox ~]$ pwd
/home/microlinux
```

### "Ici" et "à l'étage"

Voyons maintenant deux répertoires un peu particuliers. Affichez la totalité du
contenu de votre répertoire d'utilisateur&nbsp;:

```
[microlinux@linuxbox ~]$ ls -aF
./  ../  .bash_logout  .bash_profile  .bashrc  
```

Vous remarquez qu'en début de liste, vous avez un répertoire nommé "`.`" et un
autre nommé "`..`". Affichez le contenu d'un autre répertoire, avec les mêmes
options `-aF`&nbsp;:

```
[microlinux@linuxbox ~]$ ls -aF /usr
./   bin/  games/    lib/    libexec/  sbin/   src/
../  etc/  include/  lib64/  local/    share/  tmp@
```

Si vous répétez l'opération sur d'autres répertoires au hasard, vous
constaterez que chaque liste débute invariablement par ces mêmes
répertoires `.` et `..`&nbsp;:

* `.` est le répertoire courant.

* `..` est le répertoire parent.

Là encore, la mise en pratique vous aidera à saisir le concept. Essayez
ceci&nbsp;:

```
[microlinux@linuxbox ~]$ cd /etc/sysconfig/network-scripts
[microlinux@linuxbox network-scripts]$ pwd
/etc/sysconfig/network-scripts
[microlinux@linuxbox network-scripts]$ cd ..
[microlinux@linuxbox sysconfig]$ pwd
/etc/sysconfig
[microlinux@linuxbox sysconfig]$ cd ..
[microlinux@linuxbox etc]$ pwd
/etc
[microlinux@linuxbox etc]$ cd ..
[microlinux@linuxbox /]$ pwd
/
```

Chaque appel à `cd ..` nous fait ainsi remonter d'un cran dans l'arborescence,
jusqu'à ce que nous nous retrouvions à la racine.

Quant au point `.`, il faut se le représenter comme le fameux "VOUS ÊTES ICI"
sur le plan de la ville. Admettons que je me situe dans le répertoire `/etc` et
que je veuille me rendre dans le sous-répertoire `sysconfig`. Je pourrais
utiliser indépendamment ces deux notations, qui reviendraient au même&nbsp;:

```
[microlinux@linuxbox /]$ cd /etc
[microlinux@linuxbox etc]$ cd sysconfig
[microlinux@linuxbox sysconfig]$ pwd
/etc/sysconfig
```

Ou alors :

```
[microlinux@linuxbox ~]$ cd /etc
[microlinux@linuxbox etc]$ cd ./sysconfig
[microlinux@linuxbox sysconfig]$ pwd
/etc/sysconfig
```

L'utilité de cette notation vous apparaîtra un peu plus loin. Pour l'instant,
retenez simplement que `.` signifie "ici".

Notez aussi que `..` peut très bien faire partie d'un chemin. Admettons que
vous soyez dans le répertoire `/etc/sysconfig` et que vous souhaitiez vous
rendre dans `/etc/ssh`. Vous pourriez vous y prendre comme ceci&nbsp;:

```
[microlinux@linuxbox ~]$ cd /etc/sysconfig
[microlinux@linuxbox sysconfig]$ pwd
/etc/sysconfig
[microlinux@linuxbox sysconfig]$ cd ..
[microlinux@linuxbox etc]$ cd ssh
[microlinux@linuxbox ssh]$ pwd
/etc/ssh
```

Il y a moyen de faire plus court et plus élégant&nbsp;:

```
[microlinux@linuxbox ~]$ cd /etc/sysconfig
[microlinux@linuxbox sysconfig]$ pwd
/etc/sysconfig
[microlinux@linuxbox sysconfig]$ cd ../ssh
[microlinux@linuxbox ssh]$ pwd
/etc/ssh
```

> Si j'invoque `pwd` à chaque changement de répertoire, c'est uniquement à des
fins de démonstration, pour bien expliciter le répertoire courant.

Vous pouvez également monter de plusieurs crans, si cela est nécessaire. Si
votre répertoire courant est `/etc/sysconfig/network-scripts` et si vous
souhaitez vous rendre dans `/etc/ssh`, il va falloir que vous montiez de deux
crans, pour ensuite entrer dans le répertoire `ssh`. En pratique, cela
ressemblerait à l'exemple suivant&nbsp;:

```
[kikinovak@centosbox ~]$ cd /etc/sysconfig/network-scripts
[kikinovak@centosbox network-scripts]$ pwd
/etc/sysconfig/network-scripts
[kikinovak@centosbox network-scripts]$ cd ../../ssh
[kikinovak@centosbox ssh]$ pwd
/etc/ssh
```

## Pour aller plus loin 

### La philosophie Unix

Vous venez d'apprendre en tout et pour tout trois commandes et une poignée
d'options. Peut-être sentez-vous monter en vous un vague sentiment de
déception. C'est donc ça, Linux&nbsp;? Des commandes qu'il faut taper
fastidieusement dans une interface archaïque&nbsp;? 

Pour vous rassurer – et nous conforter dans notre démarche – je me permettrai
de vous donner un exemple qui semblera familier à beaucoup d'entre vous.
Imaginez que votre voiture tombe en panne un jour. Vous avez en gros deux
possibilités pour la faire réparer.

* Vous la faites remorquer au garage Lapeau&nbsp;&amp; Desfesses en
  ville&nbsp;: un endroit très *high tech* avec beaucoup de chrome et de
  carrelage blanc, sans la moindre trace de cambouis ni de poussière. Les
  mécaniciens ressemblent à des ingénieurs en blouse blanche. Ils sont armés
  jusqu'aux dents d'ordinateurs portables et ne répondent à personne. Votre
  voiture est le seul objet sale dans cet endroit étincelant de propreté.
  L'ingénieur en chef dissimule à peine son dégoût, ouvre le capot et branche
  un câble dans une prise dont vous ignoriez l'existence jusque-là. Il retourne
  devant l'écran de son portable, clique sur une série de boutons dans son
  logiciel de diagnostic et vous annonce qu'il ne peut pas vous fixer un
  rendez-vous avant le début du mois prochain, mais qu'on peut déjà établir un
  devis.

* Vous décidez d'aller voir Tony, le mécanicien du village. En guise de
  bonjour, Tony vous présente son avant-bras à peine moins maculé de cambouis
  que ses mains. Il propose de s'occuper tout de suite de votre voiture,
  l'objet le plus propre dans tout le garage. Il ouvre le capot et contemple le
  moteur en sifflotant le refrain qui vient de passer à la radio. Puis il
  fouille dans sa boîte à outils et en extrait une clé tubulaire, un tournevis
  et une pince. À peine deux minutes plus tard, il vous annonce qu'il fallait
  juste nettoyer les bougies et refixer une durite qui s'était défaite. Il
  refuse de se faire payer malgré vos protestations réitérées.

Les commandes que nous venons d'apprendre sont certes aussi peu spectaculaires
qu'une clé tubulaire, un tournevis ou une clé de douze. Vous serez d'ailleurs
probablement surpris d'apprendre que ce sont des commandes Unix, un système
d'exploitation fondé en 1969 et dont Linux est un clone. Les principes de base
d'Unix sont restés les mêmes depuis les années 1970. Douglas McIlroy, l'un des
fondateurs d'Unix, a résumé la philosophie de ce système en une série de trois
impératifs catégoriques :

1. Écrivez des programmes qui font une seule chose et qui la font bien.

2. Écrivez des programmes qui se combinent les uns avec les autres.

3. Écrivez des programmes pour gérer des flux de texte, car c'est une interface
   universelle.

Même si vous n'avez pas l'intention d'écrire des programmes Unix (ou Linux),
ces trois règles sont d'une importance capitale pour tout utilisateur de
systèmes de cette famille. À partir du moment où vous maîtrisez ne serait-ce
qu'une poignée de commandes Unix, vous apprendrez à les combiner pour résoudre
les problèmes de manière efficace. Gardez ce principe à l'esprit lors de votre
apprentissage. Nous verrons bientôt comment les tâches les plus complexes
peuvent être décomposées en une série d'opérations simples.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


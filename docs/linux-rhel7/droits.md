
**Objectif** : gérer les accès aux fichiers et aux répertoires sous Linux.

Une fois que les utilisateurs du système sont mis en place, la question des
droits d'accès se pose. Définir des permissions saines pour les utilisateurs et
leur assurer un minimum de confidentialité, c'est le B.A.-ba de la sécurité.

## Qui a le droit de faire quoi ?

Dans la leçon sur la [gestion des utilisateurs](utilisateurs.md), nous avons
comparé un système multi-utilisateurs au fonctionnement d'une grande
entreprise.  Chaque employé possède son badge qui lui donne accès aux locaux de
l'entreprise. Il a son bureau où il range ses affaires, éventuellement aussi
son casier personnel. En principe, les employés partagent les ressources de
l'entreprise, ce qui ne veut pas forcément dire que tous les employés ont accès
aux mêmes ressources.  Beaucoup disposent de leur propre bureau individuel,
d'autres travaillent en équipe dans de grandes pièces spacieuses, où l'accès
aux ordinateurs, aux photocopieuses et aux documents est ouvert à tous les
membres de l'équipe. Quelques rares privilégiés ont un accès plus large&nbsp;:
l'agent de sécurité, le PDG ou le DRH. La métaphore peut ainsi être tressée et
affinée, mais vous avez compris le principe sous-jacent.

Considérons l'exemple suivant&nbsp;:

* Le système compte trois utilisateurs : `adebuf`, `jmortreux` et `microlinux`.
  Supprimez tous les autres utilisateurs que vous avez pu créer jusqu'ici.

* Chaque utilisateur dispose de son propre répertoire (`~`)&nbsp;:

```
# ls /home/
adebuf  jmortreux  microlinux
```

* Chacun va ranger ses données personnelles dans son répertoire utilisateur.
  Dans l'exemple, chacun disposera d'un document "confidentiel" confectionné
  comme suit par exemple&nbsp;:

```
$ mkdir ~/Documents
$ cat > ~/Documents/Confidentiel.txt << EOF
> Ce document est confidentiel.
> Personne d'autre que moi ne doit pouvoir le lire.
> EOF
```

Au total, les répertoires utilisateur ressembleront donc à peu de chose près à
ceci&nbsp;:

```
# tree /home/
/home/
├── adebuf
│   └── Documents
│       └── Confidentiel.txt
├── jmortreux
│   └── Documents
│       └── Confidentiel.txt
└── microlinux
    └── Documents
        └── Confidentiel.txt

6 directories, 3 files
```

Qui a accès à quoi là-dedans&nbsp;? Est-ce que `jmortreux` pourra lire le
fichier `Confidentiel.txt` de `adebuf`&nbsp;? Est-ce que celle-ci pourra
modifier le fichier `Confidentiel.txt` de `microlinux`&nbsp;? En effet, il ne
suffit pas que chaque utilisateur dispose de son propre répertoire au-dessous
de `/home`. Aussi faut-il que ses données soient à l'abri des autres
utilisateurs de la machine. 

### Un exemple pratique

Les nouveaux utilisateurs de Linux sont souvent intimidés par les questions de
permissions et de droits d'accès, qu'ils perçoivent comme une nébuleuse
complexe et impressionnante. Ici comme ailleurs, je vous propose de rester
fidèle à la devise du grand neurologue français Charcot&nbsp;: *La théorie,
c'est bon, mais ça n'empêche pas d'exister.*

Pour commencer, créez un fichier `droits.txt` dans votre répertoire
utilisateur&nbsp;:

```
$ cat > droits.txt << EOF
> echo "Voici la date : "
> date
> EOF
```

> Rien ne vous empêche de créer et d'éditer ce fichier avec Vi. 

```
$ cat droits.txt
echo "Voici la date : "
date
```

## Comprendre les permissions dans l'affichage détaillé

Vous voilà donc avec votre fichier `droits.txt`. Qui en est le
propriétaire&nbsp;? Qui peut faire quoi avec&nbsp;? D'ailleurs, qu'est-ce qu'on
peut bien faire avec un fichier&nbsp;? En lire le contenu&nbsp;? Le
modifier&nbsp;? L'effacer&nbsp;? Et puis quoi encore&nbsp;?

$ ls -l droits.txt
-rw-rw-r--. 1 microlinux microlinux 29 May 21 09:49 droits.txt

Dans la partie droite de cet affichage détaillé, vous avez&nbsp;:

* le nom du fichier&nbsp;: `droits.txt`&nbsp;;

* sa date de création&nbsp;: le 21 mai à 9h49&nbsp;;

* sa taille&nbsp;: 29 octets.

Si vous ne tenez pas compte du `1` dans la deuxième colonne (oubliez-le pour
l'instant), la partie gauche est réservée aux droits d'accès du fichier.

Nous avons vu, dans la leçon sur la [navigation dans la console](naviguer.md),
comment décrypter sommairement la suite de dix caractères dans la première
colonne. Le moment est venu de nous y intéresser d'un peu plus près.

* Le tout premier caractère, c'est-à-dire le tiret `-` initial de la suite
  `-rw-rw-r--`, nous indique tout simplement qu'il s'agit d'un fichier.

* Les neuf caractères subséquents `rw-rw-r--` se décomposent en une série de
  trois fois trois caractères, respectivement `rw-`, `rw-` et `r--`.

* Les caractères `r`, `w`, `x` et `-` symbolisent ce que l'on a le droit de
  faire avec le fichier&nbsp;: lire (`r` comme *read*), écrire (`w` comme
  *write*), exécuter (`x` comme *e[x]ecute*) ou... rien du tout (`-` comme "que
  dalle").

* La première suite de trois caractères (`rw-`) concerne le propriétaire du
  fichier.

* La deuxième (`rw-`) concerne le groupe.

* La troisième (`r--`) définit les droits de tous les autres utilisateurs.

* Le propriétaire du fichier est désigné dans la troisième colonne&nbsp;:
  `microlinux`.

* La quatrième colonne donne le groupe du fichier&nbsp;: `microlinux`.

Notre affichage signifie donc&nbsp;: "Le fichier `droits.txt` appartient à
l'utilisateur `microlinux` et au groupe `microlinux`. Le propriétaire du
fichier et les membres du groupe ont le droit de le lire et de le modifier
(`rw-`). Tous les autres ont seulement le droit de le lire (`r--`)."

> Certaines distributions comme Red Hat Enterprise Linux, Oracle Linux, CentOS,
> Fedora, Debian et Ubuntu créent un groupe du même nom pour chaque
> utilisateur, dont l'utilisateur est le seul membre par défaut.  D'autres
> distributions comme Slackware ou SUSE rangent tous les utilisateurs dans un
> groupe `users`. Ainsi, lors de la création d'un fichier, son propriétaire
> sera toujours l'utilisateur, mais le fichier appartient au groupe `users`.  

Dans tous les cas, les droits d'accès concernent trois classes
d'utilisateurs&nbsp;:

* le propriétaire du fichier (*user*&nbsp;: `u`)&nbsp;;

* le groupe (*group*&nbsp;: `g`)&nbsp;;

* le reste du monde, les autres (*others*&nbsp;: `o`).

## Rendre un fichier exécutable

Peut-être vous en êtes-vous déjà vaguement douté, mais notre fichier
`droits.txt` contient du code exécutable. C'est un programme, eh oui&nbsp;! Un
script, plus exactement. Alors comment l'exécuter&nbsp;?

Dans un premier temps, nous allons définir des droits d'exécution pour le
propriétaire du fichier&nbsp;:

```
$ chmod u+x droits.txt
$ ls -l droits.txt
-rwxrw-r--. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

Les droits concernant le propriétaire sont passés de `rw-` à `rwx`, qui
signifie&nbsp;: "L'utilisateur `microlinux` a le droit de lire ce fichier, le
modifier ou l'effacer, mais aussi l'exécuter." Et c'est ce que nous allons
faire&nbsp;:

```
$ ./droits.txt
Voici la date :
Fri May 21 10:09:39 CEST 2021
```

> À la différence de Windows, la possibilité d'exécuter un fichier n'est
> aucunement liée à un quelconque suffixe comme `.EXE` ou `.COM`. Sous Linux,
> cette caractéristique est essentiellement liée au système de droits d'accès.

Vous voyez que les membres du groupe `microlinux` ont le droit de lire et de
modifier ce fichier (`rw-`) et que tous les autres ont seulement le droit de le
lire (`r--`). Comment empêcher complètement ces derniers d'accéder à mon
fichier&nbsp;? Tout simplement avec la commande suivante&nbsp;:

```
$ chmod go-rw droits.txt
$ ls -l droits.txt
-rwx------. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

Effectivement, les classes d'utilisateurs *group* (`g`) et *others* (`o`) n'ont
plus le droit de rien faire, comme le montre le `------` final.

Un système Linux permet d'attribuer des droits d'accès aux fichiers avec une
précision quasi-chirurgicale.

## Ajouter et retirer les droits de lecture et d'écriture

Donnons maintenant le droit à tout le monde (`a` comme *all*) de lire le
fichier&nbsp;:

```
$ chmod a+r droits.txt
$ ls -l droits.txt
-rwxr--r--. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

Ici, les trois classes d'utilisateurs (*user*, *group* et *others*) obtiennent
des droits de lecture.

De façon analogue, pour retirer les droits de lecture au groupe et aux autres,
il suffit d'invoquer la commande suivante&nbsp;:

```
$ chmod g-r,o-r droits.txt
```

Ou, plus simplement&nbsp;:

```
$ chmod go-r droits.txt
$ ls -l droits.txt
-rwx------. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

### La méthode directive

Dans les exemples jusqu'ici, nous avons vu deux approches dans la définition
des droits&nbsp;:

* une méthode *additive*, qui ajoute des droits à certaines catégories
  d'utilisateurs&nbsp;;

* une méthode *soustractive*, qui retire des droits à certaines catégories
  d'utilisateurs.

En dehors de ces deux approches, la méthode directive définit des droits très
précis pour chaque classe d'utilisateurs. Ainsi, la commande suivante donne
tous les droits au seul propriétaire&nbsp;:

```
$ chmod u=rwx,g=,o= droits.txt
```

Et si je veux rétablir les permissions initiales de mon fichier en utilisant la
méthode directive, voici comment je dois m'y prendre&nbsp;:

```
$ chmod u=rw,g=rw,o=r droits.txt
```

## Une autre approche : la notation numérique

À côté de la "notation *ugo*" (*user*, *group*, *others*), il existe une autre
façon de définir les droits d'accès des fichiers et des répertoires. Si je vous
la montre, ce n'est pas pour compliquer les choses, mais parce qu'elle est
également très répandue et que vous risquez de tomber dessus un jour ou
l'autre&nbsp;: je parle de la notation numérique.

Nous avons vu qu'il existe trois catégories d'utilisateurs, qui peuvent
bénéficier de trois droits d'accès différents.

| ***user*** | ***group*** | ***others*** |
| :---:      | :---:       | :---:        |
| `rwx`      | `rwx`       | `rwx`        |

Maintenant, affectons à chacun de ces droits une valeur numérique.

| Permission                   | Valeur numérique |
| :---                         |  ---:            |
| `r` (*read*, lecture)        | `4`              |
| `w` (*write*, écriture)      | `2`              |
| `x` (*e[x]ecute*, exécution) | `1`              |

Il suffit alors d'additionner les valeurs respectives de ces droits pour les
définir. Prenons quelques exemples.

### Attribuer tous les droits à tout le monde

Donnons des droits de lecture, d'écriture et d'exécution pour tout le monde :

* utilisateur : `4` (lecture) + `2` (écriture) + `1` (exécution) = `7`&nbsp;;

* groupe : `4` (lecture) + `2` (écriture) + `1` (exécution) = `7`&nbsp;;

* autres : `4` (lecture) + `2` (écriture) + `1` (exécution) = `7`.

En pratique, cela donne&nbsp;:

```
$ chmod 777 droits.txt
$ ls -l droits.txt
-rwxrwxrwx. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

### Retirer et ajouter des droits

Maintenant, laissons à l'utilisateur le droit de lire et d'écrire, en retirant
tous les droits aux autres&nbsp;:

* utilisateur : `4` (lecture) + `2` (écriture) + `0` (exécution) = `6`&nbsp;;

* groupe : `0` (lecture) + `0` (écriture) + `0` (exécution) = `0`&nbsp;;

* autres : `0` (lecture) + `0` (écriture) + `0` (exécution) = `0`.

```
$ chmod 600 droits.txt
$ ls -l droits.txt
-rw-------. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

Essayons autre chose : l'utilisateur a le droit de lire et d'écrire, le groupe
et tous les autres ont seulement celui de lire&nbsp;:

* utilisateur : `4` (lecture) + `2` (écriture) + `0` (exécution) = `6`&nbsp;;

* groupe : `4` (lecture) + `0` (écriture) + `0` (exécution) = `4`&nbsp;;

* autres : `4` (lecture) + `0` (écriture) + `0` (exécution) = `4`.

```
$ chmod 644 droits.txt
$ ls -l droits.txt
-rw-r--r--. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

Les deux modes de notation sont strictement équivalents. Autrement dit, le
dernier exemple aurait très bien pu être défini ainsi&nbsp;:

```
$ chmod u=rw,go=r droits.txt
$ ls -l droits.txt
-rw-r--r--. 1 microlinux microlinux 29 May 21 09:49 droits.txt
```

> Notez là encore la contraction de `g=r,o=r` en `go=r`.


Il n'est peut-être pas inutile de dresser un petit tableau récapitulatif de
toutes les combinaisons de droits possibles (en notation `rwx` ou numérique),
étant donné qu'il n'y en a pas un nombre infini. Huit au total, pour être
précis&nbsp;:

| Permission | Valeur numérique |
| :---:      |  :----           |
| `---`      | `0`              |
| `--x`      | `1`              |
| `-w-`      | `2`              |
| `-wx`      | `3` = `2`+`1`    |
| `r--`      | `4`              |
| `r-x`      | `5` = `4`+`1`    |
| `rw-`      | `6` = `4`+`2`    |
| `rwx`      | `7` = `4`+`2`+`1`|


> En règle générale, les administrateurs Unix chevronnés ainsi que les frimeurs
> ont tendance à préférer cette dernière méthode, étant donné qu'elle est plus
> "obscure". Pour ma part, j'ai décidé de vous montrer les deux manières de
> faire, car vous risquez de les rencontrer l'une comme l'autre dans des pages
> de documentation. Après, je vous conseille tout simplement d'utiliser celle
> qui vous convient le mieux.  

## Les permissions par défaut : umask

Vous vous êtes peut-être demandé d'où viennent les droits initiaux des
fichiers. 

Créons un fichier&nbsp;:

```
$ touch droits2.txt
$ ls -l droits2.txt
-rw-rw-r--. 1 microlinux microlinux 0 May 21 17:11 droits2.txt
```

Je vois que `droits2.txt` est créé d'emblée avec une structure `rw-rw-r--`. Qui
ou quoi décide des permissions pour les fichiers nouvellement créés&nbsp;?

Il faut savoir que, sur un système Linux, il n'est pas possible de créer un
fichier qui possède d'emblée les droits d'exécution. Cela signifie que les
permissions maximales que je peux obtenir pour un fichier nouvellement créé,
c'est `rw-rw-rw-`, autrement dit `666`. Diable&nbsp;!

Or, si je regarde de plus près mon fichier `droits2.txt`, il est affublé d'une
structure de droits `rw-rw-r--`, c'est-à-dire `664` en notation numérique. Si
je pars du principe que mes droits pléniers s'élèvent à `666` et que je dispose
de `664`, j'en conclus maussadement que je me suis fait gruger de `002` au
passage.

Le responsable de cette restriction se nomme `umask`. Le seul rôle de ce
réglage est de soustraire des droits lors de la création de fichiers.

```
$ umask
0002
```

Il est toutefois possible de le changer&nbsp;:

```
$ umask 0022
```

> Pourquoi la valeur de `umask` comporte-t-elle quatre chiffres alors que je
> n'en ai évoqué que trois&nbsp;? Nous occuper du premier nous mènerait trop
> loin...  Sachez donc qu'il y a quatre positions à définir en tout, mais que
> pour le moment, nous ne nous occuperons pas de la première. En revanche, on
> peut effectivement écrire `umask 002` dans la commande, au lieu de `0002`,
> pour le même résultat.  

La conséquence de cette redéfinition est immédiate. En effet, lorsque je crée
un nouveau fichier avec un `umask` de `0022`, ses droits par défaut sont
désormais de `644`&nbsp;:

```
$ touch droits3.txt
$ ls -l droits3.txt
-rw-r--r--. 1 microlinux microlinux 0 May 21 17:17 droits3.txt
```

Maintenant, soyons carrément permissifs&nbsp;:

```
$ umask 0000
$ touch droits4.txt
$ ls -l droits4.txt
-rw-rw-rw-. 1 microlinux microlinux 0 May 21 17:17 droits4.txt
```

Le fichier `droits4.txt` a les droits en lecture et écriture pour tout le
monde, ce qui est le maximum possible à la création.

La valeur de `umask` est redéfinie à chaque fois que vous vous connectez à la
machine.

> Jetez un oeil dans le fichier `/etc/profile`, aux alentours de la ligne 55.
> Je ne vous demande pas de lire ce genre de fichier comme la page des
> actualités locales du *Midi Libre*. Sachez juste que c'est là qu'est définie
> la valeur par défaut de `umask`.

## Gérer les droits d'accès aux répertoires

Depuis le début de cette section, nous avons essentiellement manipulé des
fichiers. Qu'en est-il des répertoires&nbsp;? Eh bien, voyons par nous-mêmes...

Avant de faire quoi que ce soit, assurez-vous de ne pas être `root` et de
redéfinir votre `umask` d'utilisateur à `0002`. Dans le doute, déconnectez-vous
de votre session et reconnectez-vous.

Dans un précédent exercice, nous avions créé un répertoire `test/`. S'il est
encore présent dans votre répertoire d'utilisateur, effacez-le&nbsp;:

```
$ rm -rf test/
```

Maintenant, recréez-le et affichez ses propriétés détaillées&nbsp;:

```
$ mkdir test
$ ls -ld test/
drwxrwxr-x. 2 microlinux microlinux 6 May 21 17:24 test/
```

Tiens&nbsp;! Contrairement à ce que j'ai pu énoncer plus haut, les droits
d'exécution sont bel et bien définis d'emblée. Non content de cela, ils sont
définis pour tout le monde&nbsp;!

> Remarquez au passage que, comme il s'agit cette fois d'un répertoire, le
> premier caractère est un `d` (*directory*) au lieu du tiret `-` qui
> représente un fichier.

L'explication pour cette anomalie apparente est simple, car les droits
d'exécution n'ont pas la même signification pour un répertoire que pour un
fichier. À y regarder de près, c'est même normal, car cela n'a pas de sens de
vouloir "exécuter un répertoire". Le `x` ici signifie simplement qu'on a le
droit de se placer dans le répertoire avec la commande `cd` et d'en afficher le
contenu.

Faisons la preuve par l'exemple pour nous en assurer. Dans ce répertoire
`test/`, créons trois fichiers `fichier1`, `fichier2` et `fichier3`, puis
revenons au point de départ&nbsp;:

```
$ cd test/
$ touch fichier1 fichier2 fichier3
$ ls -l
total 0
-rw-rw-r--. 1 microlinux microlinux 0 May 21 17:27 fichier1
-rw-rw-r--. 1 microlinux microlinux 0 May 21 17:27 fichier2
-rw-rw-r--. 1 microlinux microlinux 0 May 21 17:27 fichier3
$ cd ..
```

> Nous aurions pu créer nos trois fichiers avec la commande `touch
> fichier{1,2,3}`.

À partir du répertoire d'utilisateur, nous pouvons très bien afficher le
contenu de `test/`, grâce à un simple `ls test/` ou `ls -l test/`, et y revenir
en invoquant un simple `cd test/`.

Maintenant, retirons les droits d'exécution au répertoire, pour tout le monde
tant que nous y sommes. Nous avons donc le choix&nbsp;:

```
$ chmod a-x test/
```

Ou, en utilisant la notation numérique&nbsp;:

```
$ chmod 664 test/
```

Dans un cas comme dans l'autre, voici ce que nous obtenons&nbsp;:

```
$ ls -ld test/
drw-rw-r--. 2 microlinux microlinux 54 May 21 17:27 test/
```

Voyons ce que cette dernière opération a eu comme incidence sur l'accès au
répertoire&nbsp;:

```
$ cd test/
-bash: cd: test/: Permission denied
```

Dans ce cas, pouvons-nous au moins en afficher le contenu de l'extérieur&nbsp;?
L'entrée est bloquée, mais essayons de nous hisser sur la pointe des pieds et
de jeter un oeil curieux par-dessus le mur. Est-ce que nous voyons quelque
chose&nbsp;?

```
$ ls -l test/
...
total 0
-????????? ? ? ? ?              ? fichier1
-????????? ? ? ? ?              ? fichier2
-????????? ? ? ? ?              ? fichier3
```

Oui, mais non. Pas vraiment. Tout ce que nous devinons, c'est que ce répertoire
contient trois fichiers `fichier1`, `fichier2` et `fichier3`. Quant à obtenir
des informations détaillées, il n'en est pas question.

Allons plus loin dans le verrouillage et ôtons les droits de lecture au
répertoire. Là encore, mettons tout le monde à la même enseigne&nbsp;:

```
$ chmod a-r test/
$ ls -ld test/
d-w--w----. 2 microlinux microlinux 54 May 21 17:27 test/
```

> Je me permets de vous signaler au passage qu'il est assez peu usuel de garder
> un droit d'écriture sans avoir le droit de lecture. Dans la réalité
> quotidienne, nous aurions plutôt quelque chose du genre  `chmod a-rwx test/`
> ou `chmod 000 test/`.

Essayons d'afficher le contenu de ce répertoire&nbsp;:

```
$ ls -l test/
ls: cannot open directory test/: Permission denied
```

Que se passe-t-il maintenant si je restitue à l'utilisateur les droits de
lecture et d'exécution, mais pas ceux d'écriture&nbsp;? 

```
$ chmod u+rx test/
$ ls -ld test/
drwx-w----. 2 microlinux microlinux 54 May 21 17:27 test/
```

Essayez d'effectuer les manipulations suivantes&nbsp;: 

* afficher le contenu du répertoire avec `ls -l`&nbsp;;

* changer de répertoire courant avec `cd`&nbsp;;

* afficher le contenu des fichiers (vides, mais ce n'est pas grave)
  avec `cat`&nbsp;;

* renommer un fichier avec `mv`&nbsp;;

* créer un fichier `fichier4` avec `touch`.

Nous pouvons conclure de cette dernière expérience que le minimum syndical en
permissions pour travailler sans entraves dans un répertoire est le
suivant&nbsp;:

```
$ chmod u+rwx test/
```

Respectivement&nbsp;: 

```
$ chmod 700 test/
```

Soit&nbsp;:

```
$ ls -ld test/
drwx------. 2 microlinux microlinux 54 May 21 17:27 test/
```

## Changer le propriétaire et le groupe d'un fichier

Le propriétaire et le groupe d'un fichier sont-ils immuables&nbsp;? A priori,
vous vous dites que non. Vous commencez à comprendre que Linux est un système
extrêmement flexible et cela vous étonnerait qu'il y ait des choses que l'on ne
puisse pas changer. La réponse est&nbsp;: oui, mais pas n'importe comment. Et
surtout, pas n'importe qui.

Prenons un exemple pour illustrer la chose. En tant que `root`, copiez
n'importe quel fichier du système dans votre répertoire utilisateur&nbsp;:

```
# cp -v /etc/yum.conf /home/microlinux/
« /etc/yum.conf » -> « /home/microlinux/yum.conf »
```

Redevenez un utilisateur normal (`exit`, `logout` ou ++ctrl+d++)&nbsp;:
assurez-vous de vous retrouver dans votre répertoire d'utilisateur (`cd` sans
arguments le cas échéant) et regardez de près ce nouveau fichier&nbsp;:

```
$ ls -l yum.conf
-rw-r--r--. 1 root root 813 May 21 17:52 yum.conf
```

Il se trouve dans votre répertoire, mais il appartient à l'utilisateur `root` et
au groupe `root`. Essayons de changer cela avec une commande spécialement conçue
pour ce genre d'opération&nbsp;: `chown` (*change owner*, changer de propriétaire)&nbsp;:

```
$ man chown
NAME
    chown - change file owner and group
...
```

Après un coup d'oeil sur la syntaxe de la commande, je me lance&nbsp;:

```
$ chown microlinux:microlinux yum.conf
chown: changing ownership of ‘yum.conf’: Operation not permitted
```

Apparemment, le système se rebiffe. Je vais donc essayer autre chose&nbsp;:
redevenir `root` et attribuer ce fichier `/home/microlinux/yum.conf` à
l'utilisateur `microlinux` et au groupe `microlinux`.

```
$ su -
Password: ******** 
# chown microlinux:microlinux /home/microlinux/yum.conf
```

Je redeviens l'utilisateur commun des mortels que j'étais avant et je
vérifie&nbsp;:

```
# exit
logout
$ ls -l yum.conf
-rw-r--r--. 1 microlinux microlinux 813 May 21 17:52 yum.conf
```

Vous rappelez-vous la commande `sudo` décrite à la fin de la [précédente
leçon](utilisateurs.md)&nbsp;?  Elle nous aurait permis d'opérer le changement de
propriétaire en une seule commande&nbsp;:

```
$ sudo chown microlinux:microlinux yum.conf
```

### Pas de cadeaux !

Cette fois-ci, l'opération s'est déroulée comme prévu. Le fichier m'appartient
désormais et je suis tellement content... que je décide d'en faire cadeau à un
autre utilisateur présent sur le système, par exemple à `jmortreux`.

```
$ whoami
microlinux
$ chown jmortreux:jmortreux yum.conf
chown: changing ownership of ‘yum.conf’: Operation not permitted
```

Je constate alors avec consternation que Linux est un système pour utilisateurs
radins&nbsp;: on ne peut même pas faire cadeau de ses fichiers à un autre
utilisateur.

Essayons de tirer une conclusion de tout cela&nbsp;:

* Un utilisateur normal ne peut pas s'attribuer des fichiers qui appartiennent
  à quelqu'un d'autre.

* Il ne peut pas non plus attribuer ses propres fichiers à d'autres
  utilisateurs sur le système.

* L'administrateur `root` peut attribuer des fichiers à tous les utilisateurs,
  à sa guise.

* Il peut aussi, inversement, s'attribuer des fichiers appartenant aux autres
  utilisateurs.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

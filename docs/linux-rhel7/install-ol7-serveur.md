
**Objectif** : installer Oracle Linux 7.9 sur un PC ou un vrai serveur.

Maintenant que vous savez installer Linux dans une machine virtuelle, vous
voudrez probablement effectuer cette même procédure sur du vrai matériel
physique. Dans cet atelier pratique, vous aurez le choix d'installer Oracle
Linux sur :

* un PC faisant office de serveur&nbsp;;

* un vrai serveur.

Je pars du principe que vous possédez déjà un PC fixe et/ou un portable
tournant sous Windows ou macOS. Ne changez rien au système que vous utilisez au
quotidien et gardez-le en l'état. Évitez notamment les scénarios de *double
boot*, qui sont tout juste aptes à engendrer d'innommables souffrances.

## Choisir un PC faisant office de serveur 

Pas la peine de casser la tirelire pour découvrir Linux sur un ordinateur
dédié. En règle générale, un PC d'occasion vieux de six ou sept ans fera très
bien l'affaire. 

> Le PC faisant office de bac à sable physique dans mon bureau jusqu'à début
2021 était un vieux poste de travail Dell OptiPlex 330 datant de 2008. Pour
rendre cette machine plus réactive, je lui ai rajouté deux barrettes de mémoire
et j'ai remplacé le disque SATA par un disque SSD. 

Étant donné que les ateliers pratiques de cette formation se basent
exclusivement sur une installation en ligne de commande – c'est-à-dire
dépourvue d'interface graphique –, vous n'aurez pas besoin d'un foudre de
guerre pour mettre en pratique les exemples présentés. Il vous suffira de
répondre aux prérequis de l'installateur d'Oracle Linux, notamment un
processeur capable de faire tourner un système 64-bits et un gigaoctet de
mémoire vive, c'est tout.

Je vous conseille d'acheter votre PC d'occasion chez
[PlusDePC](https://www.plusdepc.com), un revendeur de matériel informatique de
déstockage et d'occasion reconditionné. Vous y trouverez des configurations
complètes (unité centrale, clavier, souris, écran plat) aux performances
décentes à des tarifs très intéressants. Le matériel est dûment testé et
garanti six mois. 

Si vous optez pour la solution PC-reconverti-en-serveur, évitez les ordinateurs
portables trop modernes. Hormis le fait que l'utilisation d'un PC portable
comme serveur est une aberration, vous risquez d'avoir quelques mauvaises
surprises avec le matériel ultra-récent et/ou exotique. Certes, il y a toujours
moyen de faire fonctionner une carte réseau non reconnue par l'installateur,
mais il vaut mieux éviter ce cas de figure dans le cadre d'une formation Linux.

## Choisir un vrai serveur 

Puisque l'objectif ultime de cette formation est sans doute l'installation d'un
serveur de production dans votre environnement de travail, rien ne vous empêche
de partir sur du "vrai" matériel serveur. Là encore, ce n'est pas la peine de
débourser une fortune. Les serveurs reconditionnés en bon état sont vendus à
des tarifs très intéressants. Optez plutôt pour le format tour que pour une
lame, à moins que vous ne disposiez d'un rack de rangement dans vos locaux.

Lorsque j'effectue une installation dans une entreprise qui ne dispose pas d'un
local à part pour les serveurs, j'utilise à peu près systématiquement les
machines HP de la gamme *Proliant Microserver*. Les serveurs de cette famille
sont particulièrement silencieux et vous pouvez les poser dans un espace de
travail sans vous faire maudire par tout le monde. Dans mon bureau, j'ai un des
premiers modèles de *HP Proliant Microserver* qui tourne quotidiennement depuis
2013 et j'en suis très satisfait.

Un facteur que l'on oublie souvent lorsqu'on achète un serveur, c'est le bruit.
Certains modèles génèrent autant de bruit qu'une batterie de sèche-cheveux au
démarrage, ce qui peut vite devenir lassant lorsqu'on essaie de travailler à
côté. 

## C'est dans les vieilles marmites qu'on fait les meilleures soupes

L'achat de matériel reconditionné – voire le reconditionnement de votre
matériel existant – offre une série d'avantages non négligeables. Vous faites
un geste pour l'environnement, puisque vous offrez une seconde vie sous Linux à
votre serveur. Et un aspect que l'on oublie parfois, c'est la compatibilité. En
achetant du matériel serveur qui date un petit peu, vous pouvez être sûr à
100 % que tous vos composants seront reconnus sans problème par Oracle
Linux&nbsp;7.

## Confectionner une clé USB d'installation

Munissez-vous d'une clé USB vide avec une capacité de 4 Go ou plus. Notez que,
de nos jours, il est difficile de trouver des clés de moins de 16 Go dans le
commerce. En revanche, certaines enseignes offrent des disques USB à faible
capacité comme porte-clés promotionnels. Ce genre de gadget convient
parfaitement pour la confection d'un support d'installation.

### Sous Windows

[Rufus](https://rufus.ie) est un logiciel libre pour Microsoft Windows qui
permet de créer une clé USB amorçable depuis un fichier ISO.

* Insérez une clé USB formatée.

* Lancez Rufus.

* Vérifiez si votre clé s'affiche bien dans le premier champ **Périphérique**.

* Sélectionnez votre fichier `Oracle-Linux-7.9-rhck-x86_64-netinstall.iso`.

* Cliquez sur **Démarrer**.

### Sous macOS

* Insérez une clé USB formatée.

* Lancez l'Utilitaire de Disque.

* Sélectionnez la clé.

* Choisissez l'option **Restaurer**.

* Effectuez un simple glisser/déposer du fichier
  `Oracle-Linux-7.9-rhck-x86_64-netinstall.iso` vers le champ de texte au
  milieu de la fenêtre.

## Graver le fichier ISO sur un CD-Rom

Étant donné que la taille du fichier ISO ne dépasse pas 700 mégaoctets, nous
pouvons éventuellement le graver sur un CD-Rom. Ne vous contentez pas de ranger
le fichier ISO sur un CD de données. Un fichier ISO équivaut *grosso modo* à un
"cliché" du CD d'installation, qu'il faut ensuite transférer sur le support
vierge en suivant une procédure bien spécifique.

### Sous Windows

* Lancez l'Explorateur de fichiers. 

* Naviguez vers le répertoire de téléchargement. 

* Repérez le fichier `Oracle-Linux-7.9-rhck-x86_64-netinstall.iso` et
  sélectionnez-le.

* Cliquez sur **Graver l'image disque**. 

* L'interface de gravure apparaît.

* Cliquez sur **Graver**. 

* Activez l'option **Vérifier le disque après la gravure**.

### Sous macOS

* Naviguez vers le répertoire de téléchargement.

* Repérez le fichier `Oracle-Linux-7.9-rhck-x86_64-netinstall.iso` et
  sélectionnez-le.

* Faites un clic droit sur le fichier.

* Sélectionnez **Graver...** dans le menu contextuel.

## Régler le BIOS

La première chose à faire lorsqu'on installe un système d'exploitation, c'est
de régler le BIOS (*Basic Input/Output System* ou "système élémentaire
d'entrée/sortie") de la machine pour qu'il démarre sur le support
d'installation, en l'occurrence le DVD ou la clé USB.

En termes très simples, le BIOS, c'est ce qui s'active immédiatement après
l'allumage de votre ordinateur, lorsque vous voyez défiler divers logos de
carte mère et de carte graphique, un test sommaire de la mémoire vive, ainsi
qu'une flopée d'informations diverses et variées que vous avez toujours eu
envie d'ignorer – et qui défilent d'ailleurs à une telle vitesse que l'on a
rarement le temps de les lire.

De nos jours, le BIOS est considéré comme obsolète, étant donné qu'il est
progressivement remplacé par l'UEFI. Si je vous le présente quand même, c'est
que vous risquez encore de le trouver sur bon nombre de machines.

Chaque constructeur possède sa touche magique qui vous permet de rentrer dans
le menu du BIOS. Il vous suffit d'appuyer dessus juste après avoir allumé votre
ordinateur. Pour la plupart, les systèmes vous affichent la touche magique à
utiliser pendant à peine une seconde. Le plus souvent, c'est la touche ++del++.
D'autres fois, c'est une des touches de fonction en haut de votre
clavier&nbsp;: ++f1++, ++f2++, ++f6++, ++f8++, ++f10++ ou ++f11.  Dans certains
cas, il est nécessaire d'appuyer sur la touche ++esc++ avant de faire tout
cela.

Tout ce que vous avez à faire ici, c'est indiquer à votre PC que le premier
périphérique utilisé au démarrage (`First Boot Device`) est votre lecteur
CD-Rom ou votre clé USB si votre machine est dépourvue de lecteur optique. Le
menu correspondant peut aussi s'appeler `Boot Priority` (priorité de démarrage)
ou bien `Boot Order` (ordre de démarrage). Enregistrez les changements et
quittez le menu du BIOS, ce qui s'effectue dans la majorité des cas en appuyant
sur la touche ++f10++.

## BIOS vs. UEFI

Le BIOS tel qu'on le connaît est une technologie établie depuis une bonne
quarantaine d'années, qui apporte donc nécessairement son lot de limitations et
autres désagréments. Des constructeurs comme Intel, AMD, Microsoft et Apple se
sont donc retroussé les manches depuis la fin des années 1990 pour développer
EFI (*Extensible Firmware Interface*), rebaptisé UEFI (*Unified Extensible
Firmware Interface*) par la suite. Les ordinateurs de bureau et les portables
neufs sont actuellement à peu près tous équipés d'UEFI. Notez que, sur le
descriptif des cartes mères modernes, le sigle `EFI` désigne à peu près
systématiquement l'UEFI.

Le système UEFI présente de nombreux avantages par rapport au BIOS. Il
s'initialise plus rapidement, il supporte l'installation parallèle de plusieurs
systèmes d'exploitation et il sait gérer les disques durs de plus de deux
téraoctets tout comme les tables de partitions GPT (*GUID Partition Tables*),
une méthode moderne qui ne souffre plus des limitations anachroniques du
partitionnement traditionnel. 

Les distributions Linux commencent petit à petit à être compatibles EFI.
Oracle Linux&nbsp;7 est capable de démarrer l'installation directement en mode
UEFI. Ceci étant dit, l'écrasante majorité des cartes mères modernes offrent la
possibilité de démarrer en mode BIOS traditionnel (`Legacy BIOS`) et je vous
conseille d'y revenir pour l'installation que nous allons effectuer dans ce
chapitre. Il se peut alors que votre support d'installation apparaisse deux
fois dans le menu de démarrage de votre PC. Dans ce cas, choisissez de démarrer
sur l'entrée de menu qui ne comporte **pas** de préfixe `EFI` ou `UEFI`.

L'écran d'accueil de l'installateur se présentera quelque peu différemment si
vous démarrez en mode UEFI. Dans ce cas, retournez dans l'interface de
configuration d'UEFI, cherchez l'option `Legacy BIOS` et activez-la. Si jamais
vous ne la trouvez pas, ne vous arrachez pas les cheveux pour autant.
L'installation en mode UEFI utilisera tout au plus un schéma de partitionnement
différent, ce qui ne nous bloquera pas lors de notre apprentissage.

## Tester la mémoire vive

Lorsque vous arrivez au menu de l'installateur d'Oracle Linux, démarrez sur
l'entrée `Troubleshooting` > `Run a memory test`.

[Memtest86](https://www.memtest86.com/) est un petit utilitaire inclus dans le
disque d'installation d'Oracle Linux, qui vérifie le bon fonctionnement de la
RAM (*Random Access Memory*, autrement dit la mémoire vive de votre machine).
Rien ne vous empêche de visualiser cette mémoire vive comme un amas de
milliards de petites cellules grises. Tout ce que l'ordinateur traite, calcule,
affiche, etc. passe par ces petites cellules.

Chez l'être humain normalement constitué, la destruction de quelques milliers
de cellules grises – suite à une soirée bien arrosée, par exemple – pourrait
presque passer inaperçue, à condition de ne pas répéter l'opération trop
souvent, bien sûr. Dans le cas d'un ordinateur, en revanche, le
dysfonctionnement ne serait-ce que d'une seule de ces milliards de cellules
peut entraîner des conséquences désastreuses, résultant en un système à peu
près inutilisable.

Le hic, c'est que les erreurs provenant d'une barrette de RAM défectueuse
peuvent se révéler perfides. Dans certains cas, elles ne se manifestent pas
immédiatement. Pendant un certain temps, l'ordinateur semble même fonctionner
normalement. Pourtant, à peine a-t-on lancé plus de trois applications en même
temps que l'on obtient des erreurs inexplicables. Ajoutez à cela le fait que
les barrettes de mémoire vendues neuves dans le commerce ne sont pas testées et
vous avez toutes les raisons de vouloir vous assurer que vous partez sur des
bases saines.

L'affichage de Memtest86 paraît quelque peu hermétique mais, parmi toutes les
informations retournées, seule une poignée nous intéresse.

* Dans la première colonne à gauche, `Memory:` affiche la quantité totale de
  RAM disponible, en mégaoctets. Vérifiez si la totalité de la mémoire dont
  vous êtes censé disposer s'affiche bien ici.

* Les deux barres de progression dessinées à l'aide d'une série de symboles
  dièse `#######` près du bord supérieur de l'écran indiquent l'état
  d'avancement des tests effectués.

* Dans la colonne des résultats, il n'y a que deux valeurs qui nous
  intéressent&nbsp;: `Pass` et `Errors`. La première indique le nombre de fois
  que Memtest86 a effectué la totalité des tests. Et si `Errors` indique autre
  chose que `0` (zéro), il vous faudra songer à remplacer la ou les barrette(s)
  incriminée(s).

Notez que Memtest86 ne s'arrête pas spontanément en bout de course. Dès que
l'ensemble des tests a été appliqué sur la totalité de la mémoire, le programme
s'exécute à nouveau depuis le début. Vous devez donc l'interrompre
manuellement, grâce à la touche ++esc++.

Comptez entre dix minutes et plusieurs heures pour un test complet, en fonction
de la puissance de calcul du processeur et de la quantité de mémoire à tester.

## Installation d'Oracle Linux

Installez Oracle Linux comme vous l'avez fait précédemment dans une machine
virtuelle&nbsp;:

* Vérifiez l'intégrité du support d'installation.

* Sélectionnez la langue du système.

* Choisissez éventuellement la disposition du clavier.

* Désactivez le service Kdump.

* Activez le réseau, vérifiez l'adresse IP et définissez le nom d'hôte de la
  machine.

* Définissez la source d'installation et la sélection de paquets pour une
  installation minimale.

### Partitionner le disque dur

Pour le partitionnement, nous devrons suivre une procédure légèrement
différente. La machine virtuelle nous offrait un disque vierge. Ici, nous
devrons très probablement supprimer les partitions existantes&nbsp;:

* Cliquez sur **Destination de l'installation**.

* Vérifiez si le disque dur est bien sélectionné.

* Gardez l'option **Configurer automatiquement le partitionnement**.

* Cochez **Je voudrais libérer de l'espace** pour faire de la place sur le
  disque dur.

* Cliquez sur **Terminé**.

* Dans l'écran subséquent, supprimez toutes les partitions existantes. Cliquez
  sur **Tout supprimer**, puis sur **Récupérer de l'espace**, ce qui vous fait
  revenir à l'écran principal.

### Terminer l'installation

À partir de là, terminez l'installation comme vous l'avez fait
auparavant&nbsp;:

* Définissez le mot de passe pour l'utilisateur `root`.

* Créez un utilisateur initial en lui conférant les privilèges d'administrateur.

* Redémarrez la machine au terme de l'installation des paquets.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


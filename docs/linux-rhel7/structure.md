
**Objectif** : découvrir sommairement l'organisation d'un système Linux.

## Où suis-je ?

Parmi les questions que peuvent se poser les utilisateurs de Windows qui
viennent de passer à Linux, voici les plus fréquentes&nbsp;:

* "Lorsque je viens de me connecter au système, où est-ce que je me
  retrouve&nbsp;?"

* "Où sont mes fichiers et mes documents&nbsp;?"

* "Où est mon lecteur `C:`&nbsp;?"

## Une structure en arborescence

Comme la plupart des systèmes d'exploitation modernes, Linux enregistre tous
ses fichiers dans une structure organisée de façon hiérarchique, en
arborescence. Imaginez votre système Linux comme un de ces grands classeurs de
dossiers qu'on voit dans les bureaux. Ce genre de meuble se subdivise en
tiroirs, chaque tiroir pouvant contenir une série de classeurs qui renferment
des documents ou d'autres classeurs à leur tour. La métaphore est rudimentaire,
mais elle vous aidera à vous faire une première idée.

> Une métaphore autrement parlante viendra compléter votre vision du système de
> fichiers&nbsp;: celle des poupées russes. Vous connaissez certainement le
> principe des poupées gigognes qui s'emboîtent les unes dans les autres. Dans
> un système Linux comme dans d'autres systèmes, chaque répertoire peut ainsi
> contenir d'autres répertoires, contenant eux-mêmes des sous-répertoires,
> jusqu'à ce qu'on arrive au bout de la hiérarchie.

## Home sweet home

En partant de votre répertoire utilisateur, montez d'un cran (`cd ..`) dans le
répertoire `/home`. Vous y verrez le répertoire correspondant à l'utilisateur
initial&nbsp;:

```
$ cd ..
$ pwd
/home
$ ls -l
total 0
drwx------. 2 microlinux microlinux 83 23 avril 08:35 microlinux
```

Pour illustrer le rôle du répertoire `/home`, nous allons créer trois nouveaux
utilisateurs&nbsp;:

```
$ sudo useradd -c "Nicolas Kovacs" nkovacs
$ sudo useradd -c "Jean Mortreux" jmortreux
$ sudo useradd -c "Agnès Debuf" adebuf
```

Les répertoires utilisateur correspondants se trouvent ici&nbsp;:

```
$ ls -l
total 0
drwx------. 2 adebuf     adebuf     62  5 mai   07:53 adebuf
drwx------. 2 jmortreux  jmortreux  62  5 mai   07:52 jmortreux
drwx------. 2 microlinux microlinux 83 23 avril 08:35 microlinux
drwx------. 2 nkovacs    nkovacs    62  5 mai   07:52 nkovacs
```

> Nous verrons la gestion des utilisateurs un peu plus loin. 

## Remonter à la racine : /

Partant du répertoire `/home`, si vous montez encore d'un cran (`cd ..`), vous
vous retrouvez à la racine du système de fichiers, symbolisée par une barre
oblique `/`. 

```
$ cd ..
$ pwd
/
$ ls -l
total 32
lrwxrwxrwx.   1 root root    7 23 avril 08:18 bin -> usr/bin
dr-xr-xr-x.   4 root root 4096 23 avril 08:27 boot
drwxr-xr-x.  19 root root 3060  5 mai   07:49 dev
drwxr-xr-x.  75 root root 8192  5 mai   07:53 etc
drwxr-xr-x.   6 root root   70  5 mai   07:53 home
lrwxrwxrwx.   1 root root    7 23 avril 08:18 lib -> usr/lib
lrwxrwxrwx.   1 root root    9 23 avril 08:18 lib64 -> usr/lib64
drwxr-xr-x.   2 root root    6 11 avril  2018 media
drwxr-xr-x.   2 root root    6 11 avril  2018 mnt
drwxr-xr-x.   2 root root    6 11 avril  2018 opt
dr-xr-xr-x. 139 root root    0  5 mai   07:49 proc
dr-xr-x---.   2 root root 4096 23 avril 08:27 root
drwxr-xr-x.  24 root root  740  5 mai   07:49 run
lrwxrwxrwx.   1 root root    8 23 avril 08:18 sbin -> usr/sbin
drwxr-xr-x.   2 root root    6 11 avril  2018 srv
dr-xr-xr-x.  13 root root    0  5 mai   07:49 sys
drwxrwxrwt.   8 root root 4096  5 mai   07:49 tmp
drwxr-xr-x.  13 root root 4096 23 avril 08:18 usr
drwxr-xr-x.  19 root root 4096 23 avril 08:28 var
```

## Le répertoire /bin

Historiquement, le répertoire `/bin` (pour *binaries* ou "binaires") contient
des commandes simples pour tous les utilisateurs et, plus précisément, toutes
les commandes dont le système a besoin pour démarrer correctement. Sur notre
système Oracle Linux, `/bin` est un lien symbolique pointant vers `/usr/bin`.
Nous aborderons les liens symboliques un peu plus loin.

```
$ ls /bin
```

## Le répertoire /boot

`/boot` est l'endroit où un système Linux range tout ce qu'il lui faut pour
démarrer (*to boot* signifie "démarrer" en anglais)&nbsp;:

```
$ ls /boot
```

Le fichier `vmlinuz-3.10.0-xxxx.el7.x86_64` est le noyau de votre machine. Vous
pouvez en avoir un seul ou toute une collection dans ce répertoire, ce que nous
verrons également plus loin. En revanche, vous n'en utiliserez jamais qu'un
seul à la fois.

Le noyau (ou kernel) est la partie du système d'exploitation qui est la plus
près de votre matériel. C'est précisément ce fichier qui est chargé, dès que
GRUB (*GRand Unified Bootloader*, le chargeur de démarrage) passe la main au
système. GRUB est en quelque sorte un logiciel ayant pour seule tâche de
démarrer le noyau. Les fichiers utilisés par GRUB se trouvent respectivement
dans `/boot/grub` et `/boot/grub2`. Si cela ne vous évoque pas grand-chose pour
l'instant, ne vous tracassez pas. 

> C'est tout à fait normal que vous n'ayez pas accès à `/boot/grub2` en tant
> que simple utilisateur. Le chargeur de démarrage fait partie des composants
> vitaux de votre système et un utilisateur "commun des mortels" n'a rien à y
> faire.  Pour l'instant, contentez-vous de savoir que ces fichiers existent et
> qu'ils sont rangés par ici.

## Le répertoire /dev

Le répertoire `/dev` (comme *device*, qui signifie "périphérique") est peuplé
d'une multitude de fichiers qui symbolisent chacun un périphérique de votre
machine. Repérez par exemple `/dev/sda` qui symbolise votre premier disque dur,
avec les partitions correspondantes `/dev/sda1` et `/dev/sda2`&nbsp;:

```
$ ls /dev
```

## Le répertoire /etc

L'étymologie de `/etc` est controversée. C'est un de ces cas de figure assez
fréquents dans la vie quotidienne où l'acceptation erronée a pris le dessus
pour devenir monnaie courante. 

La tradition veut que ETC signifie *Editable Text Configuration*, c'est-à-dire
"configuration éditable en mode texte". Voyons ce que cela signifie
concrètement. 

Dans le répertoire `/etc`, repérez le fichier `passwd` et affichez son
contenu&nbsp;:

```
$ cd /etc
$ ls -l passwd
-rw-r--r--. 1 root root 1115 Oct 14 07:19 passwd
$ cat passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
...
microlinux:x:1000:1000:Microlinux:/home/microlinux:/bin/bash
nkovacs:x:1001:1001:Nicolas Kovacs:/home/nkovacs:/bin/bash
jmortreux:x:1002:1002:Jean Mortreux:/home/jmortreux:/bin/bash
adebuf:x:1003:1003:Agnès Debuf:/home/adebuf:/bin/bash
```

La fin du fichier contient visiblement la configuration des utilisateurs
standards du système. 

> Les mots de passe des utilisateurs sont stockés dans un endroit sécurisé.  

La configuration d'un système Linux – qu'il s'agisse d'un simple poste de
travail ou d'un cluster de calcul du CERN ou de la NASA – est stockée dans de
simples fichiers texte humainement lisibles. Il est donc possible de configurer
le système en éditant ces fichiers avec un éditeur de texte comme seul outil. 

Si vous êtes curieux, jetez un oeil à quelques autres fichiers de ce
répertoire, sans vous laisser intimider par leurs noms barbares : `DIR_COLORS`,
`hostname`, `hosts`, `group`, `fstab` ou `profile`. Ne vous inquiétez pas si
vous n'y comprenez pas grand-chose. Retenez simplement que ce sont des fichiers
au format texte simple.

## Le répertoire /lib

Les bibliothèques partagées par les programmes de `/bin` et `/sbin` se trouvent
dans `/lib` (comme *libraries*, "bibliothèques" en français).

Un programme n'est pas forcément un bloc monolithique&nbsp;; il se sert d'un
ensemble de fonctions qui se situent dans une bibliothèque partagée. Ces
fichiers ne s'exécutent pas directement. Ils contiennent du code que l'on ne
veut pas réécrire chaque fois qu'un programme doit exécuter une fonction
similaire (par exemple ouvrir une fenêtre *Enregistrer sous* ou calculer un
cosinus).

Dans un système Windows, ce sont tous ces fichiers `.DLL` (*Dynamic Link
Library*, c'est-à-dire "bibliothèque de liens dynamiques") que vous trouverez
dans le répertoire `C:\WINDOWS\SYSTEM\`. Sur votre système Linux, ce sont tous
les fichiers `.so` (comme *shared object* qui signifie "objet partagé").

Si vous vous rendez dans le répertoire `/lib/modules`, vous y trouverez un
répertoire `3.10.0-xxxx.el7.x86_64`. Vous remarquerez un air de parenté avec le
nom du noyau `vmlinuz-3.10.0-xxxx.el7.x86_64`. 

Effectivement, tous les fichiers contenus dans ce répertoire appartiennent au
noyau. Ce sont là les "modules", l'équivalent de ce que les utilisateurs de
Windows ou Mac OS X connaissent sous la désignation de pilotes (*drivers* ou
"gestionnaires de périphérique")&nbsp;: des petits bouts de code que l'on
charge dans le noyau pour lui permettre de gérer tel ou tel périphérique. 

Entrez dans le répertoire `3.10.0-xxxx.el7.x86_64`, puis continuez dans
l'arborescence `kernel/drivers/net/ethernet`. Dans la liste de répertoires qui
s'affiche, vous reconnaîtrez peut-être vaguement des noms de fabricants&nbsp;:
`atheros`, `broadcom`, `intel`, `realtek`, etc. 

Jetez un oeil dans quelques-uns de ces répertoires et observez les différents
fichiers `.ko` qu'ils contiennent. Chacun correspond à un certain type de
matériel, plus précisément à une série de cartes réseau. 

Ainsi, `8139cp.ko` et `8139too.ko` dans le répertoire `realtek` correspondent à
une carte réseau équipée d'une puce (*chip*) Realtek 8139. 

De manière similaire, les fichiers commençant par `al` et `atl` dans le
répertoire `atheros` correspondent à des cartes réseau Atheros, `e1000.ko` et
`e1000e.ko` dans l'arborescence `intel` gèrent les cartes réseau Intel et ainsi
de suite. 

Dans la plupart des cas, le nom du module permet de deviner quel matériel lui correspond. 

## Les répertoires /mnt, /media et /run

Les répertoires `/media` et `/mnt` constituent par convention les points de
montage de votre système. Le répertoire `/run` est un ajout récent à la
hiérarchie sous Linux, dont un des rôles est de prendre la relève de `/media`.
C'est ici que se trouvent vos disques `D:`, `E:`, `F:`, etc.

Dans un système Linux, lorsque vous insérez un périphérique amovible comme un
disque dur externe, une clé USB, un CD-Rom ou un DVD, il doit être "monté".
Cela signifie que le système de fichiers du périphérique doit être intégré à
l'arborescence du système. Les données sont ensuite accessibles en dessous du
répertoire qui constitue ce qu'on appelle le "point de montage". Avant
d'enlever le périphérique, celui-ci doit être "démonté", c'est-à-dire que l'on
indique au système de fichiers que les données du périphérique amovible ne
doivent plus être englobées.

Sur un serveur Linux dépourvu d'environnement graphique, les opérations de
montage et de démontage s'effectuent de manière traditionnelle, en tapant une
série de commandes. Les distributions "poste de travail" modernes gèrent les
périphériques amovibles de manière complètement transparente, c'est-à-dire que
le montage s'effectue automatiquement.

Le montage et le démontage constituent un des concepts auquel un habitué des
systèmes Windows peut être complètement étranger. Pour l'instant, retenez
simplement que les répertoires `/media` et `/run` vous donnent accès aux
données des périphériques amovibles que le système gère automatiquement, par
exemple sur un poste de travail. Quant à `/mnt`, c'est le point de montage
"historique" que l'on conserve pour les systèmes de fichiers montés
manuellement, comme c'est le cas sur les serveurs. 

## Les répertoires /proc et /sys

Les répertoires `/proc` et `/sys` contiennent un système de fichiers virtuel
qui documente à la volée le noyau et les différents processus du système. Tout
ce qu'il faut retenir ici, c'est que certains fichiers contenus dans ces
répertoires nous fourniront des informations précieuses sur le système, comme
par exemple :

* le modèle et la fréquence du processeur (`/proc/cpuinfo` que nous avons vu)

* la mémoire vive et la quantité utilisée (`/proc/meminfo`)

* la synchronisation d'une grappe de disques RAID (`/proc/mdstat`) 

Quant au "système de fichiers virtuel", on peut le considérer comme un système
de fichiers volatile, dont il ne reste pas la moindre trace dès que vous
éteignez la machine.

## Le répertoire /root 

`/root`, c'est le répertoire utilisateur de... l'utilisateur `root`&nbsp;! Il
n'est donc pas étonnant que vous n'y ayez pas accès en tant qu'utilisateur
normal. "Et pourquoi pas `/home/root`&nbsp;?" penserez-vous peut-être.
L'administrateur serait-il réticent de se retrouver ainsi à pied d'égalité avec
les basses castes des utilisateurs communs&nbsp;? Oui, en quelque sorte, mais
pour une simple raison de sécurité.

Dans les installations de grande envergure, il arrive assez souvent qu'un
système Linux soit réparti sur plusieurs partitions d'un disque, voire sur
plusieurs disques et, dans certains cas, le système sur lequel vous travaillez
peut être "distribué" sur plusieurs machines, qui ne sont d'ailleurs pas
forcément dans la même pièce, ni dans le même bâtiment. Au démarrage, le
système se charge d'assembler les pièces pour vous présenter un tout cohérent.
Imaginez maintenant qu'il y ait un problème avec le disque ou la machine
contenant le répertoire `/home`. Si le répertoire d'utilisateur de `root` était
en dessous de `/home`, il serait inaccessible. `root` ne pourrait plus
s'identifier et, par conséquent, ne pourrait plus rien faire sur la machine.

> La langue anglaise désigne la racine du système de fichiers `/` par *root
> directory*. Gare à la confusion issue de l'homophonie avec *`/root`
> directory*&nbsp;!

## Le répertoire /sbin

Le répertoire `/sbin` (*system binaries*, autrement dit "binaires système")
renferme une série d'exécutables pour l'administrateur. Ces outils servent à
partitionner et formater des disques, configurer des interfaces réseau et bien
d'autres choses encore. Certaines de ces commandes peuvent être invoquées par
les utilisateurs du "commun des mortels". À titre d'exemple, la commande
suivante affiche la configuration réseau de votre machine.

```
$ /sbin/ip addr
```

Cependant, pour la plupart, ces utilitaires représentent l'équivalent numérique
d'une tronçonneuse. Dans les mains d'un expert, ils permettent d'abattre de la
besogne très rapidement. Mettez un utilisateur lambda aux commandes et
attendez-vous à un massacre.

Tout comme `/bin`, `/sbin` est un lien symbolique qui pointe vers
l'arborescence `/usr`. Nous y venons, justement.

## Le répertoire /usr

L'arborescence sous `/usr` (*Unix System Resources* ou *Unix Specific
Resources*, aucun lien avec *us(e)r*), renferme précisément tout ce qui n'est
**pas** nécessaire au fonctionnement minimal du système. Vous serez d'ailleurs
peut-être surpris d'apprendre que cela représente la part du lion. Sur notre
installation serveur minimale, `/usr` contient près de 90&nbsp;% de la totalité
du système. Vous constaterez que certains répertoires ou liens symboliques
rencontrés à la racine du système sont également présents ici&nbsp;:
`/usr/bin`, `/usr/lib` ou encore `/usr/sbin`.

Même sur notre installation minimale, l'arborescence `/usr` compte déjà plus de
25&nbsp;000 fichiers. Avec un système d'une telle complexité, il est important
que chaque chose ait une place bien définie pour que l'on s'y retrouve&nbsp;:

* `/usr/sbin` comporte d'autres commandes pour l'administrateur&nbsp;;

* `/usr/bin` renferme la majorité des exécutables pour les utilisateurs&nbsp;;

* `/usr/lib` et `/usr/lib64` contiennent les bibliothèques partagées de ces
  derniers.

Quel est le rôle des liens symboliques (ou raccourcis) `/bin`, `/lib`, `/lib64`
et `/sbin` à la racine du système&nbsp;? Traditionnellement, les systèmes Linux
opéraient la distinction et rangeaient dans ces répertoires le nombre
relativement limité d'applications et de bibliothèques qui étaient nécessaires
au démarrage ou au dépannage du système. Tout ce qui n'était pas vital *stricto
sensu* pour le démarrage avait sa place dans `/usr`. Or, depuis quelques
années, on observe une tendance croissante à faire fi de cette distinction et à
fusionner `/bin` et `/usr/bin`, `/lib` et `/usr/lib` et ainsi de suite, d'où la
série de liens symboliques.

> Sous Linux, `/usr/bin` représente à peu de choses près l'équivalent du
> répertoire `C:\Program Files` de Windows.

## Le répertoire /tmp

`/tmp` est le répertoire temporaire du système, comme son nom le suggère. C'est
l'endroit destiné à recevoir les données que vous considérez d'une certaine
manière comme volatiles, c'est-à-dire celles dont vous n'avez plus besoin après
un redémarrage de la machine.

## Le répertoire  /var

L'arborescence en dessous de `/var` contient toute une série de fichiers
variables&nbsp;: 

* des journaux

* des boîtes aux lettres de messagerie

* des sites web

Les journaux – ou *logfiles* – se situent dans `/var/log`. Ce sont des fichiers
au format texte "crachés" à la volée par différents composants d'un système en
marche. Un serveur de courrier électronique dépose les messages pour les
utilisateurs dans `/var/spool/mail`. Et lorsque vous installez un serveur web
sur votre machine, les pages de vos sites sont stockées en dessous de
`/var/www`.

## Pour aller plus loin

Étant donné que la question revient souvent, je me permets d'anticiper un peu
pour vous montrer comment je m'y suis pris pour compter le nombre de fichiers
en dessous de `/usr`, ou pour afficher l'espace disque occupé par les
arborescences respectives. Invoquez les deux commandes suivantes sans vous
préoccuper des détails pour l'instant.

```
$ sudo find /usr -type f 2> /dev/null | wc -l
26330
$ du -sh /* 2> /dev/null
0       /bin
93M     /boot
...
1.1G    /usr
99M     /var
```

Le système compte un total de 26&nbsp;330 fichiers en dessous de `/usr`, et
cette arborescence occupe 1.1 Go sur mon disque dur.

---

Installez le manuel en ligne&nbsp;:

```
$ sudo yum install man-pages man-pages-fr
```

Lisez la page de manuel qui décrit en détail la structure (ou hiérarchie) de
votre système :

```
$ man hier
```

* Utilisez les touches fléchées pour naviguer dans la documentation.

* Le raccourci ++q++ permet de quitter le manuel en ligne. 

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


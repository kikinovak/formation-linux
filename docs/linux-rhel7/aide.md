
**Objectif** : prise en main de la documentation en ligne et réflexes de base
pour trouver de l'aide.

## Unix est long et la vie est brève

Peut-être avez-vous déjà eu l'occasion de jeter un coup d'oeil à un manuel de
référence Unix&nbsp;: un de ces pavés indigestes comptant généralement un bon
millier de pages, et qui vous présentent une myriade de commandes susceptibles
d'accepter chacune une ribambelle d'options, égrenées par ordre alphabétique, 

Même si vous ne connaissez que quelques commandes avec une poignée d'options,
il arrive parfois que vous ne vous rappeliez plus la syntaxe exacte de ce que
vous souhaitez taper. C'est même fréquent, aussi bien pour les débutants que
les experts. Quelle était donc l'option pour la copie récursive d'un
répertoire&nbsp;? `cp -r` ou `-R`&nbsp;? Et comment fallait-il s'y prendre pour
voir les propriétés détaillées d'un répertoire sans en afficher le
contenu&nbsp;? 

Sous Linux, notre premier réflexe consistera à chercher l'aide directement sur
la machine. 

## Le bonheur est dans le PC

Tapez une commande, n'importe laquelle pourvu que son utilisation nécessite des
arguments. Invoquez-la sans ces derniers. Par exemple&nbsp;:

```
$ cp
cp: missing file operand
Try 'cp --help' for more information.
```

Prenons notre machine au pied de la lettre et faisons exactement ce qu'elle
nous suggère de faire&nbsp;:

```
$ cp --help
Usage: cp [OPTION]... [-T] SOURCE DEST
  or:  cp [OPTION]... SOURCE... DIRECTORY
  or:  cp [OPTION]... -t DIRECTORY SOURCE...
Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.
...
```

Le *shell* nous affiche une liste assez longue d'options applicables à la
commande `cp`, ainsi qu'une série d'explications sur son fonctionnement. 

## Afficher le manuel en ligne : man

Les commandes Unix acceptent ainsi pour la plupart une option `--help` (parfois
aussi tout simplement `-h`) affichant un écran d'aide succinct. En revanche,
toutes les commandes (à très peu d'exceptions près) disposent d'un véritable
manuel en ligne, que l'on peut afficher grâce à la commande `man` suivie du nom
de la commande sur laquelle on souhaite se renseigner&nbsp;:

```
$ man cp
CP(1)                    User Commands                                  CP(1)

NAME
  cp - copy files and directories

SYNOPSIS
  cp [OPTION]... [-T] SOURCE DEST
  cp [OPTION]... SOURCE... DIRECTORY
  cp [OPTION]... -t DIRECTORY SOURCE...

DESCRIPTION
  Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.

  Mandatory arguments to long options are mandatory for short options too.

    -a, --archive
      same as -dR --preserve=all

    --attributes-only
      don't copy the file data, just the attributes

    --backup[=CONTROL]
      make a backup of each existing destination file

    -b   like --backup but does not accept an argument

    --copy-contents
      copy contents of special files when recursive

    -d   same as --no-dereference --preserve=links

    -f, --force
      if  an  existing  destination  file cannot be opened, remove it and 
      try again (this option is ignored when the -n option is also used)

    -i, --interactive
      prompt before overwrite (overrides a previous -n option)
...
```

L'affichage des pages de manuel s'effectue par le biais du visualiseur `less` et
ce sont les raccourcis clavier de ce dernier qui vous aident à naviguer
(++space++ pour avancer d'un écran, ++q++ pour quitter et ++up++, ++down++,
++page-up++ et ++page-down++ pour avancer et reculer).

> Notre installation minimale ne compte qu'un nombre réduit de pages `man`.
> Invoquez la commande `sudo yum install -y man-pages` pour installer
> l'ensemble de la documentation en ligne sur votre système.

### Comment lire une page man ?

Les pages de manuel en ligne (ou pages `man`) sont toutes plus ou moins
organisées de la même façon.

* Tout en haut de la page se trouve la commande, avec son numéro de chapitre,
  par exemple `CP(1)`. Traditionnellement, les pages de manuel sont organisées
  en huit sections distinctes, que nous n'allons pas toutes énumérer ici.
  Retenez seulement que certaines commandes intéressent les utilisateurs du
  système, alors que d'autres seront réservées à l'administrateur. Pour
  comprendre cette distinction, affichez la page du manuel de `cfdisk` (`man
  cfdisk`), une commande qui sert à partitionner les disques durs. En haut de
  la page, `CFDISK(8)` vous indique qu'il s'agit d'une page de manuel de la
  section&nbsp;8 et donc d'une commande réservée à l'administrateur du système.
  Les commandes des simples utilisateurs sont toutes regroupées dans la
  section&nbsp;1. Certaines commandes disposent de leur page de manuel dans
  chaque section. Dans ce cas, il est nécessaire de spécifier le numéro de
  section pour les afficher séparément. À titre d'exemple, essayez
  successivement `man 1 printf` et `man 3 printf`.

* L'en-tête intitulé `NAME` fournit une description succincte de la commande.

* La section `SYNOPSIS` désigne la syntaxe de la commande, c'est-à-dire la
  façon dont il faut invoquer les options et les arguments. Ceux-ci peuvent
  être facultatifs ou obligatoires.

* `DESCRIPTION` fournit une explication détaillée du fonctionnement de la
  commande.

* La section `OPTIONS` affiche une liste exhaustive de toutes les options
  applicables à la commande, en les détaillant une par une. Pour comprendre de
  quoi il s'agit, affichez par exemple la page de manuel de la commande `ls`
  (`man ls`) et essayez de retrouver les options qui vous sont déjà familières.

* Plus loin, la section `BUGS` est quelque chose que vous chercherez en vain
  chez un éditeur de logiciels propriétaires. Si la commande a pu présenter une
  quelconque anomalie ou un quelconque dysfonctionnement dans le passé, cette
  section vous en informe. Voyez par exemple la page de manuel du *shell* Bash
  (`man bash`) pour une telle section.

* `SEE ALSO` vous renvoie d'une part vers une documentation plus détaillée (les
  pages `info`, que nous verrons tout à l'heure), d'autre part vers des
  commandes "cousines", c'est-à-dire en relation étroite. La page de manuel de
  `fdisk` vous renverra ainsi vers `cfdisk`, `parted` et `sfdisk`, trois autres
  commandes pour manipuler les tables de partitions sous Linux.

* Les pages de manuel en ligne comportent également souvent une section
  `AUTHORS` avec des informations de contact sous forme d'adresse web ou de
  courrier électronique, ce qui permet de signaler d'éventuels bogues. Ne vous
  sentez pas trop concerné par ceci, du moins pas pour l'instant.

### Recherche dans les pages du manuel

J'ai dit plus haut que les pages du manuel en ligne s'affichaient par le biais
du visualiseur `less`. Cela signifie que nous pouvons également nous servir des
fonctions de recherche intégrées dans `less`. Pour essayer ceci, cherchons par
exemple toutes les occurrences du mot "modification" dans la page de manuel
de `ls`. Une fois que la page s'affiche, invoquez la fonctionnalité de
recherche grâce à la barre oblique `/` suivie de la chaîne de caractères
`modification` que vous souhaitez trouver dans le texte.

Certaines pages de manuel sont assez longues, et la fonction de recherche
s'avérera utile pour trouver rapidement l'information qu'il vous faut.

## Afficher le manuel en ligne : info

Dans certains cas, les renseignements fournis par la commande `man` s'avèrent
insuffisants. Essayez par exemple d'obtenir des informations sur l'interpréteur
de commandes Bash en tapant `man bash`. Vous obtenez alors une série de pages
pour le moins cryptiques, qui ne vous sembleront probablement pas très
parlantes.

Voici comment afficher un manuel bien plus complet&nbsp;:

```
$ info bash
File: bash.info,  Node: Top,  Next: Introduction,  Prev: (dir),  Up: (dir)

Bash Features
*************

This text is a brief description of the features that are present in the
Bash shell (version 4.2, 28 December 2010).

...

* Menu:

* Introduction::                An introduction to the shell.
* Definitions::                 Some definitions used in the rest of this
                                manual.
* Basic Shell Features::        The shell "building blocks".
...
```

À la différence d'une simple page `man`, vous disposez ici d'un curseur.
Celui-ci vous permet de naviguer de page en page, en suivant les liens. Une
page `info` est en fait une véritable arborescence de pages organisées de façon
hiérarchique. Pour naviguer dans cette arborescence, il suffit de placer le
curseur sur les bouts de texte compris entre une étoile `*` et un
deux-points `:`.  La touche ++enter++ vous conduit alors dans le noeud (`Node`)
correspondant. Pour revenir en arrière, utilisez la touche ++u++ (comme *up*,
c'est-à-dire "remonter").  Là aussi, servez-vous du raccourci ++q++ pour
quitter la page `info`.

### Naviguer dans info

L'organisation d'une page `info` est comparable au fonctionnement d'un site
web. Les pages y sont organisées hiérarchiquement, le passage d'une page à
l'autre se faisant par le biais d'hyperliens. Le bouton `Page précédente` du
navigateur ramène en arrière.

> À en juger par les commentaires des utilisateurs chevronnés de systèmes Unix
> dans les forums ou les listes de diffusion, les pages `info` ont moins bonne
> presse que les pages `man`, en raison de leur système de navigation quelque
> peu désuet et, surtout, de l'impossibilité de les mettre en forme, notamment
> pour en imprimer un extrait ou la totalité.

## S'il ne fallait retenir que cela

Votre mémoire est une véritable passoire et vous oubliez sans cesse les
commandes les plus basiques, au point de vous retrouver incapable d'utiliser
l'aide en ligne&nbsp;? Ce n'est pas bien grave. Retenez juste ceci&nbsp;:

```
$ man man
```

La même chose vaut pour l'utilisation des pages `info`&nbsp;: 

```
$ info info
```

Bien évidemment, il existe de nombreuses façons d'obtenir de l'aide pour votre
système Linux&nbsp;: les sites de documentation, les blogs, les forums, les
listes de diffusion, les *newsgroups* (ou groupes de discussion Usenet), les
canaux IRC spécialisés, sans parler de la documentation spécifique à chaque
distribution.  Dans le monde Linux, ce n'est certainement pas la documentation
qui fait défaut, mais il est parfois difficile de s'y retrouver. Pour
l'instant, n'oubliez pas que l'aide la plus immédiate lorsque vous utilisez la
ligne de commande se trouve à portée de doigts.

## Pour aller plus loin

La meilleure adresse pour trouver de l'aide avec un système Oracle Linux est
sans doute le forum anglophone [*Applications and Infrastructure
Community*](https://community.oracle.com/tech/apps-infra/categories/oracle_linux).
Vous y trouverez non seulement une équipe de salariés d'Oracle Linux, mais
également une communauté sympathique d'administrateurs système, de développeurs
et autres professionnels compétents, qui apporteront des réponses aux questions
les plus pointues que vous pourrez leur poser. Je suis moi-même un utilisateur
régulier de ce forum, même si l'usabilité de l'interface est assez calamiteuse.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


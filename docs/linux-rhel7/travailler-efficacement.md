
**Objectif** : gagner en efficacité en utilisant la complétion automatique et
l'historique des commandes.

## Travailler moins pour taper plus

Si vous avez patiemment suivi les exemples et les exercices jusqu'ici, il y a
des chances pour que vous soyez quelque peu dépité par un certain manque de
confort de la console.

* L'interface en ligne de commande requiert la saisie de commandes
  interminables, les options, les noms de fichiers, les chemins d'accès
  complets, etc.

* Saisir tout ce texte représente une certaine quantité de travail.

* Personne n'aime travailler.

Il en résulte que personne n'aime travailler en ligne de commande. Le moment
est donc venu de se soucier du confort de l'utilisateur. La notion de confort
d'utilisation peut également s'appliquer à la ligne de commande, car nous
allons aborder une fonctionnalité extrêmement puissante du *shell*.

## La complétion automatique

Commençons par une commande simple&nbsp;:

```
$ /usr/sbin/getenforce
```

L'invocation de la commande `getenforce` avec son chemin complet représente un
certain travail. En tout et pour tout, il faut saisir vingt caractères et
confirmer par ++enter++. Pfouh&nbsp;!

Essayons donc de faire plus court. Tapez ceci, sans confirmer par
++enter++&nbsp;:

```
$ /u
```

Appuyez sur la touche ++tab++. Vous constatez que le *shell* a complété ce que
vous avez tapé&nbsp;:

```
$ /usr/
```

Continuez. Ajoutez un ++s++ et un ++b++&nbsp;:

```
$ /usr/sb
```

Appuyez à nouveau sur ++tab++&nbsp;:

```
$ /usr/sbin/
```

Ensuite, saisissez ++g++, ++e++, ++t++, ++e++&nbsp;:

```
$ /usr/sbin/gete
```

Et pour finir, appuyez une dernière fois sur ++tab++&nbsp;:

```
$ /usr/sbin/getenforce
```

Prenons un autre exemple. Imaginons que vous souhaitiez vous rendre dans le
répertoire `/boot`. Vous devriez donc taper la commande suivante&nbsp;:

```
$ cd /boot/
```

Essayons de procéder de la même façon&nbsp;:

```
$ cd /b
```

Nous appuyons sur ++tab++ et... il ne se passe rien. Tout au plus, le terminal
a émis un *bip*, c'est tout. Appuyons une seconde fois sur ++tab++&nbsp;:

```
$ cd /b
bin/  boot/
$ cd /b
```

Ici, le *shell* est confronté à une ambiguïté. Il ne sait pas s'il doit se
rendre dans `/bin` ou dans `/boot`. Lorsque vous avez appuyé une seconde fois
sur ++tab++, il vous a montré toutes les possibilités qui s'offrent à lui.
Pour lever l'ambiguïté, il suffit de fournir un caractère supplémentaire...

```
$ cd /bo
```

... suivi de ++tab++&nbsp;:

```
$ cd /boot/
```

Voyons un autre exemple pour illustrer ce fonctionnement. Je souhaite afficher
le contenu du répertoire `/sbin`. Je fais donc&nbsp;:

```
$ ls /sbin/
```

Si je me contente de saisir ++l++, ++s++, ++space++, ++slash++ et ++s++ suivis
de ++tab++, j'entends un simple *bip* et rien ne se passe. Une seconde pression
sur la touche ++tab++ me montre alors toutes les possibilités&nbsp;:

```
$ ls /s
sbin/    srv/     sys/
$ ls /s
```

Il suffit donc de saisir le ++b++ de `/sbin` pour lever l'ambiguïté et
permettre au shell de compléter le nom du répertoire.

Continuons avec un peu de pratique. Dans votre répertoire utilisateur, créez
une série de cinq fichiers vides&nbsp;:

```
$ touch fichier1.txt fichier2.txt fichier3.txt
$ touch fichier3.png fichier3.avi
```

Admettons que vous souhaitiez afficher les propriétés détaillées de chacun
d'entre eux, un par un. Commencez par le premier&nbsp;:

```
$ ls -l f
```

++tab++&nbsp;:

```
$ ls -l fichier
```

++1++, puis ++tab++&nbsp;:

```
$ ls -l fichier1.txt
```

Procédez de même avec `fichier2.txt` et `fichier3.txt`. Vous constaterez
qu'avec `fichier3.txt`, vous aurez deux ambiguïtés à lever&nbsp;:

```
$ ls -l f
```

++tab++&nbsp;:

```
$ ls -l fichier

```

++3++, ++tab++, ++tab++&nbsp;:

```
$ ls -l fichier3.
fichier3.avi  fichier3.png  fichier3.txt
$ ls -l fichier3.
```

++t++, ++tab++&nbsp;:

```
$ ls -l fichier3.txt
```

Résultat des courses&nbsp;:

* moins de touches à actionner&nbsp;;

* autant d'heures de loisirs de gagnées, que l'on pourra mettre à contribution
  pour aller à la plage ou, si le gain de temps est plus modeste dans un
  premier temps, faire une partie de démineur&nbsp;;

* plus aucune raison pour détester le travail en ligne de commande.

## La flemme devient un gage de qualité

La complétion automatique ne sert pas seulement à satisfaire le paresseux qui
sommeille en nous tous. Elle joue un autre rôle pour le moins aussi important
que celui de vous faire gagner du temps. Fidèle à mes habitudes, je fais
précéder mon propos par un exemple pratique.

Le répertoire `/etc/X11/xorg.conf.d` est censé contenir une série de réglages
de notre serveur graphique. Ne vous inquiétez pas si vous ne savez pas ce que
c'est. Pour l'instant, nous aimerions seulement afficher le contenu de ce
répertoire, sans trop nous soucier des détails techniques. Nous invoquons donc
la commande suivante, sans utiliser la complétion automatique&nbsp;:

```
$ ls /etc/x11/xorg.conf.d
```

Et nous nous retrouvons face au message d'erreur suivant&nbsp;:

```
ls: cannot access /etc/x11/xorg.conf.d: No such file or directory
```

Là, nous restons quelque peu perplexes. La documentation du serveur graphique a
pourtant insisté sur l'utilisation de ce répertoire et voilà qu'il n'existe
pas. Que se passe-t-il donc&nbsp;?

Regardez bien&nbsp;: le sous-répertoire de `/etc` s'appelle `X11` (avec un
`X` majuscule) et non `x11`, ce qui n'est pas la même chose. Maintenant,
invoquez à nouveau cette commande, mais en utilisant la complétion automatique,
c'est-à-dire en tapant&nbsp;:

```
$ ls /e
```

++tab++&nbsp;:

```
$ ls /etc/
```

++shift+x++, ++tab++&nbsp;:

```
$ ls /etc/X11/
```

++x++, ++o++, ++tab++&nbsp;:

```
$ ls /etc/X11/xorg.conf.d
```

Ici, le *shell* a complété le nom du répertoire correctement. Si nous avions
essayé de taper ++x++ au lieu de ++shift+x++ pour le répertoire `X11`, nous
nous serions tout de suite aperçu de notre erreur, suite à l'absence pour le
moins suspecte de `X11` dans la liste des répertoires proposés.

Cette petite expérience nous permet de tirer la conclusion suivante. Outre
l'avantage d'accélérer la saisie de façon considérable, la complétion
automatique offre également un contrôle de qualité, en jouant un rôle non
négligeable de correcteur.

## Répéter une commande

Dans certains cas, le passage par la case départ est inévitable. Une fois que
nous avons invoqué notre commande erronée et qu'elle nous a gratifié d'un
message d'erreur, il va bien falloir se résoudre à la ressaisir, en prenant
soin de ne pas commettre de faute de frappe. Est-il donc vraiment nécessaire de
tout recommencer depuis le début, juste à cause d'une petite coquille&nbsp;? Non,
comme vous le montre cet autre exemple&nbsp;:

``` 
$ ls /etc/X11/xorg.donf.d 
ls: cannot access /etc/X11/xorg.donf.d: No such file or directory
``` 

Après la petite seconde de surprise initiale, vous vous apercevez tout de suite
de l'erreur&nbsp;: vous avez tapé `xorg.donf.d` au lieu de `xorg.conf.d`. Pour
corriger votre erreur, il vous suffirait donc de ressaisir la commande en
prenant soin, cette fois-ci, d'écrire correctement le nom du répertoire. Avant
de faire cela, appuyez simplement sur la touche ++up++ et voyez ce qui se
passe&nbsp;:

```
$ ls /etc/X11/xorg.donf.d
```

Votre *shell* a gardé la dernière commande en mémoire. Dans ce cas, il sera
plus simple de remplacer le ++d++ par un ++c++, plutôt que de retaper
l'intégralité de la commande.

Mais ce n'est pas tout. Actionnez plusieurs fois de suite la touche ++up++ et
observez ce qui se passe. Apparemment, le *shell* n'a pas seulement mémorisé la
dernière commande, mais toutes les commandes que vous avez pu invoquer depuis
belle lurette&nbsp;! Ajoutez à cela la touche ++down++ et vous voilà capable de
naviguer dans l'historique de toutes les commandes saisies jusque-là – enfin,
pas toutes, il y a une limite quand même.

## Utiliser l'historique des commandes

Il existe un moyen très simple d'afficher la liste de toutes les commandes que
vous avez pu saisir&nbsp;:

```
$ history
...
558  alias rm='rm -i'
559  mkdir repertoiretest
560  touch repertoiretest/fichiertest
561  rm -rf repertoiretest/
562  vimtutor
...
```

Pour répéter l'une des commandes de la liste, remontez dans l'historique en
appuyant autant de fois que nécessaire sur la touche ++up++. Dans certains cas,
ce sera un exercice fastidieux, et il vaut mieux afficher l'historique complet,
puis sélectionner la commande directement en tapant un point d'exclamation
++exclam++ suivi du numéro de la commande. Admettons que je veuille ré-invoquer
la commande `mkdir repertoiretest` de l'exemple précédent, il me suffirait de
lancer la commande suivante&nbsp;:

```
$ !559
mkdir repertoiretest
```

> Soyez tout de même vigilants en utilisant cette fonctionnalité du shell. La
> commande est exécutée directement, sans attendre une quelconque confirmation
> avec la touche ++enter++. Ne l'utilisez donc pas avec des commandes
> destructives comme `rm`.

## Invoquer une commande en utilisant la recherche inversée

Dans le rayon "historique du shell", voici une dernière astuce que j'utilise
fréquemment au quotidien&nbsp;: la recherche inversée ou *reverse search*.
Admettons que la dernière fois que vous avez invoqué la commande `mkdir`,
c'était pour créer `repertoiretest`. Admettons encore que vous ayez effacé
`repertoiretest` par la suite et que vous souhaitiez le recréer maintenant.
Pour ce faire, vous avez plusieurs possibilités&nbsp;:

* invoquer `mkdir repertoiretest` tout simplement, en saisissant tous les
  caractères de la commande&nbsp;;

* actionner la touche ++up++ plusieurs fois de suite, jusqu'à ce que vous
  finissiez par tomber sur la commande souhaitée&nbsp;;

* afficher l'historique (`history`), chercher la commande que vous souhaitez,
  puis l'exécuter par le biais du point d'exclamation `!` suivi du numéro de la
  commande dans l'historique.

Il existe une autre solution, beaucoup plus simple. Tapez simplement ++ctrl+r++
et vous verrez que votre invite de commande change d'aspect&nbsp;:

```
(reverse-i-search)`':
```

À présent, dès que vous tapez les premiers caractères de la commande, le
*shell* complète instantanément avec ce qu'il trouve dans l'historique. En
l'occurrence, il me suffit ici de saisir ++m++ et ++k++ pour obtenir le
résultat attendu&nbsp;:

```
(reverse-i-search)`mk': mkdir repertoiretest
```

Il ne me reste qu'à confirmer par ++enter++ pour exécuter la commande.
Alternativement, je peux appuyer une deuxième fois sur ++ctrl+r++ pour afficher
la prochaine commande dans l'historique qui commence par `mk`.

## Pour aller plus loin

Comme tous les outils puissants, la complétion automatique et l'utilisation de
l'historique des commandes requièrent un certain entraînement avant d'être
efficace. Il se peut même qu'au début, cette façon de procéder vous ralentisse.
Faites fi de cette frustration initiale et accrochez-vous, car cela en vaut
vraiment la peine. L'utilisation systématique de ces techniques de saisie vous
fera gagner un temps considérable.

J'en profite pour vous prodiguer un autre conseil&nbsp;: **apprenez à
dactylographier**, c'est-à-dire à utiliser vos dix doigts sans regarder le
clavier. Là aussi, les bénéfices que vous en tirerez dépasseront de loin
l'investissement que cela nécessite au départ. Pensez en termes de productivité
accrue et surtout de migraines évitées. Vos yeux n'auront plus à effectuer des
va-et-vient incessants entre les touches.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


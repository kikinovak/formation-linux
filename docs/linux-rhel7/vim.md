
**Objectif** : prise en main de l'éditeur de texte Vim.

## Une réputation problématique

Vi est une partie intégrante de tout système Unix, de la même manière qu'un
château médiéval qui se respecte ne serait rien sans un solide chevalet de
torture dans son sous-sol. Gare à l'imprudent qui s'aventure dans l'édition
d'un texte avec Vi sans avoir respecté l'avertissement&nbsp;: "Vous qui
utilisez cet éditeur de texte, abandonnez tout espoir d'arriver à vos
fins&nbsp;!" Jetez un coup d'oeil sur ce qui se dit dans les forums
d'utilisateurs Linux novices au sujet de Vi&nbsp;; vous verrez que, si l'on
considère la moyenne arithmétique des opinions, il s'agit de toute évidence
d'un logiciel conçu par un vénusien halluciné. 

## L'éditeur de texte installé sur tous les systèmes Linux

Laissons donc de côté les chimères mythiques qui font la grimace à Vi et
essayons de voir la chose plus sobrement. Vi, ce n'est pas simplement un
éditeur de texte, c'est tout d'abord **le** programme d'édition de texte
standard présent sur **tous** les systèmes unixoïdes. En d'autres termes, que
vous installiez la dernière Fedora ou l'avant-dernière OpenSUSE, que vous
travailliez avec un LiveCD comme Slax, SliTaz ou Knoppix, ou que vous essayiez
de récupérer des données après un crash avec SystemRescueCd, vous aurez à votre
disposition la panoplie d'éditeurs sélectionnée par le distributeur, mais
Vi s'y trouvera toujours, dans tous les cas, invariablement. 

## Vi amélioré : Vim

Vi existe en plusieurs versions ou incarnations, les plus répandues étant
l'ancêtre `vi`, la version améliorée `vim` (ou *Vi improved*) et GVim, une version
graphique qui s'installe sur les postes de travail. Oracle Linux fournit Vi dans
trois moutures différentes&nbsp;:

* l'ancêtre Vi sous forme du paquet `vim-minimal`&nbsp;;

* la version améliorée&nbsp;: `vim-enhanced`&nbsp;;

* GVim pour les environnements graphiques&nbsp;: `vim-X11`.

Notre système minimal ne comprend pour l'instant que le paquet `vim-minimal`.
Nous allons installer la version améliorée comme ceci&nbsp;:

```
$ sudo yum install -y vim-enhanced
```

Cette opération récupère et installe automatiquement l'éditeur Vim et toutes
ses dépendances. Le téléchargement ne pèse pas lourd, un peu moins de 20
mégaoctets au total. Dorénavant, lorsque je mentionnerai "Vi", je parlerai en
fait de la version améliorée invoquée par la commande `vim`. Une fois que vous
l'aurez maîtrisée, vous ne serez pas dépaysé en utilisant une version plus
rudimentaire.

L'apprentissage de Vi se déroule généralement en deux temps :

1. Tout d'abord, il s'agit d'apprendre à survivre, c'est-à-dire réaliser des
   opérations simples d'édition de texte. 

2. Ensuite, une fois que les manipulations de base sont à peu près maîtrisées,
   on découvre progressivement le potentiel de cet éditeur. Il se révèle alors
   extrêmement puissant au quotidien et peut servir aussi bien à administrer
   des serveurs distants et confectionner des sites web qu'à élaborer des
   scripts ou des programmes.

> Parmi les nombreuses fonctionnalités que présente Vim, on trouve la
> coloration syntaxique, le formatage automatique, des fonctions de recherche
> et de remplacement, l'utilisation de macros, l'intégration du shell et
> beaucoup d'autres choses encore. Tous ces atouts en font l'outil de
> prédilection des programmeurs, des webmasters ou des administrateurs système.  

Commençons par les fonctions simples.

## Vimtutor

Il existe un grand nombre de méthodes, de livres (imprimés ou en ligne) et de
tutoriels pour apprendre Vi. Ils ont tous un point commun, celui de vous
dresser tôt ou tard une liste impressionnante de raccourcis clavier plus
rébarbatifs les uns que les autres, de façon à ce que seuls les utilisateurs
très têtus ou les compulsifs obsessionnels puissent espérer arriver au bout du
tutoriel. Sachez donc que le meilleur pédagogue pour enseigner Vi, c'est...
Vi lui-même&nbsp;!

Il existe en effet un petit logiciel d'entraînement incorporé à Vi, nommé
`vimtutor`, qui vous permet de vous entraîner sur cet outil. C'est aussi ce que
je vous conseille de faire, en faisant fi de toutes les méthodes imprimées ou
autres, car **Vi est un outil de travail qui doit avant tout vous rentrer dans
les doigts**. Vous pouvez donc ranger le petit carnet de notes destiné à
recueillir la liste de tous les raccourcis clavier. Ne cherchez pas à mémoriser
les fonctions de Vi, mais apprenez-les en les utilisant, par l'entraînement et
par la répétition.

```
$ vimtutor

===========================================================================
= B i e n v e n u e  dans  l e  T u t o r i e l  de  V I M  - Version 1.7 =
===========================================================================

  Vim est un éditeur très puissant qui a trop de commandes pour pouvoir
  toutes les expliquer dans un cours comme celui-ci, qui est conçu pour en
  décrire suffisamment afin de vous permettre d'utiliser simplement Vim.

  Le temps requis pour suivre ce cours est d'environ 25 à 30 minutes, selon
  le temps que vous passerez à expérimenter.

  ATTENTION :
  Les commandes utilisées dans les leçons modifieront le texte. Faites une
  copie de ce fichier afin de vous entraîner dessus (si vous avez lancé
  "vimtutor" ceci est déjà une copie).

  Il est important de garder en tête que ce cours est conçu pour apprendre
  par la pratique. Cela signifie que vous devez exécuter les commandes
  pour les apprendre correctement. Si vous vous contentez de lire le texte,
  vous oublierez les commandes !

  ...
```

## Un apprentissage interactif

À partir de maintenant, il suffit de lire attentivement les instructions qui
vous sont données et de les mettre en pratique. Vimtutor estime qu'il faut
"entre 25 et 30 minutes" pour nager d'une rive à l'autre. Comptez plutôt entre
trois quarts d'heure et une bonne heure si vous débutez. Si vous prévoyez
d'effectuer un passage complet par jour, il y a fort à parier qu'au bout d'une
semaine, vous soyez raisonnablement à l'aise dans l'édition de fichiers de
configuration avec Vi.

> Les raccourcis clavier de Vim paraissent pour le moins biscornus, notamment
> les déplacements du curseur, mais cela s'explique très simplement. Vim a été
> conçu spécialement pour les utilisateurs qui ont l'habitude de
> dactylographier. Si vous faites partie des gens qui utilisent leurs dix
> doigts pour taper sans regarder le clavier, vous serez très vite agréablement
> surpris par la redoutable efficacité de cet outil de travail.

## Pour aller moins loin

Vim est un outil peu intuitif qui nécessite un investissement de temps
considérable avant de pouvoir l'utiliser confortablement au quotidien. C'est
pour cela que beaucoup de formations Linux proposent l'alternative Nano, un
éditeur de texte simple et intuitif. 

Nano n'est pas présent sur notre système, mais nous pouvons l'installer
facilement&nbsp;:

```
$ sudo yum install -y nano
```

Pour vous entraîner un peu, effectuez une copie du fichier `/etc/hosts` vers
votre répertoire utilisateur&nbsp;:

```
$ cp -v /etc/hosts .
‘/etc/hosts' -> ‘./hosts'
```

Éditez ce fichier `~/hosts` avec Nano pour qu'il ressemble à ceci&nbsp;:

```
$ nano hosts

# ~/hosts
127.0.0.1     localhost.localdomain localhost 
192.168.2.10  linuxbox.local linuxbox

```
  
* Le raccourci clavier ++ctrl+o++ enregistre les modifications.

* ++ctrl+x++ permet de quitter Nano.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


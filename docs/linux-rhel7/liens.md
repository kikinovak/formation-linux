
**Objectif** : gérer les liens symboliques et les liens physiques sous Linux.

Jusqu'ici, notre prise en main du système Linux consistait essentiellement à
manipuler des fichiers et des répertoires. À l'occasion de nos travaux
pratiques, peut-être avez-vous remarqué ici ou là, la présence de fichiers
mystérieux qui ne semblent tomber ni dans l'une ni dans l'autre de ces deux
catégories.

## Les liens symboliques

Si vous affichez les détails du fichier `/bin/sh`, vous obtenez ceci&nbsp;:

```
$ ls -l /bin/sh
lrwxrwxrwx. 1 root root 4 May 15 09:57 /bin/sh -> bash
```

Cet OVNI (Objet virtuel non identifié) est un lien&nbsp;; plus exactement, un
lien symbolique. Dans l'affichage détaillé, il est identifié non pas par un
tiret `-` (fichier) ou un `d` (*directory*, c'est-à-dire répertoire), mais par
un `l` comme *link*, autrement dit un lien.

Notez la petite flèche `->` qui pointe vers un autre nom de fichier, en
l'occurrence `bash`. Il faut donc lire&nbsp;: `/bin/sh` est un lien symbolique
qui pointe vers le fichier `bash` situé dans le même répertoire.

### Créer des liens symboliques

Le meilleur moyen de maîtriser un nouvel objet, c'est de faire comme les
enfants&nbsp;: jouer avec. Dans votre répertoire d'utilisateur, créez un
répertoire `test_liens`, placez-vous dedans, puis créez un fichier `texte.txt`
avec un peu de contenu (bidon, certes)&nbsp;:

```
$ mkdir test_liens
$ cd test_liens/
$ cat > texte.txt << EOF
> Première ligne
> Deuxième ligne
> Troisième ligne
> EOF
```

Maintenant, créons un lien symbolique vers ce fichier&nbsp;:

```
$ ln -s texte.txt lien.txt
```

Voyons le résultat de cette opération&nbsp;:

```
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  9 May 26 08:09 lien.txt -> texte.txt
-rw-rw-r--. 1 microlinux microlinux 49 May 26 08:09 texte.txt
```

Arrêtons-nous là et essayons d'établir un état des lieux sommaire&nbsp;:

* les permissions `rwxrwxrwx` semblent pour le moins insolites&nbsp;;

* les deux fichiers n'ont pas la même taille&nbsp;: 9 octets pour l'un,
49 octets pour l'autre&nbsp;;

* dans la console, `lien.txt` apparaît non pas en blanc sur fond noir, mais en
turquoise.

Nous pouvons déjà expliquer les différences de taille. Le fichier `texte.txt`
comprend en gros 49 caractères, si l'on additionne y compris les retours
chariot. Quant à `lien.txt`, il pointe vers "`texte.txt`", c'est-à-dire vers un
nom de fichier comptant exactement 9&nbsp;caractères. Pouvons-nous afficher le
contenu de `lien.txt`&nbsp;?

```
$ cat lien.txt
Première ligne
Deuxième ligne
Troisième ligne
```

Vu sous cet angle, le contenu des deux fichiers est identique. Voyons si nous
pouvons ajouter du contenu&nbsp;:

```
$ echo "Quatrième ligne" >> lien.txt
$ cat lien.txt
Première ligne
Deuxième ligne
Troisième ligne
Quatrième ligne
```

Regardons à nouveau le listing détaillé&nbsp;:

```
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  9 May 26 08:09 lien.txt -> texte.txt
-rw-rw-r--. 1 microlinux microlinux 66 May 26 08:10 texte.txt
```

Depuis que nous avons ajouté du texte à `lien.txt`, la taille de ce dernier n'a
pas changé. En revanche, c'est bien `texte.txt` qui compte désormais 66 au lieu
de 49 octets.

### À quoi servent les liens symboliques ?

Les liens symboliques sont omniprésents sur un système Linux. Notre
installation minimale en compte déjà plus de 9000&nbsp;:

```
$ sudo find / -type l 2> /dev/null | wc -l
9666
```

> La commande `wc` (*word count*) sert à compter les octets, les mots ou les
> lignes d'un fichier. Avec l'option `-l` (pour `--lines`), elle affiche le
> nombre de sauts de ligne. Reportez-vous à sa page `man` pour plus
> d'informations.  

#### Exemple n°1 : awk

Dans la leçon consacrée à [la gestion des utilisateurs](utilisateurs.md), nous
avons utilisé `awk` pour extraire des informations du fichier `/etc/passwd`.
AWK est un langage de programmation et il en existe plusieurs implémentations.

```
$ ls -l /usr/bin/awk
lrwxrwxrwx. 1 root root 4 May 15 09:57 /usr/bin/awk -> gawk
```

Sur notre système, `/usr/bin/awk` pointe vers `gawk`, ce qui signifie que
lorsque nous invoquons `awk`, c'est en réalité la commande `gawk` qui est
utilisée sous le capot. Plus précisément, on dira que GNU AWK (`gawk`) est
l'implémentation de AWK sur notre système.

#### Exemple n°2 : ex

L'éditeur `ex` (raccourci pour *EXtended*) a été utilisé sur les systèmes Unix
depuis la fin des années 1970. La commande `/usr/bin/ex` existe encore sur
notre système Linux, sous forme de lien symbolique qui pointe vers
l'éditeur Vi&nbsp;:

```
$ ls -l /usr/bin/ex
lrwxrwxrwx. 1 root root 2 May 15 09:58 /usr/bin/ex -> vi
```

On peut y voir une forme d'hommage envers la tradition. Plutôt que de rendre
une commande obsolète, on va la faire pointer vers une implémentation plus
récente.

#### Exemple n°3 : gpg

Le lien symbolique utilisé pour l'outil de chiffrement `gpg` (*GNU Privacy Guard*)
répond probablement à une logique similaire&nbsp;:

```
$ ls -l /usr/bin/gpg
lrwxrwxrwx. 1 root root 4 May 15 09:59 /usr/bin/gpg -> gpg2
```

#### Conclusion

Dans tous ces cas de figure, les liens symboliques servent manifestement à
accéder à un programme (`gawk`, `vi`, `gpg2`) avec un nom habituel (`awk`,
`ex`, `gpg`).

### Lequel est le vrai ?

Un lien symbolique n'est pas seulement susceptible de pointer vers un fichier,
mais également vers un répertoire. Replacez-vous dans le répertoire
`~/test_liens` en tant qu'utilisateur et saisissez les commandes
suivantes&nbsp;:

```
$ ln -s /tmp/ depot
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  5 May 26 08:56 depot -> /tmp/
lrwxrwxrwx. 1 microlinux microlinux  9 May 26 08:09 lien.txt -> texte.txt
-rw-rw-r--. 1 microlinux microlinux 66 May 26 08:10 texte.txt
```

Nous l'avons dit [plus haut](structure.md)&nbsp;: `/tmp` est un répertoire
temporaire, comme son nom le suggère. Affichez le contenu de ce
répertoire&nbsp;:

```
$ ls /tmp/
systemd-private-6eab...-chronyd.service-wBmOBA
systemd-private-80a2...-chronyd.service-H7mM7j
systemd-private-8b6e...-chronyd.service-R2I0Pf
```

> Sans rentrer dans les détails, notons que ces données temporaires
> appartiennent à `chronyd`, un service qui se charge d'ajuster l'horloge
> système en consultant des sources de temps externes.

Et maintenant, voyez ce que contient le répertoire `depot`&nbsp;:

```
$ ls depot/
systemd-private-6eab...-chronyd.service-wBmOBA
systemd-private-80a2...-chronyd.service-H7mM7j
systemd-private-8b6e...-chronyd.service-R2I0Pf
```

Il ne s'agit pas d'une copie du contenu de `/tmp`, mais bel et bien des mêmes
données. Autrement dit, le lien symbolique `depot` fonctionne comme un
représentant à part entière du répertoire `/tmp` vers lequel il pointe.

### Casser un lien symbolique

Que se passe-t-il maintenant si la cible d'un lien symbolique vient à
disparaître&nbsp;? Nous n'allons pas effacer notre répertoire `/tmp`, ce serait
une très mauvaise idée, mais nous pouvons tenter l'expérience avec le fichier
`texte.txt`, la cible du lien symbolique `lien.txt`&nbsp;:

```
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  5 May 26 08:56 depot -> /tmp/
lrwxrwxrwx. 1 microlinux microlinux  9 May 26 08:09 lien.txt -> texte.txt
-rw-rw-r--. 1 microlinux microlinux 66 May 26 08:10 texte.txt
$ rm texte.txt
$ ls -l
total 0
lrwxrwxrwx. 1 microlinux microlinux 5 May 26 08:56 depot -> /tmp/
lrwxrwxrwx. 1 microlinux microlinux 9 May 26 08:09 lien.txt -> texte.txt
```

Un lien dont la cible a disparu est un "lien cassé" (*broken link*). Dans notre
terminal, le lien cassé apparaît en rouge sur fond noir, un choix de couleur
qui suggère que quelque chose ne tourne pas rond.

Nous pourrions très bien recréer la cible moyennant un simple `touch`. Le
fichier serait certes vide, son ancien contenu serait perdu, mais le lien
"saurait" que sa cible existe et ne rouspéterait plus.

```
$ ls -l
total 0
lrwxrwxrwx. 1 microlinux microlinux 5 May 26 08:56 depot -> /tmp/
lrwxrwxrwx. 1 microlinux microlinux 9 May 26 08:09 lien.txt -> texte.txt
$ touch texte.txt
$ ls -l
total 0
lrwxrwxrwx. 1 microlinux microlinux 5 May 26 08:56 depot -> /tmp/
lrwxrwxrwx. 1 microlinux microlinux 9 May 26 08:09 lien.txt -> texte.txt
-rw-rw-r--. 1 microlinux microlinux 0 May 26 09:06 texte.txt
```

### Effacer un lien symbolique

Nous avons vu comment il est possible d'associer des fichiers et des
répertoires à des liens symboliques. Voyons maintenant comment lever cette
association, c'est-à-dire effacer des liens&nbsp;:

```
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  5 May 26 08:56 depot -> /tmp/
lrwxrwxrwx. 1 microlinux microlinux  9 May 26 09:09 lien.txt -> texte.txt
-rw-rw-r--. 1 microlinux microlinux 66 May 26 09:09 texte.txt
$ rm lien.txt
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  5 May 26 08:56 depot -> /tmp/
-rw-rw-r--. 1 microlinux microlinux 66 May 26 09:09 texte.txt
```

Ce qu'il faut retenir ici, c'est que la suppression d'un lien symbolique par le
biais de la commande `rm` n'entraîne en aucun cas la suppression de la cible. En
effet, `texte.txt` est toujours là.

Il en va de même pour un lien symbolique qui pointe vers un répertoire :

```
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  5 May 26 08:56 depot -> /tmp/
-rw-rw-r--. 1 microlinux microlinux 66 May 26 09:09 texte.txt
[microlinux@linuxbox test_liens]$ rm depot
[microlinux@linuxbox test_liens]$ ls -ld /tmp/
drwxrwxrwt. 8 root root 4096 May 26 09:09 /tmp/
```

Ici, nous utilisons bien un simple `rm`, car `depot` n'est pas un répertoire à
proprement parler. C'est un lien symbolique pointant vers un répertoire. Vous
ne pourrez donc pas utiliser `rmdir` pour supprimer ce genre de lien.

#### Gare à la barre oblique !

Méfiez-vous de la complétion automatique lorsque vous supprimez un lien
symbolique qui pointe vers un répertoire. Notez bien que, dans l'exemple, j'ai
écrit `rm depot` et non pas `rm depot/` avec la barre oblique finale. Dans le cas
contraire, je me serais retrouvé confronté à un problème quelque peu
ubuesque&nbsp;:

```
$ ls -l
total 4
lrwxrwxrwx. 1 microlinux microlinux  5 May 26 09:14 depot -> /tmp/
-rw-rw-r--. 1 microlinux microlinux 66 May 26 09:09 texte.txt
[microlinux@linuxbox test_liens]$ rm depot/
rm: cannot remove ‘depot/’: Is a directory
[microlinux@linuxbox test_liens]$ rmdir depot/
rmdir: failed to remove ‘depot/’: Not a directory
```

## Les liens physiques

### Un nom de fichier alternatif

Il existe une autre catégorie de liens&nbsp;: les liens physiques. La
désignation de "lien" peut prêter à confusion dans ce cas&nbsp;; il vaut mieux
y voir quelque chose de l'ordre d'un "nom de fichier alternatif". En principe,
un lien symbolique n'est rien d'autre qu'un petit fichier qui pointe vers un
autre fichier, nous l'avons vu. La suppression du lien symbolique ne change
rien au fichier original en soi, qui reste intact. Un lien physique, en
revanche, constitue une référence supplémentaire à un emplacement du système de
fichiers.  C'est bien un seul et même fichier, mais accessible à partir d'un
autre endroit et sous un autre nom. Un exemple éclaircira la nuance.

Videz le répertoire `~/test_liens` et (re)créez-y un fichier `texte.txt`, comme
pour le premier cas de figure&nbsp;:

```
$ cat > texte.txt << EOF
> Première ligne
> Deuxième ligne
> Troisième ligne
> EOF
```

À présent, créez un lien physique vers `texte.txt`. C'est très simple, il
suffit d'omettre l'option `-s` de la commande `ln`&nbsp;:

```
$ ln texte.txt lien.txt
[microlinux@linuxbox test_liens]$ ls -l
total 8
-rw-rw-r--. 2 microlinux microlinux 49 May 26 09:17 lien.txt
-rw-rw-r--. 2 microlinux microlinux 49 May 26 09:17 texte.txt
```

Là aussi, arrêtons-nous un instant pour apprécier le résultat&nbsp;:

* le lien physique n'apparaît pas en turquoise dans le terminal, mais en blanc
  (ou noir) sur fond bleu&nbsp;;

* ses permissions sont identiques à celles du fichier cible&nbsp;;

* apparemment, il a également la même taille que la cible&nbsp;: 49 octets dans
  l'exemple.

Le moment est venu de vous dévoiler solennellement la signification de la
deuxième colonne du listing détaillé. Dans l'exemple, notez le `2` dans la
deuxième colonne après les droits d'accès. Il s'agit tout simplement du nombre
de liens physiques du fichier.

### Un lien physique a la vie dure

Vous avez du mal à croire qu'un lien physique se comporte en tous points comme
le fichier vers lequel il pointe&nbsp;? Pour en avoir le coeur net, il suffit
de supprimer l'original de l'exemple et de voir ce qui se passe&nbsp;:

```
$ ls
lien.txt  texte.txt
$ rm texte.txt
$ ls -l
total 4
-rw-rw-r--. 1 microlinux microlinux 49 May 26 09:17 lien.txt
$ cat lien.txt
Première ligne
Deuxième ligne
Troisième ligne
```

Ici, nous avons supprimé `texte.txt`, mais le fichier a en quelque sorte
bénéficié d'une seconde vie, sous forme du lien physique `lien.txt`. Pour nous
débarrasser une bonne fois pour toutes du fichier et de son contenu, il faudra
donc également supprimer `lien.txt`.

## Hard ou soft ?

Les liens symboliques et physiques sont souvent désignés par les termes anglais
*soft link* et *hard link*. 

Notez une restriction importante concernant la deuxième catégorie&nbsp;: un lien
physique doit obligatoirement pointer vers un fichier dans le même système de
fichiers, c'est-à-dire que la cible ne peut pas se situer sur une autre
partition que le lien. 

> Si vous ne comprenez pas cette dernière précision, ce n'est pas bien grave.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


**Culture générale et notions de base**&nbsp;: Unix, Linux, logiciels libres,
distributions Linux, Red Hat Enterprise Linux, Oracle Linux, qualité
entreprise, etc.

## Linux est un clone libre d'Unix

Linux est un système d'exploitation libre dérivé d'Unix. L'ancêtre a été créé
en 1969 et a fait tourner les ordinateurs de la terre entière bien avant que
l'humanité n'associe des mots de tous les jours comme *windows* ou *apple* à de
l'informatique. Son clone libre a été développé depuis 1983, et les premières
versions ont vu le jour en 1991. Linux conserve la robustesse légendaire des
systèmes Unix. Ce qui le différence de ces derniers, c'est qu'il peut être
librement copié et redistribué.

## Pourquoi utiliser Linux ?

Sur les serveurs, les avantages de Linux et des logiciels libres par rapport
aux solutions propriétaires établies, sont significatifs.

### La stabilité

Un serveur Linux est robuste et stable. Vous obtenez un système réactif et
rapide, qui permet de tourner en production sans vous soucier de plantages à
répétition.

### La sécurité

Linux a été conçu dès le départ avec les concepts fondamentaux d'Unix, avec une
séparation propre des processus, un système de permissions bien pensé et une
couche réseau digne de ce nom. Un serveur Linux correctement configuré et
maintenu met vos données à l'abri des attaques les plus communes qui sévissent
sur Internet. L'adoption de technologies comme
[SELinux](https://red.ht/3alRxSK) (*Security Enhanced Linux*) développé par la
NSA vous permet d'avoir quelques longueurs d'avance sur des attaquants
malveillants.

### Le coût

Un serveur Linux est économique. Vous n'avez pas de frais de licence à payer,
et le système est peu gourmand en ressources matérielles. Si vous adoptez un
système comme Oracle Linux, vous bénéficiez d'un OS de qualité entreprise, avec
un cycle de support de dix ans pour les mises à jour de sécurité pour chaque
version, et entièrement gratuit.

## Qui utilise Linux ?

Depuis le milieu des années 1990, Linux s'est répandu partout dans le monde,
sans faire de bruit, sans campagnes publicitaires, et sans qu'une
multinationale ne vienne "encourager" le déploiement à coups de lobbying et
autres méthodes douteuses. 

* L'infrastructure de l'Internet est assurée en grande partie par Linux. Les
  gigantesques parcs de serveurs des grandes entreprises comme Google, Facebook
  ou Amazon fonctionnent tous sous Linux.

* La totalité des 500 superordinateurs les plus puissants de la terre – comme
  par exemple ceux des grands centres de recherche scientifique de la NASA ou
  du CERN – tournent sous Linux. 

* Les systèmes Linux embarqués sont omniprésents dans notre quotidien et font
  tourner à peu près tout, du modem-routeur ADSL au téléviseur, du distributeur
  de billets de trains au système de navigation GPS, du téléphone portable au
  distributeur de boissons, etc.

* En France, la Gendarmerie Nationale a migré la majorité de son parc de
  90&nbsp;000 postes de travail de Microsoft Windows vers Linux.

## Qu'est-ce qu'un logiciel libre ?

Le terme anglais *free software* comporte une ambiguïté, et il s'agit de
distinguer&nbsp;:

* *free as in free speech*&nbsp;: libre dans le sens de "liberté de la parole"

* *free as in free beer*&nbsp;: gratuit dans le sens de "bière à gogo"

Selon la définition proposée par la *Free Software Foundation*, un logiciel est
libre s'il respecte les quatre conditions fondamentales&nbsp;:

* la liberté d'utiliser le logiciel

* la liberté de le copier

* la liberté d'en étudier le fonctionnement

* la liberté de le modifier et de redistribuer cette version modifiée

Une seule obligation permet de préserver ces quatre libertés. Toute personne
qui souhaite apporter des modifications au code source d'un logiciel – en vue
de l'améliorer ou d'en modifier le comportement – est tenue de publier ces
modifications sous les mêmes conditions, en respectant à son tour les quatre
libertés fondamentales. C'est l'application du principe du *copyleft* (un jeu
de mots sur *copyright*, droits d'auteur) qui évite notamment l'appropriation
du code source libre par une entreprise. Ce principe est entériné dans la
licence publique GNU (ou licence GPL) publiée en 1989.

## Choisir une version de Linux

L'utilisateur novice de Linux se retrouve d'abord confronté à un choix qui peut
s'avérer déroutant. Linux, oui, mais lequel&nbsp;? Ubuntu&nbsp;? Debian&nbsp;?
Red Hat&nbsp;?  Fedora&nbsp;? SUSE&nbsp;? OpenSUSE&nbsp;? En effet, il ne
s'agit pas d'un seul "système Linux", mais de toute une ribambelle de
"distributions".

Le portail [DistroWatch](https://distrowatch.com) recense actuellement
plusieurs centaines de distributions Linux activement maintenues. Cette liste
s'allonge toutes les semaines, et c'est sans compter les milliers de projets
privés ou autrement confidentiels dans le monde entier.

## Qu'est-ce qu'une distribution Linux ?

Dans le cas d'une des grandes distributions courantes comme Red&nbsp;Hat
Enterprise Linux, Debian, Ubuntu, CentOS, Fedora ou SUSE, une distribution
Linux est un ensemble cohérent, composé en règle générale&nbsp;:

* du système de base

* d'une série d'outils d'administration

* d'une panoplie logicielle

* d'un installateur

## Red Hat Enterprise Linux

La société [Red Hat](https://www.redhat.com/fr) a été fondée en 1993. C'est
actuellement la marque la plus reconnue dans le monde de l'Open Source. Red Hat
est le leader de Linux en entreprise.

[Red Hat Enterprise Linux](https://red.ht/2Q8XXOq) est une distribution
commerciale de qualité entreprise. Nous verrons un peu plus loin ce que cela
signifie concrètement. Le modèle économique de Red Hat, c'est la souscription.
Autrement dit, le client paye pour avoir accès au support technique de Red Hat.
En dehors de cela, Red Hat respecte scrupuleusement les règles du logiciel
libre en publiant l'ensemble des codes source de ses systèmes.

## Oracle Linux

[Oracle Linux](http://yum.oracle.com) est un clone parfait de la distribution
Red Hat Enterprise Linux.  Les ingénieurs de la société
[Oracle](https://bit.ly/3txOLBy) ont utilisé les sources de Red Hat et les ont
recompilées à leur propre sauce, en prenant soin de remplacer les logos, les
icônes et les fonds d'écran spécifiques à Red Hat par ceux d'Oracle. Cette
opération est tout à fait légale, étant donné que les sources de tous les
composants des systèmes Red Hat sont placées sous licence libre. 

Oracle Linux est donc un système techniquement identique et binairement
compatible à Red Hat Enterprise Linux. Tout comme Red Hat, Oracle fournit un
support technique payant pour les clients qui le souhaitent. 

## La qualité entreprise

En règle générale, vous pouvez utiliser votre système Linux de façon sûre tant
que vous disposez de mises à jour. Une fois que la période de support de votre
version a expiré, vous devez mettre à jour l'ensemble de la distribution vers
une version plus récente.

Imaginons maintenant que votre entreprise héberge son site de e-commerce sur un
serveur Linux. Une faille de sécurité importante vient d'être découverte sur un
des composants, et l'administrateur décide de mettre à jour le serveur.
Malheureusement, l'application ne semble plus compatible avec certains des
nouveaux composants. Le site ne fonctionne plus correctement, et il faut songer
à revoir d'urgence l'intégralité du code pour l'adapter à la nouvelle version.
C'est le scénario catastrophe.

Et c'est là où les distributions de qualité entreprise entrent en jeu. Leur
ambition est de fournir une plate-forme robuste, stable et pérenne pour faire
tourner des applications sans causer de problèmes de compatibilité. Les deux
principes de base d'une telle distribution sont donc&nbsp;:

* l'extension de la durée de support

* la mise à disposition de mises à jour peu risquées

En pratique, pendant une période d'au moins cinq ans, parfois même beaucoup
plus, un tel système bénéficiera de mises à jour de sécurité sans que celles-ci
introduisent de nouvelles fonctionnalités susceptibles de causer des mauvaises
surprises.

Les "grandes" distributions commerciales affichent cette qualité entreprise
dans leur nom même&nbsp;:

* Red Hat Enterprise Linux

* Red Hat Enterprise Workstation

* SUSE Linux Enterprise Server

* SUSE Linux Enterprise Desktop

Chacun de ces produits bénéficie en effet d'une période de support étendu
comprise entre sept et dix ans. En comparaison, la durée du système
communautaire Fedora est limitée à treize mois, ce qui est bien trop court pour
un usage en entreprise.

Tout comme Red Hat Enterprise Linux en amont, chaque version d'Oracle Linux
bénéficie d'un cycle de support de dix ans pour chaque version.

* Oracle Linux 7 est maintenu jusqu'au 30 juin 2024.

* Oracle Linux 8 est maintenu jusqu'au 31 mai 2029.

## Le choix d'Oracle Linux

Tous les ateliers pratiques de cette formation sont basés sur Oracle Linux 7,
une distribution dérivée de Red Hat Enterprise Linux 7. D'après mon expérience,
les formations qui essaient de danser à toutes les noces (comme on dit dans mon
pays natal) finissent par embrouiller les novices. En règle générale, je
conseille à mes stagiaires de s'initier à Linux (ou Unix) en optant pour un
seul système – Oracle Linux en l'occurrence – et de voir les autres, comme
Debian, Ubuntu ou FreeBSD, par la suite.

Oracle Linux est probablement le système Linux le plus ennuyeux qui existe. En
informatique, "ennuyeux" est une qualité très recherchée. Pas de drame, pas
de mauvaises surprises, juste un système qui fonctionne au quotidien. La
distribution Oracle Linux n'inclut pas les technologies dernier cri tout juste aptes
à faire exploser vos systèmes en plein vol. Au lieu de cela, l'ambition de
cette distribution – et de sa jumelle commerciale Red Hat Enterprise Linux –
consiste à fournir le système Open Source le plus stable et le plus fiable qui
existe, et ce pour une durée de dix ans pour chaque version. Il n'est donc pas
étonnant que ces deux distributions soient omniprésentes dans les entreprises
et les *datacenters* du monde entier.

## Pour en savoir plus

* Lisez le [premier chapitre](https://bit.ly/32qhVqr) de mon ouvrage
  *Administration Linux par la pratique* qui raconte en détail l'histoire des
  systèmes Unix et Linux. 

* Cette introduction à Linux est également disponible sous forme d'une [série
  de vidéos](https://bit.ly/3mZWyW6).

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


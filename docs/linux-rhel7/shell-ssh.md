
**Objectif** : se connecter localement ou à distance à un serveur Linux.

Pour administrer un serveur Linux, vous devez tout d'abord vous retrouver face
à une "invite de commande" (en anglais&nbsp;: *command prompt*), c'est-à-dire
quelque chose qui ressemble vaguement à ceci&nbsp;:

```
[microlinux@linuxbox ~]$
```

Si nous nous sommes connectés en tant que `root`, cette invite se présente un
peu différemment&nbsp;:

```
[root@linuxbox ~]#
```

## Se connecter à un serveur Linux en SSH

Le système SSH (*Secure Shell*) est un protocole sécurisé de connexion à
distance développé par l'équipe d'OpenBSD depuis 1999. Dans le cadre d'une
formation en entreprise, il est pratique de pouvoir se connecter d'emblée sur
un serveur Linux dans le réseau. 

### Votre machine virtuelle est une machine distante

À partir du moment où vous avez correctement configuré l'accès par pont comme
mode d'accès réseau de votre machine virtuelle dans VirtualBox, votre système
virtualisé se comporte comme une machine à part entière de votre réseau. Vous
pouvez donc afficher son adresse IP et vous connecter en SSH comme s'il
s'agissait d'une "vraie" machine physique distante. 

### Quelle est l'adresse IP de mon serveur ?

Vous aurez besoin de connaître l'adresse IP de votre serveur Linux pour vous
connecter à distance. La commande `ip --brief --family inet addr` affiche de
manière succincte la configuration IPv4 de vos interfaces réseau.

```
[microlinux@linuxbox ~]$ ip --brief --family inet addr
lo               UNKNOWN        127.0.0.1/8
enp0s3           UP             192.168.2.10/24
```

* Ignorez l'interface `lo`, il s'agit de votre boucle locale.

* Ici, l'adresse IP de la machine est `192.168.2.10`.

### Utiliser PuTTY sous Windows

[PuTTY](https://www.putty.org/) est un client SSH libre pour Windows, écrit par
Simon Tatham et publié sous licence MIT. Téléchargez-le sur la page du projet,
installez-le et lancez-le. 

Pour ouvrir une session distante avec PuTTY, je dois lui fournir quelques
paramètres de base comme le nom d'hôte ou l'adresse IP, le port et le type de
connexion. Voici un exemple dans mon réseau local.

* **Host Name** : `linuxbox.microlinux.lan`

* **IP address** : `192.168.2.10` (si la machine n'est pas joignable par nom
  d'hôte)

* **Port** : *22*

* **Connection Type** : *SSH*

* Cliquer sur **Open**.

* Lors de la première connexion, faire confiance à l'identité du serveur
  distant.

### Utiliser OpenSSH sous Windows

Depuis la mise à jour de septembre 2017 de Windows 10, Microsoft a enfin
intégré un support natif de SSH sur Windows. 

Le client OpenSSH n'est peut-être pas activé dans la configuration par défaut.
Avant de s'en servir, il faut donc l'activer via les **Paramètres Windows** >
**Applications** > **Gérer les fonctionnalités facultatives** > **Ajouter une
fonctionnalité**. Installez **OpenSSH Client** et redémarrez votre PC. 

Depuis l'invite de commandes Windows, utilisez la commande `ssh` pour vous
connecter à votre serveur Linux. Pour me connecter en tant que `microlinux` sur
la machine `linuxbox.microlinux.lan`, j'utilise la syntaxe suivante&nbsp;:

```
C:\Users\Microlinux> ssh microlinux@linuxbox.microlinux.lan
```

Si la machine n'est pas joignable par nom d'hôte, je peux toujours fournir son adresse IP en argument&nbsp;:

```
C:\Users\Microlinux> ssh microlinux@192.168.2.10
```

Et si je souhaite me connecter directement en tant que `root`&nbsp;:

```
C:\Users\Microlinux> ssh root@linuxbox.microlinux.lan
```

### Utiliser OpenSSH sous macOS

Sous sa couche graphique Aqua, macOS est constitué d'un système de base
FreeBSD, un Unix libre datant de 1993. Il n'est donc pas étonnant de constater
que le client OpenSSH fait partie des technologies de base de cet OS. 

Pour nous connecter au serveur Linux depuis un poste de travail tournant sous
macOS, nous lancerons d'abord l'application `Terminal` dans le sous-dossier
`Utilitaires` du dossier `Applications`. 

Pour me connecter en tant qu'utilisateur `microlinux` sur le serveur, j'invoque
la commande suivante&nbsp;:

```
$ ssh microlinux@linuxbox.microlinux.lan 
```

Pour fournir l'adresse IP plutôt que le nom d'hôte, la syntaxe sera la suivante&nbsp;:

```
$ ssh microlinux@192.168.2.10
```

Et pour me connecter en tant qu'administrateur `root`, la commande ressemblera à ceci&nbsp;:

```
$ ssh root@linuxbox.microlinux.lan
```

### Utiliser OpenSSH sous Linux ou BSD

Tous les postes de travail Linux et BSD courants et moins courants intègrent un
émulateur de terminal et un client SSH dans leur configuration de base. Pour
vous connecter à votre serveur depuis un poste de travail sous Linux, ouvrez
n'importe quel émulateur de terminal comme GNOME Terminal, Konsole, Xfce
Terminal, MATE Terminal Emulator, Eterm ou Xterm et invoquez la commande
`ssh`&nbsp;:

```
$ ssh microlinux@linuxbox.microlinux.lan
```

Ou :

```
$ ssh microlinux@192.168.2.10
```

Ou encore :

```
$ ssh root@linuxbox.microlinux.lan
```

## Basculer entre les consoles virtuelles

Si vous êtes connecté **physiquement** au serveur – autrement dit , si vous
n'avez pas ouvert une session distante – vous pouvez basculer entre les
consoles virtuelles. 

La commande `tty` affiche le nom du terminal associé à l'entrée standard.

```
[microlinux@linuxbox ~]$ tty
/dev/tty1
```

Utilisez le raccourci clavier ++alt+f2++ pour basculer vers la deuxième console
virtuelle. Connectez-vous et invoquez la commande `tty`&nbsp;:

```
Oracle Linux Server 7.9
Kernel 3.10.0-1160.el7.x86_64 on an x86_64

linuxbox login: microlinux
Password: ********
[microlinux@linuxbox ~]$ tty
/dev/tty2
```

Dans la configuration par défaut, Oracle Linux dispose de six consoles
virtuelles auxquelles vous accédez par le raccourci clavier ++alt+f1++,
++alt+f2++ et ainsi de suite jusqu'à ++alt+f6++.

Basculez vers la troisième console virtuelle (++alt+f3++), connectez-vous en
tant que `root` et, là aussi, invoquez `tty`. Puis revenez vers la première
console avec ++alt+f1++.

> Le terme "terminal" est issu de l'ère préhistorique de l'informatique, où les
> ordinateurs personnels n'existaient pas encore. Pour se connecter à l'un de
> ces ordinateurs ancestraux, il fallait s'y connecter par le biais d'un
> terminal, c'est-à-dire un bloc "stupide" – comprenez&nbsp;: sans la moindre
> puissance de calcul incorporée – composé uniquement d'un clavier et d'un
> écran, ainsi que d'un câble le reliant à l'ordinateur central. 

## Quitter la console

Pour fermer la session et revenir à l'invite de connexion, invoquez la commande
suivante&nbsp;:

```
$ logout
```

Alternativement, vous pouvez également vous déconnecter comme ceci&nbsp;:

```
$ exit
```

Enfin, le raccourci clavier ++ctrl+d++ permet de faire la même chose plus
rapidement.

## Premiers pas en ligne de commande

Commencez par taper n'importe quoi et regardez la réaction de votre système.
Par exemple&nbsp;:

```
[microlinux@linuxbox ~]$ make love
make: *** Aucune règle pour fabriquer la cible « love ». Arrêt.
[microlinux@linuxbox ~]$ 
```

Vous venez de taper une commande au hasard tout en choisissant bien, et vous
vous retrouvez face à votre premier message d'erreur. Mais que s'est-il passé
exactement ?

* L'interpréteur de commandes vous a affiché une invite `[microlinux@linuxbox
  ~]$`. Il vous a ainsi signifié qu'il était prêt à recevoir une ou plusieurs
  commandes.

* Vous avez tapé une commande : `make`.

* Vous avez fait suivre la commande `make` d'un argument : `love`.

* L'interpréteur a essayé en vain d'exécuter ce que vous lui avez demandé de
  faire et vous a dit plus ou moins clairement ce qu'il en pense, en
  l'occurrence : `make: *** Aucune règle pour fabriquer la cible « love ».
  Arrêt`.

* L'interpréteur de commandes vous affiche à nouveau l'invite, pour vous
  indiquer qu'il est prêt à recevoir une ou plusieurs commandes.

À présent, nous n'avons qu'à essayer avec des commandes qui ont un peu plus de
sens pour votre machine.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

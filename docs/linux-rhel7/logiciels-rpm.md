
**Objectif**&nbsp;: Installer et gérer des logiciels sur un système Linux de la
famille Red Hat avec le gestionnaire de paquets RPM.

**Système**&nbsp;: Oracle Linux 7.x.

## Anatomie d'un paquet RPM

Nous avons vu que la distribution de logiciels [par le biais d'archives de code
source](logiciels-source.md) n'était pas très pratique pour les utilisateurs et
les administrateurs.  Cette manière de faire a donc été remplacée par les
systèmes de paquets, parfois aussi appelés paquetages.

En règle générale, un paquet comprend tout ce qui est nécessaire pour faire
fonctionner un logiciel&nbsp;:

* les fichiers binaires précompilés&nbsp;;

* les informations de dépendances&nbsp;;

* la documentation en ligne&nbsp;;

* les modèles de fichiers de configuration.

Regardons de plus près à quoi ressemblent les paquets logiciels fournis par
Oracle Linux. Pour ce faire, nous pouvons utiliser les dépôts en ligne.
Rendez-vous sur la page d'accueil de la distribution&nbsp;:

```
$ links http://yum.oracle.com
```

Repérez la section `Browse the repositories` et suivez le lien `Oracle Linux
7`. Sur la page subséquente, repérez la section `Latest Packages` et suivez le
lien `Latest` > `x86_64`.

Maintenant, cherchez et affichez successivement les paquets suivants&nbsp;:

* `bash`

* `kernel`

* `openssl`

* `postfix`

Voici ce que vous obtenez&nbsp;:

* `bash-4.2.46-34.el7.x86_64.rpm`

* `kernel-3.10.0-1160.25.1.el7.x86_64.rpm`

* `openssl-1.0.2k-21.0.3.el7.x86_64.rpm`

* `postfix-2.10.1-9.el7.x86_64.rpm`

Le nom de chaque paquet fournit une série d'informations&nbsp;:

* le nom de l'application ou du paquet&nbsp;: `bash`, `kernel`, `openssl`,
  `postfix`&nbsp;;

* la version&nbsp;: `4.2.46`, `3.10.0`, `1.0.2k`, `2.10.1`&nbsp;;

* le numéro de *build*&nbsp;: `34`, `1160.25.1`, `21.0.3`, `9`&nbsp;;

* la version de RHEL/Oracle Linux&nbsp;: `el7`&nbsp;;

* l'architecture du paquet&nbsp;: `x86_64`&nbsp;;

* le suffixe `.rpm`.

## Le gestionnaire de paquets RPM

Le premier outil que nous utiliserons, c'est l'ancêtre RPM de Red Hat (*RPM
Package Manager*). RPM est un gestionnaire de paquets en ligne de commande
capable d'installer, de supprimer et de mettre à jour des paquets. Il permet
également d'effectuer des requêtes sur les paquets ou d'en vérifier
l'intégrité.

> RPM a été utilisé pour la première fois en 1995 par la société Red Hat pour
> sa distribution. Par la suite, il a été adopté par d'autres distributions,
> telles que SUSE, Mandriva ou ALT&nbsp;Linux.

RPM est un outil puissant, qui souffre cependant d'une réputation quelque peu
sulfureuse. Au premier abord, son utilisation paraît encombrante et peu
flexible. D'ailleurs, les distributions basées sur RPM disposent toutes au
moins d'un autre gestionnaire un peu plus maniable : Yum (que nous verrons un
peu plus loin) pour Red Hat Enterprise Linux et Oracle Linux, DNF pour Fedora,
Zypper pour OpenSUSE. Ces outils ne viennent pas pour autant remplacer RPM. Ils
peuvent très bien être utilisés conjointement&nbsp;; c'est d'ailleurs ce que
nous ferons par la suite. Les options de RPM sont nombreuses et je ne vous
présenterai ici que les commandes les plus usuelles. Considérez-les comme un
bagage de départ pour fonctionner au quotidien.

## Trouver des paquets RPM

Créez un répertoire `~/RPMS` qui servira à récupérer les paquets RPM depuis le
dépôt de téléchargement&nbsp;:

```
$ mkdir -v ~/RPMS
mkdir: created directory ‘/home/microlinux/RPMS’
$ cd ~/RPMS
$ links http://yum.oracle.com
```

Là encore, naviguez vers la section `Browse the repositories`, suivez le lien
`Oracle Linux 7`, repérez la section `Latest Packages` et suivez le lien
`Latest` > `x86_64`.

Pour commencer, récupérez les deux paquets suivants :

* `nano-2.3.1-10.el7.x86_64.rpm`

* `net-tools-2.0-0.25.20131004git.el7.x86_64.rpm`

## Installer un paquet RPM

Installez le paquet `nano` grâce à l'option `-i`. Songez à utiliser la complétion
automatique pour éviter d'avoir à taper le nom à rallonge du paquet avec toutes
les informations de version&nbsp;:

```
$ sudo rpm -i nano-2.3.1-10.el7.x86_64.rpm
```

Et voilà&nbsp;! C'est fait. L'éditeur Nano est désormais disponible sur votre
machine.

```
$ man nano
$ nano
```

## Supprimer un paquet RPM

Pour supprimer une application installée, invoquez `rpm` avec l'option `-e`. Le
simple nom du paquet en argument est suffisant&nbsp;:

```
$ sudo rpm -e nano
```

> RPM est assez laconique pour l'installation et la suppression des paquets. Il
> s'exécute et c'est tout. Contrairement à un système comme Windows, la
> suppression d'un logiciel sous Linux ne laisse pas de traces sur le système,
> si ce n'est les données produites avec le logiciel en question. Autrement
> dit, vous pouvez joyeusement installer et désinstaller autant de programmes
> que vous voulez, et même autant de fois que vous voulez, cela n'entraînera en
> aucun cas un quelconque ralentissement du système à moyen ou à long terme.

## Mode bavard ou laconique ?

L'installation peut s'effectuer en mode "bavard"&nbsp;:

```
$ sudo rpm -ivh net-tools-2.0-0.25.20131004git.el7.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:net-tools-2.0-0.25.20131004git.el################################# [100%]
```

L'option `-v` (*verbose*) rend RPM plus loquace et l'option `-h` se charge de
dessiner les barres de progression avec le signe dièse.

Il en va de même pour la suppression d'un paquet&nbsp;:

```
$ sudo rpm -ev net-tools
Preparing packages...
net-tools-2.0-0.25.20131004git.el7.x86_64
```

J'en profite pour attirer votre attention sur l'option `--test`, que j'utilise
de temps en temps. Elle permet de tester l'installation avant d'installer quoi
que ce soit et peut même être invoquée sans les droits d'administrateur&nbsp;:

```
$ rpm -ivh --test nano-2.3.1-10.el7.x86_64.rpm
Preparing...                          ################################# [100%]
$ rpm -ivh --test net-tools-2.0-0.25.20131004git.el7.x86_64.rpm
Preparing...                          ################################# [100%]
```

## Gérer les dépendances entre les paquets RPM

La compilation de `tcpdump` et de la bibliothèque `libpcap` [depuis le code
source](logiciels-source.md) nous a sensibilisés pour la gestion des
dépendances.

Récupérons le paquet binaire `tcpdump-4.9.2-4.0.1.el7_7.1.x86_64.rpm` depuis le
dépôt de téléchargement et voyons ce que donnerait une tentative
d'installation&nbsp;:

```
$ rpm -ivh --test tcpdump-4.9.2-4.0.1.el7_7.1.x86_64.rpm
error: Failed dependencies:
        libpcap >= 14:1.5.3-10 is needed by tcpdump-14:4.9.2-4.0.1.el7_7.1.x86_64
        libpcap.so.1()(64bit) is needed by tcpdump-14:4.9.2-4.0.1.el7_7.1.x86_64
```

Je relance Links et je refais un tour sur les archives en ligne pour récupérer
le paquet `libpcap-1.5.3-12.el7.x86_64.rpm`.

> Notez la différence entre les paquets 64-bits (`x86_64`) et 32-bits (`i686`). 

Je teste et j'installe `libpcap`&nbsp;:

```
$ rpm -ivh --test libpcap-1.5.3-12.el7.x86_64.rpm
Preparing...                          ################################# [100%]
$ sudo rpm -ivh libpcap-1.5.3-12.el7.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:libpcap-14:1.5.3-12.el7          ################################# [100%]
```

Maintenant que la dépendance est satisfaite, je peux installer `tcpdump`&nbsp;:

```
$ rpm -ivh --test tcpdump-4.9.2-4.0.1.el7_7.1.x86_64.rpm
Preparing...                          ################################# [100%]
$ sudo rpm -ivh tcpdump-4.9.2-4.0.1.el7_7.1.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:tcpdump-14:4.9.2-4.0.1.el7_7.1   ################################# [100%]
```

Essayons d'installer une autre application pour nous familiariser avec la
gestion des dépendances. Nmap est un scanner de ports conçu pour obtenir des
informations sur les ordinateurs distants. Récupérez le paquet `nmap` et
essayez de l'installer&nbsp;:

```
$ rpm -ivh --test nmap-6.40-19.el7.x86_64.rpm
error: Failed dependencies:
        nmap-ncat = 2:6.40-19.el7 is needed by nmap-2:6.40-19.el7.x86_64
```

Récupérons le paquet `nmap-ncat` et installons-le&nbsp;:

```
$ rpm -ivh --test nmap-ncat-6.40-19.el7.x86_64.rpm
Preparing...                          ################################# [100%]
$ sudo rpm -ivh nmap-ncat-6.40-19.el7.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:nmap-ncat-2:6.40-19.el7          ################################# [100%]
```

Apparemment, l'installation de ce paquet a satisfait la dépendance manquante :

```
$ rpm -ivh --test nmap-6.40-19.el7.x86_64.rpm 
Preparing...                          ################################# [100%]
$ sudo rpm -ivh nmap-6.40-19.el7.x86_64.rpm 
Preparing...                          ################################# [100%]
Updating / installing...
   1:nmap-2:6.40-19.el7               ################################# [100%]
```

## Mettre à jour des paquets avec RPM

Si notre installation d'Oracle Linux est raisonnablement récente, il y a des
chances à ce que notre système soit à jour. Pour montrer le fonctionnement de
la mise à jour manuelle, je vais créer le contexte nécessaire en installant une
poignée de paquets obsolètes. Dans un premier temps, je vais supprimer les
paquets `net-tools`, `nmap` et `nmap-ncat`.

```
$ sudo rpm -ev net-tools
Preparing packages...
net-tools-2.0-0.25.20131004git.el7.x86_64
$ sudo rpm -ev nmap
Preparing packages...
nmap-2:6.40-19.el7.x86_64
$ sudo rpm -ev nmap-ncat
Preparing packages...
nmap-ncat-2:6.40-19.el7.x86_64
```

Je crée un répertoire `~/obsolete` et je récupère des versions un peu anciennes
de ces trois paquets`&nbsp;:

```
$ mkdir -v ~/obsolete
mkdir: created directory ‘/home/microlinux/obsolete’
$ cd ~/obsolete/
$ links http://yum.oracle.com
...
$ ls -1
net-tools-2.0-0.22.20131004git.el7.x86_64.rpm
nmap-6.40-13.el7.x86_64.rpm
nmap-ncat-6.40-13.el7.x86_64.rpm
```

J'installe ces trois paquets "à la louche"&nbsp;:

```
$ sudo rpm -ivh *.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:nmap-ncat-2:6.40-13.el7          ################################# [ 33%]
   2:nmap-2:6.40-13.el7               ################################# [ 67%]
   3:net-tools-2.0-0.22.20131004git.el################################# [100%]
```

À présent je peux me rendre dans le répertoire `~/RPMS` et tenter la mise à
jour de ces trois composants du système. 

La mise à jour se fait avec l'option `-U` comme *update*. Essayez d'abord avec
le paquet `net-tools`&nbsp;:

```
$ rpm -Uvh --test net-tools-2.0-0.25.20131004git.el7.x86_64.rpm
Preparing...                          ################################# [100%]
$ sudo rpm -Uvh net-tools-2.0-0.25.20131004git.el7.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:net-tools-2.0-0.25.20131004git.el################################# [ 50%]
Cleaning up / removing...
   2:net-tools-2.0-0.22.20131004git.el################################# [100%]
```

La mise à jour de paquets avec RPM nous confronte parfois à un problème de
dépendances un peu particulier. Concrètement, la tentative d'actualisation du
paquet `nmap` échoue parce que ce dernier a manifestement besoin d'une version
plus récente de la dépendance `nmap-ncat`&nbsp;:

```
$ rpm -Uvh --test nmap-6.40-19.el7.x86_64.rpm
error: Failed dependencies:
  nmap-ncat = 2:6.40-19.el7 is needed by nmap-2:6.40-19.el7.x86_64
```

Or, si nous essayons de mettre à jour `nmap-ncat`, le gestionnaire nous informe
qu'il constitue une dépendance pour le paquet `nmap` dans la version
installée&nbsp;:

```
$ rpm -Uvh --test nmap-ncat-6.40-19.el7.x86_64.rpm
error: Failed dependencies:
  nmap-ncat = 2:6.40-13.el7 is needed by (installed) nmap-2:6.40-13.el7.x86_64
```

Pour sortir de ce cercle vicieux, on peut effectuer la mise à jour des deux
paquets "à la louche"&nbsp;:

```
$ rpm -Uvh --test nmap-6.40-19.el7.x86_64.rpm nmap-ncat-6.40-19.el7.x86_64.rpm
Preparing...                          ################################# [100%]
$ sudo rpm -Uvh nmap-6.40-19.el7.x86_64.rpm nmap-ncat-6.40-19.el7.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:nmap-ncat-2:6.40-19.el7          ################################# [ 25%]
   2:nmap-2:6.40-19.el7               ################################# [ 50%]
Cleaning up / removing...
   3:nmap-2:6.40-13.el7               ################################# [ 75%]
   4:nmap-ncat-2:6.40-13.el7          ################################# [100%]
```


> Si une mise à jour est disponible pour un paquet, c'est qu'elle corrige très
> probablement un *bug* ou une faille de sécurité. Vous avez donc tout intérêt
> à garder votre système à jour.

## La suppression des paquets RPM revisitée

Un peu plus haut, nous avons vu brièvement comment supprimer un seul paquet
RPM. Pour désinstaller plusieurs programmes à la fois, utilisez simplement une
liste d'éléments séparés par des espaces. Là encore, vous pouvez très bien
tester le bon déroulement de l'opération avant de vous lancer&nbsp;:

```
$ rpm -ev --test firewalld chrony
Preparing packages...
$ sudo rpm -ev firewalld chrony
Preparing packages...
firewalld-0.6.3-13.0.1.el7_9.noarch
chrony-3.4-1.0.1.el7.x86_64
```

Un paquet peut être nécessaire pour le bon fonctionnement d'un autre :

```
$ rpm -ev --test nmap-ncat
error: Failed dependencies:
  nmap-ncat = 2:6.40-19.el7 is needed by (installed) nmap-2:6.40-19.el7.x86_64
```

Pour supprimer un paquet sans vérifier les dépendances, on peut éventuellement
utiliser l'option `--nodeps`. En temps normal, il vaut mieux l'éviter, étant
donné qu'elle va très certainement casser la cohérence de votre système. Quoi
qu'il en soit, utilisez-la en connaissance de cause&nbsp;:

```
$ sudo rpm -ev --nodeps nmap-ncat
Preparing packages...
nmap-ncat-2:6.40-19.el7.x86_64
```

## Obtenir des informations sur les paquets RPM

### Sur les paquets installés

Les fonctions de requête de RPM commencent par l'option `-q` comme *query*.
Ici, je vérifie si le paquet `vim-minimal` est installé sur mon système&nbsp;:

```
$ rpm -q vim-minimal
vim-minimal-7.4.629-8.0.1.el7_9.x86_64
```

Maintenant, j'affiche la liste complète des paquets installés :

```
$ rpm -qa
...
```

Cette dernière commande résulte en un joyeux défilement de quelques centaines
de paquets en vrac. On va donc mettre un peu d'ordre là-dedans. Si vous
préférez une liste classée par ordre alphabétique, tapez&nbsp;:

```
$ rpm -qa | sort
```

Et, puisque le résultat dépasse de loin la taille d'un écran&nbsp;:

```
$ rpm -qa | sort | less
acl-2.2.51-15.el7.x86_64
aic94xx-firmware-30-6.el7.noarch
alsa-firmware-1.0.28-2.el7.noarch
alsa-lib-1.1.8-1.el7.x86_64
alsa-tools-firmware-1.1.0-1.el7.x86_64
audit-2.8.5-4.el7.x86_64
audit-libs-2.8.5-4.el7.x86_64
authconfig-6.2.8-30.el7.x86_64
basesystem-10.0-7.0.1.el7.noarch
bash-4.2.46-34.el7.x86_64
...
```

Pour effectuer une recherche insensible à la casse, avec un nom de paquet
partiel&nbsp;:

```
$ rpm -qa | grep -i grub
grub2-tools-2.02-0.87.0.9.el7_9.6.x86_64
grub2-tools-extra-2.02-0.87.0.9.el7_9.6.x86_64
grub2-common-2.02-0.87.0.9.el7_9.6.noarch
grub2-pc-modules-2.02-0.87.0.9.el7_9.6.noarch
grub2-2.02-0.87.0.9.el7_9.6.x86_64
grubby-8.28-26.0.3.el7.x86_64
grub2-pc-2.02-0.87.0.9.el7_9.6.x86_64
grub2-tools-minimal-2.02-0.87.0.9.el7_9.6.x86_64
```

L'option `--last` affiche la liste de tous les paquets RPM installés par ordre
décroissant de date d'installation&nbsp;:

```
$ rpm -qa --last | less
```

Pour afficher la liste des cinq derniers paquets installés, vous pourrez donc
taper ceci&nbsp;:

```
$ rpm -qa --last | head -n 5
nmap-ncat-6.40-19.el7.x86_64                  Thu 10 Jun 2021 10:54:34 AM CEST
nmap-6.40-19.el7.x86_64                       Thu 10 Jun 2021 10:24:23 AM CEST
net-tools-2.0-0.25.20131004git.el7.x86_64     Thu 10 Jun 2021 10:20:02 AM CEST
tcpdump-4.9.2-4.0.1.el7_7.1.x86_64            Thu 10 Jun 2021 08:56:28 AM CEST
libpcap-1.5.3-12.el7.x86_64                   Thu 10 Jun 2021 08:55:33 AM CEST
```

Certaines options nous permettent d'examiner de plus près le contenu proprement
dit d'un paquet installé. Pour afficher la liste des fichiers installés par un
paquet, par exemple, saisissez la commande suivante&nbsp;:

```
$ rpm -ql tcpdump
/usr/sbin/tcpdump
/usr/sbin/tcpslice
/usr/share/doc/tcpdump-4.9.2
/usr/share/doc/tcpdump-4.9.2/CHANGES
/usr/share/doc/tcpdump-4.9.2/CREDITS
/usr/share/doc/tcpdump-4.9.2/LICENSE
/usr/share/doc/tcpdump-4.9.2/README.md
/usr/share/man/man8/tcpdump.8.gz
/usr/share/man/man8/tcpslice.8.gz
```

Pour afficher la liste des fichiers de documentation d'un paquet
installé&nbsp;:

```
$ rpm -qd vim-minimal
/usr/share/man/man1/ex.1.gz
/usr/share/man/man1/rvi.1.gz
/usr/share/man/man1/rview.1.gz
/usr/share/man/man1/vi.1.gz
/usr/share/man/man1/view.1.gz
/usr/share/man/man1/vim.1.gz
/usr/share/man/man5/virc.5.gz
```

Et pour en savoir plus sur les fichiers de configuration d'un paquet
installé&nbsp;:

```
$ rpm -qc openssh-clients
/etc/ssh/ssh_config
```

L'option `-f` indique à quel paquet appartient un fichier&nbsp;:

```
$ rpm -qf /etc/yum.conf
yum-3.4.3-168.0.3.el7.noarch
```

Pour afficher les informations générales concernant un paquet :

```
$ rpm -qi tcpdump
Name        : tcpdump
Epoch       : 14
Version     : 4.9.2
Release     : 4.0.1.el7_7.1
Architecture: x86_64
...
URL         : http://www.tcpdump.org
Summary     : A network traffic monitoring tool
Description :
Tcpdump is a command-line tool for monitoring network traffic.
Tcpdump can capture and display the packet headers on a particular
network interface or on all interfaces.  Tcpdump can display all of
the packet headers, or just the ones that match particular criteria.

Install tcpdump if you need a program to monitor network traffic.
```

Ces options de requête sont combinables à souhait, comme le montrent les
exemples suivants. Pour afficher les informations à propos d'un fichier ou
d'une commande&nbsp;:

```
$ rpm -qif /etc/virc
Name        : vim-minimal
Epoch       : 2
Version     : 7.4.629
Release     : 8.0.1.el7_9
Architecture: x86_64
...
```

Pour afficher la liste des fichiers de configuration d'une commande&nbsp;:

```
$ rpm -qcf /usr/bin/ssh
/etc/ssh/ssh_config
```

Et pour en savoir plus sur les fichiers de documentation d'une commande :

```
$ rpm -qdf /usr/bin/yum
/usr/share/doc/yum-3.4.3/AUTHORS
/usr/share/doc/yum-3.4.3/COPYING
/usr/share/doc/yum-3.4.3/ChangeLog
/usr/share/doc/yum-3.4.3/INSTALL
/usr/share/doc/yum-3.4.3/PLUGINS
/usr/share/doc/yum-3.4.3/README
/usr/share/doc/yum-3.4.3/TODO
/usr/share/doc/yum-3.4.3/comps.rng
/usr/share/man/man5/yum.conf.5
/usr/share/man/man8/yum-shell.8
/usr/share/man/man8/yum.8
```

### Sur des paquets non installés

Les options de requête présentées jusqu'ici ne concernaient que les paquets
installés. L'option `-p` effectue des requêtes sur des paquets RPM non installés.
Ainsi, pour afficher la liste des fichiers contenus dans un paquet non
installé, on utilisera&nbsp;:

```
$ rpm -qpl net-tools-2.0-0.25.20131004git.el7.x86_64.rpm
/bin/netstat
/sbin/arp
/sbin/ether-wake
/sbin/ifconfig
/sbin/ipmaddr
/sbin/iptunnel
/sbin/mii-diag
/sbin/mii-tool
/sbin/nameif
/sbin/plipconfig
/sbin/route
/sbin/slattach
...
```

Là aussi, on pourra limiter les requêtes aux fichiers de configuration (`-qpc`)
ou de documentation (`-qpd`).

## Avantages et inconvénients d'une installation avec RPM

L'installation et la maintenance de paquets binaires avec l'outil RPM présente
indéniablement une série d'avantages par rapport à la compilation depuis le
code source.

* L'installation d'un paquet binaire est plus rapide et évite de "spammer" le
  système avec des fichiers éparpillés un peu partout.

* La suppression d'un paquet RPM se fait proprement.

* La mise à jour d'un paquet s'effectue en une simple commande.

Tout cela n'empêche que RPM reste un outil obtus à utiliser au quotidien.

* Il gère les dépendances, mais il ne vous aide pas à les résoudre.
  Concrètement, si RPM rouspète parce qu'il lui manque telle et telle
  dépendance, c'est à vous d'aller dénicher le bon paquet dans la bonne
  version, ce qui n'est pas toujours évident.

* Il arrive assez souvent que les dépendances en question refusent de
  s'installer à leur tour parce qu'elles requerront d'autres dépendances. J'ai
  évité de vous présenter ce cas de figure en détail. Si vous souhaitez en
  avoir une vague idée, essayez d'installer le paquet `vim-enhanced` avec
  l'outil RPM, en partant d'un système minimal.

* Le téléchargement et l'installation des mises à jour restent également une
  procédure laborieuse.

Si j'ai décidé de vous confronter dans un premier temps à une façon quelque peu
archaïque de gérer les logiciels, c'est d'une part pour vous donner une idée un
tout petit peu plus concrète de ce qui peut se passer lorsqu'on installe, met à
jour et supprime un logiciel. D'autre part, RPM n'est pas complètement tombé en
désuétude, loin de là, et tout administrateur système se retrouve tôt ou tard
dans des situations où ce gestionnaire de paquets rudimentaire rend de bons et
loyaux services.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>


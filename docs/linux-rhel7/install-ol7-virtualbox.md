
**Objectif**&nbsp;: installer Oracle Linux 7.9 dans une machine virtuelle sous
VirtualBox.

## Obtenir la distribution Oracle Linux

* Rendez-vous sur la page [Oracle Linux Yum Server](http://yum.oracle.com).

* Suivez les liens [*Get Started
  Here*](http://yum.oracle.com/oracle-linux-downloads.html) > [*ISO
  images*](http://yum.oracle.com/oracle-linux-isos.html).

* Repérez les images ISO de la version 7.9.

* Téléchargez l'image réseau `x86_64-boot.iso` correspondante.

* Éventuellement, renommez le fichier ISO téléchargé pour lui donner un nom
  plus explicite, par exemple `Oracle-Linux-7.9-rhck-x86_64-netinstall.iso`.

## Créer une machine virtuelle

* Démarrez VirtualBox et cliquez sur **Nouvelle** pour créer une nouvelle
  machine virtuelle.

* Définissez le nom (**Oracle Linux 7**), le type (**Linux**) et la version
  (**Oracle 64-bit**) de la machine virtuelle.

* Définissez la quantité de mémoire vive que vous souhaitez allouer à la
  machine virtuelle. L'assistant suggère 1024 Mo (1 Go), ce qui est le minimum
  requis pour que l'installateur d'Oracle Linux fonctionne correctement. Rien
  n'empêche d'allouer davantage de mémoire en fonction de la RAM disponible sur
  le système hôte.

* Créez un disque dur virtuel. Gardez l'option par défaut **Créer un disque
  virtuel maintenant** et cliquez sur **Créer**.

* Gardez le type de fichier de disque dur **VDI** par défaut.

* Faites de même pour le stockage **Dynamiquement alloué**.

* Augmentez la taille du disque dur à **60 Gio**. 

## Configurer la machine virtuelle 

Maintenant que la machine virtuelle est créée, nous allons la configurer. Plus
exactement, nous allons définir ses caractéristiques matérielles.

* Sélectionnez la machine virtuelle dans VirtualBox et cliquez sur le bouton
  **Configuration** en haut à gauche de la fenêtre principale.

* Dans la section **Système**, repérez l'onglet **Processeur** et augmentez
  éventuellement le nombre de processeurs de la machine virtuelle. Vous pouvez
  décocher la case **Activer PAE/NX**, qui ne concerne que les systèmes 32-bits.

* Dans la section **Affichage**, augmentez la quantité de mémoire vidéo au
  maximum et cochez l'option **Activer l'accélération 3D**.

* Dans la section **Stockage**, sélectionnez le champ **Vide** du
  contrôleur IDE, cliquez sur la petite flèche en dessous de l'icône du CD à
  droite de la fenêtre pour déplier le menu du lecteur optique et sélectionnez
  le fichier ISO téléchargé. Ce fichier ISO fait office de disque optique
  virtuel, ce qui nous dispense de confectionner une clé USB ou un CD-Rom
  d'installation.

* Dans la section **Réseau**, remplacez la configuration **NAT** par un **Accès
  par pont** dans le menu déroulant **Mode d'accès réseau** et terminez en
  cliquant sur **OK**.

## Installer Oracle Linux

À présent, mettez la machine virtuelle en surbrillance et cliquez sur le bouton
**Démarrer** en haut de l'écran dans la fenêtre principale de VirtualBox. Au
bout de quelques secondes, vous voyez apparaître l'écran de démarrage de
l'installateur d'Oracle Linux. La différence entre **Test this media & install
Oracle Linux 7** et **Install Oracle Linux 7**, c'est que l'option par défaut
vérifie l'intégrité du support d'installation.

Utilisez l'astuce suivante pour améliorer la résolution de l'affichage&nbsp;:

* Sélectionnez l'entrée de menu **Install Oracle Linux 7**.

* Appuyez sur la touche ++tab++ pour afficher les options de démarrage.

* Gardez à l'esprit que l'écran de démarrage de l'installateur utilise un
  clavier américain QWERTY. 
  
* Ajoutez les options `nomodeset vga=791` juste après l'option `quiet`.

* Confirmez avec la touche ++enter++.

> Pour récupérer le focus de la souris depuis la fenêtre du système virtualisé,
appuyez sur la touche ++ctrl++ droite. 

L'installateur vous somme d'abord de sélectionner les paramètres régionaux du
système avant d'afficher l'écran principal avec toutes les options.

### Choisir la disposition du clavier

Si vous souhaitez utiliser un autre clavier que la disposition proposée,
cliquez sur **Clavier**. Dans l'écran **Agencement du clavier**, sélectionnez
celui qui est proposé et cliquez sur le bouton symbolisant un signe "moins" en
bas à gauche de l'écran. Choisissez votre agencement dans la liste, cliquez sur
**Ajouter**, puis sur **Terminer** pour revenir à l'écran principal de
l'installateur.

### Partitionner le disque dur

Pour cette première installation, nous nous simplifierons la vie et nous
choisirons le partitionnement automatique.

* Cliquez sur **Destination de l'installation**.

* Vérifiez si le disque dur est bien sélectionné.

* Gardez l'option **Configurer automatiquement le partitionnement**.

* Cliquez sur **Terminé**.

L'installateur se chargera de calculer automatiquement le schéma de
partitionnement en fonction de la taille de votre disque dur et de la quantité
de mémoire vive disponible. Il choisira également les systèmes de fichiers
adaptés.

### Désactiver le service Kdump

Kdump est un mécanisme de capture lors du plantage d'un noyau. Ne vous
inquiétez pas trop si vous ne savez pas ce que c'est. Contentez-vous simplement
de le désactiver en décochant la petite case **Activer Kdump** dans la section
**Kdump**, car nous n'en avons pas besoin.

### Activer le réseau et définir le nom d'hôte

Le réseau n'est pas activé par défaut dans la section **Nom d'hôte et réseau**.
Il faut donc songer à l'activer explicitement en cliquant sur le bouton en
forme d'interrupteur en haut à droite de l'écran, ce qui fait passer sa valeur
de **0** à **1**.

Si tout se passe bien, votre serveur DHCP vous attribue vos paramètres
réseau&nbsp;:

* une adresse IP (quelque chose comme `192.168.2.10`)&nbsp;;

* un masque de sous-réseau (quelque chose comme `255.255.255.0`)&nbsp;;

* une route par défaut (quelque chose comme `192.168.2.1`)&nbsp;;

* l'adresse IP d'un serveur DNS (quelque chose comme `192.168.2.1`, mais pas
  forcément la même adresse IP que la route par défaut dans notre exemple).

DHCP signifie *Dynamic Host Configuration Protocol* et désigne un protocole
d'allocation dynamique d'adresses IP. Pour comprendre le principe de
fonctionnement du DHCP, imaginez un cours d'anglais où le professeur décide de
donner des noms typiquement anglais à ses élèves. Pour la durée du cours, tel
élève s'appellera donc Freddy, sa voisine à droite sera Pamela et son voisin à
gauche sera connu sous le nom de Brian. Pour éviter toute confusion, chaque
élève portera un prénom distinct. De façon analogue et en simplifiant un tant
soit peu, le serveur DHCP (comme celui qui est intégré dans le modem/routeur
d'un réseau domestique par exemple) dira à votre PC&nbsp;: "Pour une durée de
32 400 secondes, tu seras la machine 192.168.2.10 dans le réseau."

Pour le nom d'hôte de votre machine, choisissez-en un à votre convenance, en
remplacement de `localhost.localdomain` par défaut&nbsp;: `linuxbox`, `nestor`,
`serveur-linux`, `grossebertha`, etc.

Confirmez le choix du nom d'hôte en cliquant sur **Appliquer**.

### Configurer la source d'installation

Le support d'installation que nous avons téléchargé est assez minimaliste. Il
ne contient que le seul programme d'installation Anaconda. Les paquets du
système Oracle Linux à installer devront être récupérés depuis le réseau. Nous
devons donc configurer un miroir de téléchargement correspondant.

* Cliquez sur **Source d'Installation**.

* Utilisez la source **Sur le réseau** par défaut.

* Renseignez la source&nbsp;: `https://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64`.

* Ne cochez pas **Cette URL se réfère à une liste de miroirs**.

* Cliquez sur **Terminé**.

* Vérifiez si la **Sélection de logiciels** est actualisée correctement.

### Choix des paquets

Pour nos travaux pratiques, nous n'avons besoin que d'une installation minimale
d'Oracle Linux. 

* Ouvrez la section **Sélection de Logiciels**.

* Gardez la sélection **Installation minimale**.

* Cliquez sur **Terminé**.

### Démarrer l'installation

L'écran principal de l'installateur vous affiche une vue d'ensemble des
différents paramètres. S'il estime qu'il lui manque encore une information
vitale, il vous le fait savoir par un point d'exclamation dans un petit
triangle orange.

Une fois que vous avez fourni tous les paramètres, il ne vous reste plus qu'à
**Démarrer l'installation**. L'installateur mouline sous le capot pour
installer Oracle Linux sur le disque dur, mais nous n'avons pas tout à fait
terminé.

### Définir les paramètres de l'administrateur

L'écran **Paramètres utilisateur** nous somme de choisir un mot de passe
administrateur et nous propose de créer un premier utilisateur "commun des
mortels".

Pour simplifier quelque peu les choses, un système Linux fait *grosso modo* la
distinction entre deux types d'utilisateurs :

* Les utilisateurs du "commun des mortels" ont accès à certaines zones du
  système, si l'on peut dire. À condition que leur compte soit configuré
  correctement – nous verrons cela plus loin – ils ont suffisamment de droits
  pour travailler correctement, mais une mauvaise manipulation de leur part ne
  pourra en aucun cas porter atteinte à l'intégrité du système. On peut
  comparer ce cas de figure à une entreprise où chaque employé possède son
  propre casier, son propre bureau avec ses tiroirs qui ferment à clé. Il
  bénéficie de l'infrastructure de l'entreprise et partage une partie de son
  travail s'il le souhaite, mais personne – à l'exception de l'administrateur
  `root` – ne pourra fouiner dans ses affaires personnelles.

* L'administrateur `root`, quant à lui, possède tous les droits sur la machine.
  C'est le vigile avec l'énorme trousseau de clés qui donne accès aux moindres
  recoins du bâtiment.

Linux a une préférence marquée pour les mots de passe compliqués, le genre de
chaîne de caractères que vous obtenez lorsque votre chat marche sur le clavier.
`123456`, `654321` ou le nom du chat en question ne sont pas de bons mots de
passe, à moins que vous n'ayez l'habitude d'appeler votre animal domestique
`GnLpF3th` ou `Wgh8sTr5FgH`. Vous verrez d'ailleurs que l'installateur
protestera si le mot de passe que vous choisissez lui paraît trop simple. Dans
ce cas, vous devrez soit en choisir un autre plus compliqué, soit confirmer par
deux fois.

### Créer un utilisateur 

L'écran de création de l'utilisateur initial vous pose une série de questions.
Rien ne vous oblige de respecter l'ordre **Nom et prénom** dans le premier
champ, et vous pouvez très bien indiquer *Nicolas Kovacs*, *Gaston Lagaffe* ou
*Jean-Kevin Tartempion*.

En fonction de votre saisie initiale, l'installateur vous fera une suggestion
pour le nom d'utilisateur, mais vous n'êtes pas obligé de la suivre. Il existe
une série de règles et de conventions sur les systèmes Linux en ce qui concerne
les noms d'utilisateur.

* Il est interdit d'utiliser les caractères spéciaux et les espaces.

* Préférez les lettres minuscules. C'est une convention et rien ne vous empêche
  d'utiliser les majuscules.

* Un nom d'utilisateur est généralement composé de l'initiale du prénom, suivie
  du nom de famille. Là aussi, c'est une recommandation et vous n'êtes pas
  obligé de vous y tenir.

Si nous respectons ces règles, Gaston Lagaffe utilisera donc le nom
d'utilisateur `glagaffe`, Jacques Martin s'identifiera sur le système en tant
que `jmartin` et le login de Jean-Kevin Tartempion ressemblera à quelque chose
comme `jktartempion`.

Rien ne nous oblige pourtant à être aussi strict dans la définition du nom
d'utilisateur. Kiki Novak pourra préférer `kikinovak` à `knovak`,
Gaston Lagaffe utilisera un simple `gaston` et Jean-Kevin Tartempion favorisera
`warlordz` ou `nemesis`, plus incisifs que `jktartempion`.

Cochez l'option **Faire de cet utilisateur un administrateur**. Nous verrons
plus loin ce qu'elle signifie.

Enfin, choisissez un mot de passe pour cet utilisateur, en respectant les mêmes
règles que celles énoncées un peu plus haut pour celui de `root`.

### Fin de l'installation et redémarrage initial

Au terme de l'installation et de la configuration du système, il ne vous reste
plus qu'à **Redémarrer** la machine.

L'ordinateur redémarre et vous affiche tout d'abord l'écran du chargeur de
démarrage. Le réglage par défaut du système prévoit un temps d'attente de cinq
secondes avant le lancement automatique du système.

Dans la configuration par défaut, Oracle Linux remplace les messages de
démarrage du système par une simple barre de progression horizontale en bas de
l'écran. Pour afficher les messages, appuyez sur la touche ++esc++ sans vous
inquiéter si vous n'y comprenez pas encore grand-chose.

Au terme de l'initialisation du système, vous vous retrouvez confronté à un
message qui ressemble à peu de chose près à ceci :

```
Oracle Linux Server 7.9
Kernel 3.10.0-1160.el7.x86_64 on an x86_64

linuxbox login: _
```

À l'heure actuelle, nous disposons de deux comptes sur notre machine :

* l'administrateur `root` ;

* l'utilisateur commun des mortels (`microlinux` par exemple).

Connectez-vous en tant qu'utilisateur normal. Notez que le mot de passe ne
s'affiche pas sur l'écran.

```
linuxbox login : microlinux
Password : ********
```

Si tout se passe bien, vous vous retrouvez face à l'invite de commande :

```
[microlinux@linuxbox ~]$ _
```

Arrêtez la machine virtuelle : **Fichier** > **Fermer** > **Envoyer le signal
d'extinction** > **OK**.

## Cloner la machine virtuelle

VirtualBox permet de cloner une machine virtuelle en quelques clics. Cette
fonctionnalité peut s'avérer assez pratique dans le cadre d'une formation. 

Lors des ateliers pratiques, il arrive de temps en temps qu'un administrateur
en herbe se tire dans le pied en invoquant une commande fatale à son système.
L'utilisation d'un clone permettra alors de repartir sur des bases saines en
évitant de réinstaller tout le système. 

* Repérez la machine virtuelle dans la fenêtre principale de VirtualBox.

* Assurez-vous qu'elle est bien **Éteinte**.

* Un clic droit sur la machine virtuelle affiche le menu contextuel.

* Cliquez sur **Cloner**.

* Gardez le nom **Clone de Oracle Linux 7** proposé par défaut.

* Dans le menu déroulant **Politique d'adresse MAC**, sélectionnez **Inclure
  toutes les adresses MAC de l'interface réseau**. 

* Cliquez sur **Suivant**.

* Dans la fenêtre **Type de clone**, gardez l'option **Clone intégral** et
  cliquez sur **Cloner**.

* Patientez quelques secondes pendant l'opération de clonage.

* Pour tester le système cloné, mettez **Clone de Oracle Linux 7** en
  surbrillance et cliquez sur le bouton **Démarrer**.

Cette première installation est d'une importance capitale, étant donné qu'elle
servira de base pour la plupart des ateliers pratiques de la formation.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>



**Objectif** : manipuler les principaux formats de fichiers compressés et les
archives sous Linux.

## Les différents formats d'archivage

Les archives compressées sont omniprésentes dans le quotidien informatique et
elles sont utilisées à des fins très variées. Si vous avez l'habitude de
Windows, vous avez certainement déjà croisé des fichiers `.zip` et `.rar` dans
votre quotidien. Ces types de fichiers ne sont pas inconnus à Linux, mais les
deux formats d'archives compressées les plus largement répandus sous ce système
sont les fichiers `.tar.gz` et `.tar.bz2`.  Derrière ces extensions, quelque
peu énigmatiques pour un néophyte se cachent en réalité trois programmes, trois
petits outils.  `tar` rassemble plusieurs fichiers et répertoires en une
archive, `gzip` et `bzip2` se chargent de la compression.

Ces formats sont très bien gérés par les différents systèmes d'exploitation.
Linux sait gérer les archives au format `.zip` et `.rar` grâce aux outils
`unzip` et `unrar`. Inversement, lorsque vous travaillez sous Windows, vous
trouverez des outils libres pour traiter les archives au format `.tar.gz` et
`.tar.bz2`. Dans cet atelier pratique, nous nous concentrerons sur les deux
principaux formats natifs de Linux.

## Compresser et décompresser un fichier

### Compresser et décompresser un fichier avec gzip

`gzip` (GNU Zip) constitue l'outil de compression standard sous Linux. Il fait
une chose et une seule&nbsp;: gérer la compression de fichiers simples. Il ne
sait pas constituer des archives, c'est `tar` qui s'en charge. Cherchons dans
notre système un fichier au hasard, sur lequel nous pourrions nous
entraîner&nbsp;:

```
$ cp -v /etc/services .
‘/etc/services’ -> ‘./services’
[microlinux@linuxbox ~]$ ls -lh services
-rw-r--r--. 1 microlinux microlinux 655K Jun  3 08:39 services
```

L'affichage détaillé nous montre qu'il s'agit d'un fichier texte assez
important en termes de taille : 655 kilo-octets. C'est pour cela que je l'ai
choisi. Essayons de le compacter&nbsp;:

```
$ gzip services
```

`gzip` remplace le fichier d'origine par une version plus compacte, comportant
l'extension de fichier supplémentaire `.gz`&nbsp;:

```
$ ls -lh services.gz
-rw-r--r--. 1 microlinux microlinux 133K Jun  3 08:39 services.gz
```

Dans cet exemple, la compression est assez conséquente&nbsp;: le fichier
résultant est environ cinq fois plus petit que l'original.

> La compression varie selon un certain nombre de facteurs. `gzip` n'aura pas
> beaucoup d'effet sur des formats de fichiers comportant une compression
> initiale, comme le MP3 ou le JPEG. En revanche, il fonctionnera très bien
> avec les fichiers au format texte simple ou les images Bitmap non
> compressées.  

Pour décompresser un fichier `.gz`, vous avez deux possibilités&nbsp;:

```
$ gzip -d services.gz
```

Ou alors, ce qui revient exactement au même&nbsp;:

```
$ gunzip services.gz
```

### Compresser et décompresser un fichier avec bzip2

L'outil `bzip2` est un outil de compression au même titre que `gzip`, au détail
près qu'il utilise un algorithme un peu plus performant.

L'outil `bzip2` ne fait pas partie de notre installation minimale&nbsp;:

```
$ sudo yum install -y bzip2
```

Reprenons l'exemple précédent, avec le fichier `services`&nbsp;:

```
$ bzip2 services
```

Il en résulte un fichier compressé portant l'extension de fichier
supplémentaire `.bz2`. Regardons ce fichier de plus près&nbsp;:

```
$ ls -lh services.bz2
-rw-r--r--. 1 microlinux microlinux 122K Jun  3 08:39 services.bz2
```

Effectivement, le taux de compression est légèrement supérieur à celui proposé
par `gzip`. Le fichier compressé ne pèse plus que 122 kilo-octets, contre 133
avec `gzip`.

Pour décompresser un fichier compacté à l'aide de `bzip2`, nous avons également
le choix entre deux commandes, qui ont exactement le même effet&nbsp;:

```
$ bzip2 -d services.bz2
```

Ou&nbsp;:

```
$ bunzip2 services.bz2
```

## Manipuler les archives avec tar

### Créer une archive avec tar

`gzip` et `bzip2` ne gèrent que la compression d'un seul fichier fourni en
argument. Pour créer une archive, nous aurons recours à la commande `tar`. Là
aussi, un exemple pratique nous aidera à comprendre le fonctionnement de
l'outil en question.

Pour commencer, il nous faut un répertoire contenant quelques fichiers. Je crée
donc, au hasard, un répertoire `~/config` et j'y place tous les fichiers
`*.conf` que je trouve dans l'arborescence de `/etc` en faisant fi des
avertissements relatifs aux droits d'accès&nbsp;:

```
$ mkdir -v ~/config
mkdir: created directory ‘/home/microlinux/config’
$ find /etc -name '*.conf' 2> /dev/null -exec cp {} ~/config/ \;
$ ls ~/config/
00-keyboard.conf         namespace.conf
20-nproc.conf            NetworkManager.conf
99-sysctl.conf           nm-dispatcher.conf
access.conf              nm-ifcfg-rh.conf
...
```
Nous allons rassembler tous ces fichiers contenus dans `~/config` pour en
constituer une archive avec la commande `tar`&nbsp;:

```
$ tar -cvf config.tar config/
config/
config/vconsole.conf
config/20-nproc.conf
config/sepermit.conf
config/listen.conf
config/plymouthd.conf
config/namespace.conf
config/grub2.conf
config/dracut.conf
...
```

Le nom du programme (*tape archiver*) révèle son utilisation initiale&nbsp;:
`tar` a servi en premier lieu à la gestion d'archives sur bande magnétique.
Voici une petite explication de l'exemple que nous venons de voir&nbsp;:

* `tar` reçoit l'ordre de créer (*create*) une archive avec l'option `-c`.

* La commande nous dit ce qui se passe en coulisse avec l'option `-v`.

* Le premier argument (`config.tar`) sera interprété comme le nom de l'archive
  grâce à l'option `-f` (*file*).

Voici un exemple plus général pour vous familiariser avec la syntaxe de `tar`
pour la création d'une archive. Nous créons trois fichiers `fichier1`,
`fichier2` et `fichier3` et les rassemblons dans un fichier
`archive.tar`&nbsp;:

```
$ touch fichier{1,2,3}
$ ls fichier?
fichier1  fichier2  fichier3
$ tar -cvf archive.tar fichier1 fichier2 fichier3
fichier1
fichier2
fichier3
```

### Extraire les fichiers d'une archive

Avant de "déballer" notre archive, nous allons la déplacer dans un répertoire
nouvellement créé. En effet, étant donné que les fichiers d'origine sont
toujours en place, une extraction dans le même répertoire ne changerait rien et
écraserait tous les fichiers existants.

```
$ mkdir -v repertoire
mkdir: created directory ‘repertoire’
$ mv -v config.tar repertoire/
‘config.tar’ -> ‘repertoire/config.tar’
$ cd repertoire/
$ tar -xvf config.tar
config/
config/org.freedesktop.locale1.conf
config/limits.conf
config/tuned.conf
config/sysctl.conf
config/org.freedesktop.hostname1.conf
config/rsyslog.conf
config/dist.conf
config/journald.conf
config/asound.conf
...
```

Dans l'autre sens, c'est donc l'option `-x` (*extract*) qui procède au
dépaquetage de l'archive.

### Combiner l'archivage et la compression

Essayons de créer une archive compressée en reprenant l'exemple du début. Nous
pouvons très bien&nbsp;:

* créer l'archive avec `tar`&nbsp;;

* compresser l'archive avec `gzip` ou `bzip2`.

Voici ce que cela donnerait en pratique&nbsp;:

```
$ tar -cvf config.tar config/
$ gzip config.tar
$ ls -lh config.tar.gz
-rw-rw-r--. 1 microlinux microlinux 31K Jun  3 09:09 config.tar.gz
```

Et avec `bzip2`&nbsp;:

```
$ tar -cvf config.tar config/
$ bzip2 config.tar
$ ls -lh config.tar.bz2
-rw-rw-r--. 1 microlinux microlinux 26K Jun  3 09:10 config.tar.bz2
```

Cependant, il est possible de faire encore plus simple. `tar` comporte une
série d'options qui servent à créer une archive et la compresser à la volée.
Effacez les archives compressées de l'exercice précédent et gardez seulement le
répertoire `~/config` et son contenu. Maintenant, créez une archive compressée
avec les options suivantes&nbsp;:

```
$ tar -cvzf config.tar.gz config/
$ ls -lh config.tar.gz 
-rw-rw-r--. 1 microlinux microlinux 31K Jun  3 09:12 config.tar.gz
```

Si vous souhaitez créer une archive compressée avec `bzip2`, il suffira de
remplacer l'option `-z` par `-j`&nbsp;:

```
$ tar -cvjf config.tar.bz2 config/
$ ls -lh config.tar.bz2 
-rw-rw-r--. 1 microlinux microlinux 26K Jun  3 09:14 config.tar.bz2
```

Notez la différence de taille entre les deux archives :

```
$ ls -lh config.tar.*
-rw-rw-r--. 1 microlinux microlinux 26K Jun  3 09:14 config.tar.bz2
-rw-rw-r--. 1 microlinux microlinux 31K Jun  3 09:12 config.tar.gz
```

### Extraire une archive compressée

Inversement, l'extraction d'une archive compressée peut également s'effectuer
d'une traite&nbsp;:

* pour une archive `.tar.gz`&nbsp;:

```
$ tar -xvzf config.tar.gz
```

* et pour une archive `.tar.bz2`&nbsp;:

```
$ tar -xvjf config.tar.bz2
```

> Les options de `tar` sont souvent utilisées sans le tiret `-` initial. Vous
> pouvez donc écrire de la même façon `tar -xvjf` ou `tar xvjf`, cela n'a
> aucune incidence sur le fonctionnement de la commande.

---

*La rédaction de ces cours demande du temps et des quantités
significatives de café espresso. Vous appréciez cette formation&nbsp;? **Offrez
un café au formateur** en cliquant sur la tasse.*

<p align="center">
  <a href="https://www.paypal.com/donate?hosted_button_id=ZXL7HDFV78BFG"><img
  width="96" height="74" src="https://www.microlinux.fr/espresso.jpg"></a>
</p>

